Create table tbldischargedetails (discUserId TEXT, discWomanId TEXT, discWomanName TEXT, discDateTimeOfDischarge DATETIME, 
 discWomanInvestigations TEXT, discChildInvestigations TEXT, discChild2Investigations TEXT,discStatusOfWoman Integer, 
discReferred Integer, discReasonForDischarge TEXT, discDischargeDiagnosis TEXT, discPatientDisposition TEXT, 
discFindingsHistory TEXT, discActionTaken TEXT, discMotherCondition TEXT, discChildCondition TEXT, discChild2Condition TEXT , discAdvice TEXT, 
discComplicationObserved TEXT, discProcedures TEXT, discDateTimeOfNextFollowUp TEXT, discPlaceOfNextFollowUp TEXT, 
discAdmissionReasons TEXT,discNameOfHCProvider TEXT, discDesignationOfHCProvider TEXT, discComments TEXT, 
trans_Id TEXT, discDateOfInsertion DATETIME DEFAULT (datetime('now','localtime')), discDateModified DATETIME);

Create table tbllatentphase (latUserId TEXT, latWomanId TEXT, latWomanName TEXT, latDateOfEntry DATETIME, 
latPulse Integer, latBp TEXT, latContractions Integer,  latFhs Integer, latPv TEXT, latAdvice TEXT, trans_Id Integer, 
latDateOfInsertion DATETIME DEFAULT (datetime('now','localtime')));

Create table tblpostpartumcare (postUserId TEXT, postWomanId TEXT, postWomanName TEXT, 
postPresentingComplaints TEXT, postOtherPresentingComplaints TEXT, postExamination TEXT, postPallor TEXT, postPulse Integer, postBp TEXT, postBreastExamination TEXT, 
postInvolutionOfUterus TEXT, postLochia TEXT, postPerinealCare TEXT, postAdvice TEXT,  
trans_id Integer, postDateTimeOfEntry datetime, postDateOfInsertion DATETIME DEFAULT (datetime('now','localtime')));


