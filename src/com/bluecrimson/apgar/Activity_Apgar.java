package com.bluecrimson.apgar;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

import com.androidquery.AQuery;
import com.bc.partograph.common.AppContext;
import com.bc.partograph.common.KillAllActivitesAndGoToLogin;
import com.bc.partograph.common.Partograph_CommonClass;
import com.bc.partograph.common.Partograph_DB;
import com.bc.partograph.deliveryinfo.DeliveryInfo_Activity;
import com.bc.partograph.latentphase.LatentPhase_Activity;
import com.bc.partograph.login.LoginActivity;
import com.bc.partograph.referralinfo.ReferralInfo_Activity;
import com.bc.partograph.settings.About;
import com.bc.partograph.settings.Settings_parto;
import com.bc.partograph.sliding.GraphInformation;
import com.bc.partograph.sliding.SlidingActivity;
import com.bc.partograph.sliding.SlidingActivity.AsyncCallWS_Comments;
import com.bc.partograph.stagesoflabor.StageofLabor;
import com.bc.partograph.sync.SyncFunctions;
import com.bc.partograph.womenview.Activity_WomenView;
import com.bluecrimson.additionalDetails.Summary_Activity;
import com.bluecrimson.dischargedetails.DiscargeDetails_Activity;
import com.bluecrimson.dischargedetails.DiscgargePojo;
import com.bluecrimson.dischargedetails.DischargedWomanList_Activity;
import com.bluecrimson.partograph.R;
import com.bluecrimson.partograph.Women_Profile_Pojo;
import com.bluecrimson.postpartumcare.PostPartumCare_Activity;
import com.bluecrimson.usermanual.UseManual;
import com.bluecrimson.viewprofile.ViewProfile_Activity;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.text.Html;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

public class Activity_Apgar extends FragmentActivity implements OnClickListener {

	AQuery aq;

	Partograph_DB dbh;
	ActionBar actionBar;
	Map<String, String> insertApgar = new LinkedHashMap<String, String>();
	Thread myThread;
	String todaysDate, strcurrTime, strWomenid, strwuserId;
	public static final Map<String, String> propertynamesMap = new LinkedHashMap<String, String>();
	int no_of_children;
	int object_ID = 10;
	Map<String, String> insertApgarSecond = new LinkedHashMap<String, String>();
	int child_Id;
	public static int apgartabpos = 0;
	public static boolean isApgar;
	// 24JAn2016
	TableRow tr_totalfirst, tr_totalsecond;
	public static MenuItem menuItem;
	// updated by Arpitha on 10july2016
	ImageButton imginfo;

	public static int count = 1;

	// 03OCt2016 Arpitha
	String selecteddate;
	int year, mon, day;
	static String strlastentry;
	static String refdatetime = null;

	// 04Oct2016 Arpitha
	String regdate, apgardate, deldate;
	static final int TIME_DIALOG_ID = 999;
	String delTime;
	private int hour;
	private int minute;
	// 07Oct2016 Arpitha
	TextView txtdialogtitle;
	boolean isbaby1 = false;// 02Nov2016 Arpitha
	String apgardate2;// 02Nov2016 Arpitha

	Women_Profile_Pojo woman;// 03nov2016

	// String strapgardate;

	String strpostpartumdatetime;// 20Sep2017 Arpitha
	Date postpartumdatetime;// 20Sep2017 Arpitha
	CountDownTimer commentsTimer = null;
	public static final int START_AFTER_SECONDS = 10000;
	boolean inetOn = false;
	String serverResult;
	boolean isMsg;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_apgar);

		try {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());

			aq = new AQuery(this);

			apgartabpos = Partograph_CommonClass.curr_tabpos;
			isApgar = true;

			woman = (Women_Profile_Pojo) getIntent().getSerializableExtra("woman");

			// 05Nov2016 Arpitha
			Locale locale = null;

			if (AppContext.prefs.getBoolean("isEnglish", false)) {
				locale = new Locale("en");
				Locale.setDefault(locale);
			} else if (AppContext.prefs.getBoolean("isHindi", false)) {
				locale = new Locale("hi");
				Locale.setDefault(locale);
			}

			Configuration config = new Configuration();
			config.locale = locale;
			getResources().updateConfiguration(config, getResources().getDisplayMetrics());

			AppContext.setNamesAccordingToRes(getResources());// 05Nov2016
																// Arpitha
			dbh = Partograph_DB.getInstance(getApplicationContext());
			actionBar = getActionBar();
			// Enabling Back navigation on Action Bar icon
			actionBar.setDisplayHomeAsUpEnabled(true);
			actionBar.setHomeButtonEnabled(true);
			actionBar.setTitle(woman.getWomen_name());

			initializeScreen(aq.id(R.id.scrapgar).getView());

			// 23Aug2016 - bindu
			getwomanbasicdata();

			initialView();

			// 03Oct2016 Arpitha - datepicker
			aq.id(R.id.etapgardate).getEditText().setOnTouchListener(new View.OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if (MotionEvent.ACTION_UP == event.getAction()) {
						try {
							isbaby1 = true;// 02Nov2016 Arpitha
							caldatepicker();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					return true;
				}
			});

			// 03Oct2016 Arpitha - datepicker
			aq.id(R.id.etapgardatebaby2).getEditText().setOnTouchListener(new View.OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if (MotionEvent.ACTION_UP == event.getAction()) {
						try {
							isbaby1 = false;// 02Nov2016 Arpitha
							caldatepicker();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					return true;
				}
			});

			// 04Oct2016 Arpitha
			aq.id(R.id.etapgartime).getEditText().setOnTouchListener(new View.OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if (MotionEvent.ACTION_UP == event.getAction()) {
						// isDelTime1 = true;
						delTime = aq.id(R.id.etapgartime).getText().toString();

						if (delTime != null) {
							Calendar calendar = Calendar.getInstance();
							Date d = null;
							d = dbh.getTime(delTime);
							calendar.setTime(d);
							hour = calendar.get(Calendar.HOUR_OF_DAY);
							minute = calendar.get(Calendar.MINUTE);
						}
						isbaby1 = true;// 02Nov2016 Arpitha
						showDialog(TIME_DIALOG_ID);
					}
					return true;
				}
			});

			// 02Nov2016 Arpitha
			aq.id(R.id.etapgartimebaby2).getEditText().setOnTouchListener(new View.OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if (MotionEvent.ACTION_UP == event.getAction()) {
						// isDelTime1 = true;
						delTime = aq.id(R.id.etapgartime).getText().toString();

						if (delTime != null) {
							Calendar calendar = Calendar.getInstance();
							Date d = null;
							d = dbh.getTime(delTime);
							calendar.setTime(d);
							hour = calendar.get(Calendar.HOUR_OF_DAY);
							minute = calendar.get(Calendar.MINUTE);
						}
						isbaby1 = false;// 02Nov2016 Arpitha
						showDialog(TIME_DIALOG_ID);
					}
					return true;
				}
			});

			// 03Oct2016 Arpitha
			strlastentry = dbh.getlastentrytime(woman.getWomenId(), 3);
			// updated on 9july2016 by Arpitha
			imginfo = (ImageButton) findViewById(R.id.fhrinfo);

			imginfo.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					// TODO Auto-generated method stub
					try {
						if (MotionEvent.ACTION_UP == event.getAction()) {
							apgar_info();
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}
					return true;
				}
			});

			// 24jan2016
			tr_totalfirst = (TableRow) findViewById(R.id.tr_totalfirst);
			tr_totalsecond = (TableRow) findViewById(R.id.tr_totalsecond);
			tr_totalfirst.setVisibility(View.GONE);
			tr_totalsecond.setVisibility(View.GONE); // 24Jan2016

			myThread = null;

			Runnable myRunnableThread = new CountDownRunner();
			myThread = new Thread(myRunnableThread);
			myThread.start();

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
		KillAllActivitesAndGoToLogin.addToStack(this);
	}

	// Load the property names
	private void loadPropertynamesMap() throws Exception {
		// 24Sep2016 Arpitha
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		Cursor cursor = dbh.getPropertyMap(object_ID);

		if (cursor.getCount() > 0) {
			cursor.moveToFirst();
			do {
				propertynamesMap.put(cursor.getString(0), cursor.getString(1));
			} while (cursor.moveToNext());
		}
	}

	// updated on 23Aug2016 by bindu
	void getwomanbasicdata() throws Exception {

		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		if (woman != null) {

			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());

			String doa = "", toa = "", risk = "", dod = "", tod = "";
			if (woman != null) {
				aq.id(R.id.wname).text(woman.getWomen_name());
				aq.id(R.id.wage).text(woman.getAge() + getResources().getString(R.string.yrs));

				if (woman.getregtype() != 2) {
					doa = Partograph_CommonClass.getConvertedDateFormat(woman.getDate_of_admission(),
							Partograph_CommonClass.defdateformat);
					toa = Partograph_CommonClass.gettimein12hrformat(woman.getTime_of_admission());
					aq.id(R.id.wdoa).text(getResources().getString(R.string.reg) + ": " + doa + "/" + toa);
				} else {
					aq.id(R.id.wdoa).text(getResources().getString(R.string.reg) + ": " + woman.getDate_of_reg_entry());
				}
				aq.id(R.id.wgest)
						.text(getResources().getString(R.string.gest_age) + ":"
								+ (woman.getGestationage() == 0 ? getResources().getString(R.string.notknown)
										: woman.getGestationage() + getResources().getString(R.string.wks)));// 23Aug2016
				// -
				// bindu
				// -
				// gest
				// age
				// not
				// known
				// chk
				aq.id(R.id.wgravida)
						.text(getResources().getString(R.string.gravida_short_label) + ":" + woman.getGravida() + ", "
								+ getResources().getString(R.string.para_short_label) + ":" + woman.getPara());
				woman.getRisk_category();
				if (woman.getRisk_category() == 1) {
					risk = getResources().getString(R.string.high);// 07Oct2016
																	// Arpitha
				} else
					risk = getResources().getString(R.string.low);// 07Oct2016
																	// Arpitha
				aq.id(R.id.wtrisk).text(getResources().getString(R.string.risk_short_label) + ":" + risk);

				if (woman.getDel_type() > 0) {
					String[] deltype = getResources().getStringArray(R.array.del_type);
					aq.id(R.id.wdelstatus).text(
							getResources().getString(R.string.delstatus) + ":" + deltype[woman.getDel_type() - 1]);
					dod = Partograph_CommonClass.getConvertedDateFormat(woman.getDel_Date(),
							Partograph_CommonClass.defdateformat);
					tod = Partograph_CommonClass.gettimein12hrformat(woman.getDel_Time());
					aq.id(R.id.wdeldate).text(getResources().getString(R.string.del) + ": " + dod + "/" + tod);

				}
			}

		}
	}

	// Initial View
	private void initialView() throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		TableLayout tblbaby1_details = (TableLayout) findViewById(R.id.tblapgar);// 10Nov2016
																					// Arpitha
		TableLayout tblbaby2_details = (TableLayout) findViewById(R.id.tblapgar2);
		// 10Nov2016 Arpitha
		if (woman.getDel_result1() == 2) {
			tblbaby1_details.setVisibility(View.GONE);
			aq.id(R.id.txt1).visible();
		} else {
			tblbaby1_details.setVisibility(View.VISIBLE);
			aq.id(R.id.txt1).gone();
		}
		if (woman.getDel_result2() == 2) {
			tblbaby2_details.setVisibility(View.GONE);
			aq.id(R.id.txt2).visible();
		} else {
			tblbaby2_details.setVisibility(View.VISIBLE);
			aq.id(R.id.txt2).gone();
		}

		if (woman.getDel_result1() == 2 && woman.getDel_result2() == 2) {
			aq.id(R.id.btnsaveapgar).enabled(false);// 10Nov2016 Arpitha
			aq.id(R.id.btnsaveapgar).backgroundColor(getResources().getColor(R.color.ashgray));
			aq.id(R.id.txt1).text(getResources().getString(R.string.baby_is_dead));
			aq.id(R.id.txt2).gone();
		}

		// 10Nov2016 Arpitha

		todaysDate = Partograph_CommonClass.getTodaysDate();
		strcurrTime = Partograph_CommonClass.getCurrentTime();
		strWomenid = woman.getWomenId();
		strwuserId = woman.getUserId();

		aq.id(R.id.etapgartime).text(strcurrTime);
		aq.id(R.id.etapgardate)// changed on 03Oct201
				.text(Partograph_CommonClass.getConvertedDateFormat(todaysDate, Partograph_CommonClass.defdateformat));

		aq.id(R.id.etapgartimebaby2).text(strcurrTime);
		aq.id(R.id.etapgardatebaby2)
				.text(Partograph_CommonClass.getConvertedDateFormat(todaysDate, Partograph_CommonClass.defdateformat));

		loadPropertynamesMap();

		aq.id(R.id.btnsaveapgar1).gone();

		no_of_children = woman.getNo_of_child();

		if (no_of_children == 2 && woman.getDel_result2() != 2) {
			tblbaby2_details.setVisibility(View.VISIBLE);
		} else {
			tblbaby2_details.setVisibility(View.GONE);

		}

		child_Id = 1;
		Cursor cursor = dbh.GetApgarScoreDetails(strWomenid, strwuserId, object_ID, child_Id);
		if (cursor != null && cursor.getCount() > 0) {
			aq.id(R.id.btnviewapgar).enabled(true);
			aq.id(R.id.btnviewapgar).backgroundColor(getResources().getColor(R.color.brown));
			// Retrieve comments from tbl_comment from server
			calRetrieveComments();
			getCommentCountNotRead();
		} else {
			aq.id(R.id.btnviewapgar).enabled(false);
			aq.id(R.id.btnviewapgar).backgroundColor(getResources().getColor(R.color.ashgray));
		}

		aq.id(R.id.listCommentschild1).gone();
		aq.id(R.id.txtCommentsapgarchild1).gone();
		aq.id(R.id.listCommentschild2).gone();
		aq.id(R.id.txtCommentsapgarchild2).gone();

		updatetblComments(strwuserId, strWomenid, 10);

		// 03Oct2016 ARpitha
		refdatetime = dbh.getrefdate(woman.getWomenId(), woman.getUserId());

		// 04Oct2016 Arpitha
		regdate = woman.getDate_of_admission() + " " + woman.getTime_of_admission();
		apgardate = todaysDate;
		deldate = woman.getDel_Date() + " " + woman.getDel_Time();

		apgardate2 = todaysDate;// 02Nov2016 Arpitha

		// strapgardate = todaysDate;// 16Nov2016 Arpitha

		// 10Aug2017 Atpitha
		// if (cursor != null && cursor.getCount() <=0) {
		ArrayList<DiscgargePojo> arrVal = new ArrayList<DiscgargePojo>();
		arrVal = dbh.getDischargeData(woman.getUserId(), woman.getWomenId());
		if (arrVal.size() > 0) {
			aq.id(R.id.txtdisable).visible();
			// txtdisabled.setVisibility(View.VISIBLE);
			// aq.id(R.id.txtdisable).gone();
			// aq.id(R.id.tbldetails).gone();
			aq.id(R.id.scrapgarscore).gone();
			aq.id(R.id.btnsaveapgar).enabled(false);
			aq.id(R.id.btnsaveapgar).backgroundColor(getResources().getColor(R.color.ashgray));
			// aq.id(R.id.llparentapgar).gone();
			// }

		} // 10Aug2017 Atpitha

		strpostpartumdatetime = dbh.getPostPartumDateTime(woman.getWomenId(), woman.getUserId());// 20Sep2017
		// Arpitha

	}

	// Update tblcomments set isviewed=true
	private void updatetblComments(String userid, String womenid, int objid) throws Exception {
		// 24Sep2016 Arpitha
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		dbh.updatetblComments(userid, womenid, objid);
		Partograph_CommonClass.apgar_cnt = 0;
	}

	/**
	 * This is a Recursive method that traverse through the group and subgroup
	 * of view Sets text, assigns clickListners
	 * 
	 * @throws Exception
	 */
	private ArrayList<View> initializeScreen(View v) throws Exception {
		if (!(v instanceof ViewGroup)) {
			ArrayList<View> viewArrayList = new ArrayList<View>();
			viewArrayList.add(v);

			// Edittext
			if (v.getClass() == EditText.class) {
				aq.id(v.getId()).getEditText().setOnClickListener(this);
			}

			// Textview
			if (v.getClass() == TextView.class) {
				aq.id(v.getId()).getTextView().setOnClickListener(this);
			}
			// Button
			if (v.getClass() == Button.class) {
				aq.id(v.getId()).getButton().setOnClickListener(this);
			}

			return viewArrayList;
		}
		if (v instanceof Spinner) {
			aq.id(v.getId()).itemSelected(this, "onSpinnerClicked");
		}

		ArrayList<View> result = new ArrayList<View>();

		ViewGroup viewGroup = (ViewGroup) v;
		for (int i = 0; i < viewGroup.getChildCount(); i++) {

			View child = viewGroup.getChildAt(i);

			ArrayList<View> viewArrayList = new ArrayList<View>();
			viewArrayList.add(v);
			viewArrayList.addAll(initializeScreen(child));

			result.addAll(viewArrayList);
		}
		return result;
	}

	@Override
	public void onClick(View v) {
		try {
			// 24Sep2016 Arpitha - addToTrace
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());

			switch (v.getId()) {

			// Save apgar
			case R.id.btnsaveapgar:
				try {
					boolean isApgarAdded = false;
					int transId = 0;
					if (validateApgarFields()) {
						prepareInsertApgarMap();
						if (!insertApgar.isEmpty()) {

							String strTime = aq.id(R.id.etapgartime).getText().toString();
							// String date =
							// aq.id(R.id.etapgardate).getText().toString();

							// String strDate = strapgardate;// 16Nov2016
							// cmmented on 03Jan2017 Arpitha
							// Arpitha;//16Nov2016
							// Arpitha

							String strDate = apgardate;// 03Jan2017
							dbh.db.beginTransaction();
							transId = dbh.iCreateNewTrans(strwuserId);
							child_Id = 1;
							isApgarAdded = dbh.insertApgarDetails(strWomenid, strwuserId, insertApgar, strTime, strDate,
									transId, object_ID, child_Id);
						}

						if (no_of_children == 2 && isApgarAdded) {
							if (!insertApgarSecond.isEmpty()) {
								child_Id = 2;
								String strTime = aq.id(R.id.etapgartimebaby2).getText().toString();
								// String strDate =
								// aq.id(R.id.etapgardatebaby2).getText().toString();
								String strDate = apgardate2;// 03Jan2017 Arpitha
								isApgarAdded = dbh.insertApgarDetails(strWomenid, strwuserId, insertApgarSecond,
										strTime, strDate, transId, object_ID, child_Id);
							}
						}

						if (isApgarAdded) {
							Toast.makeText(getApplicationContext(),
									getResources().getString(R.string.apgarscoresuccess), Toast.LENGTH_LONG).show();
							dbh.iNewRecordTrans(strwuserId, transId, Partograph_DB.TBL_PROPERTYVALUES);
							commitTrans();
						} else {
							rollbackTrans();
						}

						clearDetails();
						aq.id(R.id.etappearance).getEditText().requestFocus();

					}
				} catch (Exception e) {
					e.printStackTrace();
				}

				break;

			case R.id.btnviewapgar:
				try {
					Intent viewApgaract = new Intent(this, Activity_View_Apgar.class);
					viewApgaract.putExtra("woman", woman);
					startActivity(viewApgaract);
				} catch (Exception e) {
					e.printStackTrace();
				}
				break;

			}
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);// 21Sep2016 Arpitha - add to log
			e.printStackTrace();
		}
	}

	// Validate mandatory fields
	private boolean validateApgarFields() throws Exception {
		// 24Sep2016 Arpitha - addToTrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		if (woman.getDel_result1() != 2)// 10Nov2016 Arpitha
		{
			if (aq.id(R.id.etappearance).getText().length() <= 0) {
				Toast.makeText(getApplicationContext(), getResources().getString(R.string.enter_appearance),
						Toast.LENGTH_SHORT).show();
				aq.id(R.id.etappearance).getEditText().requestFocus();
				return false;
			}

			if (aq.id(R.id.etpulse).getText().length() <= 0) {
				Toast.makeText(getApplicationContext(), getResources().getString(R.string.enter_pulse),
						Toast.LENGTH_SHORT).show();
				aq.id(R.id.etpulse).getEditText().requestFocus();
				return false;
			}

			if (aq.id(R.id.etgrimace).getText().length() <= 0) {
				Toast.makeText(getApplicationContext(), getResources().getString(R.string.enter_grimace),
						Toast.LENGTH_SHORT).show();
				aq.id(R.id.etgrimace).getEditText().requestFocus();
				return false;
			}

			if (aq.id(R.id.etactivity).getText().length() <= 0) {
				Toast.makeText(getApplicationContext(), getResources().getString(R.string.enter_activity),
						Toast.LENGTH_SHORT).show();
				aq.id(R.id.etactivity).getEditText().requestFocus();
				return false;
			}

			if (aq.id(R.id.etrespiration).getText().length() <= 0) {
				Toast.makeText(getApplicationContext(), getResources().getString(R.string.enter_respiration),
						Toast.LENGTH_SHORT).show();
				aq.id(R.id.etrespiration).getEditText().requestFocus();
				return false;
			}
		}

		if (no_of_children == 2 && woman.getDel_result2() != 2) {
			if (aq.id(R.id.etappearancebaby2).getText().length() <= 0) {
				Toast.makeText(getApplicationContext(), getResources().getString(R.string.enter_appearance),
						Toast.LENGTH_SHORT).show();
				aq.id(R.id.etappearancebaby2).getEditText().requestFocus();
				return false;
			}

			if (aq.id(R.id.etpulsebaby2).getText().length() <= 0) {
				Toast.makeText(getApplicationContext(), getResources().getString(R.string.enter_pulse),
						Toast.LENGTH_SHORT).show();
				aq.id(R.id.etpulsebaby2).getEditText().requestFocus();
				return false;
			}

			if (aq.id(R.id.etgrimacebaby2).getText().length() <= 0) {
				Toast.makeText(getApplicationContext(), getResources().getString(R.string.enter_grimace),
						Toast.LENGTH_SHORT).show();
				aq.id(R.id.etgrimacebaby2).getEditText().requestFocus();
				return false;
			}

			if (aq.id(R.id.etactivitybaby2).getText().length() <= 0) {
				Toast.makeText(getApplicationContext(), getResources().getString(R.string.enter_activity),
						Toast.LENGTH_SHORT).show();
				aq.id(R.id.etactivitybaby2).getEditText().requestFocus();
				return false;
			}

			if (aq.id(R.id.etrespirationbaby2).getText().length() <= 0) {
				Toast.makeText(getApplicationContext(), getResources().getString(R.string.enter_respiration),
						Toast.LENGTH_SHORT).show();
				aq.id(R.id.etrespirationbaby2).getEditText().requestFocus();
				return false;
			}

			// 02Nov2016 Arpitha
			String strApgarDate2 = apgardate2 + " " + aq.id(R.id.etapgartimebaby2).getText().toString();

			if ((!isApgarTimevalid(regdate, strApgarDate2, 6)) && woman.getregtype() != 2)
				return false;
			if (!isApgarTimevalid(deldate, strApgarDate2, 7))
				return false;
			if (strlastentry != null) {
				if (!isApgarTimevalid(strlastentry, strApgarDate2, 8))
					return false;
			}
			if (refdatetime != null) {
				if (!isApgarTimevalid(refdatetime, strApgarDate2, 9))
					return false;
			}
			String currenttime = Partograph_CommonClass.getTodaysDate() + " " + Partograph_CommonClass.getCurrentTime();
			if (!isApgarTimevalid(strApgarDate2, currenttime, 10))
				return false;
		}

		// 04Oct2016 Arpitha- 03Jan2017 Arpitha - changed variable strApgarDate
		String strApgarDate = apgardate + " " + aq.id(R.id.etapgartime).getText().toString();

		if ((!isApgarTimevalid(regdate, strApgarDate, 1)) && woman.getregtype() != 2)
			return false;
		if (!isApgarTimevalid(deldate, strApgarDate, 2))
			return false;
		if (strlastentry != null) {
			if (!isApgarTimevalid(strlastentry, strApgarDate, 3))
				return false;
		}
		if (refdatetime != null) {
			if (!isApgarTimevalid(refdatetime, strApgarDate, 4))
				return false;
		}
		String currenttime = Partograph_CommonClass.getTodaysDate() + " " + Partograph_CommonClass.getCurrentTime();
		if (!isApgarTimevalid(strApgarDate, currenttime, 5))
			return false;

		return true;
	}

	// Clear Apgar Score Values
	private void clearDetails() throws Exception {
		// 24Sep2016 Arpitha - addtotrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		aq.id(R.id.etappearance).text("");
		aq.id(R.id.etpulse).text("");
		aq.id(R.id.etgrimace).text("");
		aq.id(R.id.etactivity).text("");
		aq.id(R.id.etrespiration).text("");
		// updated on 28feb16 by Arpitha
		aq.id(R.id.etcomments).text("");

		aq.id(R.id.etappearancebaby2).text("");
		aq.id(R.id.etpulsebaby2).text("");
		aq.id(R.id.etgrimacebaby2).text("");
		aq.id(R.id.etactivitybaby2).text("");
		aq.id(R.id.etrespirationbaby2).text("");

		// updated on 28feb16 by Arpitha
		aq.id(R.id.etcomments2).text("");

	}

	/** Method to rollback the trnsaction */
	private void rollbackTrans() throws Exception {
		dbh.db.endTransaction();
		// dbh.db.close();
		Toast.makeText(getApplicationContext(), getResources().getString(R.string.failed), Toast.LENGTH_SHORT).show();
	}

	/** Method to commit the trnsaction */
	private void commitTrans() throws Exception {
		// 24Sep2016 Arpitha - addtotrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		dbh.db.setTransactionSuccessful();
		dbh.db.endTransaction();
		// dbh.db.close();

		calSyncMtd();
		Toast.makeText(getApplicationContext(), getResources().getString(R.string.success), Toast.LENGTH_SHORT).show();

		Intent viewApgaract = new Intent(this, Activity_View_Apgar.class);
		viewApgaract.putExtra("woman", woman);
		startActivity(viewApgaract);
		count++;

		aq.id(R.id.btnviewapgar).enabled(true);
		aq.id(R.id.btnviewapgar).backgroundColor(getResources().getColor(R.color.brown));
	}

	// Sync
	private void calSyncMtd() {
		SyncFunctions.syncUptoDate = false;
		Partograph_CommonClass.responseAckCount = 0;
		Partograph_CommonClass.AsyncCallWS task = new Partograph_CommonClass.AsyncCallWS();
		task.execute();
	}

	// Create map which contains the value for different property ids
	private void prepareInsertApgarMap() {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());// 21Sep2016
																														// Arpitha
																														// -
																														// add
																														// to
																														// trace
		try {

			// 27Sep2016 Arpitha
			if (Partograph_CommonClass.autodatetime(Activity_Apgar.this)) {
				Date lastregdate = null;

				String strlastinserteddate = dbh.getlastmaxdate(strwuserId,
						getResources().getString(R.string.partograph));
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
				if (strlastinserteddate != null) {
					lastregdate = format.parse(strlastinserteddate);
				}
				Date currentdatetime = new Date();
				if (lastregdate != null && currentdatetime.before(lastregdate)) {
					Partograph_CommonClass.showSettingsAlert(Activity_Apgar.this,
							getResources().getString(R.string.plz_enable_automatic_date_and_set_current_date));
				} else {// 27Sep2016 Arpitha

					insertApgar.put("10.1", aq.id(R.id.etappearance).getText().toString());
					insertApgar.put("10.2", aq.id(R.id.etpulse).getText().toString());
					insertApgar.put("10.3", aq.id(R.id.etgrimace).getText().toString());
					insertApgar.put("10.4", aq.id(R.id.etactivity).getText().toString());
					insertApgar.put("10.5", aq.id(R.id.etrespiration).getText().toString());
					// updated by Arpitha
					insertApgar.put("10.6", aq.id(R.id.etcomments).getText().toString());

					if (no_of_children == 2) {
						insertApgarSecond.put("10.1", aq.id(R.id.etappearancebaby2).getText().toString());
						insertApgarSecond.put("10.2", aq.id(R.id.etpulsebaby2).getText().toString());
						insertApgarSecond.put("10.3", aq.id(R.id.etgrimacebaby2).getText().toString());
						insertApgarSecond.put("10.4", aq.id(R.id.etactivitybaby2).getText().toString());
						insertApgarSecond.put("10.5", aq.id(R.id.etrespirationbaby2).getText().toString());
						// updated by Arpitha
						insertApgarSecond.put("10.6", aq.id(R.id.etcomments2).getText().toString());

					}
				}
			}
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);// 21Sep2016 Arpitha - add to log
			e.printStackTrace();
		}
	}

	class CountDownRunner implements Runnable {
		// @Override
		public void run() {
			while (!Thread.currentThread().isInterrupted()) {
				try {

					doWork();
					Thread.sleep(1000); // Pause of 1 Second

				} catch (InterruptedException e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					e.printStackTrace();
					Thread.currentThread().interrupt();
					myThread.stop();
				} catch (Exception e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					e.printStackTrace();
					myThread.stop();
				}
			}
		}
	}

	public void doWork() throws Exception {

		if (getApplicationContext() != null) {
			runOnUiThread(new Runnable() {
				public void run() {
					try {

					} catch (Exception e) {
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}
				}
			});
		}
	}

	/** This method Calls the idle timeout */
	@Override
	public void onUserInteraction() {
		super.onUserInteraction();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_womenlist, menu);
		menu.findItem(R.id.delete).setVisible(false);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		try {
			switch (item.getItemId()) {
			// Respond to the action bar's Up/Home button
			case android.R.id.home:
				NavUtils.navigateUpFromSameTask(this);
				return true;

			case R.id.logout:
				Intent login = new Intent(this, LoginActivity.class);
				startActivity(login);
				return true;

			case R.id.home:
				Intent home = new Intent(this, Activity_WomenView.class);
				startActivity(home);
				return true;

			case R.id.about:

				Intent about = new Intent(this, About.class);
				startActivity(about);
				return true;

			case R.id.sync:
				menuItem = item;
				menuItem.setActionView(R.layout.progressbar);
				menuItem.expandActionView();
				calSyncMtd();
				return true;

			case R.id.disch:
				Intent disch = new Intent(Activity_Apgar.this, DischargedWomanList_Activity.class);
				startActivity(disch);
				return true;

			case R.id.info:

				Intent info = new Intent(Activity_Apgar.this, GraphInformation.class);
				startActivity(info);
				return true;

			case R.id.usermanual:

				Intent manual = new Intent(Activity_Apgar.this, UseManual.class);
				startActivity(manual);
				return true;

			case R.id.settings:

				Intent sett = new Intent(Activity_Apgar.this, Settings_parto.class);
				startActivity(sett);
				return true;

			case R.id.summary:

				Intent sum = new Intent(Activity_Apgar.this, Summary_Activity.class);
				startActivity(sum);
				return true;

			// 03Oct2017 Arpitha
			case R.id.del:
				// Intent disc = new Intent(Activity_Apgar.this,
				// DeliveryInfo_Activity.class);
				// disc.putExtra("woman", woman);
				// startActivity(disc);

				if (woman.getDel_type() != 0)// 21Oct2016
												// Arpitha
				{// 21Oct2016 Arpitha
					// 05Jan2016
					// / showDialogToUpdateStatus();
					Partograph_CommonClass.showDialogToUpdateStatus(Activity_Apgar.this, woman);
					// loadWomendata(displayListCountddel);
					// if(adapter!=null)
					// adapter.notifyDataSetChanged();
					// loadWomendata(displayListCountddel);
				} else// 21Oct2016 Arpitha
				{

					Intent graph = new Intent(Activity_Apgar.this, DeliveryInfo_Activity.class);
					graph.putExtra("woman", woman);
					startActivity(graph);
				} // 21Oct2016 Arpitha
				return true;
			case R.id.viewprofile:
				Intent view = new Intent(Activity_Apgar.this, ViewProfile_Activity.class);
				view.putExtra("woman", woman);
				startActivity(view);
				return true;

			case R.id.ref:
				// Intent ref = new Intent(Activity_Apgar.this,
				// ReferralInfo_Activity.class);
				// ref.putExtra("woman", woman);
				// startActivity(ref);
				Cursor cur = dbh.getReferralDetails(woman.getUserId(), woman.getWomenId());
				if (cur != null && cur.getCount() > 0) {
					Partograph_CommonClass.showDialogToUpdateReferral(Activity_Apgar.this, woman);
				} else {
					Intent ref = new Intent(Activity_Apgar.this, ReferralInfo_Activity.class);
					ref.putExtra("woman", woman);
					startActivity(ref);
				}
				return true;

			case R.id.pdf:
				Partograph_CommonClass.exportPDFs(Activity_Apgar.this, woman);
				return true;

			case R.id.disc:
				Intent discharge = new Intent(Activity_Apgar.this, DiscargeDetails_Activity.class);
				discharge.putExtra("woman", woman);
				startActivity(discharge);
				return true;

			case R.id.apgar:
				if (woman.getDel_type() != 0) {
					Intent apgar = new Intent(Activity_Apgar.this, Activity_Apgar.class);
					apgar.putExtra("woman", woman);
					startActivity(apgar);
				} else
					Toast.makeText(getApplicationContext(),
							getResources().getString(R.string.msg_applicableafterdelivery), Toast.LENGTH_LONG).show();
				return true;

			case R.id.stages:
				if (woman.getDel_type() != 0) {
					Intent stages = new Intent(Activity_Apgar.this, StageofLabor.class);
					stages.putExtra("woman", woman);
					startActivity(stages);
				} else
					Toast.makeText(getApplicationContext(),
							getResources().getString(R.string.msg_applicableafterdelivery), Toast.LENGTH_LONG).show();
				return true;

			case R.id.post:
				if (woman.getDel_type() != 0) {
					Intent post = new Intent(Activity_Apgar.this, PostPartumCare_Activity.class);
					post.putExtra("woman", woman);
					startActivity(post);
				} else
					Toast.makeText(getApplicationContext(),
							getResources().getString(R.string.msg_applicableafterdelivery), Toast.LENGTH_LONG).show();
				return true;
				
			case R.id.lat:
				Intent lat = new Intent(Activity_Apgar.this, LatentPhase_Activity.class);
				lat.putExtra("woman", woman);
				startActivity(lat);
				return true;

			case R.id.parto:
				Intent parto = new Intent(Activity_Apgar.this, SlidingActivity.class);
				parto.putExtra("woman", woman);
				startActivity(parto);
				return true;// 03Oct2017 Arpitha

			}
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// menu.findItem(R.id.help).setVisible(false);
		if (Partograph_CommonClass.isRecordExistsForSync)
			menu.findItem(R.id.sync).setIcon(getResources().getDrawable(R.drawable.ic_sync_red));
		else
			menu.findItem(R.id.sync).setIcon(getResources().getDrawable(R.drawable.ic_sync_green));

		menu.findItem(R.id.delete).setVisible(false);
		menu.findItem(R.id.expertcomments).setVisible(false);
		menu.findItem(R.id.viewpartograph).setVisible(false);

		menu.findItem(R.id.addcomments).setVisible(false);
		menu.findItem(R.id.viewprofile).setVisible(false);
		// menu.findItem(R.id.sync).setVisible(false);
		menu.findItem(R.id.registration).setVisible(false);

		menu.findItem(R.id.search).setVisible(false);// 14Jun2017 Arpitha

		menu.findItem(R.id.save).setVisible(false);// 08Aug2017 Arpitha

		// 03Oct2017 Arpitha
		menu.findItem(R.id.del).setVisible(true);
		menu.findItem(R.id.ref).setVisible(true);
		menu.findItem(R.id.pdf).setVisible(true);
		menu.findItem(R.id.stages).setVisible(true);
		menu.findItem(R.id.disc).setVisible(true);
		menu.findItem(R.id.viewprofile).setVisible(true);
		menu.findItem(R.id.lat).setVisible(true);
		menu.findItem(R.id.post).setVisible(true);
		menu.findItem(R.id.parto).setVisible(true);// 03Oct2017 Arpitha
		if (isMsg)
			menu.findItem(R.id.ic_msg).setVisible(true);
		else
			menu.findItem(R.id.ic_msg).setVisible(false);

		return super.onPrepareOptionsMenu(menu);
	}

	void apgar_info() throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());// 21Sep2016
																														// Arpitha
																														// -
																														// add
																														// to
																														// trace
		final Dialog dialog = new Dialog(this);
		dialog.requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		dialog.setContentView(R.layout.apgar_info);
		dialog.getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.dialog_title);
		txtdialogtitle = (TextView) dialog.findViewById(R.id.graph_title);
		txtdialogtitle.setText(Html.fromHtml("<font color='" + getResources().getColor(R.color.appcolor) + "'>"
				+ getResources().getString(R.string.apgar_score) + "</font>"));
		ImageButton imgbtncanceldialog = (ImageButton) dialog.findViewById(R.id.imgbtncanceldialog);
		imgbtncanceldialog.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				try {
					dialog.cancel();
				} catch (Exception e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					e.printStackTrace();
				}
			}
		});

		int dividerId = dialog.getContext().getResources().getIdentifier("android:id/titleDivider", null, null);
		View divider = dialog.findViewById(dividerId);
		divider.setBackgroundColor(getResources().getColor(R.color.appcolor));// 08Feb2017
																				// Arpitha

		dialog.show();
		dialog.setCancelable(false);// 31oct2016 Arpitha
	}

	// 03OCt2016 Arpitha - caldatepicker
	private void caldatepicker() throws Exception {
		DialogFragment newFragment = new SelectDateFragment();
		newFragment.show(this.getSupportFragmentManager(), "DatePicker");
	}

	@SuppressLint("ValidFragment")
	public class SelectDateFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
		@TargetApi(Build.VERSION_CODES.HONEYCOMB)
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			Calendar calendar = Calendar.getInstance();

			Date d = null;
			d = dbh.getDateS(selecteddate == null ? todaysDate : selecteddate);
			calendar.setTime(d);
			year = calendar.get(Calendar.YEAR);
			mon = calendar.get(Calendar.MONTH);
			day = calendar.get(Calendar.DAY_OF_MONTH);

			DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, year, mon, day);

			/* remove calendar view */
			dialog.getDatePicker().setCalendarViewShown(false);

			/* Spinner View */
			dialog.getDatePicker().setSpinnersShown(true);

			dialog.setTitle(getResources().getString(R.string.pickadate));
			return dialog;
		}

		// 03Oct2016 Arpitha
		public void onDateSet(DatePicker view, int yy, int mm, int dd) {
			if (view.isShown()) {
				try {

					populateSetDate(yy, mm + 1, dd);
				} catch (Exception e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					e.printStackTrace();
				}
			}
		}
	}

	// 03Oct2016 Arpitha
	public void populateSetDate(int year, int month, int day) throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		// changed on 09 mar 2015
		selecteddate = String.valueOf(year) + "-" + String.format("%02d", month) + "-" + String.format("%02d", day);

		try {
			Date regdate = null;// 02Mar2017
			SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
			Date taken = date.parse(selecteddate);
			// Date regdate = date.parse(woman.getDate_of_admission());
			if (woman.getregtype() != 2)// 28Feb2017 Arpitha
				regdate = date.parse(woman.getDate_of_admission());
			Date deldate = date.parse(woman.getDel_Date());
			Date lastentry = null;
			Date refdate = null;

			if (strlastentry != null) {
				lastentry = date.parse(strlastentry);
			}
			if (refdatetime != null) {
				refdate = new SimpleDateFormat("yyyy-MM-dd").parse(refdatetime);
			}

			// 20Sep2017 Arpitha
			if (strpostpartumdatetime != null && strpostpartumdatetime.trim().length() > 0) {
				postpartumdatetime = new SimpleDateFormat("yyyy-MM-dd").parse(strpostpartumdatetime);
			} // 20Sep2017 Arpitha

			if (isbaby1) {
				// 20oct2016 Arpitha
				if (taken.after(new Date())) {
					Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.invalid_date),
							Activity_Apgar.this);
					aq.id(R.id.etapgardate).text(Partograph_CommonClass.getConvertedDateFormat(todaysDate,
							Partograph_CommonClass.defdateformat));
				} else if (regdate != null && taken.before(regdate) && woman.getregtype() != 2) {
					String regDate = Partograph_CommonClass.getConvertedDateFormat(woman.getDate_of_admission(),
							Partograph_CommonClass.defdateformat);// 17May2017
																	// Arpitha
					Partograph_CommonClass.displayAlertDialog(
							getResources().getString(R.string.apgar_date_cannot_before_regdate) + " " + regDate,
							Activity_Apgar.this);// 02Nov
					// -
					// include
					// regdate
					aq.id(R.id.etapgardate).text(Partograph_CommonClass.getConvertedDateFormat(todaysDate,
							Partograph_CommonClass.defdateformat));
				}

				else if (deldate != null && taken.before(deldate)) {

					String delDate = Partograph_CommonClass.getConvertedDateFormat(woman.getDel_Date(),
							Partograph_CommonClass.defdateformat);// 17May2017
																	// Arpitha
					Partograph_CommonClass.displayAlertDialog(
							getResources().getString(R.string.apgar_date_cannot_before_deldate) + " " + delDate,
							Activity_Apgar.this);// 02Nov
					// -
					// include
					// deldate
					aq.id(R.id.etapgardate).text(Partograph_CommonClass.getConvertedDateFormat(todaysDate,
							Partograph_CommonClass.defdateformat));
				} else if (lastentry != null && taken.before(lastentry)) {

					String lastpartoentry = Partograph_CommonClass.getConvertedDateFormat(strlastentry,
							Partograph_CommonClass.defdateformat);// 17May2017
																	// Arpitha
					Partograph_CommonClass.displayAlertDialog(
							getResources().getString(R.string.apgar_date_cannot_before_lastparto_entrydate) + " "
									+ lastpartoentry,
							Activity_Apgar.this);// 02Nov2016
					aq.id(R.id.etapgardate).text(Partograph_CommonClass.getConvertedDateFormat(todaysDate,
							Partograph_CommonClass.defdateformat));
				}

				// 20Sep2017 Arpitha
				else if (postpartumdatetime != null && taken.before(postpartumdatetime)) {

					String postpartum = Partograph_CommonClass.getConvertedDateFormat(
							strpostpartumdatetime.split("/")[0], Partograph_CommonClass.defdateformat);
					Partograph_CommonClass.displayAlertDialog(
							getResources().getString(R.string.apgar_datetime_cannot_be_before_postpartum_datetime,
									Activity_Apgar.this) + " " + postpartum,
							Activity_Apgar.this);
					aq.id(R.id.etapgardate).text(Partograph_CommonClass.getConvertedDateFormat(todaysDate,
							Partograph_CommonClass.defdateformat));

				} // 20Sep2017 Arpitha

				else if (refdate != null && taken.before(refdate)) {
					String refDate = Partograph_CommonClass.getConvertedDateFormat(refdatetime,
							Partograph_CommonClass.defdateformat);// 17May2017
																	// Arpitha
					Partograph_CommonClass.displayAlertDialog(
							getResources().getString(R.string.apgar_date_cannot_before_referral_date) + " " + refDate,
							Activity_Apgar.this);// 02Nov2016
					// Arpitha
					aq.id(R.id.etapgardate).text(Partograph_CommonClass.getConvertedDateFormat(todaysDate,
							Partograph_CommonClass.defdateformat));

				}

				else {
					aq.id(R.id.etapgardate).text(Partograph_CommonClass.getConvertedDateFormat(selecteddate,
							Partograph_CommonClass.defdateformat));
					apgardate = selecteddate;
				}
				// } // 20oct2016 Arpitha11Nov2016 Arpitha
			} else {

				if (taken.after(new Date())) {
					Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.second_baby) + ":"
							+ getResources().getString(R.string.invalid_date), getApplicationContext());
					aq.id(R.id.etapgardatebaby2).text(Partograph_CommonClass.getConvertedDateFormat(todaysDate,
							Partograph_CommonClass.defdateformat));
					// strapgardate = todaysDate;// 16Nov2016 Arpitha
					apgardate2 = todaysDate; // 03Jan2017 Arpitha
				} else if (regdate != null && taken.before(regdate) && woman.getregtype() != 2) {
					String regDate = Partograph_CommonClass.getConvertedDateFormat(woman.getDate_of_admission(),
							Partograph_CommonClass.defdateformat);// 17May2017
																	// Arpitha
					Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.second_baby) + ":"
							+ getResources().getString(R.string.apgar_date_cannot_before_regdate) + " " + regDate,
							Activity_Apgar.this);// 02Nov
					// -
					// include
					// regdate
					aq.id(R.id.etapgardatebaby2).text(Partograph_CommonClass.getConvertedDateFormat(todaysDate,
							Partograph_CommonClass.defdateformat));
					// strapgardate = todaysDate;// 16Nov2016 Arpitha
					apgardate2 = todaysDate; // 03Jan2017 Arpitha
				}

				else if (deldate != null && taken.before(deldate)) {
					String delDate = Partograph_CommonClass.getConvertedDateFormat(woman.getDel_Date(),
							Partograph_CommonClass.defdateformat);// 17May2017
																	// Arpitha
					Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.second_baby) + ":"
							+ getResources().getString(R.string.apgar_date_cannot_before_deldate) + " " + delDate,
							Activity_Apgar.this);// 02Nov
					// -
					// include
					// deldate
					aq.id(R.id.etapgardatebaby2).text(Partograph_CommonClass.getConvertedDateFormat(todaysDate,
							Partograph_CommonClass.defdateformat));
					// strapgardate = todaysDate;// 16Nov2016 Arpitha
					apgardate2 = todaysDate; // 03Jan2017 Arpitha
				} else if (lastentry != null && taken.before(lastentry)) {
					String strlatentry = Partograph_CommonClass.getConvertedDateFormat(strlastentry,
							Partograph_CommonClass.defdateformat);// 17May2017
																	// Arpitha
					Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.second_baby) + ":"
							+ getResources().getString(R.string.apgar_date_cannot_before_lastparto_entrydate) + " "
							+ strlatentry, Activity_Apgar.this);// 02Nov2016
					aq.id(R.id.etapgardatebaby2).text(Partograph_CommonClass.getConvertedDateFormat(todaysDate,
							Partograph_CommonClass.defdateformat));
					// strapgardate = todaysDate;// 16Nov2016 Arpitha
					apgardate2 = todaysDate; // 03Jan2017 Arpitha
				}

				// 20Sep2017 Arpitha
				else if (postpartumdatetime != null && taken.before(postpartumdatetime)) {

					String postpartum = Partograph_CommonClass.getConvertedDateFormat(
							strpostpartumdatetime.split("/")[0], Partograph_CommonClass.defdateformat);
					Partograph_CommonClass.displayAlertDialog(
							getResources().getString(R.string.apgar_datetime_cannot_be_before_postpartum_datetime,
									Activity_Apgar.this) + " " + postpartum,
							Activity_Apgar.this);
					aq.id(R.id.etapgardatebaby2).text(Partograph_CommonClass.getConvertedDateFormat(todaysDate,
							Partograph_CommonClass.defdateformat));

				} // 20Sep2017 Arpitha

				else if (refdate != null && taken.before(refdate)) {
					String refDate = Partograph_CommonClass.getConvertedDateFormat(refdatetime,
							Partograph_CommonClass.defdateformat);// 17May2017
																	// Arpitha
					Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.second_baby) + ":"
							+ getResources().getString(R.string.apgar_date_cannot_before_referral_date) + " " + refDate,
							Activity_Apgar.this);// 02Nov2016 Arpitha
					aq.id(R.id.etapgardatebaby2).text(Partograph_CommonClass.getConvertedDateFormat(todaysDate,
							Partograph_CommonClass.defdateformat));
					// strapgardate = todaysDate;// 16Nov2016 Arpitha
					apgardate2 = todaysDate; // 03Jan2017 Arpitha

				}

				else {
					aq.id(R.id.etapgardatebaby2).text(Partograph_CommonClass.getConvertedDateFormat(selecteddate,
							Partograph_CommonClass.defdateformat));
					apgardate2 = selecteddate;
					// strapgardate = selecteddate;// 16Nov2016 Arpitha
					// apgardate = todaysDate; // 03Jan2017 Arpitha
				}
			}

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
	}

	// 04OCt2016 Arpitha
	@SuppressLint("SimpleDateFormat")
	private boolean isApgarTimevalid(String date1, String date2, int i) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		try {

			Date d1 = sdf.parse(date1);
			Date d2 = sdf.parse(date2);

			String strDate1 = Partograph_CommonClass.getConvertedDateFormat(date1,
					Partograph_CommonClass.defdateformat);// 17May2017 Arpitha
			String strDate2 = Partograph_CommonClass.getConvertedDateFormat(date2,
					Partograph_CommonClass.defdateformat);// 17May2017 Arpitha

			if (d2.before(d1)) {

				if (i == 1)

					Partograph_CommonClass.displayAlertDialog(
							getResources().getString(R.string.apgar_date_cannot_before_regdate) + " - " + strDate1,
							getApplicationContext());
				if (i == 2)
					Partograph_CommonClass.displayAlertDialog(
							getResources().getString(R.string.apgar_date_cannot_before_deldate) + " - " + strDate1,
							getApplicationContext());
				if (i == 3)
					Partograph_CommonClass.displayAlertDialog(
							getResources().getString(R.string.apgar_date_cannot_before_lastparto_entrydate) + " - "
									+ date2,
							getApplicationContext());
				if (i == 4)
					Partograph_CommonClass.displayAlertDialog(
							getResources().getString(R.string.apgar_date_cannot_before_referral_date) + " - "
									+ strDate2,
							getApplicationContext());
				if (i == 5)
					Partograph_CommonClass.displayAlertDialog(
							getResources().getString(R.string.invalid_date) + " - " + strDate2,
							getApplicationContext());

				// 02Nov2016 Arpitha
				if (i == 6)

					Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.second_baby) + ":"
							+ getResources().getString(R.string.apgar_date_cannot_before_regdate) + " - " + strDate1,
							getApplicationContext());
				if (i == 7)
					Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.second_baby) + ":"
							+ getResources().getString(R.string.apgar_date_cannot_before_deldate) + " - " + strDate1,
							getApplicationContext());
				if (i == 8)
					Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.second_baby) + ":"
							+ getResources().getString(R.string.apgar_date_cannot_before_lastparto_entrydate) + " - "
							+ date2, getApplicationContext());
				if (i == 9)
					Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.second_baby) + ":"
							+ getResources().getString(R.string.apgar_date_cannot_before_referral_date) + " - " + date2,
							getApplicationContext());
				if (i == 10)
					Partograph_CommonClass.displayAlertDialog(
							getResources().getString(R.string.second_baby) + ":"
									+ getResources().getString(R.string.invalid_date) + " - " + strDate2,
							getApplicationContext());
				return false;
			} else
				return true;

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);

			e.printStackTrace();
			return false;
		}
	}

	// 04Oct2016 Arpitha
	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case TIME_DIALOG_ID:
			// set time picker as current time
			return new TimePickerDialog(this, timePickerListener, hour, minute, false);

		}
		return null;
	}

	private TimePickerDialog.OnTimeSetListener timePickerListener = new TimePickerDialog.OnTimeSetListener() {
		public void onTimeSet(TimePicker view, int selectedHour, int selectedMinute) {

			try {
				if (view.isShown()) {
					hour = selectedHour;
					minute = selectedMinute;

					Date regdatetime = null;
					String regdate = null;
					String strselecteddate;
					String strdeldate;
					Date deldate = null;
					Date lastentrydate = null;
					Date refdate = null;

					if (woman.getregtype() != 2)
						regdate = woman.getDate_of_admission() + " " + woman.getTime_of_admission();
					strdeldate = woman.getDel_Date() + " " + woman.getDel_Time();

					if (regdate != null) {
						regdatetime = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(regdate);

					}
					if (strdeldate != null) {
						deldate = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(strdeldate);
					}

					if (strlastentry != null) {
						lastentrydate = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(strlastentry);

					}
					if (refdatetime != null) {
						refdate = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(refdatetime);
					}

					if (apgardate != null) {
						strselecteddate = apgardate + " "
								+ new StringBuilder().append(padding_str(hour)).append(":").append(padding_str(minute));
					} else
						strselecteddate = todaysDate + " "
								+ new StringBuilder().append(padding_str(hour)).append(":").append(padding_str(minute));

					Date selecteddate = null;
					try {
						selecteddate = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(strselecteddate);
						Date currenttime = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(
								Partograph_CommonClass.getTodaysDate() + " " + Partograph_CommonClass.getCurrentTime());

						// 20Sep2017 Arpitha
						if (strpostpartumdatetime != null && strpostpartumdatetime.trim().length() > 0) {
							postpartumdatetime = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(strpostpartumdatetime);
						} // 20Sep2017 Arpitha

						if (isbaby1)// 02Nov2016 Arpitha
						{
							if (selecteddate.after(currenttime)) {
								Partograph_CommonClass.displayAlertDialog(
										getResources().getString(R.string.invalid_date), getApplicationContext());
								aq.id(R.id.etapgartime).text(Partograph_CommonClass.getCurrentTime());
							} else if (regdatetime != null && selecteddate.before(regdatetime)
									&& woman.getregtype() != 2) {
								String strRegDate = Partograph_CommonClass.getConvertedDateFormat(
										woman.getDate_of_admission(), Partograph_CommonClass.defdateformat);// 17May2017
																											// Arpitha
								Partograph_CommonClass
										.displayAlertDialog(
												getResources().getString(R.string.apgar_date_cannot_before_regdate)
														+ " " + strRegDate + " " + woman.getTime_of_admission(),
												Activity_Apgar.this);
								aq.id(R.id.etapgartime).text(Partograph_CommonClass.getCurrentTime());
							} else if (selecteddate.before(deldate)) {
								String strdelDate = Partograph_CommonClass.getConvertedDateFormat(woman.getDel_Date(),
										Partograph_CommonClass.defdateformat);// 17May2017
																				// Arpitha
								Partograph_CommonClass
										.displayAlertDialog(
												getResources().getString(R.string.apgar_date_cannot_before_deldate)
														+ " " + strdelDate + " " + woman.getDel_Time(),
												Activity_Apgar.this);
								aq.id(R.id.etapgartime).text(Partograph_CommonClass.getCurrentTime());
							} else if (lastentrydate != null && selecteddate.before(lastentrydate)) {
								String lastentryDate = Partograph_CommonClass.getConvertedDateFormat(strlastentry,
										Partograph_CommonClass.defdateformat);// 17May2017
																				// Arpitha
								Partograph_CommonClass.displayAlertDialog(
										getResources().getString(R.string.apgar_date_cannot_before_lastparto_entrydate)
												+ " " + lastentryDate + " " + strlastentry.split("\\s+")[1],
										Activity_Apgar.this);
								aq.id(R.id.etapgartime).text(Partograph_CommonClass.getCurrentTime());
							}

							// 20Sep2017 Arpitha
							else if (postpartumdatetime != null && selecteddate.before(postpartumdatetime)) {

								String postpartum = Partograph_CommonClass.getConvertedDateFormat(
										strpostpartumdatetime.split("/")[0], Partograph_CommonClass.defdateformat);
								Partograph_CommonClass.displayAlertDialog(getResources().getString(
										R.string.apgar_datetime_cannot_be_before_postpartum_datetime,
										Activity_Apgar.this) + " " + postpartum + " "
										+ strpostpartumdatetime.split("/")[1], getApplicationContext());
								aq.id(R.id.etapgartime).text(Partograph_CommonClass.getCurrentTime());

							} // 20Sep2017 Arpitha

							else if (refdate != null && selecteddate.before(refdate)) {
								String refDate = Partograph_CommonClass.getConvertedDateFormat(refdatetime,
										Partograph_CommonClass.defdateformat);// 17May2017
																				// Arpitha
								Partograph_CommonClass.displayAlertDialog(
										getResources().getString(R.string.apgar_date_cannot_before_referral_date) + " "
												+ refDate + " " + refdatetime.split("\\s+")[1],
										Activity_Apgar.this);
								aq.id(R.id.etapgartime).text(Partograph_CommonClass.getCurrentTime());
							} else {
								aq.id(R.id.etapgartime).text(new StringBuilder().append(padding_str(hour)).append(":")
										.append(padding_str(minute)));
							}
						} else {

							if (apgardate2 != null) {
								strselecteddate = apgardate2 + " " + new StringBuilder().append(padding_str(hour))
										.append(":").append(padding_str(minute));
							} else
								strselecteddate = todaysDate + " " + new StringBuilder().append(padding_str(hour))
										.append(":").append(padding_str(minute));

							Date selecteddate2 = null;
							selecteddate2 = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(strselecteddate);

							if (selecteddate2.after(currenttime)) {
								Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.second_baby)
										+ ":" + getResources().getString(R.string.invalid_date), Activity_Apgar.this);
								aq.id(R.id.etapgartimebaby2).text(Partograph_CommonClass.getCurrentTime());
							} else if (regdatetime != null && selecteddate2.before(regdatetime)
									&& woman.getregtype() != 2) {
								String regDate = Partograph_CommonClass.getConvertedDateFormat(
										woman.getDate_of_admission(), Partograph_CommonClass.defdateformat);// 17May2017
																											// Arpitha
								Partograph_CommonClass
										.displayAlertDialog(
												getResources().getString(R.string.second_baby) + ":"
														+ getResources()
																.getString(R.string.apgar_date_cannot_before_regdate)
														+ " " + regDate + " " + woman.getTime_of_admission(),
												Activity_Apgar.this);
								aq.id(R.id.etapgartimebaby2).text(Partograph_CommonClass.getCurrentTime());
							} else if (selecteddate2.before(deldate)) {
								String delDate = Partograph_CommonClass.getConvertedDateFormat(woman.getDel_Date(),
										Partograph_CommonClass.defdateformat);// 17May2017
																				// Arpitha
								Partograph_CommonClass
										.displayAlertDialog(
												getResources().getString(R.string.second_baby) + ":"
														+ getResources()
																.getString(R.string.apgar_date_cannot_before_deldate)
														+ " " + delDate + " " + woman.getDel_Time(),
												Activity_Apgar.this);
								aq.id(R.id.etapgartimebaby2).text(Partograph_CommonClass.getCurrentTime());
							} else if (lastentrydate != null && selecteddate2.before(lastentrydate)) {
								String lastentryDate = Partograph_CommonClass.getConvertedDateFormat(strlastentry,
										Partograph_CommonClass.defdateformat);// 17May2017
																				// Arpitha
								Partograph_CommonClass.displayAlertDialog(
										getResources().getString(R.string.second_baby) + ":"
												+ getResources().getString(
														R.string.apgar_date_cannot_before_lastparto_entrydate)
												+ " " + lastentryDate + " " + lastentryDate.split("\\s+")[1],
										Activity_Apgar.this);
								aq.id(R.id.etapgartimebaby2).text(Partograph_CommonClass.getCurrentTime());
							}

							// 20Sep2017 Arpitha
							else if (postpartumdatetime != null && selecteddate2.before(postpartumdatetime)) {

								String postpartum = Partograph_CommonClass.getConvertedDateFormat(
										strpostpartumdatetime.split("/")[0], Partograph_CommonClass.defdateformat);
								Partograph_CommonClass
										.displayAlertDialog(
												getResources().getString(R.string.second_baby) + ":"
														+ getResources().getString(
																R.string.apgar_datetime_cannot_be_before_postpartum_datetime,
																Activity_Apgar.this)
														+ " " + postpartum + " " + strpostpartumdatetime.split("/")[1],
												getApplicationContext());
								aq.id(R.id.etapgartimebaby2).text(Partograph_CommonClass.getCurrentTime());

							} // 20Sep2017 Arpitha

							else if (refdate != null && selecteddate2.before(refdate)) {
								String refDate = Partograph_CommonClass.getConvertedDateFormat(refdatetime,
										Partograph_CommonClass.defdateformat);// 17May2017
																				// Arpitha
								Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.second_baby)
										+ ":"
										+ getResources().getString(R.string.apgar_date_cannot_before_referral_date,
												Activity_Apgar.this)
										+ " " + refDate + " " + refDate.split("\\s+")[1], getApplicationContext());
								aq.id(R.id.etapgartimebaby2).text(Partograph_CommonClass.getCurrentTime());
							} else {
								aq.id(R.id.etapgartimebaby2).text(new StringBuilder().append(padding_str(hour))
										.append(":").append(padding_str(minute)));
							}

						}
					} catch (ParseException e) {
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}

				}

			} catch (Exception e) {
				AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
						+ this.getClass().getSimpleName(), e);
				e.printStackTrace();
			}
		}
	};

	private static String padding_str(int c) {
		if (c >= 10)
			return String.valueOf(c);
		else
			return "0" + String.valueOf(c);
	}

	// 20oct2016 Arpitha
	@Override
	public void onBackPressed() {
		Intent i = new Intent(Activity_Apgar.this, Activity_WomenView.class);
		startActivity(i);
	}

	// Retrieve comments from tbl_comment from server
	private void calRetrieveComments() throws Exception {

		int interval = 1;

		commentsTimer = new CountDownTimer(Long.MAX_VALUE, interval * START_AFTER_SECONDS) {

			@Override
			public void onTick(long millisUntilFinished) {
				AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
						+ this.getClass().getSimpleName());

				AsyncCallWS_Comments task = new AsyncCallWS_Comments();
				task.execute();
			}

			@Override
			public void onFinish() {
				// TODO Auto-generated method stub

			}
		}.start();

	}

	// Async cls to sync
	public class AsyncCallWS_Comments extends AsyncTask<String, Void, String> {

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());

			if (!inetOn)
				System.out.println("Internet :" + getResources().getString(R.string.chk_internet));

			else {
				if (Partograph_CommonClass.movedToInputFolder && Partograph_CommonClass.responseAckCount <= 5) {
					if (!SyncFunctions.syncUptoDate) {
						AsyncCallWS_Comments task = new AsyncCallWS_Comments();
						task.execute();
					} else {
						AsyncCallWS_Comments.this.cancel(true);

					}
				} else {
					AsyncCallWS_Comments.this.cancel(true);

				}

				try {
					getCommentCountNotRead();

				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
		}

		@Override
		protected String doInBackground(String... params) {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
			try {
				inetOn = Partograph_CommonClass.isConnectionAvailable(getApplicationContext());
				if (inetOn)
					/*
					 * serverResult = new SyncFunctions(getApplicationContext())
					 * .SyncFunctionComments( rowItems.get(pos).getUserId(),
					 * getResources().getString(R.string.dbName),
					 * Partograph_CommonClass.ipAdd);
					 */
					if (Partograph_CommonClass.sPojo != null) {
					serverResult = new SyncFunctions(getApplicationContext()).SyncFunctionComments(woman.getUserId(), Partograph_CommonClass.sPojo.getServerDb(), Partograph_CommonClass.sPojo.getIpAddress());
					}

			} catch (Exception e) {
				AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
						+ this.getClass().getSimpleName(), e);
				return e.getLocalizedMessage();
			}
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
			return serverResult;
		}

		@Override
		protected void onPreExecute() {
			System.out.println("BG PROCESS");
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
		}
	}

	// Get the count of comments not read
	public void getCommentCountNotRead() throws Exception {

		Cursor cursor = dbh.getCommentsUnread(woman.getWomenId(), woman.getUserId());

		if (cursor != null && cursor.getCount() > 0) {

			// isMsgRec = true;
			cursor.moveToFirst();

			do {

				if (cursor.getInt(1) == 10) {
					if (cursor.getInt(0) > 0)
						isMsg = true;
				}

				/*
				 * switch (cursor.getInt(1)) {
				 * 
				 * // fhr case 1: { Partograph_CommonClass.fhrcmt_cnt =
				 * cursor.getInt(0); isMsgFHR = true; break; } // afm case 2: {
				 * Partograph_CommonClass.afmcmt_cnt = cursor.getInt(0);
				 * isMsgAFM = true; break; }
				 * 
				 * // dilatation case 3: { Partograph_CommonClass.dilcmt_cnt =
				 * cursor.getInt(0); isMsgDil = true; break; } // contraction
				 * case 4: { Partograph_CommonClass.contcmt_cnt =
				 * cursor.getInt(0); isMsgCon = true; break; } // oxytocin case
				 * 5: { Partograph_CommonClass.oxycmt_cnt = cursor.getInt(0);
				 * isMsgOxy = true; break; } // drugs case 6: {
				 * Partograph_CommonClass.drugscmt_cnt = cursor.getInt(0);
				 * isMsgDrugs = true; break; } // pbp case 7: {
				 * Partograph_CommonClass.pbpcmt_cnt = cursor.getInt(0);
				 * isMsgPBP = true; break; } // temp case 8: {
				 * Partograph_CommonClass.tempcmt_cnt = cursor.getInt(0);
				 * isMsgTemp = true; break; } // urine test case 9: {
				 * Partograph_CommonClass.utcmt_cnt = cursor.getInt(0); isMsgUT
				 * = true; break; } case 10: Partograph_CommonClass.apgar_cnt =
				 * cursor.getInt(0); isMsgApg = true; break;
				 */
				// }

			} while (cursor.moveToNext());

			invalidateOptionsMenu();

		} else {

			isMsg = false;

		}

	}

}
