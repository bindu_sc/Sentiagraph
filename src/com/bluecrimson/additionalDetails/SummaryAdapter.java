package com.bluecrimson.additionalDetails;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import com.bc.partograph.common.AppContext;
import com.bc.partograph.common.Partograph_CommonClass;
import com.bc.partograph.common.Partograph_DB;
import com.bluecrimson.partograph.R;
import com.bluecrimson.partograph.Women_Profile_Pojo;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources.NotFoundException;
import android.database.DataSetObserver;
import android.graphics.Typeface;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.widget.BaseExpandableListAdapter;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class SummaryAdapter extends BaseExpandableListAdapter {

	private Activity context;
	private Map<String, ArrayList<Women_Profile_Pojo>> CategoryList;
	Partograph_DB dbh;
	String todaysDate;
	String currTime;
	// Animation
	Animation animBlink;
	ArrayList<Women_Profile_Pojo> rowItems;

	ArrayList<String> years;

	String[] heading;
	WomenListItem holder = null;
	String month_name;
	String[] months;
	int month;
	String startDay, endDay;//11April2017 Arp itha

	public SummaryAdapter(Summary_Activity context, ArrayList<String> years,
			HashMap<String, ArrayList<Women_Profile_Pojo>> categoryDataCollection, Partograph_DB dbh) {
		// TODO Auto-generated constructor stub
		this.context = context;
		this.years = years;
		this.dbh = dbh;
		this.CategoryList = categoryDataCollection;

	}

	private class WomenListItem {

		TableLayout tblsummary;

	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {

		ArrayList<String> lKeys = new ArrayList<String>(this.CategoryList.keySet());
		String xKey = lKeys.get(groupPosition);
		return xKey;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		int returnvalue = 0;
		try {

			returnvalue = 1;
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
		return returnvalue;
	}

	@Override
	public Object getGroup(int groupPosition) {
		Object returnvalue = null;

		try {

			ArrayList<String> lKeys = new ArrayList<String>(years);
			String xKey = lKeys.get(groupPosition);
			returnvalue = xKey;
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
		return returnvalue;
	}

	public int getGroupCount() {
		return this.years.size();
	}

	@Override
	public View getChildView(int groupPosition, int childPosition, boolean isLAstchild, View convertView,
			ViewGroup parent) {

		// TODO Auto-generated method stub
		try {

			LayoutInflater inflater = context.getLayoutInflater();

			if (convertView == null) {
				convertView = inflater.inflate(R.layout.new_womenlist_adapter, null);

				holder = new WomenListItem();

				convertView.setTag(holder);
			} else
				holder = (WomenListItem) convertView.getTag();

			holder.tblsummary = (TableLayout) convertView.findViewById(R.id.tblsummary);

			holder.tblsummary.removeAllViews();

			Partograph_CommonClass.getCurrentWeek();

			heading = this.context.getResources().getStringArray(R.array.summary);

			/*
			 * Calendar cal = Calendar.getInstance(); SimpleDateFormat today =
			 * new SimpleDateFormat("MMMM"); month_name =
			 * today.format(cal.getTime());
			 */
			month_name = Partograph_CommonClass.getCureentMonthName();
			Calendar cal = Calendar.getInstance();
			SimpleDateFormat currentYear = new SimpleDateFormat("yyyy");
			String year = currentYear.format(cal.getTime());

			SimpleDateFormat format = new SimpleDateFormat("MM");
			String todaysDate = Partograph_CommonClass.getTodaysDate("MM");
//			String monthId = format.format(todaysDate);
			month = Integer.parseInt(todaysDate);

			months = context.getResources().getStringArray(R.array.month);
			
			startDay = Partograph_CommonClass.getPreviousDate("yyyy-MM-dd", 7);//11April2017 Arpitha
			
			endDay = Partograph_CommonClass.getPreviousDate("yyyy-MM-dd", 1);//11April2017 Arpitha
			
			

			if (year.equalsIgnoreCase(years.get(groupPosition))) {
				
				displayCurrentYearSummary(groupPosition);

				
			} else {
				displayPreviousYearSummary(groupPosition);
			}

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}

		return convertView;
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

		// TODO Auto-generated method stub

		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.group_item, null);
		}

		String yearNme = (String) getGroup(groupPosition);
		TextView item = (TextView) convertView.findViewById(R.id.txtMnthName);
		TextView txtregcount = (TextView) convertView.findViewById(R.id.txtregcount);//01Feb17 Arpitha
		item.setTypeface(null, Typeface.BOLD);

		item.setText(yearNme);
		
		int regCount = dbh.getYearlyRegCount(yearNme, Partograph_CommonClass.user.getUserId());//01Feb17 Arpitha
		
		txtregcount.setText(context.getResources().getString(R.string.total_reg_count)+" "+""+regCount);//01Feb17 Arpitha

		return convertView;

	}

	@Override
	public boolean hasStableIds() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return true;
	}

	public void unregisterDataSetObserver(DataSetObserver observer) {
		if (observer != null) {
			super.unregisterDataSetObserver(observer);
		}
	}

	void displayCurrentYearSummary(int groupPosition) throws NotFoundException, Exception {
		TableRow trlabel = new TableRow(this.context);
		trlabel.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 40));

		TextView textmenu = new TextView(this.context);
		textmenu.setTextSize(18);
		textmenu.setPadding(10, 10, 10, 10);
		textmenu.setTextColor(this.context.getResources().getColor(R.color.brown));
		textmenu.setTextSize(18);
		textmenu.setTypeface(textmenu.getTypeface(), Typeface.BOLD);
		textmenu.setGravity(Gravity.LEFT);
		trlabel.addView(textmenu);

		TextView texttoday = new TextView(this.context);
		texttoday.setTextSize(18);
		texttoday.setPadding(10, 10, 10, 10);
		texttoday.setTextColor(this.context.getResources().getColor(R.color.brown));
		texttoday.setTextSize(15);
		texttoday.setGravity(Gravity.CENTER);
		texttoday.setTypeface(textmenu.getTypeface(), Typeface.BOLD);
		texttoday.setText(this.context.getResources()
				.getString(R.string.today) /*
											 * + "\n(" + Partograph_CommonClass.
											 * getTodaysDate("dd-MM-YYYY") + ")"
											 */);
		trlabel.addView(texttoday);

		TextView textweek = new TextView(this.context);
		textweek.setTextSize(15);
		textweek.setPadding(10, 10, 10, 10);
		textweek.setTextColor(this.context.getResources().getColor(R.color.brown));
		textweek.setTypeface(textweek.getTypeface(), Typeface.BOLD);
		textweek.setGravity(Gravity.CENTER);
		textweek.setText(Partograph_CommonClass.getPreviousDate("ddMMM", 7) + " to\n"
				+ Partograph_CommonClass.getPreviousDate("ddMMM", 1));
		trlabel.addView(textweek);

		

		for (int i = 0; i < month; i++) {
			TextView txt1 = new TextView(this.context);
			txt1.setTextSize(18);
			txt1.setPadding(10, 10, 10, 10);
			txt1.setTextColor(this.context.getResources().getColor(R.color.brown));
			txt1.setTextSize(15);
			txt1.setTypeface(txt1.getTypeface(), Typeface.BOLD);
			txt1.setGravity(Gravity.CENTER);
			txt1.setText(months[i]);
			trlabel.addView(txt1);
		}

		TextView textyear = new TextView(this.context);
		textyear.setTextSize(18);
		textyear.setPadding(10, 10, 10, 10);
		textyear.setTextColor(this.context.getResources().getColor(R.color.brown));
		textyear.setTextSize(15);
		textyear.setTypeface(textyear.getTypeface(), Typeface.BOLD);
		textyear.setGravity(Gravity.CENTER);
		textyear.setText(years.get(groupPosition));
		trlabel.addView(textyear);

		holder.tblsummary.addView(trlabel);

		TableRow trlabelVal = null;

		for (int j = 1; j <= heading.length; j++) {

			ArrayList<String> val = dbh.getPartographData(Partograph_CommonClass.user.getUserId(), j,
					startDay, endDay,month);

			trlabelVal = new TableRow(this.context);
			trlabelVal.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 40));

			TextView textmenuval = new TextView(this.context);
			textmenuval.setTextSize(18);
			textmenuval.setPadding(10, 10, 10, 10);
			textmenuval.setTextColor(this.context.getResources().getColor(R.color.brown));
			textmenuval.setTextSize(18);
			textmenuval.setText(heading[j - 1]);
			textmenuval.setGravity(Gravity.LEFT);
			trlabelVal.addView(textmenuval);

			TextView texttodayval = new TextView(this.context);
			texttodayval.setTextSize(18);
			texttodayval.setPadding(10, 10, 10, 10);
			texttodayval.setTextColor(this.context.getResources().getColor(R.color.black));
			texttodayval.setTextSize(18);
			texttodayval.setText(val.get(0));
			texttodayval.setGravity(Gravity.CENTER);
			trlabelVal.addView(texttodayval);

			TextView textweekval = new TextView(this.context);
			textweekval.setTextSize(18);
			textweekval.setPadding(10, 10, 10, 10);
			textweekval.setTextColor(this.context.getResources().getColor(R.color.black));
			textweekval.setTextSize(18);
			textweekval.setText(val.get(1));
			textweekval.setGravity(Gravity.CENTER);
			trlabelVal.addView(textweekval);
			
			for(int i=0;i<month;i++)
			{

			TextView textmonthval = new TextView(this.context);
			textmonthval.setTextSize(18);
			textmonthval.setPadding(10, 10, 10, 10);
			textmonthval.setTextColor(this.context.getResources().getColor(R.color.black));
			textmonthval.setTextSize(18);
			textmonthval.setGravity(Gravity.CENTER);
			textmonthval.setText(val.get(2+i));
			trlabelVal.addView(textmonthval);
			}

			TextView textyearval = new TextView(this.context);
			textyearval.setTextSize(18);
			textyearval.setPadding(10, 10, 10, 10);
			textyearval.setTextColor(this.context.getResources().getColor(R.color.black));
			textyearval.setTextSize(18);
			textyearval.setText(val.get(3+month-1));
			textyearval.setGravity(Gravity.CENTER);
			trlabelVal.addView(textyearval);
			
			
			

			holder.tblsummary.addView(trlabelVal);
		}
	}

	void displayPreviousYearSummary(int groupPosition) throws NotFoundException, Exception {
		TableRow trlabel = new TableRow(this.context);
		trlabel.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 40));

		TextView textmenu = new TextView(this.context);
		textmenu.setTextSize(18);
		textmenu.setPadding(10, 10, 10, 10);
		textmenu.setTextColor(this.context.getResources().getColor(R.color.brown));
		textmenu.setTextSize(18);
		textmenu.setTypeface(textmenu.getTypeface(), Typeface.BOLD);
		textmenu.setGravity(Gravity.LEFT);

		trlabel.addView(textmenu);

		TextView textjan = new TextView(this.context);
		textjan.setTextSize(18);
		textjan.setPadding(10, 10, 10, 10);
		textjan.setTextColor(this.context.getResources().getColor(R.color.brown));
		textjan.setTextSize(15);
		textjan.setGravity(Gravity.CENTER);
		textjan.setTypeface(textmenu.getTypeface(), Typeface.BOLD);
		textjan.setText(months[0]);
		trlabel.addView(textjan);

		TextView textfeb = new TextView(this.context);
		textfeb.setTextSize(15);
		textfeb.setPadding(10, 10, 10, 10);
		textfeb.setTextColor(this.context.getResources().getColor(R.color.brown));
		textfeb.setTypeface(textfeb.getTypeface(), Typeface.BOLD);
		textfeb.setGravity(Gravity.CENTER);
		textfeb.setText(months[1]);
		trlabel.addView(textfeb);

		TextView textmar = new TextView(this.context);
		textmar.setTextSize(15);
		textmar.setPadding(10, 10, 10, 10);
		textmar.setTextColor(this.context.getResources().getColor(R.color.brown));
		textmar.setTextSize(15);
		textmar.setGravity(Gravity.CENTER);
		textmar.setTypeface(textmar.getTypeface(), Typeface.BOLD);
		textmar.setText(months[2]);
		// textmar.setText(this.context.getResources().getString(R.string.thismonth)
		// + "\n" + month_name);
		trlabel.addView(textmar);

		TextView textapr = new TextView(this.context);
		textapr.setTextSize(18);
		textapr.setPadding(10, 10, 10, 10);
		textapr.setTextColor(this.context.getResources().getColor(R.color.brown));
		textapr.setTextSize(15);
		textapr.setTypeface(textapr.getTypeface(), Typeface.BOLD);
		textapr.setGravity(Gravity.CENTER);
		textapr.setText(months[3]);
		// textapr.setText(this.context.getResources().getString(R.string.thisyear)
		// + "\n" + years.get(groupPosition));
		trlabel.addView(textapr);

		TextView textmay = new TextView(this.context);
		textmay.setTextSize(15);
		textmay.setPadding(10, 10, 10, 10);
		textmay.setTextColor(this.context.getResources().getColor(R.color.brown));
		textmay.setTypeface(textmay.getTypeface(), Typeface.BOLD);
		textmay.setGravity(Gravity.CENTER);
		textmay.setText(months[4]);
		trlabel.addView(textmay);

		TextView textjun = new TextView(this.context);
		textjun.setTextSize(15);
		textjun.setPadding(10, 10, 10, 10);
		textjun.setTextColor(this.context.getResources().getColor(R.color.brown));
		textjun.setTypeface(textjun.getTypeface(), Typeface.BOLD);
		textjun.setGravity(Gravity.CENTER);
		textjun.setText(months[5]);
		trlabel.addView(textjun);

		TextView textjuly = new TextView(this.context);
		textjuly.setTextSize(15);
		textjuly.setPadding(10, 10, 10, 10);
		textjuly.setTextColor(this.context.getResources().getColor(R.color.brown));
		textjuly.setTypeface(textjuly.getTypeface(), Typeface.BOLD);
		textjuly.setGravity(Gravity.CENTER);
		textjuly.setText(months[6]);
		trlabel.addView(textjuly);

		TextView textaug = new TextView(this.context);
		textaug.setTextSize(15);
		textaug.setPadding(10, 10, 10, 10);
		textaug.setTextColor(this.context.getResources().getColor(R.color.brown));
		textaug.setTypeface(textaug.getTypeface(), Typeface.BOLD);
		textaug.setGravity(Gravity.CENTER);
		textaug.setText(months[7]);
		trlabel.addView(textaug);

		TextView texsep = new TextView(this.context);
		texsep.setTextSize(15);
		texsep.setPadding(10, 10, 10, 10);
		texsep.setTextColor(this.context.getResources().getColor(R.color.brown));
		texsep.setTypeface(texsep.getTypeface(), Typeface.BOLD);
		texsep.setGravity(Gravity.CENTER);
		texsep.setText(months[8]);
		trlabel.addView(texsep);

		TextView textoct = new TextView(this.context);
		textoct.setTextSize(15);
		textoct.setPadding(10, 10, 10, 10);
		textoct.setTextColor(this.context.getResources().getColor(R.color.brown));
		textoct.setTypeface(textoct.getTypeface(), Typeface.BOLD);
		textoct.setGravity(Gravity.CENTER);
		textoct.setText(months[9]);
		trlabel.addView(textoct);

		TextView textnov = new TextView(this.context);
		textnov.setTextSize(15);
		textnov.setPadding(10, 10, 10, 10);
		textnov.setTextColor(this.context.getResources().getColor(R.color.brown));
		textnov.setTypeface(textnov.getTypeface(), Typeface.BOLD);
		textnov.setGravity(Gravity.CENTER);
		textnov.setText(months[10]);
		trlabel.addView(textnov);

		TextView textdec = new TextView(this.context);
		textdec.setTextSize(15);
		textdec.setPadding(10, 10, 10, 10);
		textdec.setTextColor(this.context.getResources().getColor(R.color.brown));
		textdec.setTypeface(textdec.getTypeface(), Typeface.BOLD);
		textdec.setGravity(Gravity.CENTER);
		textdec.setText(months[11]);
		trlabel.addView(textdec);

		TextView texttotal = new TextView(this.context);
		texttotal.setTextSize(15);
		texttotal.setPadding(10, 10, 10, 10);
		texttotal.setTextColor(this.context.getResources().getColor(R.color.brown));
		texttotal.setTypeface(textdec.getTypeface(), Typeface.BOLD);
		texttotal.setGravity(Gravity.CENTER);
		texttotal.setText(this.context.getResources().getString(R.string.total));
		trlabel.addView(texttotal);

		holder.tblsummary.addView(trlabel);

		TableRow trlabelVal = null;

		for (int j = 1; j <= heading.length; j++) {

			ArrayList<String> val = dbh.getPreviousYearData(Partograph_CommonClass.user.getUserId(),
					years.get(groupPosition), j);

			trlabelVal = new TableRow(this.context);
			trlabelVal.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 40));

			TextView textmenuval = new TextView(this.context);
			textmenuval.setTextSize(18);
			textmenuval.setPadding(10, 10, 10, 10);
			textmenuval.setTextColor(this.context.getResources().getColor(R.color.brown));
			textmenuval.setTextSize(18);
			textmenuval.setText(heading[j - 1]);
			textmenuval.setGravity(Gravity.LEFT);
			trlabelVal.addView(textmenuval);

			TextView texttodayval = new TextView(this.context);
			texttodayval.setTextSize(18);
			texttodayval.setPadding(10, 10, 10, 10);
			texttodayval.setTextColor(this.context.getResources().getColor(R.color.black));
			texttodayval.setTextSize(18);
			texttodayval.setText(val.get(0));
			texttodayval.setGravity(Gravity.CENTER);
			trlabelVal.addView(texttodayval);

			TextView textweekval = new TextView(this.context);
			textweekval.setTextSize(18);
			textweekval.setPadding(10, 10, 10, 10);
			textweekval.setTextColor(this.context.getResources().getColor(R.color.black));
			textweekval.setTextSize(18);
			textweekval.setText(val.get(1));
			textweekval.setGravity(Gravity.CENTER);
			trlabelVal.addView(textweekval);

			TextView textmonthval = new TextView(this.context);
			textmonthval.setTextSize(18);
			textmonthval.setPadding(10, 10, 10, 10);
			textmonthval.setTextColor(this.context.getResources().getColor(R.color.black));
			textmonthval.setTextSize(18);
			textmonthval.setGravity(Gravity.CENTER);
			textmonthval.setText(val.get(2));
			trlabelVal.addView(textmonthval);

			TextView textyearval = new TextView(this.context);
			textyearval.setTextSize(18);
			textyearval.setPadding(10, 10, 10, 10);
			textyearval.setTextColor(this.context.getResources().getColor(R.color.black));
			textyearval.setTextSize(18);
			textyearval.setText(val.get(3));
			textyearval.setGravity(Gravity.CENTER);
			trlabelVal.addView(textyearval);

			TextView textmayval = new TextView(this.context);
			textmayval.setTextSize(18);
			textmayval.setPadding(10, 10, 10, 10);
			textmayval.setTextColor(this.context.getResources().getColor(R.color.black));
			textmayval.setTextSize(18);
			textmayval.setText(val.get(4));
			textmayval.setGravity(Gravity.CENTER);
			trlabelVal.addView(textmayval);

			TextView textjunval = new TextView(this.context);
			textjunval.setTextSize(18);
			textjunval.setPadding(10, 10, 10, 10);
			textjunval.setTextColor(this.context.getResources().getColor(R.color.black));
			textjunval.setTextSize(18);
			textjunval.setText(val.get(5));
			textjunval.setGravity(Gravity.CENTER);
			trlabelVal.addView(textjunval);

			TextView textjulval = new TextView(this.context);
			textjulval.setTextSize(18);
			textjulval.setPadding(10, 10, 10, 10);
			textjulval.setTextColor(this.context.getResources().getColor(R.color.black));
			textjulval.setTextSize(18);
			textjulval.setText(val.get(6));
			textjulval.setGravity(Gravity.CENTER);
			trlabelVal.addView(textjulval);

			TextView textaugval = new TextView(this.context);
			textaugval.setTextSize(18);
			textaugval.setPadding(10, 10, 10, 10);
			textaugval.setTextColor(this.context.getResources().getColor(R.color.black));
			textaugval.setTextSize(18);
			textaugval.setText(val.get(7));
			textaugval.setGravity(Gravity.CENTER);
			trlabelVal.addView(textaugval);

			TextView textsepval = new TextView(this.context);
			textsepval.setTextSize(18);
			textsepval.setPadding(10, 10, 10, 10);
			textsepval.setTextColor(this.context.getResources().getColor(R.color.black));
			textsepval.setTextSize(18);
			textsepval.setText(val.get(8));
			textsepval.setGravity(Gravity.CENTER);
			trlabelVal.addView(textsepval);

			TextView textoctval = new TextView(this.context);
			textoctval.setTextSize(18);
			textoctval.setPadding(10, 10, 10, 10);
			textoctval.setTextColor(this.context.getResources().getColor(R.color.black));
			textoctval.setTextSize(18);
			textoctval.setText(val.get(9));
			textoctval.setGravity(Gravity.CENTER);
			trlabelVal.addView(textoctval);

			TextView textnovval = new TextView(this.context);
			textnovval.setTextSize(18);
			textnovval.setPadding(10, 10, 10, 10);
			textnovval.setTextColor(this.context.getResources().getColor(R.color.black));
			textnovval.setTextSize(18);
			textnovval.setText(val.get(10));
			textnovval.setGravity(Gravity.CENTER);
			trlabelVal.addView(textnovval);

			TextView textdecval = new TextView(this.context);
			textdecval.setTextSize(18);
			textdecval.setPadding(10, 10, 10, 10);
			textdecval.setTextColor(this.context.getResources().getColor(R.color.black));
			textdecval.setTextSize(18);
			textdecval.setText(val.get(11));
			textdecval.setGravity(Gravity.CENTER);
			trlabelVal.addView(textdecval);

			int total = 0;

			for (int i = 0; i < val.size(); i++) {
				total = total + Integer.parseInt(val.get(i));
			}

			TextView texttotalval = new TextView(this.context);
			texttotalval.setTextSize(18);
			texttotalval.setPadding(10, 10, 10, 10);
			texttotalval.setTextColor(this.context.getResources().getColor(R.color.black));
			texttotalval.setTextSize(18);
			texttotalval.setText("" + total);
			texttotal.setTypeface(textdec.getTypeface(), Typeface.BOLD);
			texttotalval.setGravity(Gravity.CENTER);
			trlabelVal.addView(texttotalval);

			holder.tblsummary.addView(trlabelVal);
		}
	}

}
