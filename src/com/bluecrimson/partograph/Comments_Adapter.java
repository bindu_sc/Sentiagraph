package com.bluecrimson.partograph;

import java.util.ArrayList;

import com.bc.partograph.common.AppContext;
import com.bc.partograph.common.Partograph_CommonClass;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class Comments_Adapter extends ArrayAdapter<Add_value_pojo> {
	Context context;
	Add_value_pojo data;

	// constructor
	public Comments_Adapter(Context context, int textViewResourceId, ArrayList<Add_value_pojo> objects) {
		super(context, textViewResourceId, objects);
		this.context = context;
	}

	private class CommentsListItem {
		TextView txtComments;
		TextView txtdate_of_com;
		TextView txttime_of_com;
	}

	@Override
	public int getViewTypeCount() {
		return getCount();
	}

	@Override
	public int getItemViewType(int position) {
		return position;
	}

	// Get main view
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		try {
			CommentsListItem holder = null;
			Add_value_pojo rowItem = getItem(position);

			LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

			if (convertView == null) {
				convertView = mInflater.inflate(R.layout.activity_comment, null);

				holder = new CommentsListItem();
				holder.txtComments = (TextView) convertView.findViewById(R.id.txtComment);
				holder.txtdate_of_com = (TextView) convertView.findViewById(R.id.txtdateComments);
				holder.txttime_of_com = (TextView) convertView.findViewById(R.id.txttimecomments);

				convertView.setTag(holder);
			} else
				holder = (CommentsListItem) convertView.getTag();

			holder.txtComments.setText(rowItem.getComments() == null ? " " : rowItem.getComments());

			holder.txtdate_of_com.setText(rowItem.getStrdate() == null ? " "
					: Partograph_CommonClass.getConvdate_ddMMYYYY(rowItem.getStrdate(),
							Partograph_CommonClass.defdateformat));

			holder.txttime_of_com.setText(rowItem.getStrTime() == null ? " " : ", " + rowItem.getStrTime());

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}

		return convertView;
	}

}
