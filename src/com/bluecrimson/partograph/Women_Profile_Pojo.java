package com.bluecrimson.partograph;

import java.io.Serializable;
import java.util.ArrayList;

public class Women_Profile_Pojo implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String userId;
	String womenId;
	byte[] women_Image;
	String women_name;
	String date_of_admission;
	String Time_of_admission;
	int age;
	String address;
	String phone_No;
	String hosp_no;
	int gravida;
	int para;
	String w_attendant;
	String doc_name;
	String nurse_name;
	String facility;
	// String ruptured_memb;
	String special_inst;
	String comments;
	// String cond_while_admn;
	String del_Comments;
	String del_Time;
	String del_Date;
	int del_result1;
	int no_of_child;
	int babywt1;
	int babywt2;
	int babysex1, babysex2;
	int gestationage;
	// updated on 17july2016 by Arpitha
	// String gestationage;

	int del_result2;
	int mothersdeath;
	int del_type;
	String del_time2;
	String admitted_with;
	int risk_category;
	String risk_observed;
	int memb_pres_abs;
	String mothers_death_reason;
	String state;
	String district;
	String taluk;
	String facility_name;
	String dateinserted;
	boolean isDanger;
	int wmonth;
	int wyear;
	String datelastupdated;
	ArrayList<Integer> dangerSignsList = new ArrayList<Integer>();
	int gest_age_days;

	// 25Nov2015
	String delTypeReason;
	String deltype_otherreasons;
	String thayicardnumber;

	String lmp;
	String riskoptions;

	// 04jan2016
	String tears;
	int episiotomy, suturematerial;

	// 19jan2016
	int bloodgroup;

	// updated on 27Feb2016 by Arpitha
	String edd;
	// int Height;
	// int Weight;
	String other;
	int height_unit;
	// updated on 20May16
	String height;

	String w_weight;

	String extracomments;// 13Oct2016 Arpitha

	int regtype;// 16Oct2016 Arpitha

	String regtypereason;// 18Oct2016 Arpitha

	String date_of_reg_entry;// 10nov2016 Arpitha

	int normaltype;
	
	String regtypeOtherReason;//21Nov2016 Arpitha
	/*//05May2017 Arpitha - v2.6 mantis id-0000231
	String babypresentation;

	public String getBabypresentation() {
		return babypresentation;
	}

	public void setBabypresentation(String babypresentation) {
		this.babypresentation = babypresentation;
	}//05May2017 Arpitha - v2.6 mantis id-0000231
*/
	public int getNormaltype() {
		return normaltype;
	}

	public void setNormaltype(int normaltype) {
		this.normaltype = normaltype;
	}

	public String getDate_of_reg_entry() {
		return date_of_reg_entry;
	}

	public void setDate_of_reg_entry(String date_of_reg_entry) {
		this.date_of_reg_entry = date_of_reg_entry;
	}

	// 18Oct2016 Arpitha
	public String getRegtypereason() {
		return regtypereason;
	}

	public void setRegtypereason(String regtypereason) {
		this.regtypereason = regtypereason;
	}// 18Oct2016 Arpitha

	public int getregtype() {
		return regtype;
	}

	public void setregtype(int deltype) {
		this.regtype = deltype;
	}// 16Oct2016 Arpitha

	// 13Oct2016 Arpitha
	public String getExtracomments() {
		return extracomments;
	}// 13Oct2016 Arpitha

	// 13Oct2016 Arpitha
	public void setExtracomments(String extracomments) {
		this.extracomments = extracomments;
	}// 13Oct2016 Arpitha

	public String getW_weight() {
		return w_weight;
	}

	public void setW_weight(String w_weight) {
		this.w_weight = w_weight;
	}

	public String getHeight() {
		return height;
	}

	public void setHeight(String height) {
		this.height = height;
	}

	public int getHeight_unit() {
		return height_unit;
	}

	public void setHeight_unit(int height_unit) {
		this.height_unit = height_unit;
	}

	public String getOther() {
		return other;
	}

	public void setOther(String other) {
		this.other = other;
	}

	/*
	 * public int getHeight() { return Height; } public void setHeight(int
	 * height) { Height = height; }
	 */
	/*
	 * public int getWeight() { return Weight; } public void setWeight(int
	 * weight) { Weight = weight; }
	 */
	public String getEdd() {
		return edd;
	}

	public void setEdd(String edd) {
		this.edd = edd;
	}

	public int getGest_age_days() {
		return gest_age_days;
	}

	public void setGest_age_days(int gest_age_days) {
		this.gest_age_days = gest_age_days;
	}

	public String getDel_time2() {
		return del_time2;
	}

	public void setDel_time2(String del_time2) {
		this.del_time2 = del_time2;
	}

	public int getMothersdeath() {
		return mothersdeath;
	}

	public int isMothersdeath() {
		return mothersdeath;
	}

	public void setMothersdeath(int motherdead) {
		this.mothersdeath = motherdead;
	}

	public int getNo_of_child() {
		return no_of_child;
	}

	public void setNo_of_child(int no_of_child) {
		this.no_of_child = no_of_child;
	}

	public int getBabywt1() {
		return babywt1;
	}

	public void setBabywt1(int babywt1) {
		this.babywt1 = babywt1;
	}

	public int getBabywt2() {
		return babywt2;
	}

	public void setBabywt2(int babywt2) {
		this.babywt2 = babywt2;
	}

	public String getWomen_name() {
		return women_name;
	}

	public void setWomen_name(String women_name) {
		this.women_name = women_name;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getWomenId() {
		return womenId;
	}

	public void setWomenId(String womenId) {
		this.womenId = womenId;
	}

	public byte[] getWomen_Image() {
		return women_Image;
	}

	public void setWomen_Image(byte[] women_Image) {
		this.women_Image = women_Image;
	}

	public String getDate_of_admission() {
		return date_of_admission;
	}

	public void setDate_of_admission(String date_of_admission) {
		this.date_of_admission = date_of_admission;
	}

	public String getTime_of_admission() {
		return Time_of_admission;
	}

	public void setTime_of_admission(String time_of_admission) {
		Time_of_admission = time_of_admission;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone_No() {
		return phone_No;
	}

	public void setPhone_No(String phone_No) {
		this.phone_No = phone_No;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}
	/*
	 * public String getCond_while_admn() { return cond_while_admn; } public
	 * void setCond_while_admn(String cond_while_admn) { this.cond_while_admn =
	 * cond_while_admn; }
	 */

	public String getDel_Comments() {
		return del_Comments;
	}

	public void setDel_Comments(String del_Comments) {
		this.del_Comments = del_Comments;
	}

	public String getDel_Time() {
		return del_Time;
	}

	public void setDel_Time(String del_Time) {
		this.del_Time = del_Time;
	}

	public String getDel_Date() {
		return del_Date;
	}

	public void setDel_Date(String del_Date) {
		this.del_Date = del_Date;
	}

	public String getHosp_no() {
		return hosp_no;
	}

	public void setHosp_no(String hosp_no) {
		this.hosp_no = hosp_no;
	}

	public String getW_attendant() {
		return w_attendant;
	}

	public void setW_attendant(String w_attendant) {
		this.w_attendant = w_attendant;
	}

	public String getDoc_name() {
		return doc_name;
	}

	public void setDoc_name(String doc_name) {
		this.doc_name = doc_name;
	}

	public String getNurse_name() {
		return nurse_name;
	}

	public void setNurse_name(String nurse_name) {
		this.nurse_name = nurse_name;
	}

	public String getFacility() {
		return facility;
	}

	public void setFacility(String facility) {
		this.facility = facility;
	}

	/*
	 * public String getRuptured_memb() { return ruptured_memb; } public void
	 * setRuptured_memb(String ruptured_memb) { this.ruptured_memb =
	 * ruptured_memb; }
	 */
	public int getGestationage() {
		return gestationage;
	}

	public void setGestationage(int gestationage) {
		this.gestationage = gestationage;
	}

	public String getAdmitted_with() {
		return admitted_with;
	}

	/*
	 * public String getGestationage() { return gestationage; } public void
	 * setGestationage(String gestationage) { this.gestationage = gestationage;
	 * }
	 */
	public void setAdmitted_with(String admitted_with) {
		this.admitted_with = admitted_with;
	}

	public String getRisk_observed() {
		return risk_observed;
	}

	public void setRisk_observed(String risk_observed) {
		this.risk_observed = risk_observed;
	}

	public String getMothers_death_reason() {
		return mothers_death_reason;
	}

	public void setMothers_death_reason(String mothers_death_reason) {
		this.mothers_death_reason = mothers_death_reason;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getTaluk() {
		return taluk;
	}

	public void setTaluk(String taluk) {
		this.taluk = taluk;
	}

	public String getFacility_name() {
		return facility_name;
	}

	public void setFacility_name(String facility_name) {
		this.facility_name = facility_name;
	}

	public String getDateinserted() {
		return dateinserted;
	}

	public void setDateinserted(String dateinserted) {
		this.dateinserted = dateinserted;
	}

	public boolean isDanger() {
		return isDanger;
	}

	public void setDanger(boolean isDanger) {
		this.isDanger = isDanger;
	}

	public int getWmonth() {
		return wmonth;
	}

	public void setWmonth(int wmonth) {
		this.wmonth = wmonth;
	}

	public int getWyear() {
		return wyear;
	}

	public void setWyear(int wyear) {
		this.wyear = wyear;
	}

	public String getDatelastupdated() {
		return datelastupdated;
	}

	public void setDatelastupdated(String datelastupdated) {
		this.datelastupdated = datelastupdated;
	}

	public ArrayList<Integer> getDangerSignsList() {
		return dangerSignsList;
	}

	public void setDangerSignsList(ArrayList<Integer> dangerSignsList) {
		this.dangerSignsList = dangerSignsList;
	}

	public String getSpecial_inst() {
		return special_inst;
	}

	public void setSpecial_inst(String special_inst) {
		this.special_inst = special_inst;
	}

	public int getGravida() {
		return gravida;
	}

	public void setGravida(int gravida) {
		this.gravida = gravida;
	}

	public int getPara() {
		return para;
	}

	public void setPara(int para) {
		this.para = para;
	}

	public int getDel_result1() {
		return del_result1;
	}

	public void setDel_result1(int del_result1) {
		this.del_result1 = del_result1;
	}

	public int getBabysex1() {
		return babysex1;
	}

	public void setBabysex1(int babysex1) {
		this.babysex1 = babysex1;
	}

	public int getBabysex2() {
		return babysex2;
	}

	public void setBabysex2(int babysex2) {
		this.babysex2 = babysex2;
	}

	public int getDel_result2() {
		return del_result2;
	}

	public void setDel_result2(int del_result2) {
		this.del_result2 = del_result2;
	}

	public int getDel_type() {
		return del_type;
	}

	public void setDel_type(int del_type) {
		this.del_type = del_type;
	}

	public int getRisk_category() {
		return risk_category;
	}

	public void setRisk_category(int risk_category) {
		this.risk_category = risk_category;
	}

	public int getMemb_pres_abs() {
		return memb_pres_abs;
	}

	public void setMemb_pres_abs(int memb_pres_abs) {
		this.memb_pres_abs = memb_pres_abs;
	}

	public String getDelTypeReason() {
		return delTypeReason;
	}

	public void setDelTypeReason(String delTypeReason) {
		this.delTypeReason = delTypeReason;
	}

	public String getDeltype_otherreasons() {
		return deltype_otherreasons;
	}

	public void setDeltype_otherreasons(String deltype_otherreasons) {
		this.deltype_otherreasons = deltype_otherreasons;
	}

	public String getThayicardnumber() {
		return thayicardnumber;
	}

	public void setThayicardnumber(String thayicardnumber) {
		this.thayicardnumber = thayicardnumber;
	}

	public String getLmp() {
		return lmp;
	}

	public void setLmp(String lmp) {
		this.lmp = lmp;
	}

	public String getRiskoptions() {
		return riskoptions;
	}

	public void setRiskoptions(String riskoptions) {
		this.riskoptions = riskoptions;
	}

	public String getTears() {
		return tears;
	}

	public void setTears(String tears) {
		this.tears = tears;
	}

	public int getEpisiotomy() {
		return episiotomy;
	}

	public void setEpisiotomy(int episiotomy) {
		this.episiotomy = episiotomy;
	}

	public int getSuturematerial() {
		return suturematerial;
	}

	public void setSuturematerial(int suturematerial) {
		this.suturematerial = suturematerial;
	}

	public int getBloodgroup() {
		return bloodgroup;
	}

	public void setBloodgroup(int bloodgroup) {
		this.bloodgroup = bloodgroup;
	}

	public String getRegtypeOtherReason() {
		return regtypeOtherReason;
	}

	public void setRegtypeOtherReason(String regtypeOtherReason) {
		this.regtypeOtherReason = regtypeOtherReason;
	}
	
	

}
