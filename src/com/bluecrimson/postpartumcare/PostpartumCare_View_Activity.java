package com.bluecrimson.postpartumcare;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;

import com.androidquery.AQuery;
import com.bc.partograph.comments.Comments_Activity;
import com.bc.partograph.common.AppContext;
import com.bc.partograph.common.KillAllActivitesAndGoToLogin;
import com.bc.partograph.common.MultiSelectionSpinner;
import com.bc.partograph.common.Partograph_CommonClass;
import com.bc.partograph.common.Partograph_DB;
import com.bc.partograph.deliveryinfo.DeliveryInfo_Activity;
import com.bc.partograph.latentphase.LatentPhase_Activity;
import com.bc.partograph.login.LoginActivity;
import com.bc.partograph.referralinfo.ReferralInfo_Activity;
import com.bc.partograph.settings.About;
import com.bc.partograph.settings.Settings_parto;
import com.bc.partograph.sliding.GraphInformation;
import com.bc.partograph.sliding.SlidingActivity;
import com.bc.partograph.stagesoflabor.StageofLabor;
import com.bc.partograph.womenview.Activity_WomenView;
import com.bluecrimson.additionalDetails.Summary_Activity;
import com.bluecrimson.apgar.Activity_Apgar;
import com.bluecrimson.dischargedetails.DiscargeDetails_Activity;
import com.bluecrimson.dischargedetails.DischargedWomanList_Activity;
import com.bluecrimson.partograph.R;
import com.bluecrimson.partograph.Women_Profile_Pojo;
import com.bluecrimson.usermanual.UseManual;
import com.bluecrimson.viewprofile.ViewProfile_Activity;
import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources.NotFoundException;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

public class PostpartumCare_View_Activity extends Activity implements OnClickListener {

	AQuery aq;
	Partograph_DB dbh;
	PostPartum_Pojo postpojo;
	static Women_Profile_Pojo woman;
	MultiSelectionSpinner mspnpresentigcomplaints;
	LinkedHashMap<String, Integer> presentingcomplaintsMap;

	TableLayout tbldata;
	TableLayout tbladd;
	static Context context;
	static File file;
	private static Font small = new Font(Font.HELVETICA, 10, Font.NORMAL);
	private static Font heading = new Font(Font.HELVETICA, 10, Font.BOLD);
	static PdfPTable womanbasics;
	static PdfPTable womandelInfo;
	static ArrayList<PostPartum_Pojo> values;
	static String examination = "", pallor = "", pulse = "", bp = "", involution = "", advice = "",
			breastExamination = "", lochia = "", perinealCare = "", complaints = "";
	public static int postview;// 03Sep2017 Arpitha
	public static boolean ispostview;// 03Sep2017 Arpitha

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_postpartumcare);
		try {
			woman = (Women_Profile_Pojo) getIntent().getSerializableExtra("woman");
			aq = new AQuery(this);
			dbh = Partograph_DB.getInstance(getApplicationContext());

			postview = Partograph_CommonClass.curr_tabpos;// 03Sep2017 Arpitha
			ispostview = true;// 03Sep2017 Arpitha

			initializeScreen(aq.id(R.id.relpostpartum).getView());

			this.setTitle(getResources().getString(R.string.postpartum_care));

			mspnpresentigcomplaints = (MultiSelectionSpinner) findViewById(R.id.mspnpresentigcomplaints);

			intialView();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		KillAllActivitesAndGoToLogin.addToStack(this);
	}

	private void intialView() throws Exception {
		// TODO Auto-generated method stub

		context = PostpartumCare_View_Activity.this;

		aq.id(R.id.btnview).text(getResources().getString(R.string.add));

		// aq.id(R.id.etdatepost).text(Partograph_CommonClass.getTodaysDate(Partograph_CommonClass.defdateformat));
		// aq.id(R.id.ettimepost).text(Partograph_CommonClass.getCurrentTime());
		// setPresentingcomplaints();

		tbldata = (TableLayout) findViewById(R.id.tblpostpartum);

		tbladd = (TableLayout) findViewById(R.id.tbladd);

		values = new ArrayList<PostPartum_Pojo>();
		PostPartum_Pojo postpojo;

		Cursor cur = dbh.getPostPartumData(woman.getUserId(), woman.getWomenId());
		// Cursor cur = PostPartumCare_Activity.cur;
		if (cur.getCount() > 0) {
			cur.moveToFirst();
			do {
				postpojo = new PostPartum_Pojo();
				postpojo.setUserId(cur.getString(0));
				postpojo.setWomanId(cur.getString(1));
				postpojo.setWomanName(cur.getString(2));
				postpojo.setComplaints(cur.getString(3));
				postpojo.setOther(cur.getString(4));
				postpojo.setExamination(cur.getString(5));
				postpojo.setPallor(cur.getString(6));
				postpojo.setPulse(cur.getInt(7));
				postpojo.setBp(cur.getString(8));
				postpojo.setBreastExamination(cur.getString(9));
				postpojo.setInvolution(cur.getString(10));
				postpojo.setLochia(cur.getString(11));
				postpojo.setPerineal(cur.getString(12));
				postpojo.setAdvice(cur.getString(13));
				postpojo.setDateTimeOfEntry(cur.getString(15));
				values.add(postpojo);

			} while (cur.moveToNext());

		}

		if (values.size() > 0) {

			tbldata.setVisibility(View.VISIBLE);
			tbladd.setVisibility(View.GONE);
			aq.id(R.id.btnsave).gone();
			// aq.id(R.id.btnview).backgroundColor(R.color.ashgray);

			// for (int i = 0; i < values.size(); i++) {

			tbldata.removeAllViews();

			TableRow trlabel = new TableRow(this);
			trlabel.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 40));

			TextView textExamination = new TextView(this);
			textExamination.setTextSize(15);
			// textPulse.setText(getResources().getString(R.string.examination));
			textExamination.setPadding(10, 10, 10, 10);
			textExamination.setTextColor(getResources().getColor(R.color.black));
			// textPulse.setTextSize(18);
			textExamination.setGravity(Gravity.CENTER);
			textExamination.setText(getResources().getString(R.string.examination));
			trlabel.addView(textExamination);

			TextView textcomplaints = new TextView(this);
			textcomplaints.setTextSize(15);
			textcomplaints.setPadding(10, 10, 10, 10);
			textcomplaints.setText(getResources().getString(R.string.presenting_complaints));
			textcomplaints.setTextColor(getResources().getColor(R.color.black));
			// textbp.setTextSize(15);
			textcomplaints.setGravity(Gravity.CENTER);
			trlabel.addView(textcomplaints);

			TextView textOthers = new TextView(this);
			textOthers.setTextSize(15);
			textOthers.setText(getResources().getString(R.string.others));
			textOthers.setPadding(10, 10, 10, 10);
			textOthers.setTextColor(getResources().getColor(R.color.black));
			// textcontractions.setTextSize(15);
			textOthers.setGravity(Gravity.LEFT);
			trlabel.addView(textOthers);

			TextView textpallor = new TextView(this);
			textpallor.setTextSize(15);
			textpallor.setText(getResources().getString(R.string.pallor));
			textpallor.setPadding(10, 10, 10, 10);
			textpallor.setTextColor(getResources().getColor(R.color.black));
			// textFHS.setTextSize(15);
			textpallor.setGravity(Gravity.LEFT);
			trlabel.addView(textpallor);

			TextView textpulse = new TextView(this);
			textpulse.setTextSize(15);
			textpulse.setText(getResources().getString(R.string.pulse));
			textpulse.setPadding(10, 10, 10, 10);
			textpulse.setTextColor(getResources().getColor(R.color.black));
			// textPV.setTextSize(15);
			textpulse.setGravity(Gravity.LEFT);
			trlabel.addView(textpulse);

			TextView textbp = new TextView(this);
			textbp.setTextSize(15);
			textbp.setText(getResources().getString(R.string.txtbpvalue));
			textbp.setPadding(10, 10, 10, 10);
			textbp.setTextColor(getResources().getColor(R.color.black));
			// textPV.setTextSize(15);
			textbp.setGravity(Gravity.LEFT);
			trlabel.addView(textbp);

			TextView textbreast = new TextView(this);
			textbreast.setTextSize(15);
			textbreast.setText(getResources().getString(R.string.breast_examination));
			textbreast.setPadding(10, 10, 10, 10);
			textbreast.setTextColor(getResources().getColor(R.color.black));
			// textPV.setTextSize(15);
			textbreast.setGravity(Gravity.LEFT);
			trlabel.addView(textbreast);

			TextView textinvolution = new TextView(this);
			textinvolution.setTextSize(15);
			textinvolution.setText(getResources().getString(R.string.involution_of_uterus));
			textinvolution.setPadding(10, 10, 10, 10);
			textinvolution.setTextColor(getResources().getColor(R.color.black));
			// textPV.setTextSize(15);
			textinvolution.setGravity(Gravity.LEFT);
			trlabel.addView(textinvolution);

			TextView textlochia = new TextView(this);
			textlochia.setTextSize(15);
			textlochia.setText(getResources().getString(R.string.lochia));
			textlochia.setPadding(10, 10, 10, 10);
			textlochia.setTextColor(getResources().getColor(R.color.black));
			// textPV.setTextSize(15);
			textlochia.setGravity(Gravity.LEFT);
			trlabel.addView(textlochia);

			TextView textperinealcare = new TextView(this);
			textperinealcare.setTextSize(15);
			textperinealcare.setText(getResources().getString(R.string.perineal_care));
			textperinealcare.setPadding(10, 10, 10, 10);
			textperinealcare.setTextColor(getResources().getColor(R.color.black));
			// textPV.setTextSize(15);
			textperinealcare.setGravity(Gravity.LEFT);
			trlabel.addView(textperinealcare);

			TextView textAdvice = new TextView(this);
			textAdvice.setTextSize(15);
			textAdvice.setText(getResources().getString(R.string.advice));
			textAdvice.setPadding(10, 10, 10, 10);
			textAdvice.setTextColor(getResources().getColor(R.color.black));
			// textAdvice.setTextSize(18);
			textAdvice.setGravity(Gravity.LEFT);
			trlabel.addView(textAdvice);

			TextView textdatetime = new TextView(this);
			textdatetime.setTextSize(15);
			textdatetime.setPadding(10, 10, 10, 10);
			textdatetime.setTextColor(getResources().getColor(R.color.black));
			// textdatetime.setTextSize(18);
			textdatetime.setText(getResources().getString(R.string.date));
			textdatetime.setGravity(Gravity.CENTER);
			trlabel.addView(textdatetime);

			for (int j = 0; j < values.size(); j++) {

				String admResDisplay = "";
				if (values.get(j).getComplaints() != null) {
					String[] admres = values.get(j).getComplaints().split(",");
					String[] admResArr = null;

					admResArr = getResources().getStringArray(R.array.presenting_complaints);

					if (admResArr != null) {
						if (admres != null && admres.length > 0) {
							for (String str : admres) {
								if (str != null && str.length() > 0)
									admResDisplay = admResDisplay + admResArr[Integer.parseInt(str)] + ",";
							}
						}
					}
				}

				if (j != 0) {
					complaints = complaints + "\n" + admResDisplay;

					examination = examination + "," + values.get(j).getExamination();
					pallor = pallor + "," + values.get(j).getPallor();
					bp = bp + "," + values.get(j).getBp();
					pulse = pulse + "," + "" + values.get(j).getPulse();
					involution = involution + "," + values.get(j).getInvolution();
					advice = advice + "," + values.get(j).getAdvice();
					lochia = lochia + "," + values.get(j).getLochia();
					perinealCare = perinealCare + "," + values.get(j).getPerineal();
					breastExamination = breastExamination + "," + values.get(j).getBreastExamination();
				} else {
					examination = values.get(j).getExamination();
					pallor = values.get(j).getPallor();
					bp = values.get(j).getBp();
					pulse = "" + values.get(j).getPulse();
					involution = values.get(j).getInvolution();
					advice = values.get(j).getAdvice();
					lochia = values.get(j).getLochia();
					perinealCare = values.get(j).getPerineal();
					breastExamination = values.get(j).getBreastExamination();
					complaints = admResDisplay;
				}

				TableRow tr = new TableRow(this);
				tr.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 40));

				TextView textExaminationval = new TextView(this);
				textExaminationval.setTextSize(15);
				textExaminationval.setText(values.get(j).getExamination());
				textExaminationval.setPadding(10, 10, 10, 10);
				textExaminationval.setTextColor(getResources().getColor(R.color.black));
				// textPulse.setTextSize(18);
				textExaminationval.setGravity(Gravity.CENTER);
				tr.addView(textExaminationval);

				TextView textcomplaintsval = new TextView(this);
				textcomplaintsval.setTextSize(15);
				textcomplaintsval.setPadding(10, 10, 10, 10);
				textcomplaintsval.setText(admResDisplay);
				textcomplaintsval.setTextColor(getResources().getColor(R.color.black));
				// textbp.setTextSize(15);
				textcomplaintsval.setGravity(Gravity.CENTER);
				tr.addView(textcomplaintsval);

				TextView textOthersval = new TextView(this);
				textOthersval.setTextSize(15);
				textOthersval.setText(values.get(j).getOther());
				textOthersval.setPadding(10, 10, 10, 10);
				textOthersval.setTextColor(getResources().getColor(R.color.black));
				// textcontractions.setTextSize(15);
				textOthersval.setGravity(Gravity.LEFT);
				tr.addView(textOthersval);

				TextView textpallorval = new TextView(this);
				textpallorval.setTextSize(15);
				textpallorval.setText(values.get(j).getPallor());
				textpallorval.setPadding(10, 10, 10, 10);
				textpallorval.setTextColor(getResources().getColor(R.color.black));
				// textFHS.setTextSize(15);
				textpallorval.setGravity(Gravity.LEFT);
				tr.addView(textpallorval);

				TextView textpulseval = new TextView(this);
				textpulseval.setTextSize(15);
				textpulseval.setText("" + values.get(j).getPulse());
				textpulseval.setPadding(10, 10, 10, 10);
				textpulseval.setTextColor(getResources().getColor(R.color.black));
				// textPV.setTextSize(15);
				textpulseval.setGravity(Gravity.LEFT);
				tr.addView(textpulseval);

				TextView textbpval = new TextView(this);
				textbpval.setTextSize(15);
				textbpval.setText(values.get(j).getBp());
				textbpval.setPadding(10, 10, 10, 10);
				textbpval.setTextColor(getResources().getColor(R.color.black));
				// textPV.setTextSize(15);
				textbpval.setGravity(Gravity.LEFT);
				tr.addView(textbpval);

				TextView textbreastval = new TextView(this);
				textbreastval.setTextSize(15);
				textbreastval.setText(values.get(j).getBreastExamination());
				textbreastval.setPadding(10, 10, 10, 10);
				textbreastval.setTextColor(getResources().getColor(R.color.black));
				// textPV.setTextSize(15);
				textbreastval.setGravity(Gravity.LEFT);
				tr.addView(textbreastval);

				TextView textinvolutionval = new TextView(this);
				textinvolutionval.setTextSize(15);
				textinvolutionval.setText(values.get(j).getInvolution());
				textinvolutionval.setPadding(10, 10, 10, 10);
				textinvolutionval.setTextColor(getResources().getColor(R.color.black));
				// textPV.setTextSize(15);
				textinvolutionval.setGravity(Gravity.LEFT);
				tr.addView(textinvolutionval);

				TextView textlochiaval = new TextView(this);
				textlochiaval.setTextSize(15);
				textlochiaval.setText(values.get(j).getLochia());
				textlochiaval.setPadding(10, 10, 10, 10);
				textlochiaval.setTextColor(getResources().getColor(R.color.black));
				// textPV.setTextSize(15);
				textlochiaval.setGravity(Gravity.LEFT);
				tr.addView(textlochiaval);

				TextView textperinealcareval = new TextView(this);
				textperinealcareval.setTextSize(15);
				textperinealcareval.setText(values.get(j).getPerineal());
				textperinealcareval.setPadding(10, 10, 10, 10);
				textperinealcareval.setTextColor(getResources().getColor(R.color.black));
				// textPV.setTextSize(15);
				textperinealcareval.setGravity(Gravity.LEFT);
				tr.addView(textperinealcareval);

				TextView textAdviceval = new TextView(this);
				textAdviceval.setTextSize(15);
				textAdviceval.setText(values.get(j).getAdvice());
				textAdviceval.setPadding(10, 10, 10, 10);
				textAdviceval.setTextColor(getResources().getColor(R.color.black));
				// textAdvice.setTextSize(18);
				textAdviceval.setGravity(Gravity.LEFT);
				tr.addView(textAdviceval);

				String date = Partograph_CommonClass
						.getConvertedDateFormat(values.get(j).getDateTimeOfEntry().split("\\s+")[0], "dd-MM-yyyy") + "/"
						+ values.get(j).getDateTimeOfEntry().split("\\s+")[1];

				TextView textdatetimeval = new TextView(this);
				textdatetimeval.setTextSize(15);
				textdatetimeval.setPadding(10, 10, 10, 10);
				textdatetimeval.setTextColor(getResources().getColor(R.color.black));
				// textdatetimeval.setTextSize(18);
				textdatetimeval.setText(date);
				textdatetimeval.setGravity(Gravity.CENTER);
				tr.addView(textdatetimeval);

				View view3 = new View(this);
				view3.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 1));
				view3.setBackgroundColor(Color.BLACK);
				tbldata.addView(view3);

				if (j == 0) {
					tbldata.addView(trlabel);
					View view1 = new View(this);
					view1.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 1));
					view1.setBackgroundColor(Color.BLACK);
					tbldata.addView(view1);
				}

				tbldata.addView(tr);
			}

			/*
			 * * aq.id(R.id.etexamination).text(values.get(i).getExamination()
			 * ); aq.id(R.id.etpallor).text(values.get(i).getPallor());
			 * aq.id(R.id.etinvolution).text(values.get(i).getInvolution());
			 * aq.id(R.id.etadvicepost).text(values.get(i).getAdvice());
			 * aq.id(R.id.etbreastexamination).text(values.get(i).
			 * getBreastExamination());
			 * aq.id(R.id.etdatepost).text(values.get(i).getDateTimeOfEntry(
			 * ).split("/")[0]);
			 * aq.id(R.id.ettimepost).text(values.get(i).getDateTimeOfEntry(
			 * ).split("/")[1]); aq.id(R.id.etpulsepost).text("" +
			 * values.get(i).getPulse());
			 * aq.id(R.id.etbpsyspost).text(values.get(i).getBp().split("/")
			 * [0]);
			 * aq.id(R.id.etbpdiapost).text(values.get(i).getBp().split("/")
			 * [1]); aq.id(R.id.etlochia).text(values.get(i).getLochia());
			 * aq.id(R.id.etother).text(values.get(i).getOther());
			 * aq.id(R.id.etperinealcare).text(values.get(i).getPerineal()); }
			 */
			// }
		}

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		// TODO Auto-generated method stub
		try {
			switch (v.getId()) {

			case R.id.btnview:
				Intent i = new Intent(PostpartumCare_View_Activity.this, PostPartumCare_Activity.class);
				i.putExtra("woman", woman);
				startActivity(i);
				break;
			case R.id.imgpdfpost:
				Partograph_CommonClass.pdfAlertDialog(PostpartumCare_View_Activity.this, woman, "postpartum", null,
						null);
				break;

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}

	}

	public void commitTrans() throws Exception {
		// TODO Auto-generated method stub
		dbh.db.setTransactionSuccessful();
		dbh.db.endTransaction();

		// calSyncMtd();

	}

	/**
	 * This is a Recursive method that traverse through the group and subgroup
	 * of view Sets text, assigns clickListners
	 * 
	 * @throws Exception
	 */
	private ArrayList<View> initializeScreen(View v) throws Exception {
		if (!(v instanceof ViewGroup)) {
			ArrayList<View> viewArrayList = new ArrayList<View>();
			viewArrayList.add(v);

			// Imageview - assign ID
			if (v.getClass() == ImageView.class || v.getClass() == ImageButton.class) {
				aq.id(v.getId()).getImageView().setOnClickListener(this);
			}

			// Edittext
			if (v.getClass() == EditText.class) {
				// aq.id(v.getId()).getEditText().setOnClickListener(this);
			}

			// Button
			if (v.getClass() == Button.class) {
				aq.id(v.getId()).getButton().setOnClickListener(this);
			}

			if (v.getClass() == CheckBox.class) {
				aq.id(v.getId()).getCheckBox().setOnClickListener(this);
			}

			if (v.getClass() == RadioButton.class) {
				aq.id(v.getId()).getButton().setOnClickListener(this);
			}

			return viewArrayList;
		}
		if (v instanceof Spinner) {
			aq.id(v.getId()).itemSelected(this, "onSpinnerClicked");
		}

		if (v.getClass() == TableRow.class) {

		}

		ArrayList<View> result = new ArrayList<View>();

		ViewGroup viewGroup = (ViewGroup) v;
		for (int i = 0; i < viewGroup.getChildCount(); i++) {

			View child = viewGroup.getChildAt(i);

			ArrayList<View> viewArrayList = new ArrayList<View>();
			viewArrayList.add(v);
			viewArrayList.addAll(initializeScreen(child));

			result.addAll(viewArrayList);
		}
		return result;
	}

	boolean validateFields() throws NotFoundException, Exception {

		if (aq.id(R.id.etpulsepost).getText().toString().trim().length() <= 0) {

			Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.enter_pulse),
					PostpartumCare_View_Activity.this);
			return false;

		}

		if (aq.id(R.id.etbpsyspost).getText().toString().trim().length() <= 0
				&& aq.id(R.id.etbpdiapost).getText().toString().trim().length() <= 0) {
			Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.enter_bp),
					PostpartumCare_View_Activity.this);
			return false;

		}

		if (aq.id(R.id.etdatepost).getText().toString().trim().length() <= 0) {
			Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.enter_date),
					PostpartumCare_View_Activity.this);
			return false;

		}
		if (aq.id(R.id.ettimepost).getText().toString().trim().length() <= 0) {
			Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.enter_time),
					PostpartumCare_View_Activity.this);
			return false;

		}

		/*
		 * if (aq.id(R.id.etexamination).getText().toString().trim().length() <=
		 * 0) {
		 * 
		 * }
		 * 
		 * if (aq.id(R.id.etpallor).getText().toString().trim().length() <= 0) {
		 * 
		 * }
		 */

		/*
		 * if
		 * (aq.id(R.id.etbreastexamination).getText().toString().trim().length()
		 * <= 0) {
		 * 
		 * }
		 * 
		 * if (aq.id(R.id.etinvolution).getText().toString().trim().length() <=
		 * 0) {
		 * 
		 * }
		 * 
		 * if (aq.id(R.id.etlochia).getText().toString().trim().length() <= 0) {
		 * 
		 * } if (aq.id(R.id.etperinealcare).getText().toString().trim().length()
		 * <= 0) {
		 * 
		 * } if (aq.id(R.id.etadvicepost).getText().toString().trim().length()
		 * <= 0) {
		 * 
		 * }
		 */

		return true;

	}

	// Set options for admitted with - 9jan2016
	protected void setPresentingcomplaints() throws Exception {
		int i = 0;
		presentingcomplaintsMap = new LinkedHashMap<String, Integer>();
		List<String> complaints = null;

		complaints = Arrays.asList(getResources().getStringArray(R.array.presenting_complaints));

		if (complaints != null) {
			for (String str : complaints) {
				presentingcomplaintsMap.put(str, i);
				i++;
			}
			mspnpresentigcomplaints.setItems(complaints);
		}
	}

	public static void generatePostPartumCareDatainpdfFormat() {
		// TODO Auto-generated method stub

		Document doc = new Document(PageSize.A4);

		try {

			String path = AppContext.mainDir + "/PDF/PostPartumCare";

			File dir = new File(path);
			if (!dir.exists())
				dir.mkdirs();

			Log.d("PDFCreator", "PDF Path: " + path);

			file = new File(dir, woman.getWomen_name() + "_PostPartumCare.pdf");
			FileOutputStream fOut = new FileOutputStream(file);

			PdfWriter.getInstance(doc, fOut);

			doc.setMarginMirroring(false);

			doc.setMargins(0, 10, 15, 0);

			doc.open();

			Paragraph p_heading = new Paragraph(context.getResources().getString(R.string.postpartum_care), heading);

			p_heading.setAlignment(Paragraph.ALIGN_CENTER);

			doc.add(p_heading);

			Drawable d = context.getResources().getDrawable(R.drawable.bcicon_small);

			BitmapDrawable bitDw = ((BitmapDrawable) d);

			Bitmap bmp = bitDw.getBitmap();

			ByteArrayOutputStream stream = new ByteArrayOutputStream();

			bmp.compress(Bitmap.CompressFormat.PNG, 0, stream);

			Image image = Image.getInstance(stream.toByteArray());

			image.setIndentationLeft(10);
			doc.add(image);
			String strdatetime = Partograph_CommonClass.getConvertedDateFormat(Partograph_CommonClass.getTodaysDate(),
					Partograph_CommonClass.defdateformat) + " " + Partograph_CommonClass.getCurrentTime() + " ";
			Paragraph p1 = new Paragraph(strdatetime, small);
			p1.setAlignment(Paragraph.ALIGN_RIGHT);
			doc.add(p1);

			womanBasicInfo();

			Paragraph basicInfo = new Paragraph("Woman Basic Information", heading);
			basicInfo.setAlignment(Paragraph.ALIGN_LEFT);
			basicInfo.setIndentationLeft(30);
			doc.add(basicInfo);
			doc.add(womanbasics);

			PdfPTable womanAddress = new PdfPTable(1);
			womanAddress.setWidthPercentage(80);
			womanAddress.setSpacingBefore(1);
			womanAddress.addCell(
					getCellHeading(context.getResources().getString(R.string.address) + ": " + woman.getAddress(),
							PdfPCell.ALIGN_LEFT));

			doc.add(womanAddress);

			if (woman.getDel_type() != 0) {
				Paragraph deliveryInfo = new Paragraph("Woman Delivery Information", heading);
				deliveryInfo.setAlignment(Paragraph.ALIGN_LEFT);
				deliveryInfo.setIndentationLeft(30);
				doc.add(deliveryInfo);
				DeliveryInformation();

				doc.add(womandelInfo);

			}

			Paragraph dischargedetials = new Paragraph(
					context.getResources().getString(R.string.postpartum_care_details), heading);
			dischargedetials.setAlignment(Paragraph.ALIGN_LEFT);
			dischargedetials.setIndentationLeft(30);
			doc.add(dischargedetials);

			PdfPTable table = new PdfPTable(2);
			table.setWidthPercentage(90);
			table.setWidths(new int[] { 3, 10 });
			table.setSpacingBefore(10);

			PdfPCell cellTwo = new PdfPCell(getCellHeading(
					context.getResources().getString(R.string.presenting_complaints), Element.ALIGN_LEFT));

			PdfPCell cellOne = new PdfPCell(getCell(complaints, Element.ALIGN_LEFT));

			cellOne.setPadding(5);
			cellTwo.setPadding(5);
			cellTwo.setBorder(Rectangle.BOX);
			cellOne.setBorder(Rectangle.BOX);

			// cellOne.set
			table.addCell(cellTwo);
			table.addCell(cellOne);

			doc.add(table);

			PdfPTable womanInvestigation = new PdfPTable(2);

			womanInvestigation.setWidthPercentage(90);

			PdfPCell cell1 = new PdfPCell(
					getCellHeading(context.getResources().getString(R.string.examination), PdfPCell.CELL));

			PdfPCell cell2 = new PdfPCell(getCell(examination, PdfPCell.CELL));

			cell1.setPadding(5);
			cell2.setPadding(5);
			cell1.setBorder(Rectangle.BOX);
			cell2.setBorder(Rectangle.BOX);

			womanInvestigation.addCell(cell1);
			womanInvestigation.addCell(cell2);
			womanInvestigation.setWidths(new int[] { 3, 10 });

			doc.add(womanInvestigation);

			PdfPTable womanCondition = new PdfPTable(2);

			womanCondition.setWidthPercentage(90);

			PdfPCell womanConditioncell1 = new PdfPCell(
					getCellHeading(context.getResources().getString(R.string.pallor), PdfPCell.CELL));

			PdfPCell womanConditioncell2 = new PdfPCell(getCell(pallor, PdfPCell.CELL));

			womanConditioncell1.setPadding(5);
			womanConditioncell2.setPadding(5);
			womanConditioncell1.setBorder(Rectangle.BOX);
			womanConditioncell2.setBorder(Rectangle.BOX);

			womanCondition.addCell(womanConditioncell1);
			womanCondition.addCell(womanConditioncell2);

			womanCondition.setWidths(new int[] { 3, 10 });

			doc.add(womanCondition);

			PdfPTable table10 = new PdfPTable(2);
			table10.setWidthPercentage(90);

			PdfPCell table10cell1 = new PdfPCell(
					getCellHeading(context.getResources().getString(R.string.pulse), PdfPCell.CELL));

			PdfPCell table10cell2 = new PdfPCell(getCell(pulse, PdfPCell.CELL));

			table10cell1.setPadding(5);
			table10cell2.setPadding(5);
			table10cell1.setBorder(Rectangle.BOX);
			table10cell2.setBorder(Rectangle.BOX);

			table10.addCell(table10cell1);
			table10.addCell(table10cell2);
			table10.setWidths(new int[] { 3, 10 });
			doc.add(table10);

			PdfPTable table7 = new PdfPTable(2);
			table7.setWidthPercentage(90);

			PdfPCell table7cell1 = new PdfPCell(
					getCellHeading(context.getResources().getString(R.string.txtbpvalue), PdfPCell.CELL));

			PdfPCell table7cell2 = new PdfPCell(getCell(bp, PdfPCell.CELL));

			table7cell1.setPadding(5);
			table7cell2.setPadding(5);
			table7cell1.setBorder(Rectangle.BOX);
			table7cell2.setBorder(Rectangle.BOX);

			table7.setWidths(new int[] { 3, 10 });

			table7.addCell(table7cell1);
			table7.addCell(table7cell2);
			doc.add(table7);

			PdfPTable table1 = new PdfPTable(2);
			table1.setWidthPercentage(90);

			PdfPTable table9 = new PdfPTable(2);
			table9.setWidthPercentage(90);

			table1.setWidths(new int[] { 3, 10 });
			table9.setWidths(new int[] { 3, 10 });

			// String advice = "";

			PdfPCell table9cell1 = new PdfPCell(
					getCellHeading(context.getResources().getString(R.string.breast_examination), PdfPCell.CELL));

			PdfPCell table9cell2 = new PdfPCell(getCell(breastExamination, PdfPCell.CELL));

			table9cell1.setPadding(5);
			table9cell2.setPadding(5);
			table9cell1.setBorder(Rectangle.BOX);
			table9cell2.setBorder(Rectangle.BOX);

			table9.addCell(table9cell1);
			table9.addCell(table9cell2);
			doc.add(table9);

			PdfPCell table1cell1 = new PdfPCell(
					getCellHeading(context.getResources().getString(R.string.involution_of_uterus), PdfPCell.CELL));

			PdfPCell table1cell2 = new PdfPCell(getCell(involution, PdfPCell.CELL));

			table1cell1.setPadding(5);
			table1cell2.setPadding(5);
			table1cell1.setBorder(Rectangle.BOX);
			table1cell2.setBorder(Rectangle.BOX);

			table1.addCell(table1cell1);
			// table1.setWidths(new int[] { 5, 10 });
			table1.addCell(table1cell2);
			doc.add(table1);

			PdfPTable table11 = new PdfPTable(2);
			table11.setWidthPercentage(90);

			PdfPCell table11cell1 = new PdfPCell(
					getCellHeading(context.getResources().getString(R.string.lochia), PdfPCell.CELL));

			PdfPCell table11cell2 = new PdfPCell(getCell(lochia, PdfPCell.CELL));

			table11cell1.setPadding(5);
			table11cell2.setPadding(5);
			table11cell1.setBorder(Rectangle.BOX);
			table11cell2.setBorder(Rectangle.BOX);

			table11.setWidths(new int[] { 3, 10 });
			table11.addCell(table11cell1);
			table11.addCell(table11cell2);
			doc.add(table11);

			PdfPTable table2 = new PdfPTable(2);

			table2.setWidthPercentage(90);

			PdfPCell table2cell1 = new PdfPCell(
					getCellHeading(context.getResources().getString(R.string.perineal_care), PdfPCell.CELL));

			PdfPCell table2cell2 = new PdfPCell(getCell(perinealCare, PdfPCell.CELL));

			table2cell1.setPadding(5);
			table2cell2.setPadding(5);
			table2cell1.setBorder(Rectangle.BOX);
			table2cell2.setBorder(Rectangle.BOX);

			table2.addCell(table2cell1);
			table2.setWidths(new int[] { 3, 10 });
			table2.addCell(table2cell2);
			doc.add(table2);

			PdfPTable table12 = new PdfPTable(2);

			table12.setWidthPercentage(90);

			PdfPCell table12cell1 = new PdfPCell(
					getCellHeading(context.getResources().getString(R.string.advice), PdfPCell.CELL));

			PdfPCell table12cell2 = new PdfPCell(getCell(advice, PdfPCell.CELL));

			table12cell1.setPadding(5);
			table12cell2.setPadding(5);
			table12cell1.setBorder(Rectangle.BOX);
			table12cell2.setBorder(Rectangle.BOX);

			table12.setWidths(new int[] { 3, 10 });

			table12.addCell(table12cell1);
			table12.addCell(table12cell2);
			doc.add(table12);

			PdfPTable table8 = new PdfPTable(2);

			table8.setWidthPercentage(90);

			table8.setWidths(new int[] { 3, 10 });

			PdfPTable signatureofHC = new PdfPTable(1);
			signatureofHC.setWidthPercentage(90);
			signatureofHC.setSpacingBefore(50);
			signatureofHC.addCell(
					getCellHeading(context.getResources().getString(R.string.name_sign_trainer), PdfPCell.ALIGN_RIGHT));

			doc.add(signatureofHC);

			Rectangle rect = new Rectangle(10, 20, 585, 808);
			rect.enableBorderSide(1);
			rect.enableBorderSide(1);
			rect.enableBorderSide(1);
			rect.enableBorderSide(1);
			rect.setBorder(2);
			rect.setBorder(Rectangle.BOX);
			rect.setBorderWidth(1);
			doc.add(rect);

			doc.close();
		} catch (Exception e) {
			AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ context.getClass().getSimpleName(), e);
			e.printStackTrace();
		}

	}

	static void womanBasicInfo() {
		try {
			String delstatus;
			ArrayList<String> val = new ArrayList<String>();
			val.add(context.getResources().getString(R.string.name) + ":" + woman.getWomen_name());
			val.add(context.getResources().getString(R.string.age) + ":" + woman.getAge()
					+ context.getResources().getString(R.string.yrs));
			if (woman.getregtype() != 2) {// 12Jan2017 Arpitha
				String strDOA = Partograph_CommonClass.getConvertedDateFormat(woman.getDate_of_admission(),
						Partograph_CommonClass.defdateformat);
				val.add(context.getResources().getString(R.string.doadm) + ":" + strDOA + "/"
						+ woman.getTime_of_admission());
			} else {// 12Jan2017 Arpitha
				val.add(context.getResources().getString(R.string.doadm) + ":" + woman.getDate_of_reg_entry());
			}
			if (woman.getGestationage() == 0)
				val.add(context.getResources().getString(R.string.gest_short_label) + " "
						+ context.getResources().getString(R.string.notknown));
			else
				val.add(woman.getGestationage() + context.getResources().getString(R.string.wks) + " "
						+ woman.getGest_age_days() + context.getResources().getString(R.string.days));

			womanbasics = new PdfPTable(val.size());

			womanbasics.setWidths(new int[] { 6, 5, 12, 11 });

			for (int k = 0; k < val.size(); k++) {
				String header = val.get(k);
				PdfPCell cell = new PdfPCell();
				cell.setPhrase(new Phrase(header, new Font(Font.HELVETICA, 10, Font.BOLD)));
				cell.setHorizontalAlignment(Element.ALIGN_LEFT);
				// cell.setPadding(2);
				cell.setBorder(0);

				womanbasics.addCell(cell);
			}

			womanbasics.setWidthPercentage(90);

			ArrayList<String> arrVal = new ArrayList<String>();
			arrVal.add(context.getResources().getString(R.string.gravida) + ":" + woman.getGravida());
			arrVal.add(context.getResources().getString(R.string.para) + ":" + woman.getPara());
			arrVal.add(context.getResources().getString(R.string.facility) + ": " + woman.getFacility_name() + "["
					+ woman.getFacility() + "]");
			if (woman.getDel_type() != 0) {
				delstatus = context.getResources().getString(R.string.delivered);
			} else
				delstatus = context.getResources().getString(R.string.del_prog);
			arrVal.add(delstatus);

			for (int k = 0; k < arrVal.size(); k++) {
				String header = arrVal.get(k);
				PdfPCell cell = new PdfPCell();
				cell.setPhrase(new Phrase(header, new Font(Font.HELVETICA, 10, Font.BOLD)));
				cell.setHorizontalAlignment(Element.ALIGN_LEFT);
				// cell.setPadding(2);
				cell.setBorder(0);

				womanbasics.addCell(cell);
			}

			womanbasics.setSpacingBefore(3);

			womanbasics.completeRow();

		} catch (Exception e) {
			AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ context.getClass().getSimpleName(), e);
			e.printStackTrace();
		}
	}

	public static PdfPCell getCellHeading(String text, int alignment) {
		PdfPCell cell = new PdfPCell(new Phrase(text, new Font(Font.HELVETICA, 10, Font.BOLD)));
		cell.setPadding(0);
		cell.setHorizontalAlignment(alignment);
		cell.setBorder(PdfPCell.NO_BORDER);
		return cell;
	}

	public static PdfPCell getCell(String text, int alignment) {
		PdfPCell cell = new PdfPCell(new Phrase(text, new Font(Font.HELVETICA, 10, Font.NORMAL)));
		cell.setPadding(0);
		cell.setHorizontalAlignment(alignment);
		cell.setBorder(PdfPCell.NO_BORDER);
		return cell;
	}

	static void DeliveryInformation() throws NotFoundException, Exception {

		if (woman.getDel_type() != 0) {
			ArrayList<String> deliveryInfo = new ArrayList<String>();
			String delType = "";
			if (woman.getDel_type() == 1)
				delType = "Normal";
			else if (woman.getDel_type() == 2)
				delType = "Cesarean";
			else if (woman.getDel_type() == 3)
				delType = "Instrumental";

			deliveryInfo.add(delType);

			if (woman.getDelTypeReason() != null) {
				String[] delTypeRes = woman.getDelTypeReason().split(",");
				String[] delTypeResArr = null;
				if (woman.getDel_type() == 2)
					delTypeResArr = context.getResources().getStringArray(R.array.spnreasoncesareanValues);
				else if (woman.getDel_type() == 3)
					delTypeResArr = context.getResources().getStringArray(R.array.spnreasoninstrumental);

				String delTypeResDisplay = "";
				if (delTypeResArr != null) {
					if (delTypeRes != null && delTypeRes.length > 0) {
						for (String str : delTypeRes) {
							if (str != null && str.length() > 0)
								delTypeResDisplay = delTypeResDisplay + delTypeResArr[Integer.parseInt(str)] + "\n";
						}
					}
				}

				deliveryInfo.add(delTypeResDisplay);
			}

			deliveryInfo.add(context.getResources().getString(R.string.dod) + ": " + Partograph_CommonClass
					.getConvertedDateFormat(woman.getDel_Date(), Partograph_CommonClass.defdateformat) + " "
					+ woman.getDel_Time());

			deliveryInfo.add(context.getResources().getString(R.string.noofchild) + ": " + "" + woman.getNo_of_child());

			womandelInfo = new PdfPTable(deliveryInfo.size());

			// womandelInfo.setWidths(new int[] { 7, 5, 11, 11 });

			for (int k = 0; k < deliveryInfo.size(); k++) {
				String header = deliveryInfo.get(k);
				PdfPCell cell = new PdfPCell();
				cell.setPhrase(new Phrase(header, new Font(Font.HELVETICA, 10, Font.BOLD)));
				cell.setHorizontalAlignment(Element.ALIGN_LEFT);
				// cell.setPadding(2);
				cell.setBorder(0);

				womandelInfo.addCell(cell);

				womandelInfo.setSpacingBefore(6);

				womandelInfo.setSpacingAfter(10);

				womandelInfo.setWidthPercentage(90);

			}
			womandelInfo.completeRow();

			// }

		}

	}

	void displayAlertDialog() {

		final Dialog dialog = new Dialog(PostpartumCare_View_Activity.this);

		dialog.setTitle(getResources().getString(R.string.afm_abrreivation));
		dialog.requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		dialog.setContentView(R.layout.dialog_action);
		dialog.getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.dialog_title);
		TextView txtdialogtitle = (TextView) dialog.findViewById(R.id.graph_title);
		txtdialogtitle.setText(getResources().getString(R.string.dischrage_slip));

		ImageButton imgbtncanceldialog = (ImageButton) dialog.findViewById(R.id.imgbtncanceldialog);
		imgbtncanceldialog.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				try {
					dialog.cancel();
				} catch (Exception e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					e.printStackTrace();
				}
			}
		});

		final RadioButton rdsavepdf = (RadioButton) dialog.findViewById(R.id.rdsave);
		final RadioButton rdviewpdf = (RadioButton) dialog.findViewById(R.id.rdview);
		Button btndone = (Button) dialog.findViewById(R.id.btndone);

		// final RadioButton rdshare = (RadioButton)
		// dialog.findViewById(R.id.rdshare);

		btndone.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (rdsavepdf.isChecked()) {
					try {
						dialog.cancel();
						PostpartumCare_View_Activity.generatePostPartumCareDatainpdfFormat();
						Toast.makeText(getApplicationContext(),
								getResources().getString(R.string.latent_phase_is_saved), Toast.LENGTH_LONG).show();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}
				} else if (rdviewpdf.isChecked())

				{
					try {
						dialog.cancel();
						PostpartumCare_View_Activity.generatePostPartumCareDatainpdfFormat();
						// to view pdf file from appliction
						if (file.exists()) {

							Uri pdfpath = Uri.fromFile(file);
							Intent intent = new Intent(Intent.ACTION_VIEW);
							intent.setDataAndType(pdfpath, "application/pdf");
							intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

							try {
								startActivity(intent);
							} catch (ActivityNotFoundException e) {// 24Jan2017
																	// Arpitha
								// No application to view, ask to download one
								AlertDialog.Builder builder = new AlertDialog.Builder(
										PostpartumCare_View_Activity.this);
								builder.setTitle("No Application Found");
								builder.setMessage("Download one from Android Market?");
								builder.setPositiveButton("Yes, Please", new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										Intent marketIntent = new Intent(Intent.ACTION_VIEW);
										marketIntent.setData(Uri.parse("market://details?id=com.adobe.reader"));
										startActivity(marketIntent);
									}
								});
								builder.setNegativeButton("No, Thanks", null);
								builder.create().show();
							}
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}
				} // 24Jan2017
				/*
				 * else if (rdshare.isChecked()) { SendviaBluetoth();
				 * 
				 * }
				 */

				else {
					Toast.makeText(getApplicationContext(), getResources().getString(R.string.please_select_one_option),
							Toast.LENGTH_LONG).show();
				}

			}

		});
		dialog.show();

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_womenlist, menu);

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		if (Partograph_CommonClass.isRecordExistsForSync)
			menu.findItem(R.id.sync).setIcon(getResources().getDrawable(R.drawable.ic_sync_red));
		else
			menu.findItem(R.id.sync).setIcon(getResources().getDrawable(R.drawable.ic_sync_green));

		if (!(AppContext.checkSimState() == TelephonyManager.SIM_STATE_READY)) {
			menu.findItem(R.id.sms).setVisible(false);
		}

		menu.findItem(R.id.email).setVisible(false);

		menu.findItem(R.id.registration).setVisible(false);

		menu.findItem(R.id.delete).setVisible(false);
		menu.findItem(R.id.expertcomments).setVisible(false);
		menu.findItem(R.id.viewpartograph).setVisible(false);
		menu.findItem(R.id.ic_msg).setVisible(false);
		menu.findItem(R.id.addcomments).setVisible(false);
		menu.findItem(R.id.viewprofile).setVisible(false);
		menu.findItem(R.id.sync).setVisible(false);
		menu.findItem(R.id.search).setVisible(false);

		// 03Oct2017 Arpitha
		menu.findItem(R.id.del).setVisible(true);
		menu.findItem(R.id.ref).setVisible(true);
		menu.findItem(R.id.pdf).setVisible(true);
		menu.findItem(R.id.stages).setVisible(true);
		menu.findItem(R.id.apgar).setVisible(true);
		// menu.findItem(R.id.view).setVisible(true);
		menu.findItem(R.id.lat).setVisible(true);
		menu.findItem(R.id.parto).setVisible(true);// 03Oct2017 Arpitha

		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		try {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
			switch (item.getItemId()) {
			// Respond to the action bar's Up/Home button
			case android.R.id.home:
				NavUtils.navigateUpFromSameTask(this);
				return true;

			case R.id.logout:
				displayConfirmationAlert_exit(getResources().getString(R.string.exit_msg),
						getResources().getString(R.string.logout));
				return true;

			// case R.id.sync:
			// menuItem = item;
			// menuItem.setActionView(R.layout.progressbar);
			// menuItem.expandActionView();
			// calSyncMtd();
			// return true;

			case R.id.settings:
				if (Partograph_CommonClass.autodatetime(PostpartumCare_View_Activity.this)) {
					Intent settings = new Intent(this, Settings_parto.class);
					startActivity(settings);
				}
				return true;

			case R.id.about:

				Intent about = new Intent(this, About.class);
				startActivity(about);
				return true;

			case R.id.info:
				Intent i = new Intent(getApplicationContext(), GraphInformation.class);
				startActivity(i);
				return true;

			case R.id.sms:
				if (AppContext.checkSimState() == TelephonyManager.SIM_STATE_READY) {
					Partograph_CommonClass.display_messagedialog(PostpartumCare_View_Activity.this, woman.getUserId());
				} else
					Toast.makeText(getApplicationContext(), getResources().getString(R.string.no_sim),
							Toast.LENGTH_LONG).show();
				return true;

			case R.id.comments:
				Intent comments = new Intent(getApplicationContext(), Comments_Activity.class);
				startActivity(comments);
				return true;

			case R.id.home:
				displayConfirmationAlert_exit(getResources().getString(R.string.exit_msg),
						getResources().getString(R.string.home));
				return true;
			// 22May2017 Arpitha - v2.6
			case R.id.summary:
				Intent summary = new Intent(getApplicationContext(), Summary_Activity.class);
				startActivity(summary);
				return true;
			case R.id.usermanual:
				Intent manual = new Intent(getApplicationContext(), UseManual.class);
				startActivity(manual);
				return true;
			case R.id.changepass:
				Partograph_CommonClass.dispalyChangePasswordDialog(this);
				return true;

			case R.id.disch:
				Intent disch = new Intent(PostpartumCare_View_Activity.this, DischargedWomanList_Activity.class);
				startActivity(disch);
				return true;

			// 03Oct2017 Arpitha
			case R.id.del:
				// Intent disc = new Intent(PostpartumCare_View_Activity.this,
				// DeliveryInfo_Activity.class);
				// disc.putExtra("woman", woman);
				// startActivity(disc);

				if (woman.getDel_type() != 0)// 21Oct2016
												// Arpitha
				{// 21Oct2016 Arpitha
					// 05Jan2016
					// / showDialogToUpdateStatus();
					Partograph_CommonClass.showDialogToUpdateStatus(PostpartumCare_View_Activity.this, woman);
					// loadWomendata(displayListCountddel);
					// if(adapter!=null)
					// adapter.notifyDataSetChanged();
					// loadWomendata(displayListCountddel);
				} else// 21Oct2016 Arpitha
				{

					Intent graph = new Intent(PostpartumCare_View_Activity.this, DeliveryInfo_Activity.class);
					graph.putExtra("woman", woman);
					startActivity(graph);
				} // 21Oct2016 Arpitha
				return true;
			case R.id.viewprofile:
				Intent view = new Intent(PostpartumCare_View_Activity.this, ViewProfile_Activity.class);
				view.putExtra("woman", woman);
				startActivity(view);
				return true;

			case R.id.ref:
				// Intent ref = new Intent(PostpartumCare_View_Activity.this,
				// ReferralInfo_Activity.class);
				// ref.putExtra("woman", woman);
				// startActivity(ref);
				Cursor cur = dbh.getReferralDetails(woman.getUserId(), woman.getWomenId());
				if (cur != null && cur.getCount() > 0) {
					Partograph_CommonClass.showDialogToUpdateReferral(PostpartumCare_View_Activity.this, woman);
				} else {
					Intent ref = new Intent(PostpartumCare_View_Activity.this, ReferralInfo_Activity.class);
					ref.putExtra("woman", woman);
					startActivity(ref);
				}
				return true;

			case R.id.pdf:
				Partograph_CommonClass.exportPDFs(PostpartumCare_View_Activity.this, woman);
				return true;

			case R.id.disc:
				Intent discharge = new Intent(PostpartumCare_View_Activity.this, DiscargeDetails_Activity.class);
				discharge.putExtra("woman", woman);
				startActivity(discharge);
				return true;

			case R.id.apgar:
				if (woman.getDel_type() != 0) {
					Intent apgar = new Intent(PostpartumCare_View_Activity.this, Activity_Apgar.class);
					apgar.putExtra("woman", woman);
					startActivity(apgar);
				} else
					Toast.makeText(getApplicationContext(),
							getResources().getString(R.string.msg_applicableafterdelivery), Toast.LENGTH_LONG).show();
				return true;

			case R.id.stages:
				if (woman.getDel_type() != 0) {
					Intent stages = new Intent(PostpartumCare_View_Activity.this, StageofLabor.class);
					stages.putExtra("woman", woman);
					startActivity(stages);
				} else
					Toast.makeText(getApplicationContext(),
							getResources().getString(R.string.msg_applicableafterdelivery), Toast.LENGTH_LONG).show();
				return true;

			case R.id.post:
				if (woman.getDel_type() != 0) {
					Intent post = new Intent(PostpartumCare_View_Activity.this, PostPartumCare_Activity.class);
					post.putExtra("woman", woman);
					startActivity(post);
				} else
					Toast.makeText(getApplicationContext(),
							getResources().getString(R.string.msg_applicableafterdelivery), Toast.LENGTH_LONG).show();
				return true;

			case R.id.lat:
				Intent lat = new Intent(PostpartumCare_View_Activity.this, LatentPhase_Activity.class);
				lat.putExtra("woman", woman);
				startActivity(lat);
				return true;

			case R.id.parto:
				Intent parto = new Intent(PostpartumCare_View_Activity.this, SlidingActivity.class);
				parto.putExtra("woman", woman);
				startActivity(parto);
				return true;// 03Oct2017 Arpitha

			}
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
		return super.onOptionsItemSelected(item);
	}

	/** This method Calls the idle timeout */
	@Override
	public void onUserInteraction() {
		super.onUserInteraction();
		KillAllActivitesAndGoToLogin
				.delayedIdle(Integer.parseInt(Partograph_CommonClass.properties.getProperty("idleTimeOut")));
	}

	@Override
	public void onBackPressed() {
		try {
			displayConfirmationAlert_exit(getResources().getString(R.string.exit_msg),
					getResources().getString(R.string.home));
		} catch (NotFoundException e) {
			// TODO Auto-generated catch block
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
	}

	private void displayConfirmationAlert_exit(String exit_msg, final String classname) throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		final Dialog dialog = new Dialog(PostpartumCare_View_Activity.this);
		dialog.setTitle(Html.fromHtml("<font color='" + getResources().getColor(R.color.appcolor) + "'>"
				+ getResources().getString(R.string.alert) + "</font>"));
		dialog.setContentView(R.layout.temp_alertdialog);

		dialog.show();

		final TextView txtdialog = (TextView) dialog.findViewById(R.id.txtpulse);
		Button imgbtnyes = (Button) dialog.findViewById(R.id.imgbtnyes);
		Button imgbtnno = (Button) dialog.findViewById(R.id.imgbtnno);
		// Arpitha 27may16
		final TextView txtdialog1 = (TextView) dialog.findViewById(R.id.txtpulseval);

		final TextView txtdialog6 = (TextView) dialog.findViewById(R.id.txt);

		txtdialog1.setVisibility(View.GONE);
		txtdialog.setVisibility(View.GONE);
		txtdialog6.setText(exit_msg);
		imgbtnyes.setText(getResources().getString(R.string.yes));
		imgbtnno.setText(getResources().getString(R.string.no));

		imgbtnyes.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				dialog.cancel();

				if (classname.equalsIgnoreCase(getResources().getString(R.string.home))) {
					Intent i = new Intent(PostpartumCare_View_Activity.this, Activity_WomenView.class);
					startActivity(i);
				}

				if (classname.equalsIgnoreCase(getResources().getString(R.string.logout))) {

					Intent i = new Intent(PostpartumCare_View_Activity.this, LoginActivity.class);
					startActivity(i);

				}

			}
		});

		imgbtnno.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.cancel();

			}
		});

		int dividerId = dialog.getContext().getResources().getIdentifier("android:id/titleDivider", null, null);
		View divider = dialog.findViewById(dividerId);
		divider.setBackgroundColor(getResources().getColor(R.color.appcolor));// 08Feb2017

		dialog.show();

		dialog.setCancelable(false);

	}

}
