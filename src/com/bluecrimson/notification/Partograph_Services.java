package com.bluecrimson.notification;

import java.util.Calendar;

import com.bc.partograph.login.LoginActivity;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;
import android.widget.Toast;

public class Partograph_Services extends Service {

	public Runnable mRunnable = null;

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("Not yet implemented");
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		final Handler mHandler = new Handler();
		mRunnable = new Runnable() {
			@Override
			public void run() {
				// Partograph_DB myDBHelper = new
				// Partograph_DB(getApplicationContext());

				LoginActivity login = new LoginActivity();

				try {
					// Toast.makeText(getApplicationContext(), "Parto",
					// Toast.LENGTH_SHORT).show();

				//	Toast.makeText(getApplicationContext(), "Hiiiii", Toast.LENGTH_LONG).show();

					notification();

					// login.notification();

					// Toast.makeText(getApplicationContext(), "Hi",
					// Toast.LENGTH_LONG).show();
					/// NotificationClass.updatetblnotification();

				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				mHandler.postDelayed(mRunnable, 10 * 8000);
			}
		};
		mHandler.postDelayed(mRunnable, 10 * 8000);

		return super.onStartCommand(intent, flags, startId);
		// return START_STICKY;
	}

	public void notification() {

		AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

		Intent notificationIntent = new Intent("android.media.action.DISPLAY_NOTIFICATION");
		notificationIntent.addCategory("android.intent.category.DEFAULT");

		PendingIntent broadcast = PendingIntent.getBroadcast(this, 100, notificationIntent,
				PendingIntent.FLAG_UPDATE_CURRENT);

		// Date futureDate = new Date(new Date().getTime() + 86400000);

		// futureDate.setHours(17);
		// futureDate.setMinutes(50);
		// futureDate.setSeconds(0);

		Calendar cal = Calendar.getInstance();
		// cal.set(2016, 6, 16, 18, 0, 0);
		cal.add(Calendar.SECOND, 10);
		// cal.setTimeInMillis(System.currentTimeMillis());
		// cal.set(Calendar.HOUR_OF_DAY, 14);
		alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), 0, broadcast);

	}

	@Override
	public void onTaskRemoved(Intent rootIntent) { // TODO
		// Auto-generated method stub
		Intent restartService = new Intent(getApplicationContext(), this.getClass());
		restartService.setPackage(getPackageName());
		PendingIntent restartServicePI = PendingIntent.getService(getApplicationContext(), 1, restartService,
				PendingIntent.FLAG_ONE_SHOT);
		AlarmManager alarmService = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
		alarmService.set(AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime() + 1000, restartServicePI);
		notification();

	}

	@Override
	public void onDestroy() {
		startService(new Intent(getApplicationContext(), Partograph_Services.class));
	}

}

/*
 * package com.bluecrimson.notification;
 * 
 * import java.io.File; import java.util.Calendar;
 * 
 * import com.bc.partograph.common.AppContext;
 * 
 * import android.app.AlarmManager; import android.app.PendingIntent; import
 * android.app.Service; import android.content.Context; import
 * android.content.Intent; import android.database.sqlite.SQLiteDatabase; import
 * android.os.Environment; import android.os.Handler; import android.os.IBinder;
 * 
 * public class Partograph_Services extends Service {
 * 
 * public Runnable mRunnable = null;
 * 
 * @Override public IBinder onBind(Intent intent) { // TODO Auto-generated
 * method stub throw new UnsupportedOperationException("Not yet implemented"); }
 * 
 * @Override public int onStartCommand(Intent intent, int flags, int startId) {
 * final Handler mHandler = new Handler(); mRunnable = new Runnable() {
 * 
 * @Override public void run() { try {
 * 
 * // String dbpath = Environment.getExternalStorageDirectory() +
 * "/Partograph/DataBase/partograph.db"; // // File f = new File(dbpath); // if
 * (f.exists()) { // SQLiteDatabase db =
 * SQLiteDatabase.openDatabase(AppContext.dbDirRef + "/partograph.db", null, //
 * SQLiteDatabase.OPEN_READWRITE); // if (db != null)// 18May2017 Arpitha - v2.6
 * notification(); // }
 * 
 * } catch (Exception e) { // TODO Auto-generated catch block
 * e.printStackTrace(); }
 * 
 * mHandler.postDelayed(mRunnable, 10 * 1000); } };
 * mHandler.postDelayed(mRunnable, 10 * 1000);
 * 
 * // return super.onStartCommand(intent, flags, startId); return START_STICKY;
 * }
 * 
 * public void notification() { AlarmManager alarmManager = (AlarmManager)
 * getSystemService(Context.ALARM_SERVICE);
 * 
 * Intent notificationIntent = new
 * Intent("android.media.action.DISPLAY_NOTIFICATION");
 * notificationIntent.addCategory("android.intent.category.DEFAULT");
 * 
 * PendingIntent broadcast = PendingIntent.getBroadcast(this, 100,
 * notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);
 * 
 * Calendar cal = Calendar.getInstance(); cal.add(Calendar.SECOND, 10);
 * alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP,
 * cal.getTimeInMillis(), 0, broadcast);
 * 
 * }
 * 
 * // 27April2017 Arpitha
 * 
 * @Override public void onDestroy() { // Intent intent = new
 * Intent("restartApps"); // sendBroadcast(intent); super.onDestroy(); //
 * stopThread(); }
 * 
 * @Override public void onTaskRemoved(Intent rootIntent) { // TODO
 * Auto-generated method stub stopSelf();
 * 
 * }
 * 
 * }
 */