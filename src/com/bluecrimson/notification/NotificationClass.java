package com.bluecrimson.notification;

import java.util.ArrayList;

import com.bc.partograph.common.AppContext;
import com.bc.partograph.common.KillAllActivitesAndGoToLogin;
import com.bc.partograph.common.Partograph_CommonClass;
import com.bc.partograph.login.LoginActivity;
import com.bluecrimson.partograph.R;
import com.bluecrimson.partograph.UserPojo;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.text.Html;
import android.widget.Toast;

public class NotificationClass extends BroadcastReceiver {

	Notification_Pojo npojo;
	ArrayList<Notification_Pojo> nitems;
	String strlastdate;
	UserPojo upojo;
	String wname = "";
	Notification notification;
	String notificationcontent = "";
	Context context;

	String objectname = " ";

	String notifyMessage = " ";
	ArrayList<String> womenName = new ArrayList<String>();
	ArrayList<String> message = new ArrayList<String>();
	boolean isNotify = false;
	int duration;

	// String notificationOn;

	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub

		try {

			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
			upojo = new UserPojo();
			// Toast.makeText(context, "Sentoiagraph",
			// Toast.LENGTH_SHORT).show();

			this.context = context;
			//Toast.makeText(context, "Recv", Toast.LENGTH_LONG).show();
			getdata();

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		} //KillAllActivitesAndGoToLogin.addToStack(this);

	}

	private void updatenotified(String wid, int objectid) throws Exception {
		// TODO Auto-generated method stub
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		SQLiteDatabase db = SQLiteDatabase.openDatabase(AppContext.dbDirRef + "/partograph.db", null,
				SQLiteDatabase.OPEN_READWRITE);
		String columnname;
		if (objectid == 1) {
			columnname = "fhr";
		} else if (objectid == 3) {
			columnname = "dilatation";
		} else if (objectid == 4) {
			columnname = "contraction";
		} else
			columnname = "pulse_bp";

		String sql = "Update tbl_notification set " + columnname + "=2 where women_id='" + wid + "'";
		db.execSQL(sql);

	}

	private void updatetblnotification(String s, int i) throws Exception {
		// TODO Auto-generated method stub
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		SQLiteDatabase db = SQLiteDatabase.openDatabase(AppContext.dbDirRef + "/partograph.db", null,
				SQLiteDatabase.OPEN_READWRITE);
//		Toast.makeText(context, "update", Toast.LENGTH_LONG).show();
		String columnname;
		if (i == 1) {
			columnname = "fhr";
		} else if (i == 3) {
			columnname = "dilatation";
		} else if (i == 4) {
			columnname = "contraction";
		} else
			columnname = "pulse_bp";

		String sql = "Update tbl_notification set " + columnname + "=2 where women_id='" + s + "'";
		db.execSQL(sql);

//		Toast.makeText(context, "Data updated", Toast.LENGTH_LONG).show();

	}

	private String lastvaldatetime(String s, int i) throws Exception {
		// TODO Auto-generated method stub
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		SQLiteDatabase db = SQLiteDatabase.openDatabase(AppContext.dbDirRef + "/partograph.db", null,
				SQLiteDatabase.OPEN_READONLY);
		String date = null;
		String sql = "select max(date_of_entry) || '_' || time_of_entry from tbl_propertyvalues where object_id<=9"
				+ " and women_id = '" + s + "' and object_id=" + i
				+ " Group by trans_id order by trans_id desc LIMIT 1";
		Cursor cursor = db.rawQuery(sql, null);
		cursor.moveToFirst();
		if (cursor.getCount() > 0)

		{
			date = cursor.getString(0);

		}
		return date;
	}

	public String lastvaltime(String womenid, int object_id, String strlastdate2) throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		String str = null;
		String sql = "Select max(time_of_entry) from tbl_propertyvalues where object_id = '" + object_id
				+ "' and women_id='" + womenid + "' and date_of_entry='" + strlastdate2
				+ "' order by trans_id desc LIMIT 1";
		SQLiteDatabase s = SQLiteDatabase.openDatabase(AppContext.dbDirRef + "/partograph.db", null,
				SQLiteDatabase.OPEN_READONLY);
		Cursor res = s.rawQuery(sql, null);
		res.moveToFirst();
		if (res.getCount() > 0) {

			str = res.getString(0);

		}
		return str;
	}

	public ArrayList<String> getwomenid() throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		ArrayList<String> womenid = new ArrayList<String>();
		SQLiteDatabase s = SQLiteDatabase.openDatabase(AppContext.dbDirRef + "/partograph.db", null,
				SQLiteDatabase.OPEN_READONLY);
		String sql;

		sql = "Select women_id from tbl_notification";
		Cursor cur = s.rawQuery(sql, null);
		cur.moveToFirst();
		if (cur.getCount() > 0) {
			do {
				String id = cur.getString(0);
				womenid.add(id);
			} while (cur.moveToNext());
		}
		return womenid;
	}

	public String lastvaldate(String womenid, int object_id) throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		String str = null;
		String sql = "Select max(date_of_entry)  from tbl_propertyvalues where object_id = '" + object_id
				+ "' and women_id='" + womenid + "' order by trans_id desc LIMIT 1";
		SQLiteDatabase s = SQLiteDatabase.openDatabase(AppContext.dbDirRef + "/partograph.db", null,
				SQLiteDatabase.OPEN_READONLY);
		Cursor res = s.rawQuery(sql, null);
		res.moveToFirst();
		if (res.getCount() > 0) {

			str = res.getString(0);

		}
		return str;
	}

	private ArrayList<String> getnotificationdata() throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		ArrayList<String> wid = new ArrayList<String>();
		String sql = "Select * from tbl_notification where fhr=1 or dilatation=1 or contraction = 1 or pulse_bp=1";
		SQLiteDatabase s = SQLiteDatabase.openDatabase(AppContext.dbDirRef + "/partograph.db", null,
				SQLiteDatabase.OPEN_READONLY);
		Cursor cursor = s.rawQuery(sql, null);
		cursor.moveToFirst();
		if (cursor.getCount() > 0) {

			do {
				String womenid = cursor.getString(1);
				wid.add(womenid);
			} while (cursor.moveToNext());

		}
		return wid;
	}

	private boolean isnotify(String women_id, int objectid) throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		boolean isnotify = false;
		String columnname = null;
		if (objectid == 1) {
			columnname = "fhr=2";
		} else if (objectid == 3) {
			columnname = "dilatation=2";
		} else if (objectid == 4) {
			columnname = "contraction=2";
		} else if (objectid == 7) {
			columnname = "pulse_bp=2";
		}
		String sql = "Select * from tbl_notification where " + columnname + " and women_id='" + women_id + "'";
		SQLiteDatabase s = SQLiteDatabase.openDatabase(AppContext.dbDirRef + "/partograph.db", null,
				SQLiteDatabase.OPEN_READONLY);
		Cursor cursor = s.rawQuery(sql, null);
		cursor.moveToFirst();
		if (cursor.getCount() > 0) {
			isnotify = true;
		}
		return isnotify;
	}

	void getdata() throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		ArrayList<String> womenid = getwomenid();
		isNotify = false;
		for (String s : womenid) {

			//Toast.makeText(context, "getdata", Toast.LENGTH_LONG).show();

			// wname = getwname(s);
			// 03Jan2016 Arpitha

			String currdatetime = Partograph_CommonClass.getTodaysDate() + "_"
					+ Partograph_CommonClass.getCurrentTime();

			int[] objectid = { 1, 3, 4, 7 };
			objectname = " ";
			for (int i = 0; i < objectid.length; i++) {
				strlastdate = lastvaldatetime(s, objectid[i]);
				if (strlastdate != null) {
					if (i == 1)
						duration = 65;
					else
						duration = 35;
					boolean isvalidtime = Partograph_CommonClass.getisValidTime(strlastdate, currdatetime, duration);
					if (isvalidtime) {

						//Toast.makeText(context, "validtime", Toast.LENGTH_LONG).show();

						// updatenotified(s, objectid[i]);

						if (i == 0) {
							objectname = objectname + " " + "FHR";
						} else if (i == 1) {
							objectname = objectname + " " + "Dilatation";
						} else if (i == 2) {
							objectname = objectname + " " + "Contrcation";
						} else if (i == 3)
							objectname = objectname + " " + "Pulse & BP";

						boolean isnotify = isnotify(s, objectid[i]);

						if (!isnotify) {
							//Toast.makeText(context, "notify", Toast.LENGTH_LONG).show();

							/*
							 * if (i == 0) { objectname = objectname + " " +
							 * "FHR"; } else if (i == 1) { objectname =
							 * objectname + " " + "Dilatation"; } else if (i ==
							 * 2) { objectname = objectname + " " +
							 * "Contrcation"; } else if (i == 3) objectname =
							 * objectname + " " + "Pulse & BP";
							 */
							isNotify = true;

							// displaynotification();
							updatetblnotification(s, objectid[i]);

						}
					}
				}

			}
			if (objectname.trim().length() > 0) {
				womenName.add(getwname(s));
				message.add(objectname);
				//.Toast.makeText(context, "object", Toast.LENGTH_LONG).show();
			}

		}
		if (isNotify) {
			//Toast.makeText(context, "notification", Toast.LENGTH_LONG).show();
			displaynotification();
		}

	}

	String getwname(String wid) throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		SQLiteDatabase s = SQLiteDatabase.openDatabase(AppContext.dbDirRef + "/partograph.db", null,
				SQLiteDatabase.OPEN_READONLY);
		String wname = null;
		String sql = "Select women_name from tbl_registeredwomen where women_id='" + wid + "'";
		Cursor cursor = s.rawQuery(sql, null);
		cursor.moveToFirst();
		if (cursor.getCount() > 0) {
			wname = cursor.getString(0);
		}
		return wname;

	}

	/*
	 * private ArrayList<Notification_Pojo> getnotificationdata1() throws
	 * Exception { AppContext.addToTrace( new
	 * RuntimeException().getStackTrace()[0].getMethodName() + " - " +
	 * this.getClass().getSimpleName()); ArrayList<Notification_Pojo> val = new
	 * ArrayList<Notification_Pojo>(); Notification_Pojo npojo;
	 * ArrayList<String> wid = new ArrayList<String>(); String sql =
	 * "Select * from tbl_notification where fhr=1 or dilatation=1 or contraction = 1 or pulse_bp=1"
	 * ; SQLiteDatabase s = SQLiteDatabase.openDatabase(AppContext.dbDirRef +
	 * "/partograph.db", null, SQLiteDatabase.OPEN_READONLY); Cursor cursor =
	 * s.rawQuery(sql, null); cursor.moveToFirst(); if (cursor.getCount() > 0) {
	 * 
	 * do { // String womenid = cursor.getString(1); // wid.add(womenid); npojo
	 * = new Notification_Pojo(); npojo.setUserid(cursor.getString(0));
	 * npojo.setWomenid(cursor.getString(1)); npojo.setFhr(cursor.getInt(2));
	 * npojo.setDilatation(cursor.getInt(3));
	 * npojo.setContraction(cursor.getInt(4));
	 * npojo.setPulse_bp(cursor.getInt(5)); val.add(npojo); } while
	 * (cursor.moveToNext());
	 * 
	 * } return val; }
	 */

	void displaynotification() {
		// notificationOn =
		// Partograph_CommonClass.prefs.getString("notification", null);
		// if(notificationOn)
		notificationcontent = "";
		Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

		TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);

		nitems = new ArrayList<Notification_Pojo>();

		npojo = new Notification_Pojo();
		npojo.setWomenid("");
		npojo.setObjectid(1);

		nitems.add(npojo);

		// notificationcontent = objectname + " " + notificationcontent + "
		// value not Updated For " + wname;

		for (int i = 0; i < message.size(); i++) {
			notificationcontent = notificationcontent + " " + womenName.get(i) + ":-" + message.get(i)
					+ " value not updated\n";
		}

		//Toast.makeText(context, "dispalynoti", Toast.LENGTH_LONG).show();

		Intent notificationIntent = new Intent(context, LoginActivity.class);
		notificationIntent.putExtra("nitems", nitems);
		notificationIntent.putExtra("pos", 0);
		stackBuilder.addParentStack(LoginActivity.class);
		stackBuilder.addNextIntent(notificationIntent);

	//	Toast.makeText(context, "builder", Toast.LENGTH_LONG).show();

		PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

		NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
		notification = builder
				// .setContentTitle("Partograph")
				.setContentTitle(Html.fromHtml("<font color='" + context.getResources().getColor(R.color.appcolor)
						+ "'><b> Partograph</b></font>"))

				.setStyle(new NotificationCompat.BigTextStyle().bigText(notificationcontent))
				.setContentText(Html.fromHtml("<font color='" + context.getResources().getColor(R.color.brown)
						+ "'><b> " + notificationcontent + "</b></font>"))
				.setTicker("Partograph Alert!").setSmallIcon(R.drawable.notifi_icon).setNumber(message.size())
				.setSound(soundUri)

				.setContentIntent(pendingIntent).build();

		// builder.setContentTitle(Html.fromHtml(
		// "<font color='" + context.getResources().getColor(R.color.appcolor) +
		// "'><b> Partograph</b></font>"));

		/*
		 * builder.setContentTitle(Html.fromHtml( "<font color='" +
		 * context.getResources().getColor(R.color.appcolor) +
		 * "'><b> Partograph</b></font>"));
		 * 
		 * builder.setSmallIcon(R.drawable.notifi_icon);
		 * 
		 * builder.setContentText(Html.fromHtml("<font color='" +
		 * context.getResources().getColor(R.color.appcolor) + "'><b> " +
		 * notificationcontent + "</b></font>"));
		 */

		notification.contentIntent = pendingIntent;
		notification.flags |= Notification.FLAG_AUTO_CANCEL;

		// notification.flags |= Notification.FLAG_NO_CLEAR;

		NotificationManager notificationManager = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.notify(4, notification);

		//Toast.makeText(context, "sds", Toast.LENGTH_LONG).show();

	}

}

/*
 * package com.bluecrimson.notification;
 * 
 * import java.io.File; import java.util.ArrayList;
 * 
 * import com.bc.partograph.common.Partograph_CommonClass; import
 * com.bc.partograph.login.LoginActivity; import com.bluecrimson.partograph.R;
 * import com.bluecrimson.partograph.UserPojo;
 * 
 * import android.app.Notification; import android.app.NotificationManager;
 * import android.app.PendingIntent; import android.app.TaskStackBuilder; import
 * android.content.BroadcastReceiver; import android.content.Context; import
 * android.content.Intent; import android.database.Cursor; import
 * android.database.sqlite.SQLiteDatabase; import android.media.RingtoneManager;
 * import android.net.Uri; import android.os.Environment; import
 * android.support.v4.app.NotificationCompat; import android.text.Html; import
 * android.util.Log; import android.widget.Toast;
 * 
 * public class NotificationClass extends BroadcastReceiver {
 * 
 * Notification_Pojo npojo; ArrayList<Notification_Pojo> nitems; String
 * strlastdate; UserPojo upojo; String wname = ""; Notification notification;
 * String notificationcontent = ""; Context context; String objectname = " ";
 * String notifyMessage = " "; ArrayList<String> womenName = new
 * ArrayList<String>(); ArrayList<String> message = new ArrayList<String>();
 * boolean isNotify = false; int duration;
 * 
 * private static final String TAG = "RestartService";// 27April2017 Arpitha
 * 
 * // String notificationOn;
 * 
 * @Override public void onReceive(Context context, Intent intent) { // TODO
 * Auto-generated method stub
 * 
 * try { // AppContext.addToTrace(new //
 * RuntimeException().getStackTrace()[0].getMethodName() + " - " // +
 * this.getClass().getSimpleName()); Toast.makeText(context, "Sentoiagraph",
 * Toast.LENGTH_SHORT).show(); upojo = new UserPojo(); this.context = context;
 * // String fpath = Environment.getExternalStorageDirectory() + //
 * "/Partograph"; // File f = new File(fpath); // if(!f.isDirectory()) //
 * context.stopService(new Intent(context, // Partograph_Services.class));
 * 
 * getdata();
 * 
 * Log.e(TAG, "onReceive");// 27April2017 Arpitha context.startService(new
 * Intent(context, Partograph_Services.class));// 27April2017 // Arpitha
 * 
 * } catch (Exception e) { // AppContext.addLog( // new
 * RuntimeException().getStackTrace()[0].getMethodName() + " - " // +
 * this.getClass().getSimpleName(), // e); e.printStackTrace(); }
 * 
 * }
 * 
 * 
 * private void updatenotified(String wid, int objectid) throws Exception { //
 * TODO Auto-generated method stub AppContext.addToTrace( new
 * RuntimeException().getStackTrace()[0].getMethodName() + " - " +
 * this.getClass().getSimpleName()); SQLiteDatabase db =
 * SQLiteDatabase.openDatabase(AppContext.dbDirRef + "/partograph.db", null,
 * SQLiteDatabase.OPEN_READWRITE); String columnname; if (objectid == 1) {
 * columnname = "fhr"; } else if (objectid == 3) { columnname = "dilatation"; }
 * else if (objectid == 4) { columnname = "contraction"; } else columnname =
 * "pulse_bp";
 * 
 * String sql = "Update tbl_notification set " + columnname +
 * "=2 where women_id='" + wid + "'"; db.execSQL(sql);
 * 
 * }
 * 
 * private void updatetblnotification(String s, int i) throws Exception { //
 * TODO Auto-generated method stub // AppContext.addToTrace( // new
 * RuntimeException().getStackTrace()[0].getMethodName() + " - " + //
 * this.getClass().getSimpleName()); // String fpath =
 * Environment.getExternalStorageDirectory() + // "/Partograph"; // File file =
 * new File(fpath); // if (file.isDirectory()) { SQLiteDatabase db =
 * SQLiteDatabase.openDatabase( Environment.getExternalStorageDirectory() +
 * "/Partograph/DataBase" + "/partograph.db", null,
 * SQLiteDatabase.OPEN_READWRITE); String columnname; if (i == 1) { columnname =
 * "fhr"; } else if (i == 3) { columnname = "dilatation"; } else if (i == 4) {
 * columnname = "contraction"; } else columnname = "pulse_bp";
 * 
 * String sql = "Update tbl_notification set " + columnname +
 * "=2 where women_id='" + s + "'"; db.execSQL(sql); // }
 * 
 * }
 * 
 * private String lastvaldatetime(String s, int i) throws Exception { // TODO
 * Auto-generated method stub // AppContext.addToTrace( // new
 * RuntimeException().getStackTrace()[0].getMethodName() + " - " + //
 * this.getClass().getSimpleName()); String date = null; // String fpath =
 * Environment.getExternalStorageDirectory() + // "/Partograph"; // File file =
 * new File(fpath); // if (file.isDirectory()) { SQLiteDatabase db =
 * SQLiteDatabase.openDatabase( Environment.getExternalStorageDirectory() +
 * "/Partograph/DataBase" + "/partograph.db", null,
 * SQLiteDatabase.OPEN_READONLY);
 * 
 * String sql =
 * "select max(date_of_entry) || '_' || time_of_entry from tbl_propertyvalues where object_id<=9"
 * + " and women_id = '" + s + "' and object_id=" + i +
 * " Group by trans_id order by trans_id desc LIMIT 1"; Cursor cursor =
 * db.rawQuery(sql, null); cursor.moveToFirst(); if (cursor.getCount() > 0)
 * 
 * { date = cursor.getString(0);
 * 
 * } // } return date; }
 * 
 * public String lastvaltime(String womenid, int object_id, String strlastdate2)
 * throws Exception { // AppContext.addToTrace( // new
 * RuntimeException().getStackTrace()[0].getMethodName() + " - " + //
 * this.getClass().getSimpleName()); String str = null; String sql =
 * "Select max(time_of_entry) from tbl_propertyvalues where object_id = '" +
 * object_id + "' and women_id='" + womenid + "' and date_of_entry='" +
 * strlastdate2 + "' order by trans_id desc LIMIT 1"; // String fpath =
 * Environment.getExternalStorageDirectory() + // "/Partograph"; // File file =
 * new File(fpath); // if (file.isDirectory()) { SQLiteDatabase s =
 * SQLiteDatabase.openDatabase( Environment.getExternalStorageDirectory() +
 * "/Partograph/DataBase" + "/partograph.db", null,
 * SQLiteDatabase.OPEN_READONLY);
 * 
 * Cursor res = s.rawQuery(sql, null); res.moveToFirst(); if (res.getCount() >
 * 0) {
 * 
 * str = res.getString(0);
 * 
 * } // } return str; }
 * 
 * public ArrayList<String> getwomenid() throws Exception { //
 * AppContext.addToTrace( // new
 * RuntimeException().getStackTrace()[0].getMethodName() + " - " + //
 * this.getClass().getSimpleName()); ArrayList<String> womenid = new
 * ArrayList<String>(); // String fpath =
 * Environment.getExternalStorageDirectory() + // "/Partograph"; // File file =
 * new File(fpath); // if (file.isDirectory()) {
 * 
 * SQLiteDatabase s = SQLiteDatabase.openDatabase(
 * Environment.getExternalStorageDirectory() + "/Partograph/DataBase" +
 * "/partograph.db", null, SQLiteDatabase.OPEN_READONLY); String sql;
 * 
 * sql = "Select women_id from tbl_notification"; Cursor cur = s.rawQuery(sql,
 * null); cur.moveToFirst(); if (cur.getCount() > 0) { do { String id =
 * cur.getString(0); womenid.add(id); } while (cur.moveToNext()); } // } return
 * womenid; }
 * 
 * public String lastvaldate(String womenid, int object_id) throws Exception {
 * // AppContext.addToTrace( // new
 * RuntimeException().getStackTrace()[0].getMethodName() + " - " + //
 * this.getClass().getSimpleName()); String str = null; String sql =
 * "Select max(date_of_entry)  from tbl_propertyvalues where object_id = '" +
 * object_id + "' and women_id='" + womenid + "' order by trans_id desc LIMIT 1"
 * ; // String fpath = Environment.getExternalStorageDirectory() + //
 * "/Partograph"; // File file = new File(fpath); // if (file.isDirectory()) {
 * SQLiteDatabase s = SQLiteDatabase.openDatabase(
 * Environment.getExternalStorageDirectory() + "/Partograph/DataBase" +
 * "/partograph.db", null, SQLiteDatabase.OPEN_READONLY); Cursor res =
 * s.rawQuery(sql, null); res.moveToFirst(); if (res.getCount() > 0) {
 * 
 * str = res.getString(0);
 * 
 * } // } return str; }
 * 
 * private ArrayList<String> getnotificationdata() throws Exception { //
 * AppContext.addToTrace( // new
 * RuntimeException().getStackTrace()[0].getMethodName() + " - " + //
 * this.getClass().getSimpleName()); ArrayList<String> wid = new
 * ArrayList<String>(); String sql =
 * "Select * from tbl_notification where fhr=1 or dilatation=1 or contraction = 1 or pulse_bp=1"
 * ; String fpath = Environment.getExternalStorageDirectory() + "/Partograph";
 * File file = new File(fpath); if (file.isDirectory()) { SQLiteDatabase s =
 * SQLiteDatabase.openDatabase( Environment.getExternalStorageDirectory() +
 * "/Partograph/DataBase" + "/partograph.db", null,
 * SQLiteDatabase.OPEN_READONLY); Cursor cursor = s.rawQuery(sql, null);
 * cursor.moveToFirst(); if (cursor.getCount() > 0) {
 * 
 * do { String womenid = cursor.getString(1); wid.add(womenid); } while
 * (cursor.moveToNext());
 * 
 * } } return wid; }
 * 
 * private boolean isnotify(String women_id, int objectid) throws Exception { //
 * AppContext.addToTrace( // new
 * RuntimeException().getStackTrace()[0].getMethodName() + " - " + //
 * this.getClass().getSimpleName()); boolean isnotify = false; String columnname
 * = null; if (objectid == 1) { columnname = "fhr=2"; } else if (objectid == 3)
 * { columnname = "dilatation=2"; } else if (objectid == 4) { columnname =
 * "contraction=2"; } else if (objectid == 7) { columnname = "pulse_bp=2"; }
 * String sql = "Select * from tbl_notification where " + columnname +
 * " and women_id='" + women_id + "'"; // String fpath =
 * Environment.getExternalStorageDirectory() + // "/Partograph"; // File file =
 * new File(fpath); // if (file.isDirectory()) { SQLiteDatabase s =
 * SQLiteDatabase.openDatabase( Environment.getExternalStorageDirectory() +
 * "/Partograph/DataBase" + "/partograph.db", null,
 * SQLiteDatabase.OPEN_READONLY); Cursor cursor = s.rawQuery(sql, null);
 * cursor.moveToFirst(); if (cursor.getCount() > 0) { isnotify = true; } // }
 * return isnotify; }
 * 
 * void getdata() throws Exception { // AppContext.addToTrace( // new
 * RuntimeException().getStackTrace()[0].getMethodName() + " - " + //
 * this.getClass().getSimpleName()); ArrayList<String> womenid = getwomenid();
 * isNotify = false; for (String s : womenid) {
 * 
 * // wname = getwname(s); // 03Jan2016 Arpitha
 * 
 * String currdatetime = Partograph_CommonClass.getTodaysDate() + "_" +
 * Partograph_CommonClass.getCurrentTime();
 * 
 * int[] objectid = { 1, 3, 4, 7 }; objectname = " "; for (int i = 0; i <
 * objectid.length; i++) { strlastdate = lastvaldatetime(s, objectid[i]); if
 * (strlastdate != null) { if (i == 1) duration = 65; else duration = 35;
 * boolean isvalidtime = Partograph_CommonClass.getisValidTime(strlastdate,
 * currdatetime, duration); if (isvalidtime) {
 * 
 * // updatenotified(s, objectid[i]);
 * 
 * if (i == 0) { objectname = objectname + " " + "FHR"; } else if (i == 1) {
 * objectname = objectname + " " + "Dilatation"; } else if (i == 2) { objectname
 * = objectname + " " + "Contrcation"; } else if (i == 3) objectname =
 * objectname + " " + "Pulse & BP";
 * 
 * boolean isnotify = isnotify(s, objectid[i]);
 * 
 * if (!isnotify) {
 * 
 * if (i == 0) { objectname = objectname + " " + "FHR"; } else if (i == 1) {
 * objectname = objectname + " " + "Dilatation"; } else if (i == 2) { objectname
 * = objectname + " " + "Contrcation"; } else if (i == 3) objectname =
 * objectname + " " + "Pulse & BP"; isNotify = true;
 * 
 * // displaynotification(); updatetblnotification(s, objectid[i]);
 * 
 * } } }
 * 
 * } if (objectname.trim().length() > 0) { womenName.add(getwname(s));
 * message.add(objectname); }
 * 
 * } if (isNotify) { displaynotification(); }
 * 
 * }
 * 
 * String getwname(String wid) throws Exception { // AppContext.addToTrace( //
 * new RuntimeException().getStackTrace()[0].getMethodName() + " - " + //
 * this.getClass().getSimpleName()); // String fpath =
 * Environment.getExternalStorageDirectory() + // "/Partograph"; // File file =
 * new File(fpath); // if (file.isDirectory()) {
 * 
 * SQLiteDatabase s = SQLiteDatabase.openDatabase(
 * Environment.getExternalStorageDirectory() + "/Partograph/DataBase" +
 * "/partograph.db", null, SQLiteDatabase.OPEN_READONLY); String wname = null;
 * String sql = "Select women_name from tbl_registeredwomen where women_id='" +
 * wid + "'"; Cursor cursor = s.rawQuery(sql, null); cursor.moveToFirst(); if
 * (cursor.getCount() > 0) { wname = cursor.getString(0); } // } return wname;
 * 
 * }
 * 
 * 
 * private ArrayList<Notification_Pojo> getnotificationdata1() throws Exception
 * { AppContext.addToTrace( new
 * RuntimeException().getStackTrace()[0].getMethodName() + " - " +
 * this.getClass().getSimpleName()); ArrayList<Notification_Pojo> val = new
 * ArrayList<Notification_Pojo>(); Notification_Pojo npojo; ArrayList<String>
 * wid = new ArrayList<String>(); String sql =
 * "Select * from tbl_notification where fhr=1 or dilatation=1 or contraction = 1 or pulse_bp=1"
 * ; SQLiteDatabase s = SQLiteDatabase.openDatabase(AppContext.dbDirRef +
 * "/partograph.db", null, SQLiteDatabase.OPEN_READONLY); Cursor cursor =
 * s.rawQuery(sql, null); cursor.moveToFirst(); if (cursor.getCount() > 0) {
 * 
 * do { // String womenid = cursor.getString(1); // wid.add(womenid); npojo =
 * new Notification_Pojo(); npojo.setUserid(cursor.getString(0));
 * npojo.setWomenid(cursor.getString(1)); npojo.setFhr(cursor.getInt(2));
 * npojo.setDilatation(cursor.getInt(3));
 * npojo.setContraction(cursor.getInt(4)); npojo.setPulse_bp(cursor.getInt(5));
 * val.add(npojo); } while (cursor.moveToNext());
 * 
 * } return val; }
 * 
 * 
 * void displaynotification() { // notificationOn = //
 * Partograph_CommonClass.prefs.getString("notification", null); //
 * if(notificationOn) notificationcontent = ""; Uri soundUri =
 * RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
 * 
 * TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
 * 
 * nitems = new ArrayList<Notification_Pojo>();
 * 
 * npojo = new Notification_Pojo(); npojo.setWomenid(""); npojo.setObjectid(1);
 * 
 * nitems.add(npojo);
 * 
 * // notificationcontent = objectname + " " + notificationcontent + " // value
 * not Updated For " + wname;
 * 
 * for (int i = 0; i < message.size(); i++) { notificationcontent =
 * notificationcontent + " " + womenName.get(i) + ":-" + message.get(i) +
 * " value not updated\n"; }
 * 
 * Intent notificationIntent = new Intent(context, LoginActivity.class); //
 * notificationIntent.putExtra("nitems", nitems); //
 * notificationIntent.putExtra("pos", 0);
 * stackBuilder.addParentStack(LoginActivity.class);
 * stackBuilder.addNextIntent(notificationIntent);
 * 
 * PendingIntent pendingIntent = stackBuilder.getPendingIntent(0,
 * PendingIntent.FLAG_UPDATE_CURRENT);
 * 
 * NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
 * notification = builder // .setContentTitle("Partograph")
 * .setContentTitle(Html.fromHtml("<font color='" +
 * context.getResources().getColor(R.color.appcolor) +
 * "'><b> Partograph</b></font>"))
 * 
 * .setStyle(new NotificationCompat.BigTextStyle().bigText(notificationcontent))
 * .setContentText(Html.fromHtml("<font color='" +
 * context.getResources().getColor(R.color.brown) + "'><b> " +
 * notificationcontent + "</b></font>")) .setTicker("Partograph Alert!"
 * ).setSmallIcon(R.drawable.notifi_icon).setNumber(message.size())
 * .setSound(soundUri)
 * 
 * .setContentIntent(pendingIntent).build();
 * 
 * // builder.setContentTitle(Html.fromHtml( // "<font color='" +
 * context.getResources().getColor(R.color.appcolor) + //
 * "'><b> Partograph</b></font>"));
 * 
 * 
 * builder.setContentTitle(Html.fromHtml( "<font color='" +
 * context.getResources().getColor(R.color.appcolor) +
 * "'><b> Partograph</b></font>"));
 * 
 * builder.setSmallIcon(R.drawable.notifi_icon);
 * 
 * builder.setContentText(Html.fromHtml("<font color='" +
 * context.getResources().getColor(R.color.appcolor) + "'><b> " +
 * notificationcontent + "</b></font>"));
 * 
 * 
 * notification.contentIntent = pendingIntent; notification.flags |=
 * Notification.FLAG_AUTO_CANCEL;
 * 
 * // notification.flags |= Notification.FLAG_NO_CLEAR;
 * 
 * NotificationManager notificationManager = (NotificationManager) context
 * .getSystemService(Context.NOTIFICATION_SERVICE);
 * notificationManager.notify(4, notification);
 * 
 * }
 * 
 * }
 */