package com.bc.partograph.stagesoflabor;

import java.util.ArrayList;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.android.internal.http.multipart.Part;
import com.androidquery.AQuery;
import com.bc.partograph.common.AppContext;
import com.bc.partograph.common.KillAllActivitesAndGoToLogin;
import com.bc.partograph.common.Partograph_CommonClass;
import com.bc.partograph.common.Partograph_CommonClass.AsyncCallWS;
import com.bc.partograph.common.Partograph_DB;
import com.bc.partograph.login.LoginActivity;
import com.bc.partograph.registration.RegistrationActivity;
import com.bc.partograph.settings.Settings_parto;
import com.bc.partograph.sync.SyncFunctions;
import com.bluecrimson.partograph.R;
import com.bluecrimson.partograph.StagesofLabor_Pojo;
import com.bluecrimson.partograph.Women_Profile_Pojo;

public class ThirdStageOfLabor_View extends Activity implements OnClickListener {

	Partograph_DB dbh;
	AQuery aq;
	/*
	 * ArrayList<Women_Profile_Pojo> rowItems; int pos;
	 */
	Cursor cursor;
	String strWomenid, strwuserId;
	StagesofLabor_Pojo sData;
	public static MenuItem menuItem;
	public static int viewthirdstagepos = 0;
	public static boolean isThirdStageOfLaborView;
	Women_Profile_Pojo woman;// 03Nov2016

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_thirdstage);
		try {
			viewthirdstagepos = Partograph_CommonClass.curr_tabpos;
			isThirdStageOfLaborView = true;

			/*
			 * rowItems = (ArrayList<Women_Profile_Pojo>) getIntent()
			 * .getSerializableExtra("rowitems"); pos =
			 * getIntent().getIntExtra("position", 0);
			 */
			woman = (Women_Profile_Pojo) getIntent().getSerializableExtra("woman");
			dbh = Partograph_DB.getInstance(getApplicationContext());
			aq = new AQuery(this);
			// get the action bar
			ActionBar actionBar = getActionBar();

			// Enabling Back navigation on Action Bar icon
			actionBar.setDisplayHomeAsUpEnabled(true);
			actionBar.setHomeButtonEnabled(true);

			initializeScreen(aq.id(R.id.rlthirdstage).getView());
			initialView();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// Initial View
	private void initialView() throws Exception {
		strwuserId = woman.getUserId();
		strWomenid = woman.getWomenId();

		aq.id(R.id.tr_pph2).gone();
		aq.id(R.id.btnsavethirdstage).text(getResources().getString(R.string.previous));

		aq.id(R.id.etdateofthirdstage).enabled(false);
		aq.id(R.id.ettimeofthirdstage).enabled(false);
		aq.id(R.id.etdurationofthirdstage).enabled(false);
		aq.id(R.id.rdoxytocinnone).enabled(false);
		aq.id(R.id.rd10units1).enabled(false);
		aq.id(R.id.rd5units).enabled(false);
		aq.id(R.id.etergometrine).enabled(false);
		aq.id(R.id.rd125).enabled(false);
		aq.id(R.id.rd250).enabled(false);
		aq.id(R.id.etmisoprost).enabled(false);
		aq.id(R.id.rdcomplete).enabled(false);
		aq.id(R.id.rdincomplete).enabled(false);
		aq.id(R.id.rduterineyes).enabled(false);
		aq.id(R.id.rduterineno).enabled(false);
		aq.id(R.id.etebl).enabled(false);
		aq.id(R.id.chkpph).enabled(false);
		aq.id(R.id.rdatonic).enabled(false);
		aq.id(R.id.rdtraumatic).enabled(false);
		aq.id(R.id.rdcombined).enabled(false);
		aq.id(R.id.chkreatinedplacenta).enabled(false);
		aq.id(R.id.chkinversion).enabled(false);
		aq.id(R.id.etcommentsthirdstage).enabled(false);

		aq.id(R.id.etdateofthirdstage).getEditText().setTextColor(getResources().getColor(R.color.fontcolor_disable));
		aq.id(R.id.ettimeofthirdstage).getEditText().setTextColor(getResources().getColor(R.color.fontcolor_disable));
		aq.id(R.id.etdurationofthirdstage).getEditText()
				.setTextColor(getResources().getColor(R.color.fontcolor_disable));
		aq.id(R.id.rdoxytocinnone).getEditText().setTextColor(getResources().getColor(R.color.fontcolor_disable));
		aq.id(R.id.rd10units1).getEditText().setTextColor(getResources().getColor(R.color.fontcolor_disable));
		aq.id(R.id.rd5units).getEditText().setTextColor(getResources().getColor(R.color.fontcolor_disable));
		aq.id(R.id.etergometrine).getEditText().setTextColor(getResources().getColor(R.color.fontcolor_disable));
		aq.id(R.id.rd125).getEditText().setTextColor(getResources().getColor(R.color.fontcolor_disable));
		aq.id(R.id.rd250).enabled(false);
		aq.id(R.id.etmisoprost).getEditText().setTextColor(getResources().getColor(R.color.fontcolor_disable));
		aq.id(R.id.rdcomplete).enabled(false);
		aq.id(R.id.rdincomplete).enabled(false);
		aq.id(R.id.rduterineyes).enabled(false);
		aq.id(R.id.rduterineno).enabled(false);
		aq.id(R.id.etebl).getEditText().setTextColor(getResources().getColor(R.color.fontcolor_disable));
		aq.id(R.id.chkpph).enabled(false);
		aq.id(R.id.rdatonic).enabled(false);
		aq.id(R.id.rdtraumatic).enabled(false);
		aq.id(R.id.rdcombined).enabled(false);
		aq.id(R.id.chkreatinedplacenta).enabled(false);
		aq.id(R.id.chkinversion).enabled(false);
		aq.id(R.id.etcommentsthirdstage).getEditText().setTextColor(getResources().getColor(R.color.fontcolor_disable));

		cursor = dbh.GetThirdStageDetails(strWomenid, strwuserId, 11);
		if (cursor != null && cursor.getCount() > 0) {
			cursor.moveToFirst();
			prepareThirdStagePOJO();
		}
	}

	private void prepareThirdStagePOJO() {
		try {

			sData = new StagesofLabor_Pojo();

			sData.setDateofentry(cursor.getString(0));
			sData.setTimeofentry(cursor.getString(1));
			sData.setDurationofthirdstage(cursor.getString(2));
			sData.setOxytocin(cursor.getString(3));
			sData.setErgometrine(cursor.getString(4));
			sData.setPgf2alpha(cursor.getString(5));
			sData.setMisoprost(cursor.getString(6));
			sData.setPlacenta(cursor.getString(7));
			sData.setUterinemassage(cursor.getString(8));
			sData.setEbl(cursor.getString(9));
			sData.setPph(cursor.getString(10));
			sData.setPphproperties(cursor.getString(11));
			sData.setRetainedplacenta(cursor.getString(12));
			sData.setInversion(cursor.getString(13));
			sData.setThirdstagecomments(cursor.getString(14));

			setThirdStageOfLaborData();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
	}

	// Set the values
	private void setThirdStageOfLaborData() throws Exception {
		if (sData != null) {
			// aq.id(R.id.etdateofthirdstage).text(sData.getDateofentry());
			// commented on 24Nov2016 by Arpitha- always date was dispalying in
			// yyyy-MM-dd format
			String thirdstageDate = sData.getDateofentry();// 09Jan2017 Arpitha
			aq.id(R.id.etdateofthirdstage).text(Partograph_CommonClass.getConvertedDateFormat(thirdstageDate,
					Partograph_CommonClass.defdateformat));
			aq.id(R.id.ettimeofthirdstage).text(sData.getTimeofentry());
			aq.id(R.id.etdurationofthirdstage).text("" + sData.getDurationofthirdstage());

			if (sData.getOxytocin().equals("0"))
				aq.id(R.id.rdoxytocinnone).checked(true);
			else if (sData.getOxytocin().equals("1"))
				aq.id(R.id.rd10units1).checked(true);
			else
				aq.id(R.id.rd5units).checked(true);

			aq.id(R.id.etergometrine).text("" + sData.getErgometrine());

			if (sData.getPgf2alpha().equals("1"))
				aq.id(R.id.rd125).checked(true);
			else if (sData.getPgf2alpha().equals("2"))
				aq.id(R.id.rd250).checked(true);

			aq.id(R.id.etmisoprost).text("" + sData.getMisoprost());

			if (sData.getPlacenta().equals("1"))
				aq.id(R.id.rdcomplete).checked(true);
			else if (sData.getPlacenta().equals("2"))
				aq.id(R.id.rdincomplete).checked(true);

			if (sData.getUterinemassage().equals("1"))
				aq.id(R.id.rduterineyes).checked(true);
			else if (sData.getUterinemassage().equals("2"))
				aq.id(R.id.rduterineno).checked(true);

			if (sData.getPph().equals("1")) {
				aq.id(R.id.chkpph).checked(true);
				aq.id(R.id.tr_pph2).visible();
			} else {
				aq.id(R.id.chkpph).checked(false);
				aq.id(R.id.tr_pph2).gone();
			}

			if (sData.getPphproperties().equals("1"))
				aq.id(R.id.rdatonic).checked(true);
			else if (sData.getPphproperties().equals("2"))
				aq.id(R.id.rdtraumatic).checked(true);
			else
				aq.id(R.id.rdcombined).checked(true);

			if (sData.getRetainedplacenta().equals("1"))
				aq.id(R.id.chkreatinedplacenta).checked(true);
			else
				aq.id(R.id.chkreatinedplacenta).checked(false);

			if (sData.getInversion().equals("1"))
				aq.id(R.id.chkinversion).checked(true);
			else
				aq.id(R.id.chkinversion).checked(false);

			aq.id(R.id.etcommentsthirdstage).text(sData.getThirdstagecomments());
			aq.id(R.id.etebl).text("" + sData.getEbl());
			
			
		}
	}

	/**
	 * This is a Recursive method that traverse through the group and subgroup
	 * of view Sets text, assigns clickListners
	 * 
	 * @throws Exception
	 */
	private ArrayList<View> initializeScreen(View v) throws Exception {
		if (!(v instanceof ViewGroup)) {
			ArrayList<View> viewArrayList = new ArrayList<View>();
			viewArrayList.add(v);

			// Edittext
			if (v.getClass() == EditText.class) {
				aq.id(v.getId()).getEditText().setOnClickListener(this);
			}

			// Button
			if (v.getClass() == Button.class) {
				aq.id(v.getId()).getButton().setOnClickListener(this);
			}

			if (v.getClass() == CheckBox.class) {
				aq.id(v.getId()).getCheckBox().setOnClickListener(this);
			}

			if (v.getClass() == RadioButton.class) {
				aq.id(v.getId()).getButton().setOnClickListener(this);
			}
			return viewArrayList;
		}

		ArrayList<View> result = new ArrayList<View>();

		ViewGroup viewGroup = (ViewGroup) v;
		for (int i = 0; i < viewGroup.getChildCount(); i++) {

			View child = viewGroup.getChildAt(i);

			ArrayList<View> viewArrayList = new ArrayList<View>();
			viewArrayList.add(v);
			viewArrayList.addAll(initializeScreen(child));

			result.addAll(viewArrayList);
		}
		return result;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {

		case R.id.btnsavethirdstage: {
			try {

				boolean isPre = false;

				if (cursor != null && (!cursor.isFirst())) {
					isPre = true;
					cursor.moveToPrevious();
					prepareThirdStagePOJO();
					setThirdStageOfLaborData();
				}
				if (!isPre) {
					Toast.makeText(getApplicationContext(), getResources().getString(R.string.no_records),
							Toast.LENGTH_LONG).show();
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

			break;
		}

		default:
			break;
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_womenlist, menu);

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		try {
			switch (item.getItemId()) {
			// Respond to the action bar's Up/Home button
			case android.R.id.home:
				NavUtils.navigateUpFromSameTask(this);
				return true;

			case R.id.logout:
				Intent login = new Intent(this, LoginActivity.class);
				startActivity(login);
				return true;

			case R.id.registration:
				Intent registration = new Intent(this, RegistrationActivity.class);
				startActivity(registration);
				return true;

			case R.id.sync:
				menuItem = item;
				menuItem.setActionView(R.layout.progressbar);
				menuItem.expandActionView();
				calSyncMtd();
				return true;

			case R.id.settings:
				Intent settings = new Intent(this, Settings_parto.class);
				startActivity(settings);
				return true;

			}
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
		return super.onOptionsItemSelected(item);
	}

	// Sync
	private void calSyncMtd() throws Exception {
		SyncFunctions.syncUptoDate = false;
		Partograph_CommonClass.responseAckCount = 0;

		AsyncCallWS task = new AsyncCallWS();
		task.execute();
	}

	/** This method Calls the idle timeout */
	@Override
	public void onUserInteraction() {
		super.onUserInteraction();
		KillAllActivitesAndGoToLogin
				.delayedIdle(Integer.parseInt(Partograph_CommonClass.properties.getProperty("idleTimeOut")));
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		if (Partograph_CommonClass.isRecordExistsForSync)
			menu.findItem(R.id.sync).setIcon(getResources().getDrawable(R.drawable.ic_sync_red));
		else
			menu.findItem(R.id.sync).setIcon(getResources().getDrawable(R.drawable.ic_sync_green));
		
		menu.findItem(R.id.search).setVisible(false);//14Jun2017 Arpitha
		return super.onPrepareOptionsMenu(menu);
	}
}
