package com.bc.partograph.stagesoflabor;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import com.androidquery.AQuery;
import com.bc.partograph.common.AppContext;
import com.bc.partograph.common.Partograph_CommonClass;
import com.bc.partograph.common.Partograph_DB;
import com.bc.partograph.sync.SyncFunctions;
import com.bluecrimson.dischargedetails.DiscgargePojo;
import com.bluecrimson.partograph.Property_pojo;
import com.bluecrimson.partograph.R;
import com.bluecrimson.partograph.StagesofLabor_Pojo;
import com.bluecrimson.partograph.Women_Profile_Pojo;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

public class ThirdStageOfLabor extends Fragment implements OnClickListener {

	View rootView;

	static AQuery aq;
	int oxytocinunit, pgf2alpha, placenta = 1, uterine_massage = 1, pph, retained_placenta, inversion, pphproperties;
	Partograph_DB dbh;
	int obj_Id;
	static String todaysDate;
	String strWomenid, strwuserId;
	Cursor cursor;
	StagesofLabor_Pojo sData;

	// 26dec2015
	static int hour;
	static int minute;
	int year;
	int mon;
	int day;
	public static String selecteddate, strthirdstagedate, strthirdstagetime;
	static final int TIME_DIALOG_ID = 999;

	public static Date thirdstagedate;
	public static String duration;
	// 16Aug2016 - bindu
	Thread myThread;

	// updated on 23August2016by Arpitha
	static String strlastentry;
	static String refdatetime = null;
	static Date regdate;
	static Date deldate;

	// 22Sep2016 Arpitha
	int ebl;
	int misoprost;
	int ergometrine;

	// 23Sep2016 Arpitha
	static String del_datetime;
	static String thirdstage_datetime;
	int thirdstage_duration;

	static Women_Profile_Pojo woman;

	public static String thidStageDate, thirdStageTime;
	String thirdStageDate;
	static String strpostpartumdatetime;// 20Sep2017 Arpitha
	static Date postpartumdatetime;// 20Sep2017 Arpitha

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.fragment_thirdstage, container, false);
		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		try {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());

			aq = new AQuery(getActivity());

			woman = (Women_Profile_Pojo) getActivity().getIntent().getSerializableExtra("woman");

			dbh = Partograph_DB.getInstance(getActivity());
			initializeScreen(aq.id(R.id.rlthirdstage).getView());

			// updated on 23Aug2016 by Bindu
			getwomanbasicdata();

			initialView();

			aq.id(R.id.etdateofthirdstage).getEditText().setOnTouchListener(new View.OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if (MotionEvent.ACTION_UP == event.getAction()) {
						caldatepicker();
					}
					return true;
				}
			});

			aq.id(R.id.ettimeofthirdstage).getEditText().setOnTouchListener(new View.OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if (MotionEvent.ACTION_UP == event.getAction()) {
						strthirdstagetime = aq.id(R.id.ettimeofthirdstage).getText().toString();

						if (strthirdstagetime != null) {
							Calendar calendar = Calendar.getInstance();
							Date d = null;
							d = dbh.getTime(strthirdstagetime);
							calendar.setTime(d);
							hour = calendar.get(Calendar.HOUR_OF_DAY);
							minute = calendar.get(Calendar.MINUTE);
						}
						showtimepicker();
					}
					return true;
				}
			});

			// 23August2016 Arpitha
			strlastentry = dbh.getlastentrytime(woman.getWomenId(), 0);

			setInputFiltersForEdittext();// 10April2017 Arpitha

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}

	}

	// updated on 23Aug2016 by bindu
	void getwomanbasicdata() throws Exception {

		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		if (woman != null) {

			String doa = "", toa = "", risk = "", dod = "", tod = "";

			aq.id(R.id.wname).text(woman.getWomen_name());
			aq.id(R.id.wage).text(woman.getAge() + getResources().getString(R.string.yrs));
			if (woman.getregtype() != 2) {
				doa = Partograph_CommonClass.getConvertedDateFormat(woman.getDate_of_admission(),
						Partograph_CommonClass.defdateformat);
				toa = Partograph_CommonClass.gettimein12hrformat(woman.getTime_of_admission());
				aq.id(R.id.wdoa).text(getResources().getString(R.string.reg) + ": " + doa + "/" + toa);
			} else {
				String entryDate = Partograph_CommonClass.getConvertedDateFormat(woman.getDate_of_reg_entry(),
						Partograph_CommonClass.defdateformat);
				aq.id(R.id.wdoa).text(getResources().getString(R.string.reg) + ": " + entryDate + "/"
						+ woman.getDate_of_reg_entry().split("/")[1]);
			}

			aq.id(R.id.wgest)
					.text(getResources().getString(R.string.gest_age) + ":"
							+ (woman.getGestationage() == 0 ? getResources().getString(R.string.notknown)
									: woman.getGestationage() + getResources().getString(R.string.wks)));// 23Aug2016
			// -
			// bindu
			// -
			// gest
			// age
			// not
			// known
			// chk
			aq.id(R.id.wgravida).text(getResources().getString(R.string.gravida_short_label) + ":" + woman.getGravida()
					+ ", " + getResources().getString(R.string.para_short_label) + ":" + woman.getPara());
			woman.getRisk_category();
			if (woman.getRisk_category() == 1) {
				risk = getResources().getString(R.string.high);// 07Oct2016
				// Arpitha
			} else
				risk = getResources().getString(R.string.low);// 07Oct2016
			// Arpitha
			aq.id(R.id.wtrisk).text(getResources().getString(R.string.risk_short_label) + ":" + risk);

			if (woman.getDel_type() > 0) {
				String[] deltype = getResources().getStringArray(R.array.del_type);
				aq.id(R.id.wdelstatus)
						.text(getResources().getString(R.string.delstatus) + ":" + deltype[woman.getDel_type() - 1]);
				dod = Partograph_CommonClass.getConvertedDateFormat(woman.getDel_Date(),
						Partograph_CommonClass.defdateformat);
				tod = Partograph_CommonClass.gettimein12hrformat(woman.getDel_Time());
				aq.id(R.id.wdeldate).text(getResources().getString(R.string.del) + ": " + dod + "/" + tod);

			}

		}
	}

	private void initialView() throws Exception {
		aq.id(R.id.tr_pph2).gone();
		obj_Id = 11;
		todaysDate = Partograph_CommonClass.getTodaysDate();
		// selecteddate = todaysDate;02Nov2016 Arpitha
		strthirdstagedate = todaysDate;// 02Nov2016 Arpitha
		strWomenid = woman.getWomenId();
		strwuserId = woman.getUserId();

		String displaydateformat = Partograph_CommonClass.getConvertedDateFormat(todaysDate,
				Partograph_CommonClass.defdateformat);
		aq.id(R.id.etdateofthirdstage).text(displaydateformat);

		aq.id(R.id.ettimeofthirdstage).text(Partograph_CommonClass.getCurrentTime());

		// 23August2016 ARpitha
		refdatetime = dbh.getrefdate(woman.getWomenId(), woman.getUserId());

		if (woman.getregtype() != 2) {// 09Jan2017 Arpitha
			regdate = new SimpleDateFormat("yyyy-MM-dd HH:mm")
					.parse(woman.getDate_of_admission() + " " + woman.getTime_of_admission());
		} else
			regdate = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(woman.getDate_of_reg_entry().replace("/", " "));

		// 23Sep2016 Arpitha
		del_datetime = woman.getDel_Date() + "_" + woman.getDel_Time();

		deldate = new SimpleDateFormat("yyyy-MM-dd_HH:mm").parse(del_datetime);

		setThirdStageDetails();

		strthirdstagetime = Partograph_CommonClass.getCurrentTime();// 13Feb2017
																	// Arpitha

		strpostpartumdatetime = dbh.getPostPartumDateTime(woman.getWomenId(), woman.getUserId());// 20Sep2017
		// Arpitha
	}

	// set the data for third stage
	private void setThirdStageDetails() throws Exception {

		cursor = dbh.GetThirdStageDetails(strWomenid, strwuserId, 11);
		if (cursor != null && cursor.getCount() > 0) {

			StageofLabor.isThirdStageDetails = true;// 13Feb2017 Arpitha

			cursor.moveToFirst();

			aq.id(R.id.btnsavethirdstage).gone();

			aq.id(R.id.tr_pph2).gone();
			aq.id(R.id.etdateofthirdstage).enabled(false);
			aq.id(R.id.ettimeofthirdstage).enabled(false);
			aq.id(R.id.etdurationofthirdstage).enabled(false);
			aq.id(R.id.rdoxytocinnone).enabled(false);
			aq.id(R.id.rd10units1).enabled(false);
			aq.id(R.id.rd5units).enabled(false);
			aq.id(R.id.etergometrine).enabled(false);
			aq.id(R.id.rd125).enabled(false);
			aq.id(R.id.rd250).enabled(false);
			aq.id(R.id.etmisoprost).enabled(false);
			aq.id(R.id.rdcomplete).enabled(false);
			aq.id(R.id.rdincomplete).enabled(false);
			aq.id(R.id.rduterineyes).enabled(false);
			aq.id(R.id.rduterineno).enabled(false);
			aq.id(R.id.etebl).enabled(false);
			aq.id(R.id.chkpph).enabled(false);
			aq.id(R.id.rdatonic).enabled(false);
			aq.id(R.id.rdtraumatic).enabled(false);
			aq.id(R.id.rdcombined).enabled(false);
			aq.id(R.id.chkreatinedplacenta).enabled(false);
			aq.id(R.id.chkinversion).enabled(false);
			aq.id(R.id.etcommentsthirdstage).enabled(false);

			sData = new StagesofLabor_Pojo();

			thirdStageDate = cursor.getString(0);// 09Jan2017 Arpitha

			sData.setDateofentry(Partograph_CommonClass.getConvertedDateFormat(thirdStageDate,
					Partograph_CommonClass.defdateformat));// changed on
															// 09JAn2017 Arpitha
			sData.setTimeofentry(cursor.getString(1));
			sData.setDurationofthirdstage(cursor.getString(2));
			sData.setOxytocin(cursor.getString(3));
			sData.setErgometrine(cursor.getString(4));
			sData.setPgf2alpha(cursor.getString(5));
			sData.setMisoprost(cursor.getString(6));
			sData.setPlacenta(cursor.getString(7));
			sData.setUterinemassage(cursor.getString(8));
			sData.setEbl(cursor.getString(9));
			sData.setPph(cursor.getString(10));
			sData.setPphproperties(cursor.getString(11));
			sData.setRetainedplacenta(cursor.getString(12));
			sData.setInversion(cursor.getString(13));
			sData.setThirdstagecomments(cursor.getString(14));

			duration = cursor.getString(2);
			strthirdstagedate = cursor.getString(0);
			strthirdstagetime = cursor.getString(1);

			if (sData != null) {
				aq.id(R.id.etdateofthirdstage).text(sData.getDateofentry());
				aq.id(R.id.ettimeofthirdstage).text(sData.getTimeofentry());
				aq.id(R.id.etdurationofthirdstage).text("" + sData.getDurationofthirdstage());

				duration = "" + sData.getDurationofthirdstage();

				strthirdstagedate = cursor.getString(0);
				strthirdstagetime = cursor.getString(1);

				thidStageDate = thirdStageDate;// 12April2017 Arpitha
				thirdStageTime = sData.getTimeofentry();// 12April2017 Arpitha

				if (sData.getOxytocin().equals("0"))
					aq.id(R.id.rdoxytocinnone).checked(true);
				else if (sData.getOxytocin().equals("1"))
					aq.id(R.id.rd10units1).checked(true);
				else
					aq.id(R.id.rd5units).checked(true);

				aq.id(R.id.etergometrine).text("" + sData.getErgometrine());

				if (sData.getPgf2alpha().equals("1"))
					aq.id(R.id.rd125).checked(true);
				else if (sData.getPgf2alpha().equals("2"))
					aq.id(R.id.rd250).checked(true);

				aq.id(R.id.etmisoprost).text("" + sData.getMisoprost());

				if (sData.getPlacenta().equals("1"))
					aq.id(R.id.rdcomplete).checked(true);
				else if (sData.getPlacenta().equals("2"))
					aq.id(R.id.rdincomplete).checked(true);

				if (sData.getUterinemassage().equals("1"))
					aq.id(R.id.rduterineyes).checked(true);
				else if (sData.getUterinemassage().equals("2"))
					aq.id(R.id.rduterineno).checked(true);

				if (sData.getPph().equals("1")) {
					aq.id(R.id.chkpph).checked(true);
					aq.id(R.id.tr_pph2).visible();
				} else {
					aq.id(R.id.chkpph).checked(false);
					aq.id(R.id.tr_pph2).gone();
				}

				if (sData.getPphproperties().equals("1"))
					aq.id(R.id.rdatonic).checked(true);
				else if (sData.getPphproperties().equals("2"))
					aq.id(R.id.rdtraumatic).checked(true);
				else if (sData.getPphproperties().equals("3")) // bindu -
																// 09Aug2016 -
																// condition
																// added
					aq.id(R.id.rdcombined).checked(true);

				if (sData.getRetainedplacenta().equals("1"))
					aq.id(R.id.chkreatinedplacenta).checked(true);
				else
					aq.id(R.id.chkreatinedplacenta).checked(false);

				if (sData.getInversion().equals("1"))
					aq.id(R.id.chkinversion).checked(true);
				else
					aq.id(R.id.chkinversion).checked(false);

				aq.id(R.id.etcommentsthirdstage).text(sData.getThirdstagecomments());
				aq.id(R.id.txtmode).text(getResources().getString(R.string.viewmode));
				aq.id(R.id.etebl).text("" + sData.getEbl());

				// 05May2017 Arpitha -v2.6 - mantis id 0000241
				if (Integer.parseInt(aq.id(R.id.etdurationofthirdstage).getText().toString()) > 30) {
					aq.id(R.id.etdurationofthirdstage).textColor(getResources().getColor(R.color.red));
				} else if (Integer.parseInt(aq.id(R.id.etdurationofthirdstage).getText().toString()) > 20
						&& Integer.parseInt(aq.id(R.id.etdurationofthirdstage).getText().toString()) < 30) {
					aq.id(R.id.etdurationofthirdstage).textColor(getResources().getColor(R.color.amber));
				}

				if (Integer.parseInt(aq.id(R.id.etebl).getText().toString()) > 1000) {
					aq.id(R.id.etebl).textColor(getResources().getColor(R.color.red));
				} else if (Integer.parseInt(aq.id(R.id.etebl).getText().toString()) > 500
						&& Integer.parseInt(aq.id(R.id.etdurationofthirdstage).getText().toString()) < 1000) {
					aq.id(R.id.etebl).textColor(getResources().getColor(R.color.amber));
				} // 05May2017 Arpitha -v2.6 - mantis id 0000241

			}
		} else {
			StageofLabor.isThirdStageDetails = false;// 27Feb2017 Arpitha
			aq.id(R.id.txtmode).text(getResources().getString(R.string.editmode));
			aq.id(R.id.btnsavethirdstage).visible();
			aq.id(R.id.etdurationofthirdstage).enabled(true);
			aq.id(R.id.rdoxytocinnone).enabled(true);
			aq.id(R.id.rd10units1).enabled(true);
			aq.id(R.id.rd5units).enabled(true);
			aq.id(R.id.etergometrine).enabled(true);
			aq.id(R.id.rd125).enabled(true);
			aq.id(R.id.rd250).enabled(true);
			aq.id(R.id.etmisoprost).enabled(true);
			aq.id(R.id.rdcomplete).enabled(true);
			aq.id(R.id.rdincomplete).enabled(true);
			aq.id(R.id.rduterineyes).enabled(true);
			aq.id(R.id.rduterineno).enabled(true);
			aq.id(R.id.etebl).enabled(true);
			aq.id(R.id.chkpph).enabled(true);
			aq.id(R.id.rdatonic).enabled(true);
			aq.id(R.id.rdtraumatic).enabled(true);
			aq.id(R.id.rdcombined).enabled(true);
			aq.id(R.id.chkreatinedplacenta).enabled(true);
			aq.id(R.id.chkinversion).enabled(true);
			aq.id(R.id.etcommentsthirdstage).enabled(true);

			// 10Aug2017 Atpitha
			ArrayList<DiscgargePojo> arrVal = new ArrayList<DiscgargePojo>();
			arrVal = dbh.getDischargeData(woman.getUserId(), woman.getWomenId());
			if (arrVal.size() > 0) {
				aq.id(R.id.txtdisable).visible();
				// txtdisabled.setVisibility(View.VISIBLE);
				// aq.id(R.id.txtdisable).gone();
				aq.id(R.id.tbldetails).gone();
				aq.id(R.id.scthirdstage).gone();
				aq.id(R.id.llbtnthirdstage).gone();

			} // 10Aug2017 Atpitha

		}

	}

	/**
	 * This is a Recursive method that traverse through the group and subgroup
	 * of view Sets text, assigns clickListners
	 * 
	 * @throws Exception
	 */
	private ArrayList<View> initializeScreen(View v) throws Exception {
		if (!(v instanceof ViewGroup)) {
			ArrayList<View> viewArrayList = new ArrayList<View>();
			viewArrayList.add(v);

			// Edittext
			if (v.getClass() == EditText.class) {
				aq.id(v.getId()).getEditText().setOnClickListener(this);
			}

			// Button
			if (v.getClass() == Button.class) {
				aq.id(v.getId()).getButton().setOnClickListener(this);
			}

			if (v.getClass() == CheckBox.class) {
				aq.id(v.getId()).getCheckBox().setOnClickListener(this);
			}

			if (v.getClass() == RadioButton.class) {
				aq.id(v.getId()).getButton().setOnClickListener(this);
			}
			return viewArrayList;
		}

		ArrayList<View> result = new ArrayList<View>();

		ViewGroup viewGroup = (ViewGroup) v;
		for (int i = 0; i < viewGroup.getChildCount(); i++) {

			View child = viewGroup.getChildAt(i);

			ArrayList<View> viewArrayList = new ArrayList<View>();
			viewArrayList.add(v);
			viewArrayList.addAll(initializeScreen(child));

			result.addAll(viewArrayList);
		}
		return result;
	}

	@Override
	public void onClick(View v) {
		try {
			switch (v.getId()) {
			case R.id.btnsavethirdstage: {

				if (validateThirdStageofLabor()) {

					// 22Sep2016 Arpitha
					ebl = 0;
					misoprost = 0;
					ergometrine = 0;
					// 23Sep2016 Arpitha
					thirdstage_duration = 0;

					// 22Sep2016 Arpitha
					if (aq.id(R.id.etebl).getText().toString().trim().length() > 0) {
						ebl = Integer.parseInt(aq.id(R.id.etebl).getText().toString());
					}
					if (aq.id(R.id.etmisoprost).getText().toString().trim().length() > 0) {
						misoprost = Integer.parseInt(aq.id(R.id.etmisoprost).getText().toString());
					}
					if (aq.id(R.id.etergometrine).getText().toString().trim().length() > 0) {
						ergometrine = Integer.parseInt(aq.id(R.id.etergometrine).getText().toString());
					}
					// 23Sep2016 Arpitha
					if (aq.id(R.id.etdurationofthirdstage).getText().toString().trim().length() > 0)

					{
						thirdstage_duration = Integer.parseInt(aq.id(R.id.etdurationofthirdstage).getText().toString());
					}

					// 22Sep2016 Arpitha
					if ((ebl > 0 && (ebl < 300 || ebl > 3000))
							|| (misoprost > 0 && (misoprost < 100 || misoprost > 1200) || (ergometrine > 4))
							|| thirdstage_duration > 30) {
						displayConfirmationAlert();
					} else {
						// updated bindu - 25Aug2016 - check confirmation of
						// third stage date and time
						String msg = getResources().getString(R.string.thirdstagedatetimerecheck)
								+ aq.id(R.id.etdateofthirdstage).getText().toString() + " / "
								+ aq.id(R.id.ettimeofthirdstage).getText().toString()
								+ getResources().getString(R.string.confirmrecheck);
						ConfirmAlert(msg, getResources().getString(R.string.thirdstage));
					}

				}
			}
				break;

			case R.id.chkpph:
				if (aq.id(R.id.chkpph).isChecked()) {
					aq.id(R.id.tr_pph2).visible();
					pph = 1;
				} else {
					aq.id(R.id.tr_pph2).gone();
					pph = 0;
					// 17Nov2016 Arpitha
					aq.id(R.id.rdatonic).checked(false);
					aq.id(R.id.rdtraumatic).checked(false);
					aq.id(R.id.rdcombined).checked(false);
					pphproperties = 0;// 17Nov2016 Arpitha
				}
				break;
			case R.id.chkreatinedplacenta:
				if (aq.id(R.id.chkreatinedplacenta).isChecked()) {
					retained_placenta = 1;
				} else {
					retained_placenta = 0;
				}
				break;
			case R.id.chkinversion:
				if (aq.id(R.id.chkinversion).isChecked()) {
					inversion = 1;
				} else {
					inversion = 0;
				}
				break;
			case R.id.rdoxytocinnone:
				oxytocinunit = 0;
				break;
			case R.id.rd10units1:
				oxytocinunit = 1;
				break;
			case R.id.rd5units:
				oxytocinunit = 2;
				break;
			case R.id.rd125:
				pgf2alpha = 1;
				break;
			case R.id.rd250:
				pgf2alpha = 2;
				break;
			case R.id.rdcomplete:
				placenta = 1;
				break;
			case R.id.rdincomplete:
				placenta = 2;
				break;
			case R.id.rduterineyes:
				uterine_massage = 1;
				break;
			case R.id.rduterineno:
				uterine_massage = 2;
				break;
			case R.id.rdatonic:
				pphproperties = 1;
				break;
			case R.id.rdtraumatic:
				pphproperties = 2;
				break;
			case R.id.rdcombined:
				pphproperties = 3;
				break;

			default:
				break;
			}
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
	}

	private void savethirdstagedata() throws Exception {

		// 27Sep2016 Arpitha
		if (Partograph_CommonClass.autodatetime(getActivity())) {

			Date lastregdate = null;
			String strlastinserteddate = dbh.getlastmaxdate(strwuserId, getResources().getString(R.string.partograph));
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			if (strlastinserteddate != null) {
				lastregdate = format.parse(strlastinserteddate);
			}
			Date currentdatetime = new Date();
			if (lastregdate != null && currentdatetime.before(lastregdate)) {
				Partograph_CommonClass.showSettingsAlert(getActivity(),
						getResources().getString(R.string.plz_enable_automatic_date_and_set_current_date));
			} else {// 27Sep2016 Arpitha

				ArrayList<String> tdata = new ArrayList<String>();
				tdata.add("" + aq.id(R.id.etdurationofthirdstage).getText().toString());
				tdata.add("" + oxytocinunit);
				tdata.add("" + aq.id(R.id.etergometrine).getText().toString());
				tdata.add("" + pgf2alpha);
				tdata.add("" + aq.id(R.id.etmisoprost).getText().toString());
				tdata.add("" + placenta);
				tdata.add("" + uterine_massage);
				tdata.add("" + aq.id(R.id.etebl).getText().toString());
				tdata.add("" + pph);
				tdata.add("" + pphproperties);
				tdata.add("" + retained_placenta);
				tdata.add("" + inversion);
				tdata.add(aq.id(R.id.etcommentsthirdstage).getText().toString());

				// 19jan2016
				SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
				String currentDateandTime = sdf.format(new Date());
				tdata.add(currentDateandTime);

				ArrayList<String> prop_id = new ArrayList<String>();
				ArrayList<String> prop_name = new ArrayList<String>();
				Property_pojo pdata = new Property_pojo();
				Cursor cursor = dbh.getPropertyIds(obj_Id);

				if (cursor.getCount() > 0) {
					cursor.moveToFirst();

					do {

						prop_id.add(cursor.getString(0));
						prop_name.add(cursor.getString(1));

					} while (cursor.moveToNext());

					pdata.setObj_Id(obj_Id);
					pdata.setProp_id(prop_id);
					pdata.setProp_name(prop_name);
					pdata.setProp_value(tdata);

					// 20jan2016
					// pdata.setStrdate(selecteddate);02Nov2016 Arpitha
					pdata.setStrdate(strthirdstagedate);
					pdata.setStrTime(aq.id(R.id.ettimeofthirdstage).getText().toString());

					// 05May2017 Arpitha -v2.6 - mantis id 0000241
					if (Integer.parseInt(aq.id(R.id.etdurationofthirdstage).getText().toString()) > 30
							|| (aq.id(R.id.etebl).getText().toString().trim().length() > 0
									&& Integer.parseInt(aq.id(R.id.etebl).getText().toString()) > 1000))
						pdata.setIsDangerval1(1);
					else
						pdata.setIsDangerval1(0); // 05May2017 Arpitha -v2.6 -
													// mantis id 0000241

					dbh.db.beginTransaction();
					int transId = dbh.iCreateNewTrans(strwuserId);

					boolean isAdded = dbh.insertData(pdata, strWomenid, strwuserId, transId);

					if (isAdded) {
						dbh.iNewRecordTrans(strwuserId, transId, Partograph_DB.TBL_PROPERTYVALUES);
						commitTrans();
					} else {
						rollbackTrans();
					}
				}

			}
		}
	}

	/**
	 * Validate mandatory fields and date validations
	 * 
	 * @return true if validated else false
	 */
	private boolean validateThirdStageofLabor() {

		try {

			if (aq.id(R.id.ettimeofthirdstage).getText().length() >= 1) {
				String date1, date2;
				String reg_date = null;
				date1 = woman.getDel_Date() + " " + woman.getDel_Time();
				date2 = strthirdstagedate + " " + aq.id(R.id.ettimeofthirdstage).getText().toString();
				// aq.id(R.id.ettimeofthirdstage).getEditText().requestFocus();
				if (!isTimevalid(date1, date2)) {

					String delDate = Partograph_CommonClass.getConvertedDateFormat(woman.getDel_Date(),
							Partograph_CommonClass.defdateformat);// 25April2017
																	// Arpitha

					Toast.makeText(getActivity(),
							getResources().getString(R.string.invalidthirdstagetime) + " " + delDate + " "
									+ woman.getDel_Time() + " " + getResources().getString(R.string.please_try_again),
							Toast.LENGTH_LONG).show();
					return false;
				}

				// 23August2016 Arpitha
				String currenttim = Partograph_CommonClass.getTodaysDate() + " "
						+ Partograph_CommonClass.getCurrentTime();
				// String reg_date = woman.getDate_of_admission() + " " +
				// woman.getTime_of_admission();

				if (woman.getregtype() != 2)// 27Feb2017 Arpitha
					reg_date = woman.getDate_of_admission() + " " + woman.getTime_of_admission();
				if (reg_date != null && date2 != null)// 27Feb2017 Arpitha
				{
					if ((!isTimevalid(reg_date, date2)) && woman.getregtype() != 2) {

						String regDate = Partograph_CommonClass.getConvertedDateFormat(woman.getDate_of_admission(),
								Partograph_CommonClass.defdateformat);// 25April2017
																		// Arpitha

						Toast.makeText(getActivity(), getResources().getString(R.string.comparedateofdurationtoreg)
								+ " " + regDate + " " + woman.getTime_of_admission(), Toast.LENGTH_LONG).show();
						return false;
					}
				}
				if (refdatetime != null) {
					if (!isTimevalid(refdatetime, date2)) {

						String refDate = Partograph_CommonClass.getConvertedDateFormat(refdatetime.substring(0, 10),
								Partograph_CommonClass.defdateformat);// 25April2017
																		// Arpitha

						Toast.makeText(getActivity(),
								getResources().getString(R.string.thirdstage_cannot_be_before_refdate) + " " + refDate
										+ " " + refdatetime.substring(11),
								Toast.LENGTH_LONG).show();
						return false;
					}
				}
				if (strlastentry != null) {
					if (!isTimevalid(strlastentry, date2)) {

						String lastEntryDate = Partograph_CommonClass.getConvertedDateFormat(strlastentry,
								Partograph_CommonClass.defdateformat);// 25April2017
																		// Arpitha

						Toast.makeText(getActivity(),
								getResources().getString(R.string.thirdstage_cannot_be_before_lastpartoentry) + " "
										+ lastEntryDate + " " + strlastentry.substring(11),
								Toast.LENGTH_LONG).show();
						return false;
					}
				}

				if (!isTimevalid(date2, currenttim)) {
					Toast.makeText(getActivity(), getResources().getString(R.string.curr_date_validation),
							Toast.LENGTH_LONG).show();
					return false;
				}
			}
			// } // 20Oct2016 Arpitha11Nov2016 Arpitha

			if (aq.id(R.id.etdurationofthirdstage).getText().toString().length() <= 0) {
				Toast.makeText(getActivity(), getResources().getString(R.string.enterthirdstageduration),
						Toast.LENGTH_SHORT).show();
				aq.id(R.id.etdurationofthirdstage).getEditText().requestFocus();
				return false;
			}

			// Bindu - 10Aug2016 - validate duration value
			if (aq.id(R.id.etdurationofthirdstage).getText().toString().length() > 0) {
				int val = Integer.parseInt(aq.id(R.id.etdurationofthirdstage).getText().toString());
				if (val == 0) {
					Toast.makeText(getActivity(), getResources().getString(R.string.thirdstagevalidduration),
							Toast.LENGTH_SHORT).show();
					aq.id(R.id.etdurationofthirdstage).getEditText().requestFocus();
					return false;
				}

			}
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}

		return true;
	}

	/** Method to rollback the trnsaction */
	private void rollbackTrans() throws Exception {
		dbh.db.endTransaction();
		// dbh.db.close();
		Toast.makeText(getActivity(), getResources().getString(R.string.failed), Toast.LENGTH_SHORT).show();
	}

	/** Method to commit the trnsaction */
	private void commitTrans() throws Exception {

		dbh.db.setTransactionSuccessful();
		dbh.db.endTransaction();
		// dbh.db.close();
		// updated on 19Nov2015
		Partograph_CommonClass.isRecordExistsForSync = true;
		Partograph_CommonClass.refreshActionBarMenu(getActivity());
		calSyncMtd();
		Toast.makeText(getActivity(), getResources().getString(R.string.success), Toast.LENGTH_SHORT).show();
		setThirdStageDetails();
	}

	// Sync
	private void calSyncMtd() throws Exception {
		SyncFunctions.syncUptoDate = false;
		Partograph_CommonClass.responseAckCount = 0;
		Partograph_CommonClass.AsyncCallWS task = new Partograph_CommonClass.AsyncCallWS();
		task.execute();
	}

	private void caldatepicker() {
		DialogFragment newFragment = new SelectDateFragment();
		newFragment.show(this.getFragmentManager(), "DatePicker");
	}

	@SuppressLint("ValidFragment")
	public class SelectDateFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
		@TargetApi(Build.VERSION_CODES.HONEYCOMB)
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			Calendar calendar = Calendar.getInstance();

			Date d = null;
			d = dbh.getDateS(selecteddate == null ? todaysDate : selecteddate);
			calendar.setTime(d);
			year = calendar.get(Calendar.YEAR);
			mon = calendar.get(Calendar.MONTH);
			day = calendar.get(Calendar.DAY_OF_MONTH);

			DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, year, mon, day);

			/* remove calendar view */
			dialog.getDatePicker().setCalendarViewShown(false);

			/* Spinner View */
			dialog.getDatePicker().setSpinnersShown(true);

			dialog.setTitle(Html.fromHtml("<font color='" + getResources().getColor(R.color.appcolor) + "'>"
					+ getResources().getString(R.string.pickadate) + "</font>"));
			return dialog;
		}

		public void onDateSet(DatePicker view, int yy, int mm, int dd) {
			if (view.isShown()) {
				try {
					populateSetDate(yy, mm + 1, dd);
				} catch (Exception e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					e.printStackTrace();
				}
			}
		}
	}

	public void populateSetDate(int year, int month, int day) throws Exception {

		// changed on 09 mar 2015
		selecteddate = String.valueOf(year) + "-" + String.format("%02d", month) + "-" + String.format("%02d", day);

		try {

			Date regdate = null;
			Date taken = new SimpleDateFormat("yyyy-MM-dd").parse(selecteddate);
			if (woman.getDate_of_admission() != null)// 25Feb2017 Arpitha
				regdate = new SimpleDateFormat("yyyy-MM-dd").parse(woman.getDate_of_admission());
			else// 25Feb2017 Arpitha
				regdate = new SimpleDateFormat("yyyy-MM-dd").parse(woman.getDate_of_reg_entry());// 25Feb2017
																									// Arpitha

			
			// 20Sep2017 Arpitha
						if (strpostpartumdatetime != null && strpostpartumdatetime.trim().length() > 0) {
							postpartumdatetime = new SimpleDateFormat("yyyy-MM-dd").parse(strpostpartumdatetime);
						} // 20Sep2017 Arpitha
						
			// 23Sep2016 Arpitha
			Date deldate = new SimpleDateFormat("yyyy-MM-dd").parse(woman.getDel_Date());

			// 23August2016 Arpitha
			Date lastentry = null;
			Date refdate = null;
			if (strlastentry != null) {
				lastentry = new SimpleDateFormat("yyyy-MM-dd").parse(strlastentry);
			}
			if (refdatetime != null) {
				refdate = new SimpleDateFormat("yyyy-MM-dd").parse(refdatetime);
			}

			// 20Oct2016 Arpitha
			/*
			 * if (woman.getregtype() == 2) { if (taken.after(new Date())) {
			 * Toast.makeText(getActivity(),
			 * getResources().getString(R.string.curr_date_validation),
			 * Toast.LENGTH_SHORT).show();
			 * aq.id(R.id.etdateofthirdstage).text(Partograph_CommonClass.
			 * getConvertedDateFormat(todaysDate,
			 * Partograph_CommonClass.defdateformat)); strthirdstagedate =
			 * todaysDate; } else if (taken.before(deldate)) {
			 * Toast.makeText(getActivity(),
			 * getResources().getString(R.string.comparedateofdurationtodeldate)
			 * + " " + woman.getDel_Date(), Toast.LENGTH_SHORT).show();
			 * aq.id(R.id.etdateofthirdstage).text(Partograph_CommonClass.
			 * getConvertedDateFormat(todaysDate,
			 * Partograph_CommonClass.defdateformat)); strthirdstagedate =
			 * todaysDate; } else if (refdate != null && taken.before(refdate))
			 * { Toast.makeText(getActivity(),
			 * getResources().getString(R.string.
			 * thirdstage_cannot_be_before_refdate) + " " + refdatetime,
			 * Toast.LENGTH_SHORT).show();
			 * aq.id(R.id.etdateofthirdstage).text(Partograph_CommonClass.
			 * getConvertedDateFormat(todaysDate,
			 * Partograph_CommonClass.defdateformat)); strthirdstagedate =
			 * todaysDate;// 02Nov2016 Arpitha } else {
			 * aq.id(R.id.etdateofthirdstage).text(Partograph_CommonClass.
			 * getConvertedDateFormat(selecteddate,
			 * Partograph_CommonClass.defdateformat)); strthirdstagedate =
			 * selecteddate;
			 * 
			 * }
			 * 
			 * } else {// 20Oct2016 Arpitha
			 */ // 11Nov2016 Arpitha
			if (taken.after(new Date())) {
				Toast.makeText(getActivity(), getResources().getString(R.string.curr_date_validation),
						Toast.LENGTH_SHORT).show();
				aq.id(R.id.etdateofthirdstage).text(Partograph_CommonClass.getConvertedDateFormat(todaysDate,
						Partograph_CommonClass.defdateformat));
				strthirdstagedate = todaysDate;
			}

			else if (regdate != null && taken.before(regdate) && woman.getregtype() != 2) {
				String admDate = Partograph_CommonClass.getConvertedDateFormat(woman.getDate_of_admission(),
						Partograph_CommonClass.defdateformat);// 25April2017
																// Arpitha
				Toast.makeText(getActivity(),
						getResources().getString(R.string.comparedateofdurationtoreg) + " " + admDate,
						Toast.LENGTH_SHORT).show();
				aq.id(R.id.etdateofthirdstage).text(Partograph_CommonClass.getConvertedDateFormat(todaysDate,
						Partograph_CommonClass.defdateformat));
				strthirdstagedate = todaysDate;

			} else if (taken.before(deldate)) {
				String delDate = Partograph_CommonClass.getConvertedDateFormat(woman.getDel_Date(),
						Partograph_CommonClass.defdateformat);// 25April2017
																// Arpitha
				Toast.makeText(getActivity(),
						getResources().getString(R.string.comparedateofdurationtodeldate) + " " + delDate,
						Toast.LENGTH_SHORT).show();
				aq.id(R.id.etdateofthirdstage).text(Partograph_CommonClass.getConvertedDateFormat(todaysDate,
						Partograph_CommonClass.defdateformat));
				strthirdstagedate = todaysDate;
			} // 23August2016 by Arpitha
			else if (lastentry != null && taken.before(lastentry)) {
				String partoDate = Partograph_CommonClass.getConvertedDateFormat(strlastentry,
						Partograph_CommonClass.defdateformat);// 25April2017
																// Arpitha
				Toast.makeText(getActivity(),
						getResources().getString(R.string.thirdstage_cannot_be_before_lastpartoentry) + " " + partoDate,
						Toast.LENGTH_SHORT).show();
				aq.id(R.id.etdateofthirdstage).text(Partograph_CommonClass.getConvertedDateFormat(todaysDate,
						Partograph_CommonClass.defdateformat));

			}

			// 20Sep2017 Arpitha
			else if (postpartumdatetime != null && taken.before(postpartumdatetime)) {

				String latdate = Partograph_CommonClass.getConvertedDateFormat(strpostpartumdatetime.split("/")[0],
						Partograph_CommonClass.defdateformat);
				Toast.makeText(getActivity(),
						getResources().getString(R.string.thirdstage_datetime_cannot_be_before_postpartum_datetime)
								+ " - " + latdate ,
						Toast.LENGTH_SHORT).show();

				aq.id(R.id.etdateofthirdstage).text(Partograph_CommonClass.getConvertedDateFormat(todaysDate,
						Partograph_CommonClass.defdateformat));
				strthirdstagedate = todaysDate;// 02Nov2016 Arpitha

			} // 20Sep2017 Arpitha

			// 23August2016 Arpitha
			else if (refdate != null && taken.before(refdate)) {
				String refDate = Partograph_CommonClass.getConvertedDateFormat(refdatetime.substring(0, 10),
						Partograph_CommonClass.defdateformat);// 25April2017
																// Arpitha
				Toast.makeText(getActivity(), getResources().getString(R.string.thirdstage_cannot_be_before_refdate)
						+ " " + refDate + " " + refdatetime.substring(11), Toast.LENGTH_SHORT).show();
				aq.id(R.id.etdateofthirdstage).text(Partograph_CommonClass.getConvertedDateFormat(todaysDate,
						Partograph_CommonClass.defdateformat));
				strthirdstagedate = todaysDate;// 02Nov2016 Arpitha

			} else {
				aq.id(R.id.etdateofthirdstage).text(Partograph_CommonClass.getConvertedDateFormat(selecteddate,
						Partograph_CommonClass.defdateformat));
				strthirdstagedate = selecteddate;

				// 23Sep2016 - Calculate duration of 3rd Stage automatically
				// from thrid stage date/time and del date/time

				thirdstage_datetime = strthirdstagedate + "_" + Partograph_CommonClass.getCurrentTime();
				int hours = Partograph_CommonClass.getHoursBetDates(thirdstage_datetime, del_datetime);
				int duration = hours * 60;
				// aq.id(R.id.etdurationofthirdstage).text("" + duration);
				// commented on 18Feb2017 Arpitha

			}

			thirdstagedate = new SimpleDateFormat("yyyy-MM-dd").parse(strthirdstagedate);
			// } // 20Oct2016 Arpitha

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
	}

	public void showtimepicker() {
		DialogFragment newFragment = new TimePickerFragment();
		newFragment.show(this.getFragmentManager(), "timePicker");
	}

	public static class TimePickerFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener {
		public TimePickerFragment() {
		}

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {

			return new TimePickerDialog(getActivity(), this, hour, minute, DateFormat.is24HourFormat(getActivity()));
		}

		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

			try {
				// 23August2016 Arpitha
				String selecteddate;
				String date = aq.id(R.id.etdateofthirdstage).getText().toString();
				if (strthirdstagedate != null) {
					selecteddate = strthirdstagedate + " " + ("" + hourOfDay) + ":" + ("" + minute);
				} else {
					selecteddate = todaysDate + " " + ("" + hourOfDay) + ":" + ("" + minute);
				}
				Date seltime = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(selecteddate);
				// 23August2016 Arpitha
				Date lastentry = null;
				Date refdate = null;
				if (strlastentry != null) {
					lastentry = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(strlastentry);
				}
				if (refdatetime != null) {
					refdate = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(refdatetime);
				}
				
				// 20Sep2017 Arpitha
				if (strpostpartumdatetime != null && strpostpartumdatetime.trim().length() > 0) {
					postpartumdatetime = new SimpleDateFormat("yyyy-MM-dd/HH:mm").parse(strpostpartumdatetime);
				} // 20Sep2017 Arpitha

				if (seltime.after(new Date())) {
					Toast.makeText(getActivity(), getResources().getString(R.string.curr_date_validation),
							Toast.LENGTH_LONG).show();
				} else if (seltime.before(regdate) && woman.getregtype() != 2) {

					String admDate = Partograph_CommonClass.getConvertedDateFormat(woman.getDate_of_admission(),
							Partograph_CommonClass.defdateformat);// 25April2017
																	// Arpitha

					Toast.makeText(getActivity(), getResources().getString(R.string.comparedateofdurationtoreg) + " "
							+ admDate + " " + woman.getTime_of_admission(), Toast.LENGTH_LONG).show();
				} else if (seltime.before(deldate)) {

					String delDate = Partograph_CommonClass.getConvertedDateFormat(woman.getDel_Date(),
							Partograph_CommonClass.defdateformat);// 25April2017
																	// Arpitha

					Toast.makeText(getActivity(), getResources().getString(R.string.comparedateofdurationtodeldate)
							+ " " + delDate + " " + woman.getDel_Time(), Toast.LENGTH_LONG).show();
				} else if (refdate != null && seltime.before(refdate)) {

					String refDate = Partograph_CommonClass.getConvertedDateFormat(refdatetime.substring(0, 10),
							Partograph_CommonClass.defdateformat);// 25April2017
																	// Arpitha

					Toast.makeText(getActivity(), getResources().getString(R.string.thirdstage_cannot_be_before_refdate)
							+ " " + refDate + " " + refdatetime.substring(11), Toast.LENGTH_LONG).show();
				} else if (lastentry != null && seltime.before(lastentry)) {

					String lastEntryDate = Partograph_CommonClass.getConvertedDateFormat(strlastentry.substring(0, 10),
							Partograph_CommonClass.defdateformat);// 25April2017
																	// Arpitha

					Toast.makeText(getActivity(),
							getResources().getString(R.string.thirdstage_cannot_be_before_lastpartoentry) + " "
									+ lastEntryDate + " " + strlastentry.substring(11),
							Toast.LENGTH_LONG).show();
				}

				// 20Sep2017 Arpitha
				else if (postpartumdatetime != null && seltime.before(postpartumdatetime)) {

					String latdate = Partograph_CommonClass.getConvertedDateFormat(strpostpartumdatetime.split("/")[0],
							Partograph_CommonClass.defdateformat);
					Toast.makeText(getActivity(),
							getResources().getString(R.string.thirdstage_datetime_cannot_be_before_postpartum_datetime)
									+ " - " + latdate + " " + strpostpartumdatetime.split("/")[1],
							Toast.LENGTH_SHORT).show();

				} // 20Sep2017 Arpitha

				else {
					aq.id(R.id.ettimeofthirdstage).text(
							new StringBuilder().append(padding_str(hourOfDay)).append(":").append(padding_str(minute)));

					if (strthirdstagedate != null) {

						thirdstage_datetime = strthirdstagedate + "_"
								+ aq.id(R.id.ettimeofthirdstage).getText().toString();
					} else
						thirdstage_datetime = todaysDate + "_" + aq.id(R.id.ettimeofthirdstage).getText().toString();
					int hours = Partograph_CommonClass.getHoursBetDates(thirdstage_datetime, del_datetime);
					int duration = hours * 60;
					// aq.id(R.id.etdurationofthirdstage).text("" +
					// duration);commented on 18Feb2017 Arpitha
				}
				// } // 20Oct2016 Arpitha commented on 11Nov2016 Arpitha

			} catch (Exception e) {
				AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
						+ this.getClass().getSimpleName(), e);
				e.printStackTrace();
			}
		}
	}

	private static String padding_str(int c) throws Exception {
		if (c >= 10)
			return String.valueOf(c);
		else
			return "0" + String.valueOf(c);
	}

	// updated on 13nov2015
	@SuppressLint("SimpleDateFormat")
	private boolean isTimevalid(String date1, String date2) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Date dateofreg, dateofdelivery;
		try {
			dateofreg = sdf.parse(date1);
			dateofdelivery = sdf.parse(date2);

			if (dateofdelivery.before(dateofreg)) {

				return false;
			} else
				return true;

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);

			e.printStackTrace();
			return false;
		}
	}

	// 16Aug2016 - bindu
	public void doWork() throws Exception {

		if (getActivity() != null) {
			getActivity().runOnUiThread(new Runnable() {
				public void run() {
					try {
						String curTime = Partograph_CommonClass.getCurrentTime();
						aq.id(R.id.ettimeofthirdstage).text(curTime);

					} catch (Exception e) {
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}
				}
			});
		}
	}

	class CountDownRunner implements Runnable {
		// @Override
		public void run() {
			while (!Thread.currentThread().isInterrupted()) {
				try {

					doWork();
					Thread.sleep(1000); // Pause of 1 Second

				} catch (InterruptedException e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					e.printStackTrace();
					Thread.currentThread().interrupt();
					myThread.stop();
				} catch (Exception e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					e.printStackTrace();
					myThread.stop();
				}
			}
		}
	}

	/**
	 * updated bindu - 25Aug2016 Save Confirmation Alert
	 * 
	 * @param goToScreen
	 */
	private void ConfirmAlert(String msg, final String goToScreen) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

		// set dialog message
		alertDialogBuilder.setMessage(msg).setCancelable(false)
				.setNegativeButton(getResources().getString(R.string.recheck), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {

						if (goToScreen.equalsIgnoreCase(getResources().getString(R.string.thirdstage))) {
							try {
								aq.id(R.id.etdateofthirdstage).getEditText().requestFocus();
							} catch (Exception e) {
								e.printStackTrace();
							}
						}

						dialog.cancel();
					}
				}).setPositiveButton(getResources().getString(R.string.save), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						try {
							if (goToScreen.equalsIgnoreCase(getResources().getString(R.string.thirdstage))) {
								savethirdstagedata();
								dialog.cancel();
							} else
								dialog.cancel();
						} catch (Exception e) {
							AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
									+ this.getClass().getSimpleName(), e);

							e.printStackTrace();
						}

					}
				});
		AlertDialog alertDialog1 = alertDialogBuilder.create();
		alertDialog1.show();
	}

	// 22Sep2016 Arpitha
	private void displayConfirmationAlert() throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		final Dialog dialog = new Dialog(getActivity());
		dialog.setTitle(Html.fromHtml("<font color='" + getResources().getColor(R.color.appcolor) + "'>"
				+ getResources().getString(R.string.warning) + "</font>"));
		dialog.setContentView(R.layout.temp_alertdialog);

		dialog.show();

		final TextView txtdialog = (TextView) dialog.findViewById(R.id.txtpulse);
		Button imgbtnyes = (Button) dialog.findViewById(R.id.imgbtnyes);
		Button imgbtnno = (Button) dialog.findViewById(R.id.imgbtnno);
		// Arpitha 27may16
		final TextView txtdialog1 = (TextView) dialog.findViewById(R.id.txtpulseval);

		final TextView txtdialog6 = (TextView) dialog.findViewById(R.id.txt);

		// 04Oct2016 ARpitha
		String alertmsg = "";
		String values = "";

		// 23Sep2016 Arpitha
		if (thirdstage_duration > 30) {
			alertmsg = alertmsg + getResources().getString(R.string.duration_third_stage_is) + "\n";
			values = values + "" + thirdstage_duration + getResources().getString(R.string.minutes) + "\n";

		}

		if (ergometrine > 4) {
			alertmsg = alertmsg + getResources().getString(R.string.ergometrine_val_is) + "\n";
			values = values + "" + ergometrine + "\n";

		}

		if (misoprost > 0 && (misoprost < 100 || misoprost > 1200)) {
			alertmsg = alertmsg + getResources().getString(R.string.misoprost_val_is) + "\n";
			values = values + "" + misoprost + "\n";

		}

		// 22Sep2016 Arpitha
		if (ebl > 0 && (ebl < 300 || ebl > 3000)) {
			alertmsg = alertmsg + getResources().getString(R.string.ebl_val_is) + "\n";
			values = values + "" + ebl + "\n";

		}

		txtdialog6.setText(getResources().getString(R.string.seems_to_be_abnormal_recheck_or_save));

		txtdialog.setText(alertmsg);
		txtdialog1.setText(values);

		imgbtnyes.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				try {
					dialog.cancel();

					// updated bindu - 25Aug2016 - check confirmation of third
					// stage date and time
					String msg = getResources().getString(R.string.thirdstagedatetimerecheck)
							+ aq.id(R.id.etdateofthirdstage).getText().toString() + " / "
							+ aq.id(R.id.ettimeofthirdstage).getText().toString()
							+ getResources().getString(R.string.confirmrecheck);
					ConfirmAlert(msg, getResources().getString(R.string.thirdstage));

				} catch (Exception e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					e.printStackTrace();
				}

			}
		});

		imgbtnno.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.cancel();

			}
		});

		int dividerId = dialog.getContext().getResources().getIdentifier("android:id/titleDivider", null, null);
		View divider = dialog.findViewById(dividerId);
		divider.setBackgroundColor(getResources().getColor(R.color.appcolor));// 08Feb2017
		dialog.show();
	}

	// 10April2017 Arpitha
	// setInput Filters
	private void setInputFiltersForEdittext() throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		aq.id(R.id.etcommentsthirdstage).getEditText().setFilters(new InputFilter[] { filter });

	}

	// To avoid special characters in Input type
	public static InputFilter filter = new InputFilter() {
		@Override
		public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
			// String blockCharacterSet =
			// "~#^|$%*!@/()-'\":;?{}=!$^';?Ã—Ã·<>{}â‚¬Â£Â¥â‚©%&+*.[]1234567890Â¶";
			String blockCharacterSet = "~#^|$%*!@/()-'\":;?{}=!$^';?Ã—Ã·<>{}â‚¬Â£Â¥â‚©%&+*[]~#^|$%&*!`@_=?;]}{[()\"\\Â¥Â®Â¿ÃŒâ„¢â‚¹Â°^âˆšÏ€Ã·Ã—â–³Â¶Â£â€¢Â¢â‚¬â™¥â™¡â˜…â˜†â–²â–¼â†‘â†�â†“â†’Â¤â–³â™‚â™€â„ƒ||â–³Â©c/o||Â¿Â¡â„…â„¢Â®â‚¹Â°Â¢`â€¢âˆšÏ€Â¶âˆ†Â¢Â°âˆ†Â¶Ï€âˆšâ€¢`";
			if (source != null && blockCharacterSet.contains(("" + source))) {
				return "";
			}
			return null;
		}
	};
}
