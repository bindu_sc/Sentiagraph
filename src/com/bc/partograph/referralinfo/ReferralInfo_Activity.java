package com.bc.partograph.referralinfo;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;

import com.bc.partograph.comments.Comments_Activity;
import com.bc.partograph.common.AppContext;
import com.bc.partograph.common.KillAllActivitesAndGoToLogin;
import com.bc.partograph.common.MessageLogPojo;
import com.bc.partograph.common.MultiSelectionSpinner;
import com.bc.partograph.common.Partograph_CommonClass;
import com.bc.partograph.common.Partograph_DB;
import com.bc.partograph.common.SendSMS;
import com.bc.partograph.deliveryinfo.DeliveryInfo_Activity;
import com.bc.partograph.latentphase.LatentPhase_Activity;
import com.bc.partograph.login.LoginActivity;
import com.bc.partograph.settings.About;
import com.bc.partograph.settings.Settings_parto;
import com.bc.partograph.sliding.GraphInformation;
import com.bc.partograph.sliding.SlidingActivity;
import com.bc.partograph.stagesoflabor.StageofLabor;
import com.bc.partograph.womenview.Activity_WomenView;
import com.bc.partograph.womenview.Fragment_DIP;
import com.bluecrimson.additionalDetails.Summary_Activity;
import com.bluecrimson.apgar.Activity_Apgar;
import com.bluecrimson.dischargedetails.DiscargeDetails_Activity;
import com.bluecrimson.dischargedetails.DiscgargePojo;
import com.bluecrimson.dischargedetails.DischargedWomanList_Activity;
import com.bluecrimson.partograph.R;
import com.bluecrimson.partograph.WomenReferral_pojo;
import com.bluecrimson.partograph.Women_Profile_Pojo;
import com.bluecrimson.postpartumcare.PostPartumCare_Activity;
import com.bluecrimson.usermanual.UseManual;
import com.bluecrimson.viewprofile.ViewProfile_Activity;
import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.ColumnText;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources.NotFoundException;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import harmony.java.awt.Color;

public class ReferralInfo_Activity extends FragmentActivity implements OnClickListener {

	EditText etplaceofreferral, etreasonforreferral, etdateofreferral;
	Button btnCancelRef, btnSaveRef;
	// boolean isDateRefClicked = false;
	static EditText ettimeofreferral;
	static boolean isTimeRefClicked = false;
	Spinner spnreferralfacility;
	int referralfacility;
	RadioButton rd_motherconscious, rd_motherunconscious, rd_babyalive, rd_babydead;
	int conditionofmother = 1, conditionofbaby = 1;
	EditText etheatrate, etspo2;
	String selecteddate;
	TableRow trspo2, trheartrate;
	WomenReferral_pojo wrefpojo;
	static String strRefDate;
	MultiSelectionSpinner mspnreasonforreferral;
	LinkedHashMap<String, Integer> reasonforreferralMap;
	ArrayList<WomenReferral_pojo> womenrefpojoItems;
	EditText etdescriptionofreferral, etbpsystolicreferral, etbpdiastolicreferral, etpulsereferral;
	TextView txtrefmode;
	private static int hour;
	private static int minute;
	int year, mon, day;
	TextView txtwname, txtwage, txtwdoa, txtwtoa, txtwgest, txtwgravida, txtwrisk, txtwdeltime, txtwdeldate,
			txtwdelstatus;
	boolean isMessageLogsaved = false;
	String todaysDate, womenId, wUserId;
	Partograph_DB dbh;
	int option;
	String strRegdate, strlastentrytime;
	String refdate;
	String bp;
	EditText etmesss;
	ArrayList<WomenReferral_pojo> rpojo;
	String admResDisplay = "";
	ArrayList<String> values;
	String p_no = "";
	int heartrate;
	int spo2;
	int bpsysval = 0, bpdiaval = 0, pulval = 0;
	static final int TIME_DIALOG_ID = 999;
	public static boolean isdip = false;// 23oct2016 Arpitha
	public static boolean isdelivered = false;// 23oct2016 Arpitha
	Date deltime = null;
	String strdeltime = null;
	Women_Profile_Pojo woman;// 03Nov2016

	ImageView imgpdf;
	private static Font small = new Font(Font.HELVETICA, 10, Font.BOLD);// 03Feb2017
	// Arpitha

	TextView txtdialogtitle;
	File file;
	String path;

	LinearLayout llbaby;// 20March2017 Arpitha

	TextView txtdisabled;// 10Aug2017 Arpitha
	RelativeLayout relRef;// 10Aug2017 Arpitha
	public static int ref;
	public static boolean isreferral;

	String strlatdatetime;// 20Sep2017 Arpitha
	Date latDatetime;// 20Sep2017 Arpitha
	String strpostpartumdatetime;// 20Sep2017 Arpitha
	Date postpartumdatetime;// 20Sep2017 Arpitha

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_reference);
		try {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());

			woman = (Women_Profile_Pojo) getIntent().getSerializableExtra("woman");

			dbh = Partograph_DB.getInstance(getApplicationContext());

			// 05Nov2016 Arpitha
			Locale locale = null;

			ref = Partograph_CommonClass.curr_tabpos;// 03Sep2017 Arpitha
			isreferral = true;// 03Sep2017 Arpitha

			if (AppContext.prefs.getBoolean("isEnglish", false)) {
				locale = new Locale("en");
				Locale.setDefault(locale);
			} else if (AppContext.prefs.getBoolean("isHindi", false)) {
				locale = new Locale("hi");
				Locale.setDefault(locale);
			}

			this.setTitle(getResources().getString(R.string.referrral_info));

			Configuration config = new Configuration();
			config.locale = locale;
			getResources().updateConfiguration(config, getResources().getDisplayMetrics());

			AppContext.setNamesAccordingToRes(getResources());// 05Nov2016
																// Arpitha

			// 02nov2016 Arpitha
			// get the action bar
			ActionBar actionBar = getActionBar();

			// Enabling Back navigation on Action Bar icon
			actionBar.setDisplayHomeAsUpEnabled(true);// 02nov2016 Arpitha

			todaysDate = Partograph_CommonClass.getTodaysDate();
			option = 2;

			womenId = woman.getWomenId();
			wUserId = woman.getUserId();
			if (woman.getregtype() != 2)// 12Jan2017 Arpitha
			{
				strRegdate = woman.getDate_of_admission() + " " + woman.getTime_of_admission();
			}
			strlastentrytime = dbh.getlastentrytime(womenId, 0);

			AssignIdsToReferralWidgets();

			initialViewReferral();

			etdateofreferral.setText(Partograph_CommonClass.getConvertedDateFormat(
					Partograph_CommonClass.getTodaysDate(), Partograph_CommonClass.defdateformat));

			Fragment_DIP.istears = false;
			values = new ArrayList<String>();
			values = Partograph_CommonClass.getphonenumber(option);

			imgpdf.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					try {

						Partograph_CommonClass.pdfAlertDialog(ReferralInfo_Activity.this, woman, "referral", null,
								null);

						// displayAlertDialog();
						// createPdfDocument(0);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}

				}
			});

			etdateofreferral.setOnTouchListener(new View.OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if (MotionEvent.ACTION_UP == event.getAction()) {
						try {
							// isDateRefClicked = true;
							caldatepicker();
						} catch (Exception e) {
							AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
									+ this.getClass().getSimpleName(), e);
							e.printStackTrace();
						}
					}
					return true;
				}
			});

			/*
			 * if (woman.getDel_type() == 0) { isdip = true; isdelivered =
			 * false; } else { isdip = false; isdelivered = true; }
			 */// commented on 03April2017 Arpitha

			ettimeofreferral.setOnTouchListener(new View.OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if (MotionEvent.ACTION_UP == event.getAction()) {
						showDialog(TIME_DIALOG_ID);
					}
					return true;
				}
			});

			// Referred to facility
			spnreferralfacility.setOnItemSelectedListener(new OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> adapter, View arg1, int arg2, long arg3) {
					referralfacility = adapter.getSelectedItemPosition();
				}

				@Override
				public void onNothingSelected(AdapterView<?> arg0) {

				}
			});

			// 04jan2016
			rd_motherconscious.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					conditionofmother = 1;
				}
			});

			rd_motherunconscious.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					conditionofmother = 0;
				}
			});

			rd_babyalive.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					conditionofbaby = 1;
					etspo2.setVisibility(View.VISIBLE);
					etheatrate.setVisibility(View.VISIBLE);
					trspo2.setVisibility(View.VISIBLE);
					trheartrate.setVisibility(View.VISIBLE);
				}
			});

			rd_babydead.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					conditionofbaby = 0;
					etspo2.setVisibility(View.GONE);
					etheatrate.setVisibility(View.GONE);
					trspo2.setVisibility(View.GONE);
					trheartrate.setVisibility(View.GONE);
					etheatrate.setText("");// 29Sep2016 Arpitha
					etspo2.setText("");// 29Sep2016 Arpitha
				}
			});

			btnSaveRef.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {

					try {

						spo2 = 0;
						heartrate = 0;
						// 22Sep2016 Arpitha -
						if (etpulsereferral.getText().toString().length() > 0) {
							pulval = Integer.parseInt(etpulsereferral.getText().toString());
						}

						if (etbpsystolicreferral.getText().toString().length() > 0
								&& etbpdiastolicreferral.getText().toString().length() > 0) {
							bpsysval = Integer.parseInt(etbpsystolicreferral.getText().toString());
							bpdiaval = Integer.parseInt(etbpdiastolicreferral.getText().toString());
						}

						// 22Sep2016 Arpitha -
						if (etheatrate.getText().toString().trim().length() > 0) {
							heartrate = Integer.parseInt(etheatrate.getText().toString());
						}
						// 22Sep2016 Arpitha
						if (etspo2.getText().toString().trim().length() > 0) {
							spo2 = Integer.parseInt(etspo2.getText().toString());
						}

						refdate = (strRefDate == null ? todaysDate : strRefDate) + " "
								+ ettimeofreferral.getText().toString();

						if (validateReferralFields()) {

							// 27Sep2016 Arpitha
							if (Partograph_CommonClass.autodatetime(getApplicationContext())) {

								Date lastregdate = null;
								String strlastinserteddate = dbh.getlastmaxdate(wUserId,
										getResources().getString(R.string.referral));// 02Oct2016
																						// Arpitha
																						// -
																						// string
																						// value
																						// from
																						// strings.xml
								SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd_HHmmss");
								if (strlastinserteddate != null) {
									lastregdate = format.parse(strlastinserteddate);
								}
								Date currentdatetime = new Date();
								if (lastregdate != null && currentdatetime.before(lastregdate)) {
									Partograph_CommonClass.showSettingsAlert(getApplicationContext(), getResources()
											.getString(R.string.plz_enable_automatic_date_and_set_current_date));
								} else {// 27Sep2016 Arpitha

									// 22Sep2016 Arpitha
									// spo2 = 0;
									// heartrate = 0;
									if ((pulval > 120 || pulval < 50)
											|| ((bpsysval < 80 || bpsysval > 160) || (bpdiaval < 50 || bpdiaval >= 110))
											|| (heartrate > 0 && (heartrate < 50 || heartrate > 200))
											|| (spo2 > 0 && (spo2 < 40 || spo2 > 100)))

									{
										displayConfirmationAlert("", getResources().getString(R.string.confirmation));
									}

									else {
										// updated bindu - 25Aug2016 - check
										// confirmation of Ref date and time
										String msg = getResources().getString(R.string.refdatetimerecheck)
												+ etdateofreferral.getText().toString() + " / "
												+ ettimeofreferral.getText().toString()
												+ getResources().getString(R.string.confirmrecheck);
										ConfirmAlert(msg, getResources().getString(R.string.referral));// 02Oct2016
																										// Arpitha
																										// -
																										// string
																										// value
																										// from
																										// strings.xml
									}

								}
							}
						}
					} catch (Exception e) {
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);

						e.printStackTrace();
					}
				}
			});

			// 02Nov2016 Arpitha
			btnCancelRef.setText(getResources().getString(R.string.clear));
			btnCancelRef.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					try {
						displayConfirmationAlert(getResources().getString(R.string.clear_msg),
								getResources().getString(R.string.clear));
					} catch (NotFoundException e) {
						// TODO Auto-generated catch block
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}
				}
			});

			setInputFiltersForEdittext();// 10April2017 Arpitha

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
		KillAllActivitesAndGoToLogin.addToStack(this);

	}

	@Override
	public void onBackPressed() {
		try {
			displayConfirmationAlert("", getResources().getString(R.string.exit_msg));
		} catch (NotFoundException e) {
			// TODO Auto-generated catch block
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// 25Sep2016 Arpitha - addToTrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_womenlist, menu);

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		try {
			// 25Sep2016 Arpitha - addToTrace
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
			switch (item.getItemId()) {
			// Respond to the action bar's Up/Home button
			case android.R.id.home:
				NavUtils.navigateUpFromSameTask(this);
				return true;

			case R.id.logout:
				Intent login = new Intent(this, LoginActivity.class);
				startActivity(login);
				return true;

			case R.id.settings:
				if (Partograph_CommonClass.autodatetime(ReferralInfo_Activity.this)) {
					Intent settings = new Intent(this, Settings_parto.class);
					startActivity(settings);
				}
				return true;

			case R.id.about:

				Intent about = new Intent(this, About.class);
				startActivity(about);
				return true;

			// 13Oct2016 Arpitha
			case R.id.sms:
				if (AppContext.checkSimState() == TelephonyManager.SIM_STATE_READY) {
					Partograph_CommonClass.display_messagedialog(ReferralInfo_Activity.this, wUserId);
				} else
					Toast.makeText(getApplicationContext(), getResources().getString(R.string.no_sim),
							Toast.LENGTH_LONG).show();
				return true;

			// 13Oct2016 Arpitha
			case R.id.comments:
				Intent comments = new Intent(getApplicationContext(), Comments_Activity.class);
				startActivity(comments);
				return true;

			case R.id.home:
				Intent settings = new Intent(ReferralInfo_Activity.this, Activity_WomenView.class);
				startActivity(settings);
				return true;

			case R.id.info:
				Intent info = new Intent(ReferralInfo_Activity.this, GraphInformation.class);
				startActivity(info);
				return true;

			case R.id.usermanual:
				Intent usermanual = new Intent(ReferralInfo_Activity.this, UseManual.class);
				startActivity(usermanual);
				return true;
			case R.id.summary:
				Intent summm = new Intent(ReferralInfo_Activity.this, Summary_Activity.class);
				startActivity(summm);
				return true;

			case R.id.changepass:
				Partograph_CommonClass.dispalyChangePasswordDialog(ReferralInfo_Activity.this);

				return true;
			case R.id.disch:
				Intent disch = new Intent(ReferralInfo_Activity.this, DischargedWomanList_Activity.class);
				startActivity(disch);
				return true;

			// 03Oct2017 Arpitha
			case R.id.del:
				/*
				 * Intent disc = new Intent(ReferralInfo_Activity.this,
				 * DeliveryInfo_Activity.class); disc.putExtra("woman", woman);
				 * startActivity(disc);
				 */
				if (woman.getDel_type() != 0)// 21Oct2016
				// Arpitha
				{// 21Oct2016 Arpitha
					// 05Jan2016
					// / showDialogToUpdateStatus();
					Partograph_CommonClass.showDialogToUpdateStatus(ReferralInfo_Activity.this, woman);
					// loadWomendata(displayListCountddel);
					// if(adapter!=null)
					// adapter.notifyDataSetChanged();
					// loadWomendata(displayListCountddel);
				} else// 21Oct2016 Arpitha
				{

					Intent graph = new Intent(ReferralInfo_Activity.this, DeliveryInfo_Activity.class);
					graph.putExtra("woman", woman);
					startActivity(graph);
				} // 21Oct2016 Arpitha
				return true;
			// return true;
			case R.id.viewprofile:
				Intent view = new Intent(ReferralInfo_Activity.this, ViewProfile_Activity.class);
				view.putExtra("woman", woman);
				startActivity(view);
				return true;

			case R.id.ref:
				Intent ref = new Intent(ReferralInfo_Activity.this, ReferralInfo_Activity.class);
				ref.putExtra("woman", woman);
				startActivity(ref);
				return true;

			case R.id.pdf:
				Partograph_CommonClass.exportPDFs(ReferralInfo_Activity.this, woman);
				return true;

			case R.id.disc:
				Intent discharge = new Intent(ReferralInfo_Activity.this, DiscargeDetails_Activity.class);
				discharge.putExtra("woman", woman);
				startActivity(discharge);
				return true;

			case R.id.apgar:
				if (woman.getDel_type() != 0) {
					Intent apgar = new Intent(ReferralInfo_Activity.this, Activity_Apgar.class);
					apgar.putExtra("woman", woman);
					startActivity(apgar);
				} else
					Toast.makeText(getApplicationContext(),
							getResources().getString(R.string.msg_applicableafterdelivery), Toast.LENGTH_LONG).show();
				return true;

			case R.id.stages:
				if (woman.getDel_type() != 0) {
					Intent stages = new Intent(ReferralInfo_Activity.this, StageofLabor.class);
					stages.putExtra("woman", woman);
					startActivity(stages);
				} else
					Toast.makeText(getApplicationContext(),
							getResources().getString(R.string.msg_applicableafterdelivery), Toast.LENGTH_LONG).show();
				return true;

			case R.id.post:
				if (woman.getDel_type() != 0) {
					Intent post = new Intent(ReferralInfo_Activity.this, PostPartumCare_Activity.class);
					post.putExtra("woman", woman);
					startActivity(post);
				} else
					Toast.makeText(getApplicationContext(),
							getResources().getString(R.string.msg_applicableafterdelivery), Toast.LENGTH_LONG).show();
				return true;

			case R.id.lat:
				Intent lat = new Intent(ReferralInfo_Activity.this, LatentPhase_Activity.class);
				lat.putExtra("woman", woman);
				startActivity(lat);
				return true;

			case R.id.parto:
				Intent parto = new Intent(ReferralInfo_Activity.this, SlidingActivity.class);
				parto.putExtra("woman", woman);
				startActivity(parto);
				return true;// 03Oct2017 Arpitha
			}
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
		return super.onOptionsItemSelected(item);
	}

	/*
	 * * Called when invalidateOptionsMenu() is triggered
	 */
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// if nav drawer is opened, hide the action items
		menu.findItem(R.id.sync).setVisible(false);
		menu.findItem(R.id.registration).setVisible(false);

		if (!(AppContext.checkSimState() == TelephonyManager.SIM_STATE_READY)) {
			menu.findItem(R.id.sms).setVisible(false);
		}

		// menu.findItem(R.id.changepass).setVisible(false);// 03April2017
		// Arpitha

		menu.findItem(R.id.delete).setVisible(false);
		menu.findItem(R.id.expertcomments).setVisible(false);
		menu.findItem(R.id.viewpartograph).setVisible(false);
		menu.findItem(R.id.ic_msg).setVisible(false);
		menu.findItem(R.id.addcomments).setVisible(false);
		menu.findItem(R.id.viewprofile).setVisible(false);
		// menu.findItem(R.id.sync).setVisible(false);
		menu.findItem(R.id.registration).setVisible(false);

		menu.findItem(R.id.search).setVisible(false);// 14Jun2017 Arpitha

		menu.findItem(R.id.save).setVisible(false);// 08Aug2017 Arpitha

		// 03Oct2017 Arpitha
		menu.findItem(R.id.del).setVisible(true);
		menu.findItem(R.id.ref).setVisible(true);
		menu.findItem(R.id.pdf).setVisible(true);
		menu.findItem(R.id.stages).setVisible(true);
		menu.findItem(R.id.apgar).setVisible(true);
		menu.findItem(R.id.post).setVisible(true);
		menu.findItem(R.id.parto).setVisible(true);// 03Oct2017 Arpitha

		return super.onPrepareOptionsMenu(menu);
	}

	private void displayConfirmationAlert(String exit_msg, final String classname) throws Exception {

		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		final Dialog dialog = new Dialog(ReferralInfo_Activity.this);
		dialog.setTitle(getResources().getString(R.string.warning));
		dialog.setContentView(R.layout.temp_alertdialog);

		dialog.show();

		final TextView txtdialog = (TextView) dialog.findViewById(R.id.txtpulse);
		Button imgbtnyes = (Button) dialog.findViewById(R.id.imgbtnyes);
		Button imgbtnno = (Button) dialog.findViewById(R.id.imgbtnno);
		final TextView txtdialog1 = (TextView) dialog.findViewById(R.id.txtpulseval);

		final TextView txtdialog6 = (TextView) dialog.findViewById(R.id.txt);

		String strwarningmess = "";
		String strval = "";

		if (classname.equalsIgnoreCase(getResources().getString(R.string.clear))) {
			txtdialog6.setText(getResources().getString(R.string.clear_msg));
			txtdialog1.setVisibility(View.GONE);
			txtdialog1.setVisibility(View.GONE);
			imgbtnyes.setText(getResources().getString(R.string.yes));
			imgbtnno.setText(getResources().getString(R.string.no));

		}

		if (classname.equalsIgnoreCase(getResources().getString(R.string.exit_msg))) {
			txtdialog6.setText(getResources().getString(R.string.exit_msg));
			txtdialog1.setVisibility(View.GONE);
			txtdialog1.setVisibility(View.GONE);
			imgbtnyes.setText(getResources().getString(R.string.yes));
			imgbtnno.setText(getResources().getString(R.string.no));

		}

		if (classname.equalsIgnoreCase(getResources().getString(R.string.confirmation))) {

			if (pulval > 120 || pulval < 50) {
				strwarningmess = strwarningmess + getResources().getString(R.string.pul_val) + "\n";
				strval = strval + "" + pulval + "\n";

			}
			if (bpsysval < 80 || bpsysval > 160) {
				strwarningmess = strwarningmess + getResources().getString(R.string.bp_sys) + "\n";
				strval = strval + "" + bpsysval + "\n";

			}
			if (bpdiaval < 50 || bpdiaval >= 110) {

				strwarningmess = strwarningmess + getResources().getString(R.string.bp_dia) + "\n";
				strval = strval + "" + bpdiaval + "\n";
			}
			txtdialog6.setText(getResources().getString(R.string.bp_val));

			// 22Sep2016 Arpitha
			if (heartrate > 0 && (heartrate < 50 || heartrate > 200)) {
				strwarningmess = strwarningmess + getResources().getString(R.string.heart_rate_val_is) + "\n";
				strval = strval + "" + heartrate + "\n";
			}
			if (spo2 > 0 && (spo2 < 40 || spo2 > 100)) {
				strwarningmess = strwarningmess + getResources().getString(R.string.spo2_val_is) + "\n";
				strval = strval + "" + spo2 + "\n";

			}
			txtdialog.setText(strwarningmess);
			txtdialog1.setText(strval);
		}

		imgbtnyes.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				try {
					dialog.cancel();

					if (classname.equalsIgnoreCase(getResources().getString(R.string.clear))) {
						clear();
					}
					if (classname.equalsIgnoreCase(getResources().getString(R.string.exit_msg))) {
						Intent i = new Intent(ReferralInfo_Activity.this, Activity_WomenView.class);
						startActivity(i);

					}
					if (classname.equalsIgnoreCase(getResources().getString(R.string.confirmation))) {
						// updated bindu - 29Aug2016 - check confirmation of Ref
						// date and time
						String msg = getResources().getString(R.string.refdatetimerecheck)
								+ etdateofreferral.getText().toString() + " / " + ettimeofreferral.getText().toString()
								+ getResources().getString(R.string.confirmrecheck);
						ConfirmAlert(msg, getResources().getString(R.string.referral));// 02Oct2016
																						// Arpitha
																						// -
																						// string
																						// val
																						// from
																						// strings.xml

					}

				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});

		// imgbtnno.setText(getResources().getString(R.string.clear));
		imgbtnno.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.cancel();
				try {
					// clear();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});

		dialog.setCancelable(false);

	}

	public void clear() throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		etdateofreferral.setText(
				Partograph_CommonClass.getConvertedDateFormat(todaysDate, Partograph_CommonClass.defdateformat));
		ettimeofreferral.setText(Partograph_CommonClass.getCurrentTime());
		etpulsereferral.setText("");
		etbpsystolicreferral.setText("");
		etbpdiastolicreferral.setText("");
		etplaceofreferral.setText("");
		etdescriptionofreferral.setText("");
		etspo2.setText("");
		etheatrate.setText("");
		spnreferralfacility.setSelection(0);
		rd_babyalive.setChecked(true);
		rd_babydead.setChecked(false);
		rd_motherconscious.setChecked(true);
		rd_motherunconscious.setChecked(false);

		mspnreasonforreferral.setSelection(20);// 17feb2017 Arpitha

	}

	// Assign Ids to Referral Widgets
	private void AssignIdsToReferralWidgets() throws Exception {
		// TODO Auto-generated method stub
		etplaceofreferral = (EditText) findViewById(R.id.etplaceofreferral);
		etreasonforreferral = (EditText) findViewById(R.id.etreasonforreferral);
		etdateofreferral = (EditText) findViewById(R.id.etdateofreferrence);
		ettimeofreferral = (EditText) findViewById(R.id.ettimeofreferrence);
		spnreferralfacility = (Spinner) findViewById(R.id.spnreferredtofacility);
		btnCancelRef = (Button) findViewById(R.id.btncancelref);
		btnSaveRef = (Button) findViewById(R.id.btnsaveref);
		trspo2 = (TableRow) findViewById(R.id.trspo2);
		trheartrate = (TableRow) findViewById(R.id.trheartrate);

		etdescriptionofreferral = (EditText) findViewById(R.id.etdescforreferral);
		etbpsystolicreferral = (EditText) findViewById(R.id.etbpsystolicreferral);
		etbpdiastolicreferral = (EditText) findViewById(R.id.etbpdiastolicreferral);
		etpulsereferral = (EditText) findViewById(R.id.etpulsereferral);
		rd_motherconscious = (RadioButton) findViewById(R.id.rd_motherconscious);
		rd_motherunconscious = (RadioButton) findViewById(R.id.rd_motherunconscious);
		rd_babyalive = (RadioButton) findViewById(R.id.rd_babyalive);
		rd_babydead = (RadioButton) findViewById(R.id.rd_babydead);
		mspnreasonforreferral = (MultiSelectionSpinner) findViewById(R.id.mspnreasonforreferral);

		txtrefmode = (TextView) findViewById(R.id.txtmode);

		etheatrate = (EditText) findViewById(R.id.etheartrate);
		etspo2 = (EditText) findViewById(R.id.etspo2);

		txtwage = (TextView) findViewById(R.id.wage);
		txtwdoa = (TextView) findViewById(R.id.wdoa);
		txtwgravida = (TextView) findViewById(R.id.wgravida);
		txtwname = (TextView) findViewById(R.id.wname);
		// txtwpara = (TextView) findViewById(R.id.wpara);
		txtwrisk = (TextView) findViewById(R.id.wtrisk);
		txtwtoa = (TextView) findViewById(R.id.wtoa);
		txtwgest = (TextView) findViewById(R.id.wgest);

		txtwdelstatus = (TextView) findViewById(R.id.wdelstatus);
		txtwdeldate = (TextView) findViewById(R.id.wdeldate);

		imgpdf = (ImageView) findViewById(R.id.imgpdf);

		llbaby = (LinearLayout) findViewById(R.id.llbaby);// 20March2017 Arpitha

		txtdisabled = (TextView) findViewById(R.id.txtdisable);// 10Aug2017
		// Arpitha

		relRef = (RelativeLayout) findViewById(R.id.relreference);// 10Aug2017
																	// Arpitha
	}

	// Initial View of the referral Screen
	private void initialViewReferral() throws Exception {

		getwomanbasicdata();

		etreasonforreferral.setVisibility(View.GONE);
		setReasonForReferral();

		todaysDate = Partograph_CommonClass.getTodaysDate(); // 12jan2016
		ettimeofreferral.setText(Partograph_CommonClass.getCurrentTime());

		String refTime = ettimeofreferral.getText().toString();
		Calendar calendar = Calendar.getInstance();
		Date d = null;
		d = dbh.getTime(refTime);
		calendar.setTime(d);
		hour = calendar.get(Calendar.HOUR_OF_DAY);
		minute = calendar.get(Calendar.MINUTE);

		spnreferralfacility.setEnabled(true);
		etplaceofreferral.setEnabled(true);
		etreasonforreferral.setEnabled(true);
		etdateofreferral.setEnabled(true);
		ettimeofreferral.setEnabled(true);
		btnSaveRef.setEnabled(true);
		btnSaveRef.setBackgroundColor(getResources().getColor(R.color.brown));

		txtrefmode.setText(getResources().getString(R.string.editmode));
		etheatrate.setEnabled(true);
		etspo2.setEnabled(true);

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		if (woman.getDel_Date() != null && woman.getDel_Time() != null) {
			strdeltime = woman.getDel_Date() + " " + woman.getDel_Time();
			deltime = sdf.parse(strdeltime);
		}

		// 20March2017 Arpitha
		if (woman.getDel_type() == 0) {
			llbaby.setVisibility(View.GONE);
		} // 20March2017 Arpitha

		// 10Aug2017 Atpitha
		ArrayList<DiscgargePojo> arrVal = new ArrayList<DiscgargePojo>();
		arrVal = dbh.getDischargeData(woman.getUserId(), womenId);
		if (arrVal.size() > 0 && !(DiscargeDetails_Activity.updateReferral)) {
			txtdisabled.setVisibility(View.VISIBLE);
			relRef.setVisibility(View.GONE);

		} // 10Aug2017 Atpitha

		strlatdatetime = dbh.getLatenPhaseDateTime(womenId, woman.getUserId());// 20Sep2017
		// Arpitha

		strpostpartumdatetime = dbh.getPostPartumDateTime(womenId, woman.getUserId());// 20Sep2017
		// Arpitha

	}

	// Validate Referral mandatory fields
	private boolean validateReferralFields() throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		// 22August2016
		if (etplaceofreferral.getText().toString().trim().length() <= 0) {// 11April2017
																			// Arpitha
			displayAlertDialog(getResources().getString(R.string.ent_placeofreferral));
			etplaceofreferral.requestFocus();
			return false;
		}

		// 24Aug2016 - bindu make reason for referral mandatory
		if (mspnreasonforreferral.getSelectedIndicies().size() <= 0) {
			displayAlertDialog(getResources().getString(R.string.plsselectreasonforref));
			etdescriptionofreferral.requestFocus();
			return false;
		}

		// 4jan2016
		if (etdescriptionofreferral.getText().toString().trim().length() <= 0) {// 11April2017
																				// Arpitha
			displayAlertDialog(getResources().getString(R.string.enterdescofreferral));
			etdescriptionofreferral.requestFocus();
			return false;
		}

		// 12Jan2016
		if (etpulsereferral.getText().length() <= 0) {
			displayAlertDialog(getResources().getString(R.string.enterpulseval));
			etpulsereferral.requestFocus();
			return false;
		} // 12jan2016

		// Changes made by Arpitha
		if (etbpsystolicreferral.getText().length() <= 0 && etbpdiastolicreferral.getText().length() <= 0) {
			displayAlertDialog(getResources().getString(R.string.enter_bp));
			etbpsystolicreferral.requestFocus();
			return false;
		}

		if (etbpsystolicreferral.getText().length() > 0) {
			if (etbpdiastolicreferral.getText().length() <= 0) {
				displayAlertDialog(getResources().getString(R.string.enterbpdiaval));
				etbpdiastolicreferral.requestFocus();
				return false;
			}
		}

		if (etbpdiastolicreferral.getText().length() > 0) {
			if (etbpsystolicreferral.getText().length() <= 0) {
				displayAlertDialog(getResources().getString(R.string.enterbpsysval));
				etbpsystolicreferral.requestFocus();
				return false;
			}
		}

		if (etspo2.getText().toString().equalsIgnoreCase("0")) {
			Toast.makeText(getApplicationContext(), getResources().getString(R.string.invalid_value), Toast.LENGTH_LONG)
					.show();
			etspo2.requestFocus();
			return false;
		}

		// 22August2016 Arpitha
		String currenttim = Partograph_CommonClass.getTodaysDate() + " " + Partograph_CommonClass.getCurrentTime();

		// 22August2016 Arpitha
		if (etdateofreferral.getText().length() >= 1) {
			// String refdate
			// if (woman.getregtype() != 2) {
			if ((!isDeliveryTimevalid(strRegdate, refdate, 3)) && woman.getregtype() != 2)
				return false;
			if (strlastentrytime != null) {
				if (!isDeliveryTimevalid(strlastentrytime, refdate, 4))
					return false;
			}
			// }
			// changed on 24August2016 Arpitha
			if (!isDeliveryTimevalid(refdate, currenttim, 5))
				return false;

			if (deltime != null) {
				if (!isDeliveryTimevalid(strdeltime, refdate, 6))
					return false;
			}

			// 20Sep2017 Arpitha
			if (strlatdatetime != null) {
				strlatdatetime = strlatdatetime.replace("/", " ");
				if (!isDeliveryTimevalid(strlatdatetime, refdate, 6))
					return false;
			}
			if (strpostpartumdatetime != null) {
				strpostpartumdatetime = strpostpartumdatetime.replace("/", " ");
				if (!isDeliveryTimevalid(strpostpartumdatetime, refdate, 6))
					return false;
			}
		}

		if (ettimeofreferral.getText().length() >= 1) {

			// if (woman.getregtype() != 2) {
			if ((!(isDeliveryTimevalid(strRegdate, refdate, 3))) && woman.getregtype() != 2)
				return false;
			if (strlastentrytime != null) {
				if (!(isDeliveryTimevalid(strlastentrytime, refdate, 4)))
					return false;
			}
			// }
			if (!isDeliveryTimevalid(refdate, currenttim, 5))
				return false;

			if (deltime != null) {
				if (!isDeliveryTimevalid(strdeltime, refdate, 6))
					return false;
			}

			// 20Sep2017 Arpitha
			if (strlatdatetime != null) {
				strlatdatetime = strlatdatetime.replace("/", " ");
				if (!isDeliveryTimevalid(strlatdatetime, refdate, 6))
					return false;
			}
			if (strpostpartumdatetime != null) {
				strpostpartumdatetime = strpostpartumdatetime.replace("/", " ");
				if (!isDeliveryTimevalid(strpostpartumdatetime, refdate, 6))
					return false;
			}
		}

		return true;
	}

	private void caldatepicker() throws Exception {
		DialogFragment newFragment = new SelectDateFragment();
		newFragment.show(this.getSupportFragmentManager(), "DatePicker");
	}

	@SuppressLint("ValidFragment")
	public class SelectDateFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
		@TargetApi(Build.VERSION_CODES.HONEYCOMB)
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			DatePickerDialog dialog = null;
			try {
				Calendar calendar = Calendar.getInstance();
				Date d = null;
				d = dbh.getDateS(selecteddate == null ? todaysDate : selecteddate);
				calendar.setTime(d);
				year = calendar.get(Calendar.YEAR);
				mon = calendar.get(Calendar.MONTH);
				day = calendar.get(Calendar.DAY_OF_MONTH);

				dialog = new DatePickerDialog(getActivity(), this, year, mon, day);

				// remove calendar view
				dialog.getDatePicker().setCalendarViewShown(false);

				// Spinner View
				dialog.getDatePicker().setSpinnersShown(true);

				dialog.setTitle(getResources().getString(R.string.pickadate));
			} catch (Exception e) {
				AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
						+ this.getClass().getSimpleName(), e);
				e.printStackTrace();
			}
			return dialog;
		}

		public void onDateSet(DatePicker view, int yy, int mm, int dd) {
			if (view.isShown()) {
				try {
					populateSetDate(yy, mm + 1, dd);
				} catch (Exception e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					e.printStackTrace();
				}
			}
		}
	}

	public void populateSetDate(int year, int month, int day) throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		selecteddate = String.valueOf(year) + "-" + String.format("%02d", month) + "-" + String.format("%02d", day);

		try {

			strRefDate = null;

			Date taken = new SimpleDateFormat("yyyy-MM-dd").parse(selecteddate);
			Date regdate = new SimpleDateFormat("yyyy-MM-dd").parse(woman.getDate_of_admission());
			Date lastentrydate = null;
			Date deldate = null;

			// 20Sep2017 Arpitha
			if (strlatdatetime != null && strlatdatetime.trim().length() > 0) {
				latDatetime = new SimpleDateFormat("yyyy-MM-dd").parse(strlatdatetime);
			} // 20Sep2017 Arpitha

			// 20Sep2017 Arpitha
			if (strpostpartumdatetime != null && strpostpartumdatetime.trim().length() > 0) {
				postpartumdatetime = new SimpleDateFormat("yyyy-MM-dd").parse(strpostpartumdatetime);
			} // 20Sep2017 Arpitha

			// 23oct2016 Arpitha
			if (woman.getDel_Date() != null) {
				deldate = new SimpleDateFormat("yyyy-MM-dd").parse(woman.getDel_Date());

			}
			// updated on 29july2016 by Arpitha
			if (strlastentrytime != null) {
				lastentrydate = new SimpleDateFormat("yyyy-MM-dd").parse(strlastentrytime);
			}

			if (taken.after(new Date())) {

				Toast.makeText(getApplicationContext(), getResources().getString(R.string.curr_date_validation),
						Toast.LENGTH_SHORT).show();

				etdateofreferral.setText(Partograph_CommonClass.getConvertedDateFormat(todaysDate,
						Partograph_CommonClass.defdateformat));
				strRefDate = todaysDate;

			} else if (taken.before(regdate) && woman.getregtype() != 2) {

				String regDtae = Partograph_CommonClass.getConvertedDateFormat(strRegdate,
						Partograph_CommonClass.defdateformat);// 17April2017
																// Arpitha

				Toast.makeText(getApplicationContext(),
						getResources().getString(R.string.refdate_cant_be_before_reg_date) + " - " + regDtae,
						Toast.LENGTH_SHORT).show();

				etdateofreferral.setText(Partograph_CommonClass.getConvertedDateFormat(todaysDate,
						Partograph_CommonClass.defdateformat));
				strRefDate = todaysDate;

			}

			// 20Sep2017 Arpitha
			else if (latDatetime != null && taken.before(latDatetime)) {

				String latdate = Partograph_CommonClass.getConvertedDateFormat(strlatdatetime.split("/")[0],
						Partograph_CommonClass.defdateformat);
				Toast.makeText(getApplicationContext(),
						getResources().getString(R.string.referral_datetime_cannot_be_before_lattentphase_datetime)
								+ " - " + latdate,
						Toast.LENGTH_SHORT).show();

				etdateofreferral.setText(Partograph_CommonClass.getConvertedDateFormat(todaysDate,
						Partograph_CommonClass.defdateformat));
				strRefDate = todaysDate;

			} // 20Sep2017 Arpitha

			else if (deldate != null && taken.before(deldate))

			{
				String delDate = Partograph_CommonClass.getConvertedDateFormat(woman.getDel_Date(),
						Partograph_CommonClass.defdateformat);// 17April2017
																// Arpitha

				Toast.makeText(getApplicationContext(),
						getResources().getString(R.string.refdate_after_deldate) + " - " + delDate, Toast.LENGTH_SHORT)
						.show();

				etdateofreferral.setText(Partograph_CommonClass.getConvertedDateFormat(todaysDate,
						Partograph_CommonClass.defdateformat));
				strRefDate = todaysDate;
			}

			else if (lastentrydate != null && taken.before(lastentrydate)) {

				String lastentryDate = Partograph_CommonClass.getConvertedDateFormat(strlastentrytime,
						Partograph_CommonClass.defdateformat);// 17April2017
																// Arpitha

				// 24August2016 Arpitha - included date tim evalue
				Toast.makeText(getApplicationContext(),
						getResources().getString(R.string.refdate_before_lastparto) + " - " + lastentryDate,
						Toast.LENGTH_SHORT).show();

				etdateofreferral.setText(Partograph_CommonClass.getConvertedDateFormat(todaysDate,
						Partograph_CommonClass.defdateformat));
				strRefDate = todaysDate;

			}

			// 20Sep2017 Arpitha
			else if (postpartumdatetime != null && taken.before(postpartumdatetime)) {

				String latdate = Partograph_CommonClass.getConvertedDateFormat(strpostpartumdatetime.split("/")[0],
						Partograph_CommonClass.defdateformat);
				Toast.makeText(getApplicationContext(),
						getResources().getString(R.string.referral_datetime_cannot_be_before_postpartume_datetime)
								+ " - " + latdate,
						Toast.LENGTH_SHORT).show();

				etdateofreferral.setText(Partograph_CommonClass.getConvertedDateFormat(todaysDate,
						Partograph_CommonClass.defdateformat));
				strRefDate = todaysDate;

			} // 20Sep2017 Arpitha

			else {

				etdateofreferral.setText(Partograph_CommonClass.getConvertedDateFormat(selecteddate,
						Partograph_CommonClass.defdateformat));
				strRefDate = selecteddate;

			}
			// }11Nov2016 Arpitha

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
	}

	// updated on 13nov2015
	@SuppressLint("SimpleDateFormat")
	private boolean isDeliveryTimevalid(String date1, String date2, int i) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		// Date dateofreg, dateofdelivery;
		try {
			// dateofreg = sdf.parse(date1);
			// dateofdelivery = sdf.parse(date2);

			// 22August2016 Arpitha

			Date d1 = sdf.parse(date1);
			Date d2 = sdf.parse(date2);

			String strDate1 = Partograph_CommonClass.getConvertedDateFormat(date1,
					Partograph_CommonClass.defdateformat);// 17April2017 Arpitha

			strDate1 = strDate1 + " " + date1.substring(11);

			String strDate2 = Partograph_CommonClass.getConvertedDateFormat(date2,
					Partograph_CommonClass.defdateformat);// 17April2017 Arpitha

			strDate2 = strDate2 + " " + date1.substring(11);

			if (d2.before(d1)) {
				if (i == 3)

					displayAlertDialog(
							getResources().getString(R.string.refdate_cant_be_before_reg_date) + " - " + strDate1);
				if (i == 4)
					displayAlertDialog(getResources().getString(R.string.refdate_before_lastparto) + " - " + strDate1);
				if (i == 5)
					displayAlertDialog(getResources().getString(R.string.refdate_after_currentdate) + " - " + strDate2);
				if (i == 6)
					displayAlertDialog(getResources().getString(R.string.refdate_after_deldate) + " - " + strDate1);
				return false;
			} else
				return true;

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);

			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Method to commit the trnsaction
	 * 
	 * @throws Exception
	 */
	private void commitTrans() throws Exception {

		isdelivered = false;// 21Nov2016 Arpitha

		if (AppContext.checkSimState() == TelephonyManager.SIM_STATE_READY) {

			displayConfirmationAlert_sms(getResources().getString(R.string.send_mess), woman.getWomenId());

		} else {

			if (woman.getDel_type() != 0)// 18Nov2016 Arpitha
			{
				Fragment_DIP.isdelstatus = true;
				Intent i = new Intent(ReferralInfo_Activity.this, Activity_WomenView.class);
				startActivity(i);
			} else {// 18Nov2016 Arpitha
				if (DiscargeDetails_Activity.isdisc) {
					Intent i = new Intent(ReferralInfo_Activity.this, DischargedWomanList_Activity.class);
					startActivity(i);
				} else {
					Fragment_DIP.isreferral_updated = true;
					Intent i = new Intent(ReferralInfo_Activity.this, Activity_WomenView.class);
					startActivity(i);
				}
			}

		}
		dbh.db.setTransactionSuccessful();
		dbh.db.endTransaction();

	}

	/** Method to rollback the trnsaction */
	private void rollbackTrans() throws Exception {
		dbh.db.endTransaction();
		// dbh.db.close();
		Toast.makeText(getApplicationContext(), getResources().getString(R.string.failed), Toast.LENGTH_SHORT).show();
	}

	// Set options for reason for referral - 04jan0216
	protected void setReasonForReferral() throws Exception {
		int i = 0;
		reasonforreferralMap = new LinkedHashMap<String, Integer>();
		List<String> reasonStrArr = null;

		// 06jul2016 - assign diff array to get the position.
		List<String> reasonStrArrvalues = null;

		reasonStrArrvalues = Arrays.asList(getResources().getStringArray(R.array.reasonforreferralvalues));

		// to get the values(pos) for the options
		if (reasonStrArrvalues != null) {
			for (String str : reasonStrArrvalues) {
				reasonforreferralMap.put(str, i);
				i++;
			}
		}

		// reasonStrArr =
		// Arrays.asList(getResources().getStringArray(R.array.reasonforreferral));
		if (woman.getDel_type() == 0)// 03April2017 Arpitha
			reasonStrArr = Arrays.asList(getResources().getStringArray(R.array.reasonforreferraldip));// 03April2017
																										// Arpitha
		else// 03April2017 Arpitha
			reasonStrArr = Arrays.asList(getResources().getStringArray(R.array.reasonforreferraldelivered));// 03April2017
																											// Arpitha

		if (reasonStrArr != null) {
			mspnreasonforreferral.setItems(reasonStrArr);
		}

	}

	// Alert dialog
	private void displayAlertDialog(String message) throws Exception {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ReferralInfo_Activity.this);

		// set dialog message
		alertDialogBuilder.setMessage(message)

				.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// if this button is clicked, just close
						// the dialog box and do nothing
						dialog.cancel();
					}
				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();
		// show it
		alertDialog.show();
	}

	private static String padding_str(int c) throws Exception {
		if (c >= 10)
			return String.valueOf(c);
		else
			return "0" + String.valueOf(c);
	}

	private boolean displayConfirmationAlert_sms(String exit_msg, final String classname) throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		final Dialog dialog = new Dialog(ReferralInfo_Activity.this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.alertdialog);

		dialog.show();

		final TextView txtdialog = (TextView) dialog.findViewById(R.id.txtdialog);
		TextView txt1 = (TextView) dialog.findViewById(R.id.txtval);
		TextView txt2 = (TextView) dialog.findViewById(R.id.txtval1);
		Button imgbtnyes = (Button) dialog.findViewById(R.id.imgbtnyes);
		Button imgbtnno = (Button) dialog.findViewById(R.id.imgbtnno);

		txtdialog.setText(exit_msg);
		txt1.setVisibility(View.GONE);
		txt2.setVisibility(View.GONE);

		imgbtnyes.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				dialog.cancel();

				try {

					display_messagedialog(classname);

				} catch (Exception e) {
					// TODO Auto-generated catch block
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					e.printStackTrace();
				}

			}
		});

		imgbtnno.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.cancel();
				try {

					Fragment_DIP.isreferral_updated = true;// 02Nov2016 Arpitha
					Intent i = new Intent(ReferralInfo_Activity.this, Activity_WomenView.class);
					startActivity(i);

				} catch (Exception e) {
					// TODO Auto-generated catch block
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					e.printStackTrace();
				}
			}
		});
		return true;
	}

	public void display_messagedialog(final String womenid) throws Exception {
		try {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
			final Dialog sms_dialog = new Dialog(ReferralInfo_Activity.this);
			sms_dialog.setTitle(Html.fromHtml("<font color='" + getResources().getColor(R.color.appcolor) + "'>"
					+ getResources().getString(R.string.send_sms) + "</font>"));// 02Oct2016
			// Arpitha
			// -
			// string
			// value
			// from
			// strings.xml

			sms_dialog.setContentView(R.layout.alertdialog_sms);

			int dividerId = sms_dialog.getContext().getResources().getIdentifier("android:id/titleDivider", null, null);
			View divider = sms_dialog.findViewById(dividerId);
			divider.setBackgroundColor(getResources().getColor(R.color.appcolor));// 08Feb2017

			sms_dialog.show();

			TextView txt1 = (TextView) sms_dialog.findViewById(R.id.txtval);
			TextView txt2 = (TextView) sms_dialog.findViewById(R.id.txtval1);
			final EditText etphno = (EditText) sms_dialog.findViewById(R.id.etphnno);

			etmesss = (EditText) sms_dialog.findViewById(R.id.etmess);
			EditText etopt = (EditText) sms_dialog.findViewById(R.id.etreasonoptions);
			TextView txtreason = (TextView) sms_dialog.findViewById(R.id.txtval6);
			ImageButton imgsend = (ImageButton) sms_dialog.findViewById(R.id.imgsend);
			ImageButton imgcancel = (ImageButton) sms_dialog.findViewById(R.id.imgcancel);

			for (int i = 0; i < values.size(); i++) {
				p_no = p_no + values.get(i) + ",";
			}
			etphno.setText(p_no);

			Cursor c = dbh.getReferralDetails(wUserId, womenid);
			if (c != null && c.getCount() > 0) {
				c.moveToFirst();
				String RefResDisplay = "";
				String womanname = c.getString(2);
				String desc = c.getString(8);
				String reasonforref = c.getString(7);
				if (reasonforref != null && reasonforref.length() > 0) {

					String[] admres = reasonforref.split(",");
					String[] admResArr = null;

					admResArr = getResources().getStringArray(R.array.reasonforreferralvalues);

					if (admResArr != null) {
						if (admres != null && admres.length > 0) {
							for (String str : admres) {
								if (str != null && str.length() > 0 && (!str.equalsIgnoreCase("null")))// 09nov2016
																										// checking
																										// for
																										// str
																										// null
									RefResDisplay = RefResDisplay + admResArr[Integer.parseInt(str)] + ",";

							}
						}
					}
				}

				txt1.setText(womanname);
				txt2.setText(desc);
				if (RefResDisplay.length() > 0) {
					etopt.setText(RefResDisplay);
				} else {
					etopt.setVisibility(View.GONE);
					txtreason.setVisibility(View.GONE);
				}

			}

			imgsend.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {

					// sms_dialog.cancel();

					try {

						if (AppContext.checkSimState() == TelephonyManager.SIM_STATE_READY) {

							if (values != null && values.size() > 0) {
								InserttblMessageLog(etphno.getText().toString(), etmesss.getText().toString(), womenid);
								if (isMessageLogsaved) {
									Toast.makeText(getApplicationContext(),
											getResources().getString(R.string.sending_sms), Toast.LENGTH_LONG).show();
									sendSMSFunction();
								}
							} else {

								// 25Aug2016 - bindu
								if (etphno.getText().toString().length() > 0
										&& etphno.getText().toString().length() >= 10) {
									InserttblMessageLog(etphno.getText().toString(), etmesss.getText().toString(),
											womenid);
									if (isMessageLogsaved) {
										Toast.makeText(getApplicationContext(),
												getResources().getString(R.string.sending_sms), Toast.LENGTH_LONG)
												.show();
										sendSMSFunction();
									}
									sms_dialog.cancel();

								} else if (etphno.getText().toString().length() <= 0) {
									Toast.makeText(getApplicationContext(),
											getResources().getString(R.string.entr_phno), Toast.LENGTH_LONG).show();
								} else if (etphno.getText().toString().length() > 0
										&& etphno.getText().toString().length() < 10) {
									Toast.makeText(getApplicationContext(),
											getResources().getString(R.string.plz_entr_valid_phn_number),
											Toast.LENGTH_LONG).show();
								}
							}
							// sms_dialog.cancel();
						} else
							Toast.makeText(getApplicationContext(), getResources().getString(R.string.no_sim),
									Toast.LENGTH_LONG).show();

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
			});

			imgcancel.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					sms_dialog.cancel();
					Intent i = new Intent(ReferralInfo_Activity.this, Activity_WomenView.class);
					startActivity(i);
				}
			});

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	// Insert to record to Message Log
	private void InserttblMessageLog(String phoneno, String comments, String womanid) throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		String message = "";
		Cursor c = dbh.getReferralDetails(wUserId, womanid);
		if (c != null && c.getCount() > 0) {
			c.moveToFirst();
			String RefResDisplay = "";
			String womanname = c.getString(2);
			String desc = c.getString(8);
			String reasonforref = c.getString(7);
			if (reasonforref != null && reasonforref.length() > 0) {

				String[] admres = reasonforref.split(",");
				String[] admResArr = null;

				admResArr = getResources().getStringArray(R.array.reasonforreferralvaluessms);

				if (admResArr != null) {
					if (admres != null && admres.length > 0) {
						for (String str : admres) {
							if (str != null && str.length() > 0)
								RefResDisplay = RefResDisplay + admResArr[Integer.parseInt(str)] + ",";

						}
					}
				}
			}

			if (comments.length() > 0)
				comments = getResources().getString(R.string.cmnts) + comments; // 11Sep2016
																				// -
																				// bindu
																				// chk
																				// comm
			// length

			message = getResources().getString(R.string.epartograph_referral) + womanname + ", "
					+ getResources().getString(R.string.reaon) + RefResDisplay + getResources().getString(R.string.desc)
					+ desc + comments;// 02Oct2016 Arpitha -

			ArrayList<MessageLogPojo> mlpArr = new ArrayList<MessageLogPojo>();

			if (phoneno.length() > 0) {
				String[] phn = phoneno.split("\\,");
				for (int i = 0; i < phn.length; i++) {
					String num = phn[i];
					if (num != null && num.length() > 0) {
						MessageLogPojo mlp = new MessageLogPojo(wUserId, womenId, num, desc, RefResDisplay, message, 1,
								0);
						mlpArr.add(mlp);
					}
				}
			}

			if (mlpArr != null) {
				for (MessageLogPojo mlpp : mlpArr) {
					int transId = dbh.iCreateNewTrans(wUserId);// 15Oct2016
																// Arpitha
					boolean isinserted = dbh.insertToMessageLog(mlpp, transId);// 15Oct2016
																				// Arpitha
					if (isinserted) {
						isMessageLogsaved = true;
						dbh.iNewRecordTrans(wUserId, transId, Partograph_DB.TBL_MESSAGELOG);// 15Oct2016
																							// Arpitha
					} else
						throw new Exception("InserttblMessageLog(): Failed to Insert Message to tblMessageLog ");
				}
			}
		}
	}

	private void ConfirmAlert(String msg, final String goToScreen) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ReferralInfo_Activity.this);

		// set dialog message
		alertDialogBuilder.setMessage(msg).setCancelable(false)
				.setNegativeButton(getResources().getString(R.string.recheck), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {

						// Arpitha
						// -
						// string
						// value
						// from
						// strings.xml
						// from
						// strings.xml
						etdateofreferral.requestFocus();

						dialog.cancel();
					}
				}).setPositiveButton(getResources().getString(R.string.save), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						try {
							// string
							// value
							// from
							// strings.xml
							referral_data();
							dialog.cancel();

						} catch (Exception e) {
							AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
									+ this.getClass().getSimpleName(), e);

							e.printStackTrace();
						}

					}
				});
		AlertDialog alertDialog1 = alertDialogBuilder.create();
		alertDialog1.show();
	}

	/**
	 * This method invokes after successfull save of record Sends SMS to the
	 * Specified Number
	 */
	private void sendSMSFunction() throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		new SendSMS();
		Fragment_DIP.isreferral_updated = true;// 02Nov2016 Arpitha
		Intent i = new Intent(ReferralInfo_Activity.this, Activity_WomenView.class);
		startActivity(i);

	}

	void getwomanbasicdata() throws Exception {

		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		if (woman != null) {

			String doa = "", toa = "", risk = "", dod = "", tod = "";

			txtwname.setText(woman.getWomen_name());
			txtwage.setText(woman.getAge() + getResources().getString(R.string.yrs));// 02Oct2016
																						// Arpitha
																						// -
																						// string
																						// value
																						// from
			if (woman.getregtype() != 2) {// strings.xml
				doa = Partograph_CommonClass.getConvertedDateFormat(woman.getDate_of_admission(),
						Partograph_CommonClass.defdateformat);
				toa = woman.getTime_of_admission();
				txtwdoa.setText(getResources().getString(R.string.reg) + ": " + doa + "/" + toa);
			} else {

				txtwdoa.setText(getResources().getString(R.string.doe) + ": " + woman.getDate_of_reg_entry());
			}
			txtwgest.setText(getResources().getString(R.string.gest_age) + ":"
					+ (woman.getGestationage() == 0 ? getResources().getString(R.string.notknown)
							: woman.getGestationage() + getResources().getString(R.string.wks)));// 23Aug2016

			txtwgravida.setText(getResources().getString(R.string.gravida_short_label) + ":" + woman.getGravida() + ", "
					+ getResources().getString(R.string.para_short_label) + ":" + woman.getPara());
			// woman.getRisk_category();
			if (woman.getRisk_category() == 1) {
				risk = getResources().getString(R.string.high);
			} else
				risk = getResources().getString(R.string.low);
			txtwrisk.setText(getResources().getString(R.string.risk_short_label) + ":" + risk);

			// 31Oct2016 Arpitha
			if (woman.getDel_type() > 0) {
				String[] deltype = getResources().getStringArray(R.array.del_type);
				txtwdelstatus
						.setText(getResources().getString(R.string.delstatus) + ":" + deltype[woman.getDel_type() - 1]);
				dod = Partograph_CommonClass.getConvertedDateFormat(woman.getDel_Date(),
						Partograph_CommonClass.defdateformat);
				tod = Partograph_CommonClass.gettimein12hrformat(woman.getDel_Time());
				txtwdeldate.setText(getResources().getString(R.string.del) + ": " + dod + "/" + tod);

			}

		}
	}

	@Override
	protected Dialog onCreateDialog(int id) {

		try {
			switch (id) {
			case TIME_DIALOG_ID:
				// set time picker as current time
				return new TimePickerDialog(this, timePickerListener, hour, minute, false);

			}
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
		return null;
	}

	private TimePickerDialog.OnTimeSetListener timePickerListener = new TimePickerDialog.OnTimeSetListener() {
		public void onTimeSet(TimePicker view, int selectedHour, int selectedMinute) {
			hour = selectedHour;
			minute = selectedMinute;

			try {

				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
				Date rtime = null;
				String reftime = null;
				Date lastentrytime = null;
				Date deltime = null;
				String strdeltime = null;

				// 20Sep2017 Arpitha
				if (strlatdatetime != null && strlatdatetime.trim().length() > 0) {
					strlatdatetime = strlatdatetime.replace("/", " ");
					latDatetime = sdf.parse(strlatdatetime);
				} // 20Sep2017 Arpitha

				// 20Sep2017 Arpitha
				if (strpostpartumdatetime != null && strpostpartumdatetime.trim().length() > 0) {
					strpostpartumdatetime = strpostpartumdatetime.replace("/", " ");
					postpartumdatetime = sdf.parse(strpostpartumdatetime);
				} // 20Sep2017 Arpitha

				if (woman.getDel_Date() != null && woman.getDel_Time() != null) {
					strdeltime = woman.getDel_Date() + " " + woman.getDel_Time();
					deltime = sdf.parse(strdeltime);
				}

				Date regdate = null;
				if (strRegdate != null)
					regdate = sdf.parse(strRegdate);
				if (strlastentrytime != null) {
					lastentrytime = sdf.parse(strlastentrytime);
				}
				String strcurrenttime = sdf.format(new Date());
				Date currenttime = sdf.parse(strcurrenttime);
				if (strRefDate != null) {
					reftime = strRefDate + " " + ("" + hour) + ":" + ("" + minute);
				} else
					reftime = Partograph_CommonClass.getTodaysDate() + " " + ("" + hour) + ":" + ("" + minute);
				rtime = sdf.parse(reftime);

				// 11Nov2016 Arpitha
				if (regdate != null && rtime.before(regdate) && woman.getregtype() != 2) {

					String regtime = Partograph_CommonClass.getConvertedDateFormat(strRegdate,
							Partograph_CommonClass.defdateformat);// 17April2017
																	// Arpitha

					regtime = regtime + " " + strRegdate.substring(11);// 17April2017
																		// Arpitha

					Toast.makeText(getApplicationContext(),
							getResources().getString(R.string.invalid_ref_time) + " - " + regtime, Toast.LENGTH_LONG)
							.show();
					ettimeofreferral.setText(Partograph_CommonClass.getCurrentTime());

				}

				// 20Sep2017 Arpitha
				else if (latDatetime != null && rtime.before(latDatetime)) {

					String latdate = Partograph_CommonClass.getConvertedDateFormat(strlatdatetime.split(" ")[0],
							Partograph_CommonClass.defdateformat);
					Toast.makeText(getApplicationContext(),
							getResources().getString(R.string.referral_datetime_cannot_be_before_lattentphase_datetime)
									+ " - " + latdate + " " + strlatdatetime.split(" ")[1],
							Toast.LENGTH_SHORT).show();

					ettimeofreferral.setText(Partograph_CommonClass.getCurrentTime());

				} // 20Sep2017 Arpitha

				// 23oct2016 Arpitha
				else if (deltime != null && rtime.before(deltime)) {

					String deliverytime = Partograph_CommonClass.getConvertedDateFormat(strdeltime,
							Partograph_CommonClass.defdateformat);// 17April2017
																	// Arpitha

					deliverytime = deliverytime + " " + strdeltime.substring(11);// 17April2017
					// Arpitha

					Toast.makeText(getApplicationContext(),
							getResources().getString(R.string.refdate_after_deldate) + " - " + deliverytime,
							Toast.LENGTH_LONG).show();
					ettimeofreferral.setText(Partograph_CommonClass.getCurrentTime());
				}

				// updated on 29july2016 by Arpitha
				else if (lastentrytime != null && rtime.before(lastentrytime)) {

					String lastentryDatetime = Partograph_CommonClass.getConvertedDateFormat(strlastentrytime,
							Partograph_CommonClass.defdateformat);// 17April2017
																	// Arpitha

					lastentryDatetime = lastentryDatetime + " " + strlastentrytime.substring(11);// 17April2017
					// Arpitha

					Toast.makeText(getApplicationContext(),
							getResources().getString(R.string.reftime_before_lastparto1) + " - " + lastentryDatetime,
							Toast.LENGTH_LONG).show();
					ettimeofreferral.setText(Partograph_CommonClass.getCurrentTime());
				}

				// 20Sep2017 Arpitha
				else if (postpartumdatetime != null && rtime.before(postpartumdatetime)) {

					String latdate = Partograph_CommonClass.getConvertedDateFormat(strpostpartumdatetime.split(" ")[0],
							Partograph_CommonClass.defdateformat);
					Toast.makeText(getApplicationContext(),
							getResources().getString(R.string.referral_datetime_cannot_be_before_postpartume_datetime)
									+ " - " + latdate + " " + strpostpartumdatetime.split(" ")[1],
							Toast.LENGTH_SHORT).show();

					ettimeofreferral.setText(Partograph_CommonClass.getCurrentTime());

				} // 20Sep2017 Arpitha

				// updated on 29july2016 by Arpitha
				else if (currenttime != null && rtime.after(currenttime)) {

					String curtime = Partograph_CommonClass.getConvertedDateFormat(strcurrenttime,
							Partograph_CommonClass.defdateformat);// 17April2017
																	// Arpitha

					curtime = curtime + " " + strcurrenttime.substring(11);// 17April2017
					// Arpitha

					Toast.makeText(getApplicationContext(),
							getResources().getString(R.string.reftim_aft_curtime) + " - " + curtime, Toast.LENGTH_LONG)
							.show();
					ettimeofreferral.setText(Partograph_CommonClass.getCurrentTime());

				}

				else {
					ettimeofreferral.setText(
							new StringBuilder().append(padding_str(hour)).append(":").append(padding_str(minute)));
				}
				// }
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	};

	void referral_data() {
		try {

			// if (Partograph_CommonClass.isTimeValid(strRegdate, refdate)) {

			wrefpojo = new WomenReferral_pojo();

			wrefpojo.setWomenname(woman.getWomen_name());
			wrefpojo.setWomenid(woman.getWomenId());
			wrefpojo.setUserid(wUserId);
			wrefpojo.setFacility(woman.getFacility());
			wrefpojo.setFacilityname(woman.getFacility_name());
			wrefpojo.setReferredtofacilityid(referralfacility);
			wrefpojo.setPlaceofreferral(etplaceofreferral.getText().toString());
			// wrefpojo.setReasonforreferral(etreasonforreferral.getText().toString());

			// updated on 27Feb2016 by Arpitha

			wrefpojo.setHeartrate(etheatrate.getText().toString());
			wrefpojo.setSpo2(etspo2.getText().toString());

			String rdate = (strRefDate == null ? todaysDate : strRefDate);

			wrefpojo.setDateofreferral(rdate);
			wrefpojo.setTimeofreferral(ettimeofreferral.getText().toString());
			wrefpojo.setCreated_date(Partograph_CommonClass.getCurrentDateandTime());
			wrefpojo.setLastupdateddate(Partograph_CommonClass.getCurrentDateandTime());

			// 04jan2016
			List<String> selectedriskoption = mspnreasonforreferral.getSelectedStrings();
			String selIds = "";
			if (selectedriskoption != null && selectedriskoption.size() > 0) {
				for (String str : selectedriskoption) {
					if (selectedriskoption.indexOf(str) != selectedriskoption.size() - 1)
						selIds = selIds + reasonforreferralMap.get(str) + ",";
					else
						selIds = selIds + reasonforreferralMap.get(str);
				}
			}
			wrefpojo.setReasonforreferral(selIds);
			String bp = etbpsystolicreferral.getText().toString() + "/" + etbpdiastolicreferral.getText().toString();
			wrefpojo.setBp(bp);
			wrefpojo.setPulse(etpulsereferral.getText().toString() + "");
			wrefpojo.setConditionofmother(conditionofmother);
			wrefpojo.setConditionofbaby(conditionofbaby);
			wrefpojo.setDescriptionofreferral(etdescriptionofreferral.getText().toString());

			dbh.db.beginTransaction();
			int transId = dbh.iCreateNewTrans(wUserId);
			String updateRef = "";
			if (womenrefpojoItems != null && womenrefpojoItems.size() > 0) {
				updateRef = dbh.updateWomenReferralData(wrefpojo, transId);

				if (updateRef.length() > 0) {

					commitTrans();
				} else
					rollbackTrans();

			} else {
				boolean isAdded = dbh.addReferral(wrefpojo, transId);

				if (isAdded) {
					dbh.iNewRecordTrans(wUserId, transId, Partograph_DB.TBL_REFERRAL);
					commitTrans();
					dbh.deletetblnotificationrow(womenId, wUserId);// 17Oct2016
																	// Arpitha

				} else {
					rollbackTrans();
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		// } else
		// Toast.makeText(getApplicationContext(),
		// getResources().getString(R.string.reftime_cant_be_before_reg_date),
		// Toast.LENGTH_SHORT).show();

	}

	// 07Nov2016 Arpitha
	/** This method Calls the idle timeout */
	@Override
	public void onUserInteraction() {
		super.onUserInteraction();
		KillAllActivitesAndGoToLogin
				.delayedIdle(Integer.parseInt(Partograph_CommonClass.properties.getProperty("idleTimeOut")));
	}

	// 02Feb2017 Arpitha
	public void generateReferralPdf1() throws Exception {
		Document doc = new Document(PageSize.A4);

		try {

			path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Partograph/PDF/ReferralSheet";

			File dir = new File(path);
			if (!dir.exists())
				dir.mkdirs();

			Log.d("PDFCreator", "PDF Path: " + path);

			file = new File(dir, woman.getWomen_name() + "_Referral.pdf");
			FileOutputStream fOut = new FileOutputStream(file);

			PdfWriter writer = PdfWriter.getInstance(doc, fOut);

			doc.setMarginMirroring(false);
			doc.setMargins(-25, -20, 30, -50);

			doc.open();

			Font fontTitle = new Font(Font.HELVETICA, 20, Font.BOLD);
			Font fontHeading = new Font(Font.HELVETICA, 10, Font.BOLD);
			Font font = new Font(Font.HELVETICA, 10, Font.NORMAL);

			Paragraph p_heading = new Paragraph(getResources().getString(R.string.referral_slips), fontTitle);
			p_heading.setAlignment(Paragraph.ALIGN_CENTER);
			p_heading.setFont(fontTitle);
			doc.add(p_heading);

			PdfPTable pdffacilityDetails = new PdfPTable(1);

			addEmptyLine(pdffacilityDetails, 3);

			PdfPCell cellFacilityName = new PdfPCell();
			cellFacilityName.setPhrase(new Phrase(getResources().getString(R.string.name_of_the_referring_facility)
					+ " " + woman.getFacility_name() + ", " + woman.getFacility(), font));
			cellFacilityName.setHorizontalAlignment(Element.ALIGN_LEFT);
			cellFacilityName.setPaddingTop(5);
			cellFacilityName.setPaddingLeft(5);
			;
			// cellFacilityName.setPaddingBottom(5);
			cellFacilityName.setBorderWidthBottom(0);

			// cellFacilityName.setUseAscender(true);

			pdffacilityDetails.addCell(cellFacilityName);

			pdffacilityDetails.completeRow();

			PdfPCell cellFacilityAddress = new PdfPCell();
			cellFacilityAddress
					.setPhrase(new Phrase(getResources().getString(R.string.address) + ": " + woman.getFacility_name()
							+ ", " + woman.getTaluk() + "[T], " + woman.getDistrict() + "[D]", font));
			cellFacilityAddress.setHorizontalAlignment(Element.ALIGN_LEFT);
			cellFacilityAddress.setBorderWidthBottom(0);
			cellFacilityAddress.setBorderWidthTop(0);
			cellFacilityAddress.setPaddingTop(5);
			// cellFacilityAddress.setPaddingBottom(5);

			pdffacilityDetails.addCell(cellFacilityAddress);

			pdffacilityDetails.completeRow();

			PdfPCell cellTelephone = new PdfPCell();
			cellTelephone.setPhrase(new Phrase(getResources().getString(R.string.telephone) + ": " + "\n", font));
			cellTelephone.setHorizontalAlignment(Element.ALIGN_LEFT);
			cellTelephone.setBorderWidthTop(0);
			cellTelephone.setPaddingTop(3);
			cellTelephone.setPaddingBottom(5);

			pdffacilityDetails.addCell(cellTelephone);

			pdffacilityDetails.completeRow();

			String woman_name = "";
			String woman_age = "";
			if (woman.getWomen_name().length() > 0) {
				woman_name = woman.getWomen_name();
			} else {
				woman_name = getResources().getString(R.string.doted_line);
			}

			if (woman.getAge() > 0) {
				woman_age = "" + woman.getAge();
			} else
				woman_age = getResources().getString(R.string.doted_line);

			PdfPCell cellPatientName = new PdfPCell();
			cellPatientName.setPhrase(new Phrase(getResources().getString(R.string.name_of_the_patient) + ": "
					+ woman_name + "    " + getResources().getString(R.string.age) + ":  " + "" + woman_age + " "
					+ getResources().getString(R.string.yrs), font));
			cellPatientName.setHorizontalAlignment(Element.ALIGN_LEFT);
			cellPatientName.setBorder(0);
			cellPatientName.setPaddingTop(10);

			pdffacilityDetails.addCell(cellPatientName);
			pdffacilityDetails.completeRow();

			PdfPCell cellHusbandName = new PdfPCell();
			cellHusbandName.setPhrase(new Phrase(getResources().getString(R.string.husbands_name) + ": "
					+ getResources().getString(R.string.doted_line), font));
			cellHusbandName.setHorizontalAlignment(Element.ALIGN_LEFT);
			cellHusbandName.setBorder(0);
			cellHusbandName.setPaddingTop(7);
			cellHusbandName.setPaddingBottom(2);

			pdffacilityDetails.addCell(cellHusbandName);
			pdffacilityDetails.completeRow();

			String woman_address = "";

			if (woman.getAddress().length() > 0) {
				woman_address = woman.getAddress();
			} else
				woman_address = "..............";
			// woman_address = ".............." +
			// getResources().getString(R.string.doted_line);

			PdfPCell cellAddress = new PdfPCell();
			cellAddress.setPhrase(
					new Phrase(getResources().getString(R.string.address) + ": " + woman_address + "\n", font));
			cellAddress.setHorizontalAlignment(Element.ALIGN_LEFT);
			cellAddress.setBorder(0);
			cellAddress.setPaddingTop(7);
			cellAddress.setPaddingBottom(2);

			pdffacilityDetails.addCell(cellAddress);
			pdffacilityDetails.completeRow();

			if (woman.getAddress().length() <= 0) {
				PdfPCell cellAddressExtra = new PdfPCell();
				cellAddressExtra.setPhrase(new Phrase(
						getResources().getString(R.string.doted_line) + "...........................", font));
				cellAddressExtra.setHorizontalAlignment(Element.ALIGN_LEFT);
				cellAddressExtra.setBorder(0);
				cellAddressExtra.setPaddingTop(7);
				cellAddressExtra.setPaddingBottom(2);

				pdffacilityDetails.addCell(cellAddressExtra);
				pdffacilityDetails.completeRow();
			}

			addEmptyLine(pdffacilityDetails, 2);

			addHorizantalLine(pdffacilityDetails, 1);

			// addEmptyLine(pdffacilityDetails, 1);

			String refralDate = Partograph_CommonClass
					.getConvertedDateFormat(strRefDate == null ? todaysDate : strRefDate, "dd/MM/yyyy");

			String referralTime = Partograph_CommonClass.gettimein12hrformat(ettimeofreferral.getText().toString());

			String referralPlace = etplaceofreferral.getText().toString();

			if (referralPlace.trim().length() <= 0) {
				referralPlace = getResources().getString(R.string.doted_line);
			}

			/*
			 * String facilityName = ""; facilityName =
			 * etplaceofreferral.getText().toString();
			 * if(facilityName.trim().length()<=0) { facilityName =
			 * getResources().getString(R.string.doted_line); }
			 */

			PdfPCell cellReferedDate = new PdfPCell();
			cellReferedDate
					.setPhrase(
							new Phrase(
									getResources().getString(R.string.referred_on) + ": " + refralDate + " "
											+ getResources().getString(R.string.at) + " " + referralTime + " "
											+ getResources().getString(R.string.time_to) + "  " + referralPlace + " "
											+ getResources().getString(R.string.name_of_facility_for_management),
									font));
			cellReferedDate.setHorizontalAlignment(Element.ALIGN_LEFT);
			cellReferedDate.setBorder(0);
			cellReferedDate.setPaddingTop(5);
			cellReferedDate.setPaddingBottom(5);

			pdffacilityDetails.addCell(cellReferedDate);
			pdffacilityDetails.completeRow();

			PdfPCell cellProvisionalDiagnosis = new PdfPCell();
			cellProvisionalDiagnosis.setPhrase(
					new Phrase(getResources().getString(R.string.provisional_diagnosis) + " : ", fontHeading));
			cellProvisionalDiagnosis.setHorizontalAlignment(Element.ALIGN_LEFT);
			cellProvisionalDiagnosis.setBorder(0);
			cellProvisionalDiagnosis.setPaddingBottom(10);
			pdffacilityDetails.addCell(cellProvisionalDiagnosis);
			pdffacilityDetails.completeRow();

			String gravida = "G" + woman.getGravida() + " " + "P" + woman.getPara();

			PdfPCell ProvisionalDiagnosis = new PdfPCell();
			ProvisionalDiagnosis.setPhrase(new Phrase(gravida, font));
			ProvisionalDiagnosis.setHorizontalAlignment(Element.ALIGN_LEFT);
			ProvisionalDiagnosis.setPaddingBottom(40);
			ProvisionalDiagnosis.setPaddingTop(5);

			pdffacilityDetails.addCell(ProvisionalDiagnosis);
			pdffacilityDetails.completeRow();

			// addEmptyLine(pdffacilityDetails, 2);

			String regDate;
			String regTime = "";
			if (woman.getregtype() != 2)
				regDate = Partograph_CommonClass.getConvertedDateFormat(woman.getDate_of_admission(), "dd/MM/yyyy");
			else
				regDate = Partograph_CommonClass.getConvertedDateFormat(woman.getDate_of_reg_entry(), "dd/MM/yyyy");

			if (woman.getregtype() != 2)
				regTime = Partograph_CommonClass.gettimein12hrformat(woman.getTime_of_admission());

			PdfPCell cellAdmittedDate = new PdfPCell();
			cellAdmittedDate
					.setPhrase(new Phrase(getResources().getString(R.string.admitted_in_the_referring_facility_on) + " "
							+ regDate + " " + getResources().getString(R.string.at) + " " + regTime + " "
							+ getResources().getString(R.string.time_with), font));
			cellAdmittedDate.setHorizontalAlignment(Element.ALIGN_LEFT);
			cellAdmittedDate.setBorder(0);
			cellAdmittedDate.setPaddingTop(10);

			pdffacilityDetails.addCell(cellAdmittedDate);
			pdffacilityDetails.completeRow();

			// addEmptyLine(pdffacilityDetails, 2);

			PdfPCell cellChiefCompaints = new PdfPCell();
			cellChiefCompaints
					.setPhrase(new Phrase(getResources().getString(R.string.chief_complaints_of), fontHeading));
			cellChiefCompaints.setHorizontalAlignment(Element.ALIGN_LEFT);
			cellChiefCompaints.setBorder(0);

			pdffacilityDetails.addCell(cellChiefCompaints);
			pdffacilityDetails.completeRow();

			String reasonforHRDisplay = "";
			String AdmittedWithDisplay = "";
			if (woman.getregtype() != 2) {
				String[] reasonforhighrisk = woman.getRiskoptions().split(",");
				String[] reasonforHRArr = null;
				reasonforHRArr = getResources().getStringArray(R.array.riskoptionsvalues);

				if (reasonforHRArr != null) {
					if (reasonforhighrisk != null && reasonforhighrisk.length > 0) {
						for (String str : reasonforhighrisk) {
							if (str != null && str.length() > 0)
								reasonforHRDisplay = reasonforHRDisplay + reasonforHRArr[Integer.parseInt(str)] + ", ";
						}
					}
				}
			}

			if (woman.getregtype() != 2) {
				String[] AdmittedWith = woman.getAdmitted_with().split(",");
				String[] AdmittedWithArray = null;
				AdmittedWithArray = getResources().getStringArray(R.array.admittedarraylist);

				if (AdmittedWithArray != null) {
					if (AdmittedWith != null && AdmittedWith.length > 0) {
						for (String str : AdmittedWith) {
							if (str != null && str.length() > 0)
								AdmittedWithDisplay = AdmittedWithDisplay + AdmittedWithArray[Integer.parseInt(str)]
										+ ", ";
						}
					}
				}
			}

			PdfPCell ChiefCompaintsRisk = new PdfPCell();
			if (reasonforHRDisplay.length() <= 0) {
				reasonforHRDisplay = getResources().getString(R.string.doted_line)
						+ getResources().getString(R.string.short_doted_line);
				ChiefCompaintsRisk.setPaddingBottom(10);
			}

			Phrase risk = new Phrase(new Chunk("\u2022", FontFactory.getFont(FontFactory.HELVETICA, 20)));
			risk.add(new Phrase(
					new Chunk(reasonforHRDisplay + " ", FontFactory.getFont(FontFactory.HELVETICA, 13, Font.NORMAL))));

			ChiefCompaintsRisk.setPhrase(risk);
			ChiefCompaintsRisk.setHorizontalAlignment(Element.ALIGN_LEFT);
			ChiefCompaintsRisk.setBorder(0);
			ChiefCompaintsRisk.setPaddingLeft(25);

			pdffacilityDetails.addCell(ChiefCompaintsRisk);
			pdffacilityDetails.completeRow();

			PdfPCell ChiefCompaintsAdmittedWith = new PdfPCell();
			if (AdmittedWithDisplay.length() <= 0) {
				// AdmittedWithDisplay = "\(U+FE4D)";
				AdmittedWithDisplay = getResources().getString(R.string.doted_line)
						+ getResources().getString(R.string.short_doted_line);
				ChiefCompaintsAdmittedWith.setPaddingBottom(10);
			}

			Phrase admitted = new Phrase(new Chunk("\u2022", FontFactory.getFont(FontFactory.HELVETICA, 20)));
			admitted.add(new Phrase(
					new Chunk(AdmittedWithDisplay + " ", FontFactory.getFont(FontFactory.HELVETICA, 13, Font.NORMAL))));

			ChiefCompaintsAdmittedWith.setPhrase(admitted);
			ChiefCompaintsAdmittedWith.setHorizontalAlignment(Element.ALIGN_LEFT);
			ChiefCompaintsAdmittedWith.setBorder(0);
			ChiefCompaintsAdmittedWith.setPaddingLeft(25);

			pdffacilityDetails.addCell(ChiefCompaintsAdmittedWith);
			pdffacilityDetails.completeRow();

			Phrase compl = new Phrase(new Chunk("\u2022", FontFactory.getFont(FontFactory.HELVETICA, 20)));
			compl.add(
					new Phrase(new Chunk(
							getResources().getString(R.string.doted_line)
									+ getResources().getString(R.string.short_doted_line) + " ",
							FontFactory.getFont(FontFactory.HELVETICA, 13, Font.NORMAL))));

			PdfPCell ChiefCompaints = new PdfPCell();
			ChiefCompaints.setPhrase(compl);
			ChiefCompaints.setHorizontalAlignment(Element.ALIGN_LEFT);
			ChiefCompaints.setBorder(0);
			ChiefCompaints.setPaddingLeft(25);
			ChiefCompaints.setPaddingBottom(5);

			pdffacilityDetails.addCell(ChiefCompaints);
			pdffacilityDetails.completeRow();

			PdfPCell cellsummary = new PdfPCell();
			cellsummary.setPhrase(new Phrase(getResources().getString(R.string.summary_of_management), font));
			cellsummary.setHorizontalAlignment(Element.ALIGN_LEFT);
			cellsummary.setBorder(0);
			cellsummary.setPaddingTop(2);
			cellsummary.setPaddingBottom(2);

			pdffacilityDetails.addCell(cellsummary);
			pdffacilityDetails.completeRow();

			PdfPCell cellInvestigation = new PdfPCell();
			cellInvestigation.setPhrase(new Phrase(getResources().getString(R.string.investigations) + "     :", font));
			cellInvestigation.setHorizontalAlignment(Element.ALIGN_LEFT);
			cellInvestigation.setBorder(0);
			cellInvestigation.setPaddingTop(2);
			cellInvestigation.setPaddingBottom(2);

			pdffacilityDetails.addCell(cellInvestigation);
			pdffacilityDetails.completeRow();

			addEmptyLine(pdffacilityDetails, 2);

			int grpId = woman.getBloodgroup();

			String[] bldgrps = getResources().getStringArray(R.array.bloodgrp);

			PdfPCell cellBloodGroup = new PdfPCell();
			cellBloodGroup.setPhrase(
					new Phrase(getResources().getString(R.string.bloodgroup) + "       : " + bldgrps[grpId], font));
			cellBloodGroup.setHorizontalAlignment(Element.ALIGN_LEFT);
			cellBloodGroup.setBorder(0);
			cellBloodGroup.setPaddingTop(2);
			cellBloodGroup.setPaddingBottom(2);

			pdffacilityDetails.addCell(cellBloodGroup);
			pdffacilityDetails.completeRow();

			PdfPCell cellHb = new PdfPCell();
			cellHb.setPhrase(new Phrase(getResources().getString(R.string.hb) + "                       :", font));
			cellHb.setHorizontalAlignment(Element.ALIGN_LEFT);
			cellHb.setBorder(0);
			cellHb.setPaddingTop(2);
			cellHb.setPaddingBottom(2);

			pdffacilityDetails.addCell(cellHb);
			pdffacilityDetails.completeRow();

			PdfPCell cellUrine = new PdfPCell();
			cellUrine.setPhrase(new Phrase(getResources().getString(R.string.urine_re) + "            :", font));
			cellUrine.setHorizontalAlignment(Element.ALIGN_LEFT);
			cellUrine.setBorder(0);
			cellUrine.setPaddingTop(2);
			cellUrine.setPaddingBottom(2);

			pdffacilityDetails.addCell(cellUrine);
			pdffacilityDetails.completeRow();

			String consiuousness;
			if (rd_motherconscious.isChecked()) {
				consiuousness = getResources().getString(R.string.conscious);
			} else
				consiuousness = getResources().getString(R.string.unconscious);

			PdfPCell cellCondition = new PdfPCell();
			cellCondition.setPhrase(new Phrase(
					getResources().getString(R.string.condtion_when_referred) + " : " + consiuousness, font));
			cellCondition.setHorizontalAlignment(Element.ALIGN_LEFT);
			cellCondition.setBorder(0);
			cellCondition.setPaddingTop(2);
			cellCondition.setPaddingBottom(2);

			pdffacilityDetails.addCell(cellCondition);
			pdffacilityDetails.completeRow();

			String bp = "";
			if (etbpdiastolicreferral.getText().toString().length() > 0
					&& etbpsystolicreferral.getText().toString().length() > 0) {
				bp = etbpsystolicreferral.getText().toString() + " / " + etbpdiastolicreferral.getText().toString();
			}

			PdfPCell cellcounsiousness = new PdfPCell();
			cellcounsiousness.setPhrase(new Phrase(getResources().getString(R.string.consciousness)
					+ " :                " + getResources().getString(R.string.temperatur) + ":                   "
					+ getResources().getString(R.string.pulse) + " : " + etpulsereferral.getText().toString()
					+ "                  " + getResources().getString(R.string.txtbpvalue) + " : " + bp, font));
			/*
			 * cellcounsiousness.setPhrase(new
			 * Phrase(getResources().getString(R.string.consciousness) +
			 * "     : ", font));
			 */
			cellcounsiousness.setHorizontalAlignment(Element.ALIGN_LEFT);
			cellcounsiousness.setBorder(0);
			cellcounsiousness.setPaddingTop(4);
			cellcounsiousness.setPaddingBottom(4);
			pdffacilityDetails.addCell(cellcounsiousness);
			pdffacilityDetails.completeRow();

			/*
			 * PdfPCell cellTemp = new PdfPCell(new
			 * Phrase(getResources().getString(R.string.temperatur) +
			 * "        : ", font)); cellTemp.setBorder(0);
			 * 
			 * pdffacilityDetails.addCell(cellTemp);
			 * pdffacilityDetails.completeRow();
			 * 
			 * PdfPCell cellPulse = new PdfPCell(); cellPulse.setPhrase( new
			 * Phrase(getResources().getString(R.string.pulse) +
			 * "                     : " + etpulsereferral.getText().toString(),
			 * new Font(Font.HELVETICA, 11, Font.NORMAL)));
			 * cellPulse.setHorizontalAlignment(Element.ALIGN_LEFT);
			 * cellPulse.setBorder(0); cellPulse.setPadding(2);
			 * 
			 * pdffacilityDetails.addCell(cellPulse);
			 * pdffacilityDetails.completeRow();
			 * 
			 * PdfPCell cellBp = new PdfPCell(); cellBp.setPhrase(new
			 * Phrase(getResources().getString(R.string.txtbpvalue) +
			 * "                       : " +
			 * etbpsystolicreferral.getText().toString() + "/" +
			 * etbpdiastolicreferral.getText().toString(), font));
			 * cellBp.setHorizontalAlignment(Element.ALIGN_LEFT);
			 * cellBp.setBorder(0); cellBp.setPadding(2);
			 * 
			 * pdffacilityDetails.addCell(cellBp);
			 * pdffacilityDetails.completeRow();
			 */

			String[] reasonforreferral = womenrefpojoItems.get(0).getReasonforreferral().split(",");
			String[] reasonforrefArr = null;
			reasonforrefArr = getResources().getStringArray(R.array.reasonforreferralvalues);

			String reasonforrefDisplay = "";
			if (reasonforrefArr != null) {
				if (reasonforreferral != null && reasonforreferral.length > 0) {
					for (String str : reasonforreferral) {
						if (str != null && str.length() > 0)
							reasonforrefDisplay = reasonforrefDisplay + reasonforrefArr[Integer.parseInt(str)] + ", ";
					}
				}
			}

			PdfPCell cellOthers = new PdfPCell();
			cellOthers.setPhrase(new Phrase(getResources().getString(R.string.others_specify) + " "
					+ reasonforrefDisplay + " " + getResources().getString(R.string.desc) + " : "
					+ etdescriptionofreferral.getText().toString() + "\n", font));
			cellOthers.setHorizontalAlignment(Element.ALIGN_LEFT);
			cellOthers.setBorder(0);
			cellOthers.setPaddingTop(2);
			cellOthers.setPaddingBottom(2);

			pdffacilityDetails.addCell(cellOthers);
			pdffacilityDetails.completeRow();

			PdfPCell cellOthersExtra = new PdfPCell();
			cellOthersExtra.setPhrase(new Phrase(
					getResources().getString(R.string.doted_line) + getResources().getString(R.string.short_doted_line),
					font));
			cellOthersExtra.setHorizontalAlignment(Element.ALIGN_LEFT);
			cellOthersExtra.setBorder(0);
			cellOthersExtra.setPaddingTop(2);
			cellOthersExtra.setPaddingBottom(2);

			pdffacilityDetails.addCell(cellOthersExtra);
			pdffacilityDetails.completeRow();

			addEmptyLine(pdffacilityDetails, 2);

			// String s = Html.fromHtml("<font
			// color='"+getResources().getColor(R.color.red)+"'><b>"+getResources().getString(R.string.information_on_referral)+"</b></font")+"
			// "+Html.fromHtml("<font
			// color='"+getResources().getColor(R.color.red)+"'>"+getResources().getString(R.string.yes)+"
			// / "+getResources().getString(R.string.no)+"</font>");

			// String s = "Ads";

			Phrase infophase = new Phrase(new Chunk(getResources().getString(R.string.information_on_referral) + " ",
					FontFactory.getFont(FontFactory.HELVETICA, 13, Font.BOLD)));
			infophase.add(new Phrase(
					new Chunk(getResources().getString(R.string.yes) + " / " + getResources().getString(R.string.no),
							FontFactory.getFont(FontFactory.HELVETICA, 13))));

			PdfPCell cellInform = new PdfPCell();
			cellInform.setPhrase(infophase);
			cellInform.setHorizontalAlignment(Element.ALIGN_LEFT);
			cellInform.setBorder(0);
			// cellInform.setPaddingTop(25);
			// cellInform.addElement(font);

			pdffacilityDetails.addCell(cellInform);
			pdffacilityDetails.completeRow();

			addEmptyLine(pdffacilityDetails, 1);

			PdfPCell cellpersonName = new PdfPCell();
			cellpersonName.setPhrase(new Phrase(getResources().getString(R.string.name_of_person), fontHeading));
			cellpersonName.setHorizontalAlignment(Element.ALIGN_LEFT);
			cellpersonName.setBorder(0);
			cellAdmittedDate.setPaddingTop(20);

			pdffacilityDetails.addCell(cellpersonName);
			pdffacilityDetails.completeRow();

			addEmptyLine(pdffacilityDetails, 1);

			PdfPCell cellTransportation = new PdfPCell();

			Phrase datePhrase = new Phrase(new Chunk(getResources().getString(R.string.mode_of_transportation) + " ",
					FontFactory.getFont(FontFactory.HELVETICA, 13, Font.BOLD)));
			datePhrase.add(new Phrase(new Chunk(getResources().getString(R.string.govt_outsorced),
					FontFactory.getFont(FontFactory.HELVETICA, 13))));

			cellTransportation.setPhrase(datePhrase);
			cellTransportation.setHorizontalAlignment(Element.ALIGN_LEFT);
			cellTransportation.setBorder(0);
			cellAdmittedDate.setPaddingTop(20);

			pdffacilityDetails.addCell(cellTransportation);
			pdffacilityDetails.completeRow();

			// addEmptyLine(pdffacilityDetails, 9);

			PdfPCell cellSignature = new PdfPCell();
			cellSignature.setPhrase(new Phrase(getResources().getString(R.string.signature), font));
			cellSignature.setHorizontalAlignment(Element.ALIGN_BOTTOM);
			cellSignature.setBorder(0);
			cellSignature.setPaddingLeft(260);
			cellSignature.setPaddingRight(0);
			cellSignature.setPaddingBottom(0);
			cellSignature.setBorderWidthBottom(0);
			cellSignature.setPaddingTop(25);

			pdffacilityDetails.addCell(cellSignature);
			pdffacilityDetails.completeRow();

			doc.add(pdffacilityDetails);

			String strdatetime = Partograph_CommonClass.getConvertedDateFormat(Partograph_CommonClass.getTodaysDate(),
					Partograph_CommonClass.defdateformat) + " " + Partograph_CommonClass.getCurrentTime() + " ";

			Rectangle rect = new Rectangle(575, 770, 20, 20);
			rect.enableBorderSide(1);
			rect.enableBorderSide(1);
			rect.enableBorderSide(1);
			rect.enableBorderSide(1);
			rect.setBorder(2);
			rect.setBorderColor(Color.BLACK);
			rect.setBorder(Rectangle.BOX);
			rect.setBorderWidth(1);
			doc.add(rect);

			PdfContentByte cb = writer.getDirectContent();

			Font fontfootere = new Font(Font.HELVETICA, 9, Font.NORMAL);

			Phrase footer = new Phrase(strdatetime, fontfootere);

			ColumnText.showTextAligned(cb, Element.ALIGN_RIGHT, footer, doc.right() - 40, doc.bottom() + 60, 0);

		} catch (

		DocumentException de)

		{
			Log.e("PDFCreator", "DocumentException:" + de);
		} catch (

		IOException e)

		{
			Log.e("PDFCreator", "ioException:" + e);
		} finally

		{
			doc.close();
		}

	}

	private void addEmptyLine_para(PdfPTable p1, int number) {

		for (int i = 0; i < number; i++) {
			p1.addCell(" ");

		}
	}

	private void addEmptyLine(PdfPTable p1, int number) {

		for (int i = 0; i < number; i++) {

			PdfPCell cell = new PdfPCell();
			cell.setBorder(0);
			p1.addCell(cell);

		}
	}

	private void addHorizantalLine(PdfPTable p1, int number) {

		for (int i = 0; i < number; i++) {
			// p1.addCell("");

			PdfPCell cell = new PdfPCell();
			cell.setBorderWidthTop(0);
			cell.setBorderWidthLeft(0);
			cell.setBorderWidthRight(0);
			cell.setBorderWidthBottom(1);

			p1.addCell(cell);

		}
	}

	// 02Feb2017 Arpitha
	public void generateReferralPdf() throws Exception {
		Document doc = new Document(PageSize.A4);

		try {

			// path =
			// Environment.getExternalStorageDirectory().getAbsolutePath() +
			// "/partograph/ReferralSheet";

			path = AppContext.mainDir + "/ReferralSheet";// 03April2017 Arpitha

			File dir = new File(path);
			if (!dir.exists())
				dir.mkdirs();

			Log.d("PDFCreator", "PDF Path: " + path);

			file = new File(dir, woman.getWomen_name() + "_ReferralSheet.pdf");
			FileOutputStream fOut = new FileOutputStream(file);

			PdfWriter writer = PdfWriter.getInstance(doc, fOut);

			doc.setMarginMirroring(false);
			doc.setMargins(-25, -20, 65, -50);

			doc.open();

			Font fontTitle = new Font(Font.HELVETICA, 25, Font.BOLD);
			Font fontHeading = new Font(Font.HELVETICA, 13, Font.BOLD);
			Font font = new Font(Font.HELVETICA, 13, Font.NORMAL);

			Paragraph p_heading = new Paragraph(getResources().getString(R.string.referral_slips), fontTitle);
			p_heading.setAlignment(Paragraph.ALIGN_CENTER);
			p_heading.setFont(fontTitle);
			doc.add(p_heading);

			PdfPTable pdffacilityDetails = new PdfPTable(1);

			addEmptyLine(pdffacilityDetails, 3);

			PdfPCell cellFacilityName = new PdfPCell();
			cellFacilityName.setPhrase(new Phrase(getResources().getString(R.string.name_of_the_referring_facility)
					+ " " + woman.getFacility_name() + ", " + woman.getFacility(), font));
			cellFacilityName.setHorizontalAlignment(Element.ALIGN_LEFT);
			cellFacilityName.setPaddingTop(5);
			cellFacilityName.setPaddingLeft(5);
			;
			// cellFacilityName.setPaddingBottom(5);
			cellFacilityName.setBorderWidthBottom(0);

			// cellFacilityName.setUseAscender(true);

			pdffacilityDetails.addCell(cellFacilityName);

			pdffacilityDetails.completeRow();

			PdfPCell cellFacilityAddress = new PdfPCell();
			cellFacilityAddress.setPhrase(new Phrase("Address" + ": " + woman.getFacility_name() + ", "
					+ woman.getTaluk() + "[T], " + woman.getDistrict() + "[D]", font));
			cellFacilityAddress.setHorizontalAlignment(Element.ALIGN_LEFT);
			cellFacilityAddress.setBorderWidthBottom(0);
			cellFacilityAddress.setBorderWidthTop(0);
			cellFacilityAddress.setPaddingTop(5);
			// cellFacilityAddress.setPaddingBottom(5);

			pdffacilityDetails.addCell(cellFacilityAddress);

			pdffacilityDetails.completeRow();

			PdfPCell cellTelephone = new PdfPCell();
			cellTelephone.setPhrase(new Phrase(getResources().getString(R.string.telephone) + ": " + "\n", font));
			cellTelephone.setHorizontalAlignment(Element.ALIGN_LEFT);
			cellTelephone.setBorderWidthTop(0);
			cellTelephone.setPaddingTop(3);
			cellTelephone.setPaddingBottom(5);

			pdffacilityDetails.addCell(cellTelephone);

			pdffacilityDetails.completeRow();

			String woman_name = "";
			String woman_age = "";
			if (woman.getWomen_name().length() > 0) {
				woman_name = woman.getWomen_name();
			} else {
				woman_name = getResources().getString(R.string.doted_line);
			}

			if (woman.getAge() > 0) {
				woman_age = "" + woman.getAge();
			} else
				woman_age = getResources().getString(R.string.doted_line);

			PdfPCell cellPatientName = new PdfPCell();
			cellPatientName.setPhrase(new Phrase(getResources().getString(R.string.name_of_the_patient) + ": "
					+ woman_name + "    " + getResources().getString(R.string.age) + ":  " + "" + woman_age + " "
					+ getResources().getString(R.string.yrs), font));
			cellPatientName.setHorizontalAlignment(Element.ALIGN_LEFT);
			cellPatientName.setBorder(0);
			cellPatientName.setPaddingTop(10);

			pdffacilityDetails.addCell(cellPatientName);
			pdffacilityDetails.completeRow();

			PdfPCell cellHusbandName = new PdfPCell();
			cellHusbandName.setPhrase(new Phrase(getResources().getString(R.string.husbands_name) + ": "
					+ getResources().getString(R.string.doted_line), font));
			cellHusbandName.setHorizontalAlignment(Element.ALIGN_LEFT);
			cellHusbandName.setBorder(0);
			cellHusbandName.setPaddingTop(7);
			cellHusbandName.setPaddingBottom(2);

			pdffacilityDetails.addCell(cellHusbandName);
			pdffacilityDetails.completeRow();

			String woman_address = "";

			if (woman.getAddress().length() > 0) {
				woman_address = woman.getAddress();
			} else
				woman_address = ".............." + getResources().getString(R.string.doted_line);

			PdfPCell cellAddress = new PdfPCell();
			cellAddress.setPhrase(new Phrase("Address" + ": " + woman_address + "\n", font));
			cellAddress.setHorizontalAlignment(Element.ALIGN_LEFT);
			cellAddress.setBorder(0);
			cellAddress.setPaddingTop(7);
			cellAddress.setPaddingBottom(2);

			pdffacilityDetails.addCell(cellAddress);
			pdffacilityDetails.completeRow();

			if (woman.getAddress().length() <= 0) {
				PdfPCell cellAddressExtra = new PdfPCell();
				cellAddressExtra.setPhrase(new Phrase(
						getResources().getString(R.string.doted_line) + "...........................", font));
				cellAddressExtra.setHorizontalAlignment(Element.ALIGN_LEFT);
				cellAddressExtra.setBorder(0);
				cellAddressExtra.setPaddingTop(7);
				cellAddressExtra.setPaddingBottom(2);

				pdffacilityDetails.addCell(cellAddressExtra);
				pdffacilityDetails.completeRow();
			}

			addEmptyLine(pdffacilityDetails, 2);

			addHorizantalLine(pdffacilityDetails, 1);

			// addEmptyLine(pdffacilityDetails, 1);

			String refralDate = Partograph_CommonClass
					.getConvertedDateFormat(strRefDate == null ? todaysDate : strRefDate, "dd/MM/yyyy");

			String referralTime = Partograph_CommonClass.gettimein12hrformat(ettimeofreferral.getText().toString());

			String referralPlace = etplaceofreferral.getText().toString();

			if (referralPlace.trim().length() <= 0) {
				referralPlace = getResources().getString(R.string.short_doted_line)
						+ getResources().getString(R.string.short_doted_line);
			}

			PdfPCell cellReferedDate = new PdfPCell();
			cellReferedDate
					.setPhrase(
							new Phrase(
									getResources().getString(R.string.referred_on) + ": " + refralDate + " "
											+ getResources().getString(R.string.at) + " " + referralTime + " "
											+ getResources().getString(R.string.time_to) + "  " + referralPlace + " "
											+ getResources().getString(R.string.name_of_facility_for_management),
									font));
			cellReferedDate.setHorizontalAlignment(Element.ALIGN_LEFT);
			cellReferedDate.setBorder(0);
			cellReferedDate.setPaddingTop(5);
			cellReferedDate.setPaddingBottom(5);

			pdffacilityDetails.addCell(cellReferedDate);
			pdffacilityDetails.completeRow();

			PdfPCell cellProvisionalDiagnosis = new PdfPCell();
			cellProvisionalDiagnosis.setPhrase(
					new Phrase(getResources().getString(R.string.provisional_diagnosis) + " : ", fontHeading));
			cellProvisionalDiagnosis.setHorizontalAlignment(Element.ALIGN_LEFT);
			cellProvisionalDiagnosis.setBorder(0);
			cellProvisionalDiagnosis.setPaddingBottom(10);
			pdffacilityDetails.addCell(cellProvisionalDiagnosis);
			pdffacilityDetails.completeRow();

			String gravida = "G" + woman.getGravida() + " " + "P" + woman.getPara();

			PdfPCell ProvisionalDiagnosis = new PdfPCell();
			ProvisionalDiagnosis.setPhrase(new Phrase(gravida, font));
			ProvisionalDiagnosis.setHorizontalAlignment(Element.ALIGN_LEFT);
			ProvisionalDiagnosis.setPaddingBottom(40);
			ProvisionalDiagnosis.setPaddingTop(5);

			pdffacilityDetails.addCell(ProvisionalDiagnosis);
			pdffacilityDetails.completeRow();

			// addEmptyLine(pdffacilityDetails, 2);

			String regDate;
			String regTime = "";
			if (woman.getregtype() != 2)
				regDate = Partograph_CommonClass.getConvertedDateFormat(woman.getDate_of_admission(), "dd/MM/yyyy");
			else
				regDate = Partograph_CommonClass.getConvertedDateFormat(woman.getDate_of_reg_entry(), "dd/MM/yyyy");

			if (woman.getregtype() != 2)
				regTime = Partograph_CommonClass.gettimein12hrformat(woman.getTime_of_admission());

			PdfPCell cellAdmittedDate = new PdfPCell();
			cellAdmittedDate
					.setPhrase(new Phrase(getResources().getString(R.string.admitted_in_the_referring_facility_on) + " "
							+ regDate + " " + getResources().getString(R.string.at) + " " + regTime + " "
							+ getResources().getString(R.string.time_with), font));
			cellAdmittedDate.setHorizontalAlignment(Element.ALIGN_LEFT);
			cellAdmittedDate.setBorder(0);
			cellAdmittedDate.setPaddingTop(10);

			pdffacilityDetails.addCell(cellAdmittedDate);
			pdffacilityDetails.completeRow();

			// addEmptyLine(pdffacilityDetails, 2);

			PdfPCell cellChiefCompaints = new PdfPCell();
			cellChiefCompaints
					.setPhrase(new Phrase(getResources().getString(R.string.chief_complaints_of), fontHeading));
			cellChiefCompaints.setHorizontalAlignment(Element.ALIGN_LEFT);
			cellChiefCompaints.setBorder(0);

			pdffacilityDetails.addCell(cellChiefCompaints);
			pdffacilityDetails.completeRow();

			String reasonforHRDisplay = "";
			String AdmittedWithDisplay = "";
			if (woman.getregtype() != 2) {
				String[] reasonforhighrisk = woman.getRiskoptions().split(",");
				String[] reasonforHRArr = null;
				reasonforHRArr = getResources().getStringArray(R.array.riskoptionsvalues);

				if (reasonforHRArr != null) {
					if (reasonforhighrisk != null && reasonforhighrisk.length > 0) {
						for (String str : reasonforhighrisk) {
							if (str != null && str.length() > 0)
								reasonforHRDisplay = reasonforHRDisplay + reasonforHRArr[Integer.parseInt(str)] + ", ";
						}
					}
				}
			}

			if (woman.getregtype() != 2) {
				String[] AdmittedWith = woman.getAdmitted_with().split(",");
				String[] AdmittedWithArray = null;
				AdmittedWithArray = getResources().getStringArray(R.array.admittedarraylist);

				if (AdmittedWithArray != null) {
					if (AdmittedWith != null && AdmittedWith.length > 0) {
						for (String str : AdmittedWith) {
							if (str != null && str.length() > 0)
								AdmittedWithDisplay = AdmittedWithDisplay + AdmittedWithArray[Integer.parseInt(str)]
										+ ", ";
						}
					}
				}
			}

			PdfPCell ChiefCompaintsRisk = new PdfPCell();
			if (reasonforHRDisplay.length() <= 0) {
				reasonforHRDisplay = getResources().getString(R.string.doted_line)
						+ getResources().getString(R.string.short_doted_line);
				ChiefCompaintsRisk.setPaddingBottom(10);
			}

			Phrase risk = new Phrase(new Chunk("\u2022", FontFactory.getFont(FontFactory.HELVETICA, 20)));
			risk.add(new Phrase(
					new Chunk(reasonforHRDisplay + " ", FontFactory.getFont(FontFactory.HELVETICA, 13, Font.NORMAL))));

			ChiefCompaintsRisk.setPhrase(risk);
			ChiefCompaintsRisk.setHorizontalAlignment(Element.ALIGN_LEFT);
			ChiefCompaintsRisk.setBorder(0);
			ChiefCompaintsRisk.setPaddingLeft(25);

			pdffacilityDetails.addCell(ChiefCompaintsRisk);
			pdffacilityDetails.completeRow();

			PdfPCell ChiefCompaintsAdmittedWith = new PdfPCell();
			if (AdmittedWithDisplay.length() <= 0) {
				// AdmittedWithDisplay = "\(U+FE4D)";
				AdmittedWithDisplay = getResources().getString(R.string.doted_line)
						+ getResources().getString(R.string.short_doted_line);
				ChiefCompaintsAdmittedWith.setPaddingBottom(10);
			}

			Phrase admitted = new Phrase(new Chunk("\u2022", FontFactory.getFont(FontFactory.HELVETICA, 20)));
			admitted.add(new Phrase(
					new Chunk(AdmittedWithDisplay + " ", FontFactory.getFont(FontFactory.HELVETICA, 13, Font.NORMAL))));

			ChiefCompaintsAdmittedWith.setPhrase(admitted);
			ChiefCompaintsAdmittedWith.setHorizontalAlignment(Element.ALIGN_LEFT);
			ChiefCompaintsAdmittedWith.setBorder(0);
			ChiefCompaintsAdmittedWith.setPaddingLeft(25);

			pdffacilityDetails.addCell(ChiefCompaintsAdmittedWith);
			pdffacilityDetails.completeRow();

			Phrase compl = new Phrase(new Chunk("\u2022", FontFactory.getFont(FontFactory.HELVETICA, 20)));
			compl.add(
					new Phrase(new Chunk(
							getResources().getString(R.string.doted_line)
									+ getResources().getString(R.string.short_doted_line) + " ",
							FontFactory.getFont(FontFactory.HELVETICA, 13, Font.NORMAL))));

			PdfPCell ChiefCompaints = new PdfPCell();
			ChiefCompaints.setPhrase(compl);
			ChiefCompaints.setHorizontalAlignment(Element.ALIGN_LEFT);
			ChiefCompaints.setBorder(0);
			ChiefCompaints.setPaddingLeft(25);
			ChiefCompaints.setPaddingBottom(5);

			pdffacilityDetails.addCell(ChiefCompaints);
			pdffacilityDetails.completeRow();

			PdfPCell cellsummary = new PdfPCell();
			cellsummary.setPhrase(new Phrase(getResources().getString(R.string.summary_of_management), font));
			cellsummary.setHorizontalAlignment(Element.ALIGN_LEFT);
			cellsummary.setBorder(0);
			cellsummary.setPaddingTop(2);
			cellsummary.setPaddingBottom(2);

			pdffacilityDetails.addCell(cellsummary);
			pdffacilityDetails.completeRow();

			PdfPCell cellInvestigation = new PdfPCell();
			cellInvestigation.setPhrase(new Phrase(getResources().getString(R.string.investigations) + "     :", font));
			cellInvestigation.setHorizontalAlignment(Element.ALIGN_LEFT);
			cellInvestigation.setBorder(0);
			cellInvestigation.setPaddingTop(2);
			cellInvestigation.setPaddingBottom(2);

			pdffacilityDetails.addCell(cellInvestigation);
			pdffacilityDetails.completeRow();

			addEmptyLine(pdffacilityDetails, 2);

			int grpId = woman.getBloodgroup();

			String[] bldgrps = getResources().getStringArray(R.array.bloodgrp);

			PdfPCell cellBloodGroup = new PdfPCell();
			cellBloodGroup.setPhrase(new Phrase("Blood Group" + "       : " + bldgrps[grpId], font));
			cellBloodGroup.setHorizontalAlignment(Element.ALIGN_LEFT);
			cellBloodGroup.setBorder(0);
			cellBloodGroup.setPaddingTop(2);
			cellBloodGroup.setPaddingBottom(2);

			pdffacilityDetails.addCell(cellBloodGroup);
			pdffacilityDetails.completeRow();

			PdfPCell cellHb = new PdfPCell();
			cellHb.setPhrase(new Phrase(getResources().getString(R.string.hb) + "                       :", font));
			cellHb.setHorizontalAlignment(Element.ALIGN_LEFT);
			cellHb.setBorder(0);
			cellHb.setPaddingTop(2);
			cellHb.setPaddingBottom(2);

			pdffacilityDetails.addCell(cellHb);
			pdffacilityDetails.completeRow();

			PdfPCell cellUrine = new PdfPCell();
			cellUrine.setPhrase(new Phrase(getResources().getString(R.string.urine_re) + "            :", font));
			cellUrine.setHorizontalAlignment(Element.ALIGN_LEFT);
			cellUrine.setBorder(0);
			cellUrine.setPaddingTop(2);
			cellUrine.setPaddingBottom(2);

			pdffacilityDetails.addCell(cellUrine);
			pdffacilityDetails.completeRow();

			String consiuousness;
			if (rd_motherconscious.isChecked()) {
				consiuousness = getResources().getString(R.string.conscious);
			} else
				consiuousness = getResources().getString(R.string.unconscious);

			PdfPCell cellCondition = new PdfPCell();
			cellCondition.setPhrase(new Phrase(
					getResources().getString(R.string.condtion_when_referred) + " : " + consiuousness, font));
			cellCondition.setHorizontalAlignment(Element.ALIGN_LEFT);
			cellCondition.setBorder(0);
			cellCondition.setPaddingTop(2);
			cellCondition.setPaddingBottom(2);

			pdffacilityDetails.addCell(cellCondition);
			pdffacilityDetails.completeRow();

			String bp = "";
			if (etbpdiastolicreferral.getText().toString().length() > 0
					&& etbpsystolicreferral.getText().toString().length() > 0) {
				bp = etbpsystolicreferral.getText().toString() + " / " + etbpdiastolicreferral.getText().toString();
			}

			PdfPCell cellcounsiousness = new PdfPCell();
			cellcounsiousness.setPhrase(new Phrase(getResources().getString(R.string.consciousness)
					+ " :                " + "Temperature" + ":                   " + "Pulse" + " : "
					+ etpulsereferral.getText().toString() + "                  " + "BP" + " : " + bp, font));
			/*
			 * cellcounsiousness.setPhrase(new
			 * Phrase(getResources().getString(R.string.consciousness) +
			 * "     : ", font));
			 */
			cellcounsiousness.setHorizontalAlignment(Element.ALIGN_LEFT);
			cellcounsiousness.setBorder(0);
			cellcounsiousness.setPaddingTop(4);
			cellcounsiousness.setPaddingBottom(4);
			pdffacilityDetails.addCell(cellcounsiousness);
			pdffacilityDetails.completeRow();

			/*
			 * PdfPCell cellTemp = new PdfPCell(new
			 * Phrase(getResources().getString(R.string.temperatur) +
			 * "        : ", font)); cellTemp.setBorder(0);
			 * 
			 * pdffacilityDetails.addCell(cellTemp);
			 * pdffacilityDetails.completeRow();
			 * 
			 * PdfPCell cellPulse = new PdfPCell(); cellPulse.setPhrase( new
			 * Phrase(getResources().getString(R.string.pulse) +
			 * "                     : " + etpulsereferral.getText().toString(),
			 * new Font(Font.HELVETICA, 11, Font.NORMAL)));
			 * cellPulse.setHorizontalAlignment(Element.ALIGN_LEFT);
			 * cellPulse.setBorder(0); cellPulse.setPadding(2);
			 * 
			 * pdffacilityDetails.addCell(cellPulse);
			 * pdffacilityDetails.completeRow();
			 * 
			 * PdfPCell cellBp = new PdfPCell(); cellBp.setPhrase(new
			 * Phrase(getResources().getString(R.string.txtbpvalue) +
			 * "                       : " +
			 * etbpsystolicreferral.getText().toString() + "/" +
			 * etbpdiastolicreferral.getText().toString(), font));
			 * cellBp.setHorizontalAlignment(Element.ALIGN_LEFT);
			 * cellBp.setBorder(0); cellBp.setPadding(2);
			 * 
			 * pdffacilityDetails.addCell(cellBp);
			 * pdffacilityDetails.completeRow();
			 */

			// String[] reasonforreferral =
			// womenrefpojoItems.get(0).getReasonforreferral().split(",");
			// String[] reasonforrefArr = null;
			// reasonforrefArr =
			// getResources().getStringArray(R.array.reasonforreferralvalues);

			String reasonforrefDisplay = "";
			/*
			 * if (reasonforrefArr != null) { if (reasonforreferral != null &&
			 * reasonforreferral.length > 0) { for (String str :
			 * reasonforreferral) { if (str != null && str.length() > 0)
			 * reasonforrefDisplay = reasonforrefDisplay +
			 * reasonforrefArr[Integer.parseInt(str)] + ", "; } } }
			 */

			PdfPCell cellOthers = new PdfPCell();
			cellOthers.setPhrase(new Phrase(getResources().getString(R.string.others_specify) + " "
					+ reasonforrefDisplay + " " + getResources().getString(R.string.desc) + " : "
					+ etdescriptionofreferral.getText().toString() + "\n", font));
			cellOthers.setHorizontalAlignment(Element.ALIGN_LEFT);
			cellOthers.setBorder(0);
			cellOthers.setPaddingTop(2);
			cellOthers.setPaddingBottom(2);

			pdffacilityDetails.addCell(cellOthers);
			pdffacilityDetails.completeRow();

			PdfPCell cellOthersExtra = new PdfPCell();
			cellOthersExtra.setPhrase(new Phrase(
					getResources().getString(R.string.doted_line) + getResources().getString(R.string.short_doted_line),
					font));
			cellOthersExtra.setHorizontalAlignment(Element.ALIGN_LEFT);
			cellOthersExtra.setBorder(0);
			cellOthersExtra.setPaddingTop(2);
			cellOthersExtra.setPaddingBottom(2);

			pdffacilityDetails.addCell(cellOthersExtra);
			pdffacilityDetails.completeRow();

			addEmptyLine(pdffacilityDetails, 2);

			// String s = Html.fromHtml("<font
			// color='"+getResources().getColor(R.color.red)+"'><b>"+getResources().getString(R.string.information_on_referral)+"</b></font")+"
			// "+Html.fromHtml("<font
			// color='"+getResources().getColor(R.color.red)+"'>"+getResources().getString(R.string.yes)+"
			// / "+getResources().getString(R.string.no)+"</font>");

			// String s = "Ads";

			Phrase infophase = new Phrase(new Chunk(getResources().getString(R.string.information_on_referral) + " ",
					FontFactory.getFont(FontFactory.HELVETICA, 13, Font.BOLD)));
			infophase.add(new Phrase(
					new Chunk(getResources().getString(R.string.yes) + " / " + getResources().getString(R.string.no),
							FontFactory.getFont(FontFactory.HELVETICA, 13))));

			PdfPCell cellInform = new PdfPCell();
			cellInform.setPhrase(infophase);
			cellInform.setHorizontalAlignment(Element.ALIGN_LEFT);
			cellInform.setBorder(0);
			// cellInform.setPaddingTop(25);
			// cellInform.addElement(font);

			pdffacilityDetails.addCell(cellInform);
			pdffacilityDetails.completeRow();

			addEmptyLine(pdffacilityDetails, 1);

			PdfPCell cellpersonName = new PdfPCell();
			cellpersonName.setPhrase(new Phrase(getResources().getString(R.string.name_of_person), fontHeading));
			cellpersonName.setHorizontalAlignment(Element.ALIGN_LEFT);
			cellpersonName.setBorder(0);
			cellAdmittedDate.setPaddingTop(20);

			pdffacilityDetails.addCell(cellpersonName);
			pdffacilityDetails.completeRow();

			addEmptyLine(pdffacilityDetails, 1);

			PdfPCell cellTransportation = new PdfPCell();

			Phrase datePhrase = new Phrase(new Chunk(getResources().getString(R.string.mode_of_transportation) + " ",
					FontFactory.getFont(FontFactory.HELVETICA, 13, Font.BOLD)));
			datePhrase.add(new Phrase(new Chunk(getResources().getString(R.string.govt_outsorced),
					FontFactory.getFont(FontFactory.HELVETICA, 13))));

			cellTransportation.setPhrase(datePhrase);
			cellTransportation.setHorizontalAlignment(Element.ALIGN_LEFT);
			cellTransportation.setBorder(0);
			cellAdmittedDate.setPaddingTop(20);

			pdffacilityDetails.addCell(cellTransportation);
			pdffacilityDetails.completeRow();

			// addEmptyLine(pdffacilityDetails, 9);

			PdfPCell cellSignature = new PdfPCell();
			cellSignature.setPhrase(new Phrase(getResources().getString(R.string.signature), font));
			cellSignature.setHorizontalAlignment(Element.ALIGN_BOTTOM);
			cellSignature.setBorder(0);
			cellSignature.setPaddingLeft(260);
			cellSignature.setPaddingRight(0);
			cellSignature.setPaddingBottom(0);
			cellSignature.setBorderWidthBottom(0);
			cellSignature.setPaddingTop(25);

			pdffacilityDetails.addCell(cellSignature);
			pdffacilityDetails.completeRow();

			doc.add(pdffacilityDetails);

			String strdatetime = Partograph_CommonClass.getConvertedDateFormat(Partograph_CommonClass.getTodaysDate(),
					Partograph_CommonClass.defdateformat) + " " + Partograph_CommonClass.getCurrentTime() + " ";

			Rectangle rect = new Rectangle(575, 770, 20, 20);
			rect.enableBorderSide(1);
			rect.enableBorderSide(1);
			rect.enableBorderSide(1);
			rect.enableBorderSide(1);
			rect.setBorder(2);
			rect.setBorderColor(Color.BLACK);
			rect.setBorder(Rectangle.BOX);
			rect.setBorderWidth(1);
			doc.add(rect);

			PdfContentByte cb = writer.getDirectContent();

			Font fontfootere = new Font(Font.HELVETICA, 9, Font.NORMAL);

			Phrase footer = new Phrase(strdatetime, fontfootere);

			ColumnText.showTextAligned(cb, Element.ALIGN_RIGHT, footer, doc.right() - 40, doc.bottom() + 60, 0);

		} catch (

		DocumentException de)

		{
			Log.e("PDFCreator", "DocumentException:" + de);
		} catch (

		IOException e)

		{
			Log.e("PDFCreator", "ioException:" + e);
		} finally

		{
			doc.close();
			// Toast.makeText(getApplicationContext(), "PDF is generated",
			// Toast.LENGTH_LONG).show();
		}

	}

	// 16Jan2017 Arpitha
	void displayAlertDialog() {

		final Dialog dialog = new Dialog(ReferralInfo_Activity.this);

		dialog.setTitle(getResources().getString(R.string.afm_abrreivation));
		dialog.requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		dialog.setContentView(R.layout.dialog_action);
		dialog.getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.dialog_title);
		txtdialogtitle = (TextView) dialog.findViewById(R.id.graph_title);
		txtdialogtitle.setText(getResources().getString(R.string.partograph));

		ImageButton imgbtncanceldialog = (ImageButton) dialog.findViewById(R.id.imgbtncanceldialog);
		imgbtncanceldialog.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				try {
					dialog.cancel();
				} catch (Exception e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					e.printStackTrace();
				}
			}
		});

		final RadioButton rdsavepdf = (RadioButton) dialog.findViewById(R.id.rdsave);
		final RadioButton rdviewpdf = (RadioButton) dialog.findViewById(R.id.rdview);
		Button btndone = (Button) dialog.findViewById(R.id.btndone);

		final RadioButton rdshare = (RadioButton) dialog.findViewById(R.id.rdshare);

		btndone.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (rdsavepdf.isChecked()) {
					try {
						dialog.cancel();
						// Partograph_CommonClass.generateReferralPdf();
						Toast.makeText(getApplicationContext(),
								getResources().getString(R.string.pdf_file_is_saved_at_path) + " "
										+ "storage/Internal Memory/Pdf/ReferralSheet",
								Toast.LENGTH_LONG).show();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}
				} else if (rdviewpdf.isChecked())

				{
					try {
						dialog.cancel();
						// Partograph_CommonClass.generateReferralPdf();
						// to view pdf file from appliction
						if (file.exists()) {

							Uri pdfpath = Uri.fromFile(file);
							Intent intent = new Intent(Intent.ACTION_VIEW);
							intent.setDataAndType(pdfpath, "application/pdf");
							intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

							try {
								startActivity(intent);
							} catch (ActivityNotFoundException e) {// 24Jan2017
																	// Arpitha
								// No application to view, ask to download one
								AlertDialog.Builder builder = new AlertDialog.Builder(ReferralInfo_Activity.this);
								builder.setTitle("No Application Found");
								builder.setMessage("Download one from Android Market?");
								builder.setPositiveButton("Yes, Please", new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										Intent marketIntent = new Intent(Intent.ACTION_VIEW);
										marketIntent.setData(Uri.parse("market://details?id=com.adobe.reader"));
										startActivity(marketIntent);
									}
								});
								builder.setNegativeButton("No, Thanks", null);
								builder.create().show();
							}
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}
				} // 24Jan2017
				/*
				 * else if (rdshare.isChecked()) { // SendviaBluetoth();
				 * 
				 * }
				 */

				else {
					Toast.makeText(getApplicationContext(), getResources().getString(R.string.please_select_one_option),
							Toast.LENGTH_LONG).show();
				}

			}
		});
		dialog.show();

	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub

	}

	// 10April2017 Arpitha
	// setInput Filters
	private void setInputFiltersForEdittext() throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		etplaceofreferral.setFilters(new InputFilter[] { filter });
		etdescriptionofreferral.setFilters(new InputFilter[] { filter });
		// etpulsereferral.setFilters(new InputFilter[] { filter });
		// etbpdiastolicreferral.setFilters(new InputFilter[] { filter });
		// etbpsystolicreferral.setFilters(new InputFilter[] { filter });
	}

	// To avoid special characters in Input type
	public static InputFilter filter = new InputFilter() {
		@Override
		public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
			// String blockCharacterSet =
			// "~#^|$%*!@/()-'\":;?{}=!$^';?Ã—Ã·<>{}â‚¬Â£Â¥â‚©%&+*.[]1234567890Â¶";
			String blockCharacterSet = "~#^|$%*!@/()-'\":;?{}=!$^';?Ã—Ã·<>{}â‚¬Â£Â¥â‚©%&+*[]~#^|$%&*!`@_=?}{[()\"\\Â¥Â®Â¿ÃŒâ„¢â‚¹Â°^âˆšÏ€Ã·Ã—â–³Â¶Â£â€¢Â¢â‚¬â™¥â™¡â˜…â˜†â–²â–¼â†‘â†�â†“â†’Â¤â–³â™‚â™€â„ƒ||â–³Â©c/o||Â¿Â¡â„…â„¢Â®â‚¹Â°Â¢`â€¢âˆšÏ€Â¶âˆ†Â¢Â°âˆ†Â¶Ï€âˆšâ€¢`";
			if (source != null && blockCharacterSet.contains(("" + source))) {
				return "";
			}
			return null;
		}
	};

}
