package com.bc.partograph.latentphase;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;

import com.androidquery.AQuery;
import com.bc.partograph.comments.Comments_Activity;
import com.bc.partograph.common.AppContext;
import com.bc.partograph.common.KillAllActivitesAndGoToLogin;
import com.bc.partograph.common.Partograph_CommonClass;
import com.bc.partograph.common.Partograph_CommonClass.AsyncCallWS;
import com.bc.partograph.common.Partograph_DB;
import com.bc.partograph.deliveryinfo.DeliveryInfo_Activity;
import com.bc.partograph.login.LoginActivity;
import com.bc.partograph.referralinfo.ReferralInfo_Activity;
import com.bc.partograph.settings.About;
import com.bc.partograph.settings.Settings_parto;
import com.bc.partograph.sliding.GraphInformation;
import com.bc.partograph.sliding.SlidingActivity;
import com.bc.partograph.stagesoflabor.StageofLabor;
import com.bc.partograph.sync.SyncFunctions;
import com.bc.partograph.womenview.Activity_WomenView;
import com.bluecrimson.additionalDetails.Summary_Activity;
import com.bluecrimson.apgar.Activity_Apgar;
import com.bluecrimson.dischargedetails.DiscargeDetails_Activity;
import com.bluecrimson.dischargedetails.DiscgargePojo;
import com.bluecrimson.dischargedetails.DischargedWomanList_Activity;
import com.bluecrimson.partograph.R;
import com.bluecrimson.partograph.Women_Profile_Pojo;
import com.bluecrimson.postpartumcare.PostPartumCare_Activity;
import com.bluecrimson.usermanual.UseManual;
import com.bluecrimson.viewprofile.ViewProfile_Activity;
import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources.NotFoundException;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

public class LatentPhase_Activity extends Activity implements OnClickListener {

	AQuery aq;
	Partograph_DB dbh;
	String selecteddate;
	int year, mon, day;
	static String todaysDate;
	String date;
	static Women_Profile_Pojo woman;
	Thread myThread;
	LatentPhasePojo lpojo;
	TableLayout tbl;
	ArrayList<LatentPhasePojo> arrVal;
	File file;
	private static Font small = new Font(Font.HELVETICA, 8, Font.NORMAL);
	private static Font heading = new Font(Font.HELVETICA, 11, Font.BOLD);
	static PdfPTable womanbasics;
	static PdfPTable lat;
	String advice = "", bp = "", pulse = "", fhs = "", pv = "", contractions = "";
	public static int latent;
	public static boolean islatent;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_latentphase);
		try {
			woman = (Women_Profile_Pojo) getIntent().getSerializableExtra("woman");
			aq = new AQuery(this);

			latent = Partograph_CommonClass.curr_tabpos;// 03Sep2017 Arpitha
			islatent = true;// 03Sep2017 Arpitha

			initializeScreen(aq.id(R.id.rdlat).getView());

			tbl = (TableLayout) findViewById(R.id.tblvalues);

			this.setTitle(getResources().getString(R.string.latent_phase));

			getwomanbasicdata();

			dbh = Partograph_DB.getInstance(getApplicationContext());
			intialview();
			myThread = null;

			Runnable myRunnableThread = new CountDownRunner();
			myThread = new Thread(myRunnableThread);
			myThread.start();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}KillAllActivitesAndGoToLogin.addToStack(this);

	}

	private void intialview() throws Exception {
		// TODO Auto-generated method stub

		if (woman.getDel_type() != 0 || dbh.getPartoData(woman.getWomenId(), woman.getUserId())
				|| dbh.getisWomenReferred(woman.getWomenId(), woman.getUserId()) || woman.getregtype() == 2) {
			aq.id(R.id.reladddata).gone();
			aq.id(R.id.txtnodata).visible();
			aq.id(R.id.txtnodata).text(getResources().getString(R.string.latent_phase_data_is_disabled));
		}else
			aq.id(R.id.reladddata).visible();

		aq.id(R.id.etdatelat).text(Partograph_CommonClass.getTodaysDate(Partograph_CommonClass.defdateformat));
		aq.id(R.id.ettimelat).text(Partograph_CommonClass.getCurrentTime());

		// arrVal = new ArrayList<LatentPhasePojo>();

		/*
		 * if (woman.getregtype() == 2) { aq.id(R.id.txtnodata).visible();
		 * aq.id(R.id.txtnodata).text(getResources().getString(R.string.
		 * latent_phase_data_is_disabled)); }
		 */

		displayValues();

		// 10Aug2017 Atpitha
		ArrayList<DiscgargePojo> arrValDisc = new ArrayList<DiscgargePojo>();
		arrValDisc = dbh.getDischargeData(woman.getUserId(), woman.getWomenId());
		if (arrValDisc.size() > 0) {
			// aq.id(R.id.txtnodata).setLayerType11(type, paint)
			aq.id(R.id.txtnodata).visible();
			aq.id(R.id.txtnodata)
					.text(getResources().getString(R.string.cannot_enter_any_data_after_updating_discharge_details));
			aq.id(R.id.reladddata).gone();
			// txtdisabled.setVisibility(View.VISIBLE);
			// reldel.setVisibility(View.GONE);

		} // 10Aug2017 Atpitha
			// else
			// aq.id(R.id.txtnodata).gone();
			// aq.id(R.id.txtnodata).text(getResources().getString(R.string.latent_phase_data_is_disabled));

		if (arrVal.size() > 0) {
			aq.id(R.id.txtnodata).gone();
		}

	}

	public void doWork() throws Exception {
		if (LatentPhase_Activity.this != null) {
			LatentPhase_Activity.this.runOnUiThread(new Runnable() {
				public void run() {
					try {

						aq.id(R.id.ettimelat).text(Partograph_CommonClass.getCurrentTime());

					} catch (Exception e) {
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}
				}
			});
		}
	}

	class CountDownRunner implements Runnable {
		// @Override
		public void run() {
			while (!Thread.currentThread().isInterrupted()) {
				try {
					doWork();
					Thread.sleep(1000); // Pause of 1 Second
				} catch (InterruptedException e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					Thread.currentThread().interrupt();
				} catch (Exception e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
				}
			}
		}
	}

	/**
	 * This is a Recursive method that traverse through the group and subgroup
	 * of view Sets text, assigns clickListners
	 * 
	 * @throws Exception
	 */
	private ArrayList<View> initializeScreen(View v) throws Exception {
		if (!(v instanceof ViewGroup)) {
			ArrayList<View> viewArrayList = new ArrayList<View>();
			viewArrayList.add(v);

			// Imageview - assign ID
			if (v.getClass() == ImageView.class || v.getClass() == ImageButton.class) {
				aq.id(v.getId()).getImageView().setOnClickListener(this);
			}

			// Edittext
			if (v.getClass() == EditText.class) {
				// aq.id(v.getId()).getEditText().setOnClickListener(this);
			}

			// Button
			if (v.getClass() == Button.class) {
				aq.id(v.getId()).getButton().setOnClickListener(this);
			}

			if (v.getClass() == CheckBox.class) {
				aq.id(v.getId()).getCheckBox().setOnClickListener(this);
			}

			if (v.getClass() == RadioButton.class) {
				aq.id(v.getId()).getButton().setOnClickListener(this);
			}

			return viewArrayList;
		}
		if (v instanceof Spinner) {
			aq.id(v.getId()).itemSelected(this, "onSpinnerClicked");
		}

		if (v.getClass() == TableRow.class) {

		}

		ArrayList<View> result = new ArrayList<View>();

		ViewGroup viewGroup = (ViewGroup) v;
		for (int i = 0; i < viewGroup.getChildCount(); i++) {

			View child = viewGroup.getChildAt(i);

			ArrayList<View> viewArrayList = new ArrayList<View>();
			viewArrayList.add(v);
			viewArrayList.addAll(initializeScreen(child));

			result.addAll(viewArrayList);
		}
		return result;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		try {
			switch (v.getId()) {

			case R.id.btnsave:
				saveLatentData();
				break;
			case R.id.imgpdf:
				Partograph_CommonClass.pdfAlertDialog(LatentPhase_Activity.this, woman, "latentphase", arrVal, null);
				// displayAlertDialog();
				break;

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}

	}

	private void saveLatentData() {
		// TODO Auto-generated method stub
		try {
			if (validtaedAllFields()) {

				lpojo = new LatentPhasePojo();
				lpojo.setUserId(woman.getUserId());
				lpojo.setWomanId(woman.getWomenId());
				lpojo.setWomanName(woman.getWomen_name());
				String pulse = aq.id(R.id.etpulselat).getText().toString();
				if (pulse.trim().length() > 0)
					lpojo.setPulse(Integer.parseInt(pulse));
				String cont = aq.id(R.id.etcontrcationslat).getText().toString();
				if (cont.trim().length() > 0)
					lpojo.setContractions(Integer.parseInt(cont));
				String fhs = aq.id(R.id.etfhslat).getText().toString();
				if (fhs.trim().length() > 0)
					lpojo.setFhs(Integer.parseInt(fhs));
				String pv = aq.id(R.id.etpvlat).getText().toString();
				if (pv.trim().length() > 0)
					lpojo.setPv(pv);
				String bp = aq.id(R.id.etbpsyslat).getText().toString() + "/"
						+ aq.id(R.id.etbpdialat).getText().toString();
				if (aq.id(R.id.etbpsyslat).getText().toString().length() > 0
						&& aq.id(R.id.etbpsyslat).getText().toString().length() > 0)
					lpojo.setBp(bp);
				lpojo.setAdvice(aq.id(R.id.etadvicelat).getText().toString());
				// lpojo.setDuration(aq.id(R.id.etduration).getText().toString());
				lpojo.setDatetime(
						Partograph_CommonClass.getTodaysDate() + "/" + aq.id(R.id.ettimelat).getText().toString());

				dbh.db.beginTransaction();
				int transId = dbh.iCreateNewTrans(woman.getUserId());
				boolean isDataInserted = dbh.addLatentData(lpojo, transId);
				if (isDataInserted) {
					dbh.iNewRecordTrans(woman.getUserId(), transId, Partograph_DB.TBL_LATENTPHASE);

					commitTrans();
					Toast.makeText(getApplicationContext(), getResources().getString(R.string.data_added_successfully),
							Toast.LENGTH_SHORT).show();
					displayValues();

				} else {
					Partograph_CommonClass.rollbackTrans(LatentPhase_Activity.this);
				}

			}
		} catch (NotFoundException e) {
			// TODO Auto-generated catch block
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}

	}

	private boolean validtaedAllFields() throws NotFoundException, Exception {
		// TODO Auto-generated method stub
		if (aq.id(R.id.etdatelat).getText().toString().trim().length() <= 0) {
			Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.enter_date),
					LatentPhase_Activity.this);
			return false;
		}
		if (aq.id(R.id.ettimelat).getText().toString().trim().length() <= 0) {
			Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.enter_time),
					LatentPhase_Activity.this);
			return false;
		}

		if (aq.id(R.id.etpulselat).getText().toString().trim().length() <= 0
				&& (aq.id(R.id.etbpsyslat).getText().toString().trim().length() <= 0
						&& aq.id(R.id.etbpdialat).getText().toString().trim().length() <= 0)
				&& (aq.id(R.id.etcontrcationslat).getText().toString().trim().length() <= 0)
				&& aq.id(R.id.etfhslat).getText().toString().trim().length() <= 0
				&& aq.id(R.id.etpvlat).getText().toString().trim().length() <= 0) {
			Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.no_data_found_to_save),
					LatentPhase_Activity.this);
			return false;
		}

		if (aq.id(R.id.etbpsyslat).getText().toString().trim().length() > 0
				|| aq.id(R.id.etbpdialat).getText().toString().trim().length() > 0) {

			if (aq.id(R.id.etbpsyslat).getText().toString().trim().length() <= 0) {
				Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.enterbpsystolicval),
						LatentPhase_Activity.this);
				return false;

			} else if (aq.id(R.id.etbpdialat).getText().toString().trim().length() <= 0) {
				Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.enterbpdiastolicval),
						LatentPhase_Activity.this);
				return false;

			}

		}

		/*
		 * if
		 * (aq.id(R.id.etcontrcationslat).getText().toString().trim().length() >
		 * 0 || aq.id(R.id.etduration).getText().toString().trim().length() > 0)
		 * { if
		 * (aq.id(R.id.etcontrcationslat).getText().toString().trim().length()
		 * <= 0) {
		 * Partograph_CommonClass.displayAlertDialog(getResources().getString(R.
		 * string.enter_contractions_val), LatentPhase_Activity.this); return
		 * false; } if
		 * (aq.id(R.id.etduration).getText().toString().trim().length() <= 0) {
		 * Partograph_CommonClass.displayAlertDialog(getResources().getString(R.
		 * string.enterdurationval), LatentPhase_Activity.this); return false; }
		 * 
		 * }
		 */

		return true;
	}

	public void commitTrans() throws Exception {
		// TODO Auto-generated method stub
		dbh.db.setTransactionSuccessful();
		dbh.db.endTransaction();
		aq.id(R.id.etpulselat).text("");
		aq.id(R.id.etbpsyslat).text("");
		aq.id(R.id.etbpdialat).text("");
		aq.id(R.id.etcontrcationslat).text("");
		aq.id(R.id.etadvicelat).text("");
		aq.id(R.id.etfhslat).text("");
		aq.id(R.id.etpvlat).text("");
		aq.id(R.id.etduration).text("");
		aq.id(R.id.etdatelat).text(Partograph_CommonClass.getTodaysDate(Partograph_CommonClass.defdateformat));
		aq.id(R.id.ettimelat).text(Partograph_CommonClass.getCurrentTime());
		calSyncMtd();

	}

	// Sync
	private void calSyncMtd() throws Exception {
		// 25Sep2016 Arpitha - addToTrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		SyncFunctions.syncUptoDate = false;
		Partograph_CommonClass.responseAckCount = 0;

		AsyncCallWS task = new AsyncCallWS();
		task.execute();
	}

	// Display values
	private void displayValues() throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		Cursor cur = dbh.getLatentPhaseData(woman.getUserId(), woman.getWomenId(), 1);
		arrVal = new ArrayList<LatentPhasePojo>();
		if (cur != null && cur.getCount() > 0) {
			cur.moveToFirst();
			do {
				lpojo = new LatentPhasePojo();
				lpojo.setUserId(cur.getString(0));
				lpojo.setWomanId(cur.getString(1));
				lpojo.setWomanName(cur.getString(2));
				lpojo.setPulse(cur.getInt(4));
				lpojo.setBp(cur.getString(5));
				lpojo.setContractions(cur.getInt(6));
				// lpojo.setDuration(cur.getString(7));
				lpojo.setFhs(cur.getInt(7));
				lpojo.setPv(cur.getString(8));
				lpojo.setAdvice(cur.getString(9));
				lpojo.setDatetime(cur.getString(3));
				arrVal.add(lpojo);

			} while (cur.moveToNext());
		}
		if (arrVal.size() > 0) {

			aq.id(R.id.txtnodata).gone();

			tbl.removeAllViews();

			TableRow trlabel = new TableRow(this);
			trlabel.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 40));

			TextView textdatetime = new TextView(this);
			textdatetime.setTextSize(15);
			textdatetime.setPadding(10, 10, 10, 10);
			textdatetime.setTextColor(getResources().getColor(R.color.black));
			textdatetime.setTextSize(18);
			textdatetime
					.setText(getResources().getString(R.string.date) + " & " + getResources().getString(R.string.time));
			textdatetime.setGravity(Gravity.CENTER);
			trlabel.addView(textdatetime);

			TextView textPulse = new TextView(this);
			textPulse.setTextSize(15);
			textPulse.setText(getResources().getString(R.string.pulse));
			textPulse.setPadding(10, 10, 10, 10);
			textPulse.setTextColor(getResources().getColor(R.color.black));
			// textPulse.setTextSize(18);
			textPulse.setGravity(Gravity.CENTER);
			trlabel.addView(textPulse);

			TextView textbp = new TextView(this);
			textbp.setTextSize(15);
			textbp.setPadding(10, 10, 10, 10);
			textbp.setText(getResources().getString(R.string.txtbpvalue));
			textbp.setTextColor(getResources().getColor(R.color.black));
			// textbp.setTextSize(15);
			textbp.setGravity(Gravity.CENTER);
			trlabel.addView(textbp);

			TextView textcontractions = new TextView(this);
			textcontractions.setTextSize(15);
			textcontractions.setText(getResources().getString(R.string.contraction));
			textcontractions.setPadding(10, 10, 10, 10);
			textcontractions.setTextColor(getResources().getColor(R.color.black));
			// textcontractions.setTextSize(15);
			textcontractions.setGravity(Gravity.LEFT);
			trlabel.addView(textcontractions);

			TextView textFHS = new TextView(this);
			textFHS.setTextSize(15);
			textFHS.setText(getResources().getString(R.string.fhs));
			textFHS.setPadding(10, 10, 10, 10);
			textFHS.setTextColor(getResources().getColor(R.color.black));
			// textFHS.setTextSize(15);
			textFHS.setGravity(Gravity.LEFT);
			trlabel.addView(textFHS);

			TextView textPV = new TextView(this);
			textPV.setTextSize(15);
			textPV.setText(getResources().getString(R.string.pv));
			textPV.setPadding(10, 10, 10, 10);
			textPV.setTextColor(getResources().getColor(R.color.black));
			// textPV.setTextSize(15);
			textPV.setGravity(Gravity.LEFT);
			trlabel.addView(textPV);

			TextView textAdvice = new TextView(this);
			textAdvice.setTextSize(15);
			textAdvice.setText(getResources().getString(R.string.advice));
			textAdvice.setPadding(10, 10, 10, 10);
			textAdvice.setTextColor(getResources().getColor(R.color.black));
			// textAdvice.setTextSize(18);
			textAdvice.setGravity(Gravity.LEFT);
			trlabel.addView(textAdvice);

			for (int i = 0; i < arrVal.size(); i++) {
				TableRow tr = new TableRow(this);
				tr.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 40));

				TextView textdatetimeVal = new TextView(this);
				textdatetimeVal.setTextSize(15);
				textdatetimeVal.setPadding(10, 10, 10, 10);
				textdatetimeVal.setTextColor(getResources().getColor(R.color.black));
				// textdatetimeVal.setTextSize(18);
				textdatetimeVal.setText(Partograph_CommonClass.getConvertedDateFormat(
						arrVal.get(i).getDatetime().split("/")[0], Partograph_CommonClass.defdateformat) + " "
						+ arrVal.get(i).getDatetime().split("/")[1]);
				textdatetimeVal.setGravity(Gravity.CENTER);
				tr.addView(textdatetimeVal);

				int pulse = arrVal.get(i).getPulse();

				TextView textPulseVal = new TextView(this);
				textPulseVal.setTextSize(15);
				if (pulse > 0)
					textPulseVal.setText("" + pulse);
				textPulseVal.setPadding(10, 10, 10, 10);
				textPulseVal.setTextColor(getResources().getColor(R.color.black));
				// textPulseVal.setTextSize(18);
				textPulseVal.setGravity(Gravity.CENTER);
				tr.addView(textPulseVal);

				TextView textbpVal = new TextView(this);
				textbpVal.setTextSize(15);
				textbpVal.setPadding(10, 10, 10, 10);
				if (arrVal.get(i).getBp() != null && arrVal.get(i).getBp().trim().length() > 0)
					textbpVal.setText(arrVal.get(i).getBp());
				textbpVal.setTextColor(getResources().getColor(R.color.black));
				// textbpVal.setTextSize(18);
				textbpVal.setGravity(Gravity.CENTER);
				tr.addView(textbpVal);

				int contractions = arrVal.get(i).getContractions();

				TextView textcontractionsVal = new TextView(this);
				textcontractionsVal.setTextSize(15);
				if (contractions > 0)
					textcontractionsVal.setText("" + contractions);
				textcontractionsVal.setPadding(10, 10, 10, 10);
				textcontractionsVal.setTextColor(getResources().getColor(R.color.black));
				// textcontractionsVal.setTextSize(18);
				textcontractionsVal.setGravity(Gravity.CENTER);
				tr.addView(textcontractionsVal);

				int fhs = arrVal.get(i).getFhs();
				TextView textFHSVal = new TextView(this);
				textFHSVal.setTextSize(15);
				if (fhs > 0)
					textFHSVal.setText("" + fhs);
				textFHSVal.setPadding(10, 10, 10, 10);
				textFHSVal.setTextColor(getResources().getColor(R.color.black));
				// textFHSVal.setTextSize(18);
				textFHSVal.setGravity(Gravity.CENTER);
				tr.addView(textFHSVal);

				String pv = arrVal.get(i).getPv();

				TextView textPVVal = new TextView(this);
				textPVVal.setTextSize(15);
				if (pv != null && pv.length() > 0)
					textPVVal.setText("" + pv);
				textPVVal.setPadding(10, 10, 10, 10);
				textPVVal.setTextColor(getResources().getColor(R.color.black));
				// textPVVal.setTextSize(18);
				textPVVal.setGravity(Gravity.CENTER);
				tr.addView(textPVVal);

				TextView textAdviceVal = new TextView(this);
				textAdviceVal.setTextSize(15);
				textAdviceVal.setText(arrVal.get(i).getAdvice());
				textAdviceVal.setPadding(10, 10, 10, 10);
				textAdviceVal.setTextColor(getResources().getColor(R.color.black));
				// textAdviceVal.setTextSize(18);
				textAdviceVal.setGravity(Gravity.CENTER);
				tr.addView(textAdviceVal);

				View view3 = new View(this);
				view3.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 1));
				view3.setBackgroundColor(Color.DKGRAY);
				tbl.addView(view3);

				if (i == 0) {
					tbl.addView(trlabel);
					View view1 = new View(this);
					view1.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 1));
					view1.setBackgroundColor(Color.DKGRAY);
					tbl.addView(view1);
				}

				tbl.addView(tr);
			}
		} else {

			if (woman.getDel_type() != 0 || dbh.getPartoData(woman.getWomenId(), woman.getUserId())
					|| dbh.getisWomenReferred(woman.getWomenId(), woman.getUserId()) || woman.getregtype() == 2) {
				aq.id(R.id.reladddata).gone();
				aq.id(R.id.txtnodata).visible();
				aq.id(R.id.txtnodata).text(getResources().getString(R.string.latent_phase_data_is_disabled));
			} else {
				aq.id(R.id.txtnodata).visible();
				aq.id(R.id.txtnodata).text(getResources().getString(R.string.no_woman));
			}
		}
	}

	void displayAlertDialog() {

		final Dialog dialog = new Dialog(LatentPhase_Activity.this);

		dialog.setTitle(getResources().getString(R.string.afm_abrreivation));
		dialog.requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		dialog.setContentView(R.layout.dialog_action);
		dialog.getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.dialog_title);
		TextView txtdialogtitle = (TextView) dialog.findViewById(R.id.graph_title);
		txtdialogtitle.setText(getResources().getString(R.string.dischrage_slip));

		ImageButton imgbtncanceldialog = (ImageButton) dialog.findViewById(R.id.imgbtncanceldialog);
		imgbtncanceldialog.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				try {
					dialog.cancel();
				} catch (Exception e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					e.printStackTrace();
				}
			}
		});

		final RadioButton rdsavepdf = (RadioButton) dialog.findViewById(R.id.rdsave);
		final RadioButton rdviewpdf = (RadioButton) dialog.findViewById(R.id.rdview);
		Button btndone = (Button) dialog.findViewById(R.id.btndone);

		// final RadioButton rdshare = (RadioButton)
		// dialog.findViewById(R.id.rdshare);

		btndone.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (rdsavepdf.isChecked()) {
					try {
						dialog.cancel();
						generateLatentPhaseinpdfFormat();
						Toast.makeText(getApplicationContext(),
								getResources().getString(R.string.latent_phase_is_saved), Toast.LENGTH_LONG).show();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}
				} else if (rdviewpdf.isChecked())

				{
					try {
						dialog.cancel();
						generateLatentPhaseinpdfFormat();
						// to view pdf file from appliction
						if (file.exists()) {

							Uri pdfpath = Uri.fromFile(file);
							Intent intent = new Intent(Intent.ACTION_VIEW);
							intent.setDataAndType(pdfpath, "application/pdf");
							intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

							try {
								startActivity(intent);
							} catch (ActivityNotFoundException e) {// 24Jan2017
																	// Arpitha
								// No application to view, ask to download one
								AlertDialog.Builder builder = new AlertDialog.Builder(LatentPhase_Activity.this);
								builder.setTitle("No Application Found");
								builder.setMessage("Download one from Android Market?");
								builder.setPositiveButton("Yes, Please", new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										Intent marketIntent = new Intent(Intent.ACTION_VIEW);
										marketIntent.setData(Uri.parse("market://details?id=com.adobe.reader"));
										startActivity(marketIntent);
									}
								});
								builder.setNegativeButton("No, Thanks", null);
								builder.create().show();
							}
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}
				} // 24Jan2017
				/*
				 * else if (rdshare.isChecked()) { SendviaBluetoth();
				 * 
				 * }
				 */

				else {
					Toast.makeText(getApplicationContext(), getResources().getString(R.string.please_select_one_option),
							Toast.LENGTH_LONG).show();
				}

			}

		});
		dialog.show();

	}

	private void generateLatentPhaseinpdfFormat() {
		// TODO Auto-generated method stub

		// TODO Auto-generated method stub
		Document doc = new Document(PageSize.A4);

		try {

			String path = AppContext.mainDir + "/PDF/Latent Phase";

			File dir = new File(path);
			if (!dir.exists())
				dir.mkdirs();

			Log.d("PDFCreator", "PDF Path: " + path);

			file = new File(dir, woman.getWomen_name() + "_LatentPhase.pdf");
			FileOutputStream fOut = new FileOutputStream(file);

			PdfWriter.getInstance(doc, fOut);

			doc.setMarginMirroring(false);
			// doc.setMargins(-25, -20, 65, -50);
			doc.setMargins(0, 10, 10, 0);

			doc.open();

			Paragraph p_heading = new Paragraph(getResources().getString(R.string.latent_phase), heading);
			// Font paraFont = new Font(Font.HELVETICA);
			// p_heading.setFont(paraFont);
			p_heading.setAlignment(Paragraph.ALIGN_CENTER);

			doc.add(p_heading);

			Drawable d = getResources().getDrawable(R.drawable.bcicon_small);

			BitmapDrawable bitDw = ((BitmapDrawable) d);

			Bitmap bmp = bitDw.getBitmap();

			ByteArrayOutputStream stream = new ByteArrayOutputStream();

			bmp.compress(Bitmap.CompressFormat.PNG, 0, stream);

			Image image = Image.getInstance(stream.toByteArray());

			image.setIndentationLeft(10);
			doc.add(image);
			String strdatetime = Partograph_CommonClass.getConvertedDateFormat(Partograph_CommonClass.getTodaysDate(),
					Partograph_CommonClass.defdateformat) + " " + Partograph_CommonClass.getCurrentTime() + " ";
			Paragraph p1 = new Paragraph(strdatetime, small);
			p1.setAlignment(Paragraph.ALIGN_RIGHT);
			doc.add(p1);

			womanBasicInfo();

			Paragraph basicInfo = new Paragraph("Woman Basic Information", heading);
			basicInfo.setAlignment(Paragraph.ALIGN_LEFT);
			basicInfo.setIndentationLeft(30);
			doc.add(basicInfo);

			doc.add(womanbasics);
			dispalyLatentPhase();
			doc.add(lat);

			// Rectangle rect = new Rectangle(10, 10, 590, 808);
			Rectangle rect = new Rectangle(10, 10, 585, 808);
			rect.enableBorderSide(1);
			rect.enableBorderSide(1);
			rect.enableBorderSide(1);
			rect.enableBorderSide(1);
			rect.setBorder(2);
			// rect.setBorderColor(context.getResources().getColor(R.color.black));
			rect.setBorder(Rectangle.BOX);
			rect.setBorderWidth(1);
			doc.add(rect);

			doc.close();
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}

	}

	void womanBasicInfo() {
		try {
			String delstatus;
			ArrayList<String> val = new ArrayList<String>();
			val.add(getResources().getString(R.string.name) + ":" + woman.getWomen_name());
			val.add(getResources().getString(R.string.age) + ":" + woman.getAge()
					+ getResources().getString(R.string.yrs));
			if (woman.getregtype() != 2) {// 12Jan2017 Arpitha
				String strDOA = Partograph_CommonClass.getConvertedDateFormat(woman.getDate_of_admission(),
						Partograph_CommonClass.defdateformat);
				val.add(getResources().getString(R.string.doadm) + ":" + strDOA + "/" + woman.getTime_of_admission());
			} else {// 12Jan2017 Arpitha
				val.add(getResources().getString(R.string.doadm) + ":" + woman.getDate_of_reg_entry());
			}
			if (woman.getGestationage() == 0)
				val.add(getResources().getString(R.string.gest_short_label) + " "
						+ getResources().getString(R.string.notknown));
			else
				val.add(woman.getGestationage() + getResources().getString(R.string.wks) + " "
						+ woman.getGest_age_days() + getResources().getString(R.string.days));

			womanbasics = new PdfPTable(val.size());

			womanbasics.setWidths(new int[] { 7, 5, 11, 11 });

			for (int k = 0; k < val.size(); k++) {
				String header = val.get(k);
				PdfPCell cell = new PdfPCell();
				cell.setPhrase(new Phrase(header, new Font(Font.HELVETICA, 10, Font.BOLD)));
				cell.setHorizontalAlignment(Element.ALIGN_LEFT);
				// cell.setPadding(2);
				cell.setBorder(0);

				womanbasics.addCell(cell);
			}

			ArrayList<String> arrVal = new ArrayList<String>();
			arrVal.add(getResources().getString(R.string.gravida) + ":" + woman.getGravida());
			arrVal.add(getResources().getString(R.string.para) + ":" + woman.getPara());
			arrVal.add(getResources().getString(R.string.facility) + ": " + woman.getFacility_name() + "["
					+ woman.getFacility() + "]");
			if (woman.getDel_type() != 0) {
				delstatus = getResources().getString(R.string.delivered);
			} else
				delstatus = getResources().getString(R.string.del_prog);
			arrVal.add(delstatus);

			for (int k = 0; k < arrVal.size(); k++) {
				String header = arrVal.get(k);
				PdfPCell cell = new PdfPCell();
				cell.setPhrase(new Phrase(header, new Font(Font.HELVETICA, 10, Font.BOLD)));
				cell.setHorizontalAlignment(Element.ALIGN_LEFT);
				// cell.setPadding(2);
				cell.setBorder(0);

				womanbasics.addCell(cell);
			}

			womanbasics.setWidthPercentage(90);

			womanbasics.setSpacingBefore(3);

			womanbasics.completeRow();

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
	}

	PdfPTable displayLatentPhase() {
		try {

			ArrayList<String> arrTemp = new ArrayList<String>();
			arrTemp.add(getResources().getString(R.string.date) + " & " + getResources().getString(R.string.time));
			arrTemp.add(getResources().getString(R.string.pulse));
			arrTemp.add(getResources().getString(R.string.txtbpvalue));
			arrTemp.add(getResources().getString(R.string.contraction));
			arrTemp.add(getResources().getString(R.string.fhs));
			arrTemp.add(getResources().getString(R.string.pv));
			arrTemp.add(getResources().getString(R.string.advice));

			// for (int i = 0; i < arrVal.size(); i++) {
			// String tempVal = arrVal.get(i).getStrVal();
			// arrTemp.add(tempVal);
			// }

			lat = new PdfPTable(7);
			lat.setWidths(new int[] { 9, 5, 5, 5, 5, 5, 5 });
			for (int k = 0; k < 7; k++) {
				String header_temperature = " ";
				if (k < arrTemp.size()) {
					header_temperature = arrTemp.get(k);
				}
				PdfPCell cell = new PdfPCell();
				// cell.setGrayFill(0.9f);
				cell.setPhrase(new Phrase(header_temperature, new Font(Font.HELVETICA, 10, Font.BOLD)));
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				lat.addCell(cell);
			}

			lat.completeRow();
			lat.setWidthPercentage(90);

			lat.setSpacingAfter(4);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return lat;
	}

	PdfPTable dispalyLatentPhase() {
		try {
			ArrayList<String> latphase = new ArrayList<String>();
			latphase.add(getResources().getString(R.string.date) + " & " + getResources().getString(R.string.time));
			latphase.add(getResources().getString(R.string.pulse));
			latphase.add(getResources().getString(R.string.txtbpvalue));
			latphase.add(getResources().getString(R.string.contraction));
			latphase.add(getResources().getString(R.string.fhs));
			latphase.add(getResources().getString(R.string.pv));
			latphase.add(getResources().getString(R.string.advice));

			/*
			 * for (int k = 0; k <
			 * Partograph_CommonClass.listValuesItemsafm.size(); k++) {
			 * 
			 * String val =
			 * Partograph_CommonClass.listValuesItemsafm.get(k).getAmnioticfluid
			 * (); if (val.equals("0")) val =
			 * context.getResources().getString(R.string.c); else if
			 * (val.equals("1")) val =
			 * context.getResources().getString(R.string.m); // 26Nov2015 else
			 * if (val.equals("2")) val =
			 * context.getResources().getString(R.string.i); else if
			 * (val.equals("3")) val =
			 * context.getResources().getString(R.string.b);
			 * amniotic_row.add(val); }
			 */

			lat = new PdfPTable(7);
			lat.setSpacingBefore(20);
			lat.setWidths(new int[] { 8, 4, 5, 7, 4, 5, 9 });
			for (int k = 0; k < 7; k++) {
				String header = " ";
				if (k < latphase.size()) {
					header = latphase.get(k);
				}
				PdfPCell cell = new PdfPCell();
				// cell.setGrayFill(0.9f);
				cell.setPhrase(new Phrase(header, new Font(Font.HELVETICA, 10, Font.BOLD)));
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell.setPadding(8);
				lat.addCell(cell);
			}

			lat.completeRow();

			/*
			 * ArrayList<String> latVal1 = new ArrayList<String>();
			 * moulding.add("Moulding"); for (int l = 0; l <
			 * Partograph_CommonClass.listValuesItemsafm.size(); l++) { String
			 * mouldingVal =
			 * Partograph_CommonClass.listValuesItemsafm.get(l).getMoulding();
			 * 
			 * if (mouldingVal.equalsIgnoreCase("0")) mouldingVal =
			 * getResources().getString(R.string.m1); else if
			 * (mouldingVal.equalsIgnoreCase("1")) mouldingVal =
			 * getResources().getString(R.string.m2); else mouldingVal =
			 * getResources().getString(R.string.m3);
			 * 
			 * moulding.add(mouldingVal);
			 * 
			 * }
			 * 
			 * ArrayList<LatentPhasePojo> latVal = new
			 * ArrayList<LatentPhasePojo>(); //latVal =
			 */
			// ArrayList<LatentPhasePojo> latval = new
			// ArrayList<LatentPhasePojo>();

			for (int i = 0; i < arrVal.size(); i++)

			{
				ArrayList<String> val = new ArrayList<String>();
				String date = Partograph_CommonClass.getConvertedDateFormat(arrVal.get(i).getDatetime().split("/")[0],
						"dd-MM-yyyy") + "/" + arrVal.get(i).getDatetime().split("/")[1];
				val.add(date);

				if (arrVal.get(i).getPulse() > 0) {
					pulse = "" + arrVal.get(i).getPulse();
					//
					val.add(pulse);
				} else
					val.add("");

				if (arrVal.get(i).getBp() != null && arrVal.get(i).getBp().trim().length() > 0) {
					bp = arrVal.get(i).getBp();
					val.add(bp);
				} else
					val.add("");
				if (arrVal.get(i).getContractions() > 0) {
					contractions = "" + arrVal.get(i).getContractions();
					val.add(contractions);
				} else
					val.add("");
				// val.add("" + arrVal.get(0).getDuration());
				if (arrVal.get(i).getFhs() > 0) {
					fhs = "" + arrVal.get(i).getFhs();
					val.add(fhs);
				} else
					val.add("");
				if (arrVal.get(i).getPv() != null && arrVal.get(i).getPv().trim().length() > 0) {
					pv = "" + arrVal.get(i).getPv();
					val.add(pv);
				} else
					val.add("");
				advice = arrVal.get(i).getAdvice();
				val.add(advice);

				for (int x = 0; x < 7; x++) {
					String strVal = " ";
					// if (x < arrVal.size()) {
					// if (val.size() < x)
					strVal = val.get(x);
					// else
					// strVal = "";

					// }
					// String val = latval.get(x).getDatetime();
					PdfPCell cell_moulding = new PdfPCell();
					cell_moulding.setPhrase(new Phrase(strVal, new Font(Font.HELVETICA, 10, Font.NORMAL)));
					cell_moulding.setHorizontalAlignment(Element.ALIGN_CENTER);
					cell_moulding.setPadding(5);
					lat.addCell(cell_moulding);
				}

				lat.completeRow();
			}
		} catch (Exception e) {
			// TODO: handle exception
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
		return lat;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_womenlist, menu);

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		if (Partograph_CommonClass.isRecordExistsForSync)
			menu.findItem(R.id.sync).setIcon(getResources().getDrawable(R.drawable.ic_sync_red));
		else
			menu.findItem(R.id.sync).setIcon(getResources().getDrawable(R.drawable.ic_sync_green));

		if (!(AppContext.checkSimState() == TelephonyManager.SIM_STATE_READY)) {
			menu.findItem(R.id.sms).setVisible(false);
		}

		menu.findItem(R.id.email).setVisible(false);

		menu.findItem(R.id.registration).setVisible(false);

		menu.findItem(R.id.delete).setVisible(false);
		menu.findItem(R.id.expertcomments).setVisible(false);
		menu.findItem(R.id.viewpartograph).setVisible(false);
		menu.findItem(R.id.ic_msg).setVisible(false);
		menu.findItem(R.id.addcomments).setVisible(false);
		menu.findItem(R.id.viewprofile).setVisible(false);
		menu.findItem(R.id.sync).setVisible(false);
		menu.findItem(R.id.search).setVisible(false);
		// 03Oct2017 Arpitha
		menu.findItem(R.id.del).setVisible(true);
		menu.findItem(R.id.ref).setVisible(true);
		menu.findItem(R.id.pdf).setVisible(true);
		menu.findItem(R.id.stages).setVisible(true);
		menu.findItem(R.id.apgar).setVisible(true);
		menu.findItem(R.id.post).setVisible(true);
		menu.findItem(R.id.parto).setVisible(true);// 03Oct2017 Arpitha

		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		try {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
			switch (item.getItemId()) {
			// Respond to the action bar's Up/Home button
			case android.R.id.home:
				NavUtils.navigateUpFromSameTask(this);
				return true;

			case R.id.logout:
				displayConfirmationAlert_exit(getResources().getString(R.string.exit_msg),
						getResources().getString(R.string.logout));
				return true;

			// case R.id.sync:
			// menuItem = item;
			// menuItem.setActionView(R.layout.progressbar);
			// menuItem.expandActionView();
			// calSyncMtd();
			// return true;

			case R.id.settings:
				if (Partograph_CommonClass.autodatetime(LatentPhase_Activity.this)) {
					Intent settings = new Intent(this, Settings_parto.class);
					startActivity(settings);
				}
				return true;

			case R.id.about:

				Intent about = new Intent(this, About.class);
				startActivity(about);
				return true;

			case R.id.info:
				Intent i = new Intent(getApplicationContext(), GraphInformation.class);
				startActivity(i);
				return true;

			case R.id.sms:
				if (AppContext.checkSimState() == TelephonyManager.SIM_STATE_READY) {
					Partograph_CommonClass.display_messagedialog(LatentPhase_Activity.this, woman.getUserId());
				} else
					Toast.makeText(getApplicationContext(), getResources().getString(R.string.no_sim),
							Toast.LENGTH_LONG).show();
				return true;

			case R.id.comments:
				Intent comments = new Intent(getApplicationContext(), Comments_Activity.class);
				startActivity(comments);
				return true;

			case R.id.home:
				displayConfirmationAlert_exit(getResources().getString(R.string.exit_msg),
						getResources().getString(R.string.home));
				return true;
			// 22May2017 Arpitha - v2.6
			case R.id.summary:
				Intent summary = new Intent(getApplicationContext(), Summary_Activity.class);
				startActivity(summary);
				return true;
			case R.id.usermanual:
				Intent manual = new Intent(getApplicationContext(), UseManual.class);
				startActivity(manual);
				return true;
			case R.id.changepass:
				Partograph_CommonClass.dispalyChangePasswordDialog(this);
				return true;

			case R.id.disch:
				Intent disch = new Intent(LatentPhase_Activity.this, DischargedWomanList_Activity.class);
				startActivity(disch);
				return true;

			// 03Oct2017 Arpitha
			case R.id.del:
				/*Intent disc = new Intent(LatentPhase_Activity.this, DeliveryInfo_Activity.class);
				disc.putExtra("woman", woman);
				startActivity(disc);*/

				if (woman.getDel_type() != 0)// 21Oct2016
												// Arpitha
				{// 21Oct2016 Arpitha
					// 05Jan2016
					// / showDialogToUpdateStatus();
					Partograph_CommonClass.showDialogToUpdateStatus(LatentPhase_Activity.this, woman);
//					loadWomendata(displayListCountddel);
//					if(adapter!=null)
//					adapter.notifyDataSetChanged();
					// loadWomendata(displayListCountddel);
				} else// 21Oct2016 Arpitha
				{

					Intent graph = new Intent(LatentPhase_Activity.this, DeliveryInfo_Activity.class);
					graph.putExtra("woman", woman);
					startActivity(graph);
				} // 21Oct2016 Arpitha
				return true;
			case R.id.viewprofile:
				Intent view = new Intent(LatentPhase_Activity.this, ViewProfile_Activity.class);
				view.putExtra("woman", woman);
				startActivity(view);
				return true;

			case R.id.ref:
				/*Intent ref = new Intent(LatentPhase_Activity.this, ReferralInfo_Activity.class);
				ref.putExtra("woman", woman);
				startActivity(ref);*/
				Cursor cur = dbh.getReferralDetails(woman.getUserId(), woman.getWomenId());
				if (cur != null && cur.getCount() > 0) {
					Partograph_CommonClass.showDialogToUpdateReferral(LatentPhase_Activity.this, woman);
				} else {
					Intent ref = new Intent(LatentPhase_Activity.this, ReferralInfo_Activity.class);
					ref.putExtra("woman", woman);
					startActivity(ref);
				}
				return true;

			case R.id.pdf:
				Partograph_CommonClass.exportPDFs(LatentPhase_Activity.this, woman);
				return true;

			case R.id.disc:
				Intent discharge = new Intent(LatentPhase_Activity.this, DiscargeDetails_Activity.class);
				discharge.putExtra("woman", woman);
				startActivity(discharge);
				return true;
				
			case R.id.apgar:
				if(woman.getDel_type()!=0)
				{
				Intent apgar = new Intent(LatentPhase_Activity.this, Activity_Apgar.class);
				apgar.putExtra("woman", woman);
				startActivity(apgar);
				}else
					Toast.makeText(getApplicationContext(), getResources().getString(R.string.msg_applicableafterdelivery), Toast.LENGTH_LONG).show();
				return true;
				
			case R.id.stages:
				if(woman.getDel_type()!=0)
				{
				Intent stages = new Intent(LatentPhase_Activity.this, StageofLabor.class);
				stages.putExtra("woman", woman);
				startActivity(stages);
				}else
					Toast.makeText(getApplicationContext(), getResources().getString(R.string.msg_applicableafterdelivery), Toast.LENGTH_LONG).show();
				return true;
				
			case R.id.post:
				if(woman.getDel_type()!=0)
				{
				Intent post = new Intent(LatentPhase_Activity.this, PostPartumCare_Activity.class);
				post.putExtra("woman", woman);
				startActivity(post);
				}else
					Toast.makeText(getApplicationContext(), getResources().getString(R.string.msg_applicableafterdelivery), Toast.LENGTH_LONG).show();
				return true;

			case R.id.parto:
				Intent parto = new Intent(LatentPhase_Activity.this, SlidingActivity.class);
				parto.putExtra("woman", woman);
				startActivity(parto);
				return true;// 03Oct2017 Arpitha

			}
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
		return super.onOptionsItemSelected(item);
	}

	/** This method Calls the idle timeout */
	@Override
	public void onUserInteraction() {
		super.onUserInteraction();
		KillAllActivitesAndGoToLogin
				.delayedIdle(Integer.parseInt(Partograph_CommonClass.properties.getProperty("idleTimeOut")));
	}

	@Override
	public void onBackPressed() {
		try {
			displayConfirmationAlert_exit(getResources().getString(R.string.exit_msg),
					getResources().getString(R.string.home));
		} catch (NotFoundException e) {
			// TODO Auto-generated catch block
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
	}

	private void displayConfirmationAlert_exit(String exit_msg, final String classname) throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		final Dialog dialog = new Dialog(LatentPhase_Activity.this);
		dialog.setTitle(Html.fromHtml("<font color='" + getResources().getColor(R.color.appcolor) + "'>"
				+ getResources().getString(R.string.alert) + "</font>"));
		dialog.setContentView(R.layout.temp_alertdialog);

		dialog.show();

		final TextView txtdialog = (TextView) dialog.findViewById(R.id.txtpulse);
		Button imgbtnyes = (Button) dialog.findViewById(R.id.imgbtnyes);
		Button imgbtnno = (Button) dialog.findViewById(R.id.imgbtnno);
		// Arpitha 27may16
		final TextView txtdialog1 = (TextView) dialog.findViewById(R.id.txtpulseval);

		final TextView txtdialog6 = (TextView) dialog.findViewById(R.id.txt);

		txtdialog1.setVisibility(View.GONE);
		txtdialog.setVisibility(View.GONE);
		txtdialog6.setText(exit_msg);
		imgbtnyes.setText(getResources().getString(R.string.yes));
		imgbtnno.setText(getResources().getString(R.string.no));

		imgbtnyes.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				dialog.cancel();

				if (classname.equalsIgnoreCase(getResources().getString(R.string.home))) {
					Intent i = new Intent(LatentPhase_Activity.this, Activity_WomenView.class);
					startActivity(i);
				}

				if (classname.equalsIgnoreCase(getResources().getString(R.string.logout))) {

					Intent i = new Intent(LatentPhase_Activity.this, LoginActivity.class);
					startActivity(i);

				}

			}
		});

		imgbtnno.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.cancel();

			}
		});

		int dividerId = dialog.getContext().getResources().getIdentifier("android:id/titleDivider", null, null);
		View divider = dialog.findViewById(dividerId);
		divider.setBackgroundColor(getResources().getColor(R.color.appcolor));// 08Feb2017

		dialog.show();

		dialog.setCancelable(false);

	}

	void getwomanbasicdata() throws Exception {

		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		if (woman != null) {

			String doa = "", toa = "", risk = "", dod = "", tod = "";

			aq.id(R.id.wname).text(woman.getWomen_name());
			aq.id(R.id.wage).text(woman.getAge() + getResources().getString(R.string.yrs));
			if (woman.getregtype() != 2) {
				doa = Partograph_CommonClass.getConvertedDateFormat(woman.getDate_of_admission(),
						Partograph_CommonClass.defdateformat);
				// toa =
				// Partograph_CommonClass.gettimein12hrformat(woman.getTime_of_admission());
				aq.id(R.id.wdoa)
						.text(getResources().getString(R.string.reg) + ": " + doa + "/" + woman.getTime_of_admission());
			} else {

				aq.id(R.id.wdoa).text(getResources().getString(R.string.doe) + ": " + woman.getDate_of_reg_entry());
			}

			aq.id(R.id.wgest)
					.text(getResources().getString(R.string.gest_age) + ":"
							+ (woman.getGestationage() == 0 ? getResources().getString(R.string.notknown)
									: woman.getGestationage() + getResources().getString(R.string.wks)));
			aq.id(R.id.wgravida).text(getResources().getString(R.string.gravida_short_label) + ":" + woman.getGravida()
					+ ", " + getResources().getString(R.string.para_short_label) + ":" + woman.getPara());
			woman.getRisk_category();
			if (woman.getRisk_category() == 1) {
				risk = getResources().getString(R.string.high);
			} else
				risk = getResources().getString(R.string.low);
			aq.id(R.id.wtrisk).text(getResources().getString(R.string.risk_short_label) + ":" + risk);

			if (woman.getDel_type() > 0) {
				aq.id(R.id.trdelstatus).visible();

				dod = Partograph_CommonClass.getConvertedDateFormat(woman.getDel_Date(),
						Partograph_CommonClass.defdateformat);

				String gender = "";
				if (woman.getBabysex1() == 0)
					gender = getResources().getString(R.string.male);
				else
					gender = getResources().getString(R.string.female);

				String[] deltype = getResources().getStringArray(R.array.del_type);
				aq.id(R.id.wdelstatus).text(getResources().getString(R.string.del) + ":"
						+ deltype[woman.getDel_type() - 1] + "," + dod + "/" + woman.getDel_Time() + "," + gender);

				String gender2 = "";
				if (woman.getBabysex2() == 0)
					gender = getResources().getString(R.string.male);
				else
					gender = getResources().getString(R.string.female);
				if (woman.getNo_of_child() == 2)
					aq.id(R.id.wdeldate).text(getResources().getString(R.string.second_baby) + ": "
							+ woman.getDel_time2() + "," + gender2);

				// tod =
				// Partograph_CommonClass.gettimein12hrformat(woman.getDel_Time());
				// aq.id(R.id.wdeldate)
				// .text(getResources().getString(R.string.del) + ": " + dod +
				// "/" + woman.getDel_Time());

			}

		}
	}

}
