package com.bc.partograph.sliding;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import org.achartengine.GraphicalView;

import com.bc.partograph.comments.Comments_Activity;
import com.bc.partograph.common.AppContext;
import com.bc.partograph.common.KillAllActivitesAndGoToLogin;
import com.bc.partograph.common.Partograph_CommonClass;
import com.bc.partograph.common.Partograph_CommonClass.AsyncCallWS;
import com.bc.partograph.common.Partograph_DB;
import com.bc.partograph.deliveryinfo.DeliveryInfo_Activity;
import com.bc.partograph.latentphase.LatentPhase_Activity;
import com.bc.partograph.login.LoginActivity;
import com.bc.partograph.referralinfo.ReferralInfo_Activity;
import com.bc.partograph.settings.About;
import com.bc.partograph.settings.Settings_parto;
import com.bc.partograph.stagesoflabor.StageofLabor;
import com.bc.partograph.sync.SyncFunctions;
import com.bc.partograph.womenview.Activity_WomenView;
import com.bluecrimson.additionalDetails.Summary_Activity;
import com.bluecrimson.apgar.Activity_Apgar;
import com.bluecrimson.dischargedetails.DiscargeDetails_Activity;
import com.bluecrimson.dischargedetails.DiscgargePojo;
import com.bluecrimson.dischargedetails.DischargedWomanList_Activity;
import com.bluecrimson.partograph.Adapter_ExpertComments;
import com.bluecrimson.partograph.Add_value_pojo;
import com.bluecrimson.partograph.R;
import com.bluecrimson.partograph.Women_Profile_Pojo;
import com.bluecrimson.postpartumcare.PostPartumCare_Activity;
import com.bluecrimson.usermanual.UseManual;
import com.bluecrimson.viewprofile.ViewProfile_Activity;
import com.lowagie.text.pdf.PdfPTable;

import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

public class SlidingActivity extends Activity {
	private DrawerLayout mDrawerLayout;
	public ListView mDrawerList;
	private ActionBarDrawerToggle mDrawerToggle;
	// nav drawer title
	private CharSequence mDrawerTitle;

	// used to store app title
	private CharSequence mTitle;

	// slide menu items
	private String[] navMenuTitles;
	private TypedArray navMenuIcons;

	private ArrayList<NavDrawerItem> navDrawerItems;
	private NavDrawerListAdapter adapter;

	Partograph_DB dbh;
	boolean inetOn = false;
	String serverResult;
	CountDownTimer commentsTimer = null;
	public static final int START_AFTER_SECONDS = 10000;
	public static int tabpos = 0;
	public static boolean isGraph = false;
	public static String womenid, userid;
	public static ArrayList<Integer> dangerList;
	// ActionBar actionBar;
	public static MenuItem menuItem;

	// public static String womenId_ex;
	public static int comptabpos = 0;

	// 21Dec2015

	// 4jan2016
	public static boolean isMsgFHR = false, isMsgAFM = false, isMsgDil = false, isMsgCon = false, isMsgOxy = false,
			isMsgDrugs = false, isMsgPBP = false, isMsgTemp = false, isMsgUT = false, isMsgApg = false;

	TableLayout tbllayoutfhr, tbllayoutdil, tbllayoutcontract, tbllayoutpbp, tbllayoutafm, tbllayoutoxytocin,
			tbllayoutdrugs, tbllayouttemp, tbllayouturinetest;
	// TextView txtdata;
	LinearLayout llwinfo;
	GraphicalView mChartViewDilatation = null;
	GraphicalView mChartViewFHR = null;
	GraphicalView mChartViewContraction = null;
	GraphicalView mChartViewPBP = null;
	ListView listcommentsfhr;
	// int posi;

	Women_Profile_Pojo wpojo = null;

	boolean isViewParto;
	// ArrayList<String> womnid;
	// public static int fhr_count;
	boolean isview = false;

	// AQuery aq;

	// updated on 7july2016 by Arpitha
	// private TypedArray navmenunotify_icon;
	// String lastdate;
	// String lasttime;
	public static boolean ispulsetimevalid;
	// String currenttime;
	// String islasttime;
	public static boolean isfhrtimevalid;
	public static boolean isdilatationtimevalid;
	public static boolean iscontractiontimevalid;
	// String strCurrentTime;

	TextView txtdialogtitle;

	// 31Aug2016 - bindu
	// LinkedHashMap<Integer, Double> newDilValuesCervix;
	// LinkedHashMap<Integer, Double> newDilValuesDescent;

	// 23Sep2016 Arpitha
	public static boolean isReferred = false;

	public static HashMap<String, Integer> partovaluemap;

	Women_Profile_Pojo woman;
	ImageView imgpdf;// 07Dec2016 Arpitha

	String path;// 27march2017 Arpitha
	File dir;// 27march2017 Arpitha
	File file;// 27march2017 Arpitha
	// private static Font small = new Font(Font.TIMES_ROMAN, 10, Font.BOLD);
	PdfPTable womanbasics;
	PdfPTable tableAFM;
	PdfPTable table_oxytocin;
	PdfPTable table_drug;
	PdfPTable table_temp;
	PdfPTable table_urine;
	Context context;// 16May2017 Arpitha - v2.6

	public static ArrayList<DiscgargePojo> arrVal;// 15Aug2017 Arpitha

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sliding_activity);

		try {

			woman = (Women_Profile_Pojo) getIntent().getSerializableExtra("woman");

			// 05Nov2016 Arpitha
			Locale locale = null;

			if (AppContext.prefs.getBoolean("isEnglish", false)) {
				locale = new Locale("en");
				Locale.setDefault(locale);
			} else if (AppContext.prefs.getBoolean("isHindi", false)) {
				locale = new Locale("hi");
				Locale.setDefault(locale);
			}

			Configuration config = new Configuration();
			config.locale = locale;
			getResources().updateConfiguration(config, getResources().getDisplayMetrics());

			AppContext.setNamesAccordingToRes(getResources());// 05Nov2016
																// Arpitha

			dbh = Partograph_DB.getInstance(getApplicationContext());

			context = SlidingActivity.this;// 15May2017 Arpitha - v2.6

			tabpos = Partograph_CommonClass.curr_tabpos;
			isGraph = true;

			// dbh.getColorCodedValuesFromTable();

			// Updated 18Aug2016 - bindu
			isMsgFHR = false;
			isMsgAFM = false;
			isMsgDil = false;
			isMsgCon = false;
			isMsgOxy = false;
			isMsgDrugs = false;
			isMsgPBP = false;
			isMsgTemp = false;
			isMsgUT = false;
			isMsgApg = false;

			womenid = woman.getWomenId();
			userid = woman.getUserId();

			mTitle = mDrawerTitle = woman.getWomen_name();

			navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);

			// nav drawer icons from resources
			navMenuIcons = getResources().obtainTypedArray(R.array.nav_drawer_icons);

			// updated on 7july2016 by Arpitha
			// navmenunotify_icon =
			// getResources().obtainTypedArray(R.array.nav_drawer_notify_icons);

			mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
			mDrawerList = (ListView) findViewById(R.id.list_slidermenu);

			// updated 18Aug2016 - only if delivered then check for comments
			if (woman.getDel_type() == 0) {
				calRetrieveComments();
				getCommentCountNotRead();
			}
			// 15Aug2017 Arpitha
			arrVal = new ArrayList<DiscgargePojo>();
			arrVal = dbh.getDischargeData(woman.getUserId(), woman.getWomenId());// 15Aug2017
																					// Arpitha

			// updated on 7july2016 by Arpitha
			if (arrVal.size() <= 0)//12Oct2017 Arpitha
				notification();

			partovaluemap = new HashMap<String, Integer>();
			partovaluemap.put(navMenuTitles[0], 0);
			partovaluemap.put(navMenuTitles[1], 1);
			partovaluemap.put(navMenuTitles[2], 2);
			partovaluemap.put(navMenuTitles[3], 3);
			partovaluemap.put(navMenuTitles[4], 4);
			partovaluemap.put(navMenuTitles[5], 5);
			partovaluemap.put(navMenuTitles[6], 6);
			partovaluemap.put(navMenuTitles[7], 7);
			partovaluemap.put(navMenuTitles[8], 8);
			partovaluemap.put(navMenuTitles[9], 9);
			// 29Dec2016 Arpitha
			partovaluemap.put(navMenuTitles[10], 10);
			partovaluemap.put(navMenuTitles[11], 11);
			partovaluemap.put(navMenuTitles[12], 12);
			// 11May2017 Arpitha - v2.6
			partovaluemap.put(navMenuTitles[13], 13);

			calNavDrawerItems();

			// enabling action bar app icon and behaving it as toggle button
			getActionBar().setDisplayHomeAsUpEnabled(true);
			getActionBar().setHomeButtonEnabled(true);

			mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.drawable.ic_drawer, // nav
																									// menu
																									// toggle
																									// icon
					R.string.app_name, // nav drawer open - description for
										// accessibility
					R.string.app_name // nav drawer close - description for
										// accessibility
			) {
				public void onDrawerClosed(View view) {
					getActionBar().setTitle(mTitle);
					// calling onPrepareOptionsMenu() to show action bar icons
					invalidateOptionsMenu();
				}

				public void onDrawerOpened(View drawerView) {
					getActionBar().setTitle(mDrawerTitle);
					// calling onPrepareOptionsMenu() to hide action bar icons
					invalidateOptionsMenu();

					// if nav drawer is opened, hide the action items
					boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);

					if (drawerOpen) {

						// navDrawerItems.remove(1);
						try {
							calNavDrawerItems();
							notification();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
									+ this.getClass().getSimpleName(), e);
							e.printStackTrace();
						}

						adapter.notifyDataSetChanged();
						mDrawerList.setAdapter(adapter);
					}

				}
			};
			mDrawerLayout.setDrawerListener(mDrawerToggle);

			if (savedInstanceState == null) {
				// on first time display view for first nav item

				boolean isPartoDildataAvailable = dbh.getPartoDilData(womenid, userid);
				if (isPartoDildataAvailable) {
					displayView(0);

				} else
					displayView(2);
			}

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}

		// KillAllActivitesAndGoToLogin.activity_stack.add(this);
		KillAllActivitesAndGoToLogin.addToStack(this);
	}

	/**
	 * Slide menu item click listener
	 */
	private class SlideMenuClickListener implements ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

			// display view for selected nav drawer item
			try {
				// displayView(position);
				String name = navDrawerItems.get(position).getTitle();// 31oct

				displayView(partovaluemap.get(name));
				// String val = (String)parent.getItemAtPosition(position);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
						+ this.getClass().getSimpleName(), e);
				e.printStackTrace();
			}
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// getMenuInflater().inflate(R.menu.main, menu);
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_womenlist, menu);

		return super.onCreateOptionsMenu(menu);
	}

	// changes made on 26Mar2015
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// toggle nav drawer on selecting action bar app icon/title
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		// Handle action bar actions click
		try {
			switch (item.getItemId()) {
			// Respond to the action bar's Up/Home button
			case android.R.id.home:

				// NavUtils.navigateUpFromSameTask(this);
				displayConfirmationAlert(getResources().getString(R.string.exit_msg), "");
				return true;

			case R.id.home:

				displayConfirmationAlert(getResources().getString(R.string.exit_msg), "main");
				return true;

			case R.id.viewpartograph:
				showpartograph();

				return true;

			case R.id.expertcomments:
				showExpertComments();

				return true;

			case R.id.logout:

				displayConfirmationAlert(getResources().getString(R.string.exit_msg), "login");
				return true;

			case R.id.viewprofile:

				displayConfirmationAlert(getResources().getString(R.string.exit_msg), "view_profile");
				return true;

			case R.id.info:

				displayConfirmationAlert(getResources().getString(R.string.exit_msg), "info");
				return true;

			case R.id.settings:

				displayConfirmationAlert(getResources().getString(R.string.exit_msg), "settings");
				return true;

			case R.id.sync:
				menuItem = item;
				menuItem.setActionView(R.layout.progressbar);
				menuItem.expandActionView();
				calSyncMtd();
				return true;

			// 12May2017 Arpitha
			case R.id.sms:
				Partograph_CommonClass.display_messagedialog(getApplicationContext(), woman.getUserId());
				return true;

			case R.id.comments:
				displayConfirmationAlert(getResources().getString(R.string.exit_msg),
						getResources().getString(R.string.comments));
				return true;

			case R.id.usermanual:
				displayConfirmationAlert(getResources().getString(R.string.exit_msg),
						getResources().getString(R.string.user_manual));
				return true;

			case R.id.summary:
				displayConfirmationAlert(getResources().getString(R.string.exit_msg),
						getResources().getString(R.string.summary));
				return true;

			case R.id.about:
				displayConfirmationAlert(getResources().getString(R.string.exit_msg),
						getResources().getString(R.string.about));
				return true;

			case R.id.changepass:
				Partograph_CommonClass.dispalyChangePasswordDialog(SlidingActivity.this);// 12May2017
																							// Arpitha
				return true;

			case R.id.disch:
				Intent disch = new Intent(SlidingActivity.this, DischargedWomanList_Activity.class);
				startActivity(disch);
				return true;

			// 03Oct2017 Arpitha
			case R.id.del:
//				Intent disc = new Intent(SlidingActivity.this, DeliveryInfo_Activity.class);
//				disc.putExtra("woman", woman);
//				startActivity(disc);

				if (woman.getDel_type() != 0)// 21Oct2016
												// Arpitha
				{// 21Oct2016 Arpitha
					// 05Jan2016
					// / showDialogToUpdateStatus();
					Partograph_CommonClass.showDialogToUpdateStatus(SlidingActivity.this, woman);
//					loadWomendata(displayListCountddel);
//					if(adapter!=null)
//					adapter.notifyDataSetChanged();
					// loadWomendata(displayListCountddel);
				} else// 21Oct2016 Arpitha
				{

					Intent graph = new Intent(SlidingActivity.this, DeliveryInfo_Activity.class);
					graph.putExtra("woman", woman);
					startActivity(graph);
				} // 21Oct2016 Arpitha
				return true;
			/*
			 * case R.id.viewprofile: Intent view = new
			 * Intent(LatentPhase_Activity.this, ViewProfile_Activity.class);
			 * view.putExtra("woman", woman); startActivity(view); return true;
			 */

			case R.id.ref:
				/*Intent ref = new Intent(SlidingActivity.this, ReferralInfo_Activity.class);
				ref.putExtra("woman", woman);
				startActivity(ref);*/
				Cursor cur = dbh.getReferralDetails(woman.getUserId(), woman.getWomenId());
				if (cur != null && cur.getCount() > 0) {
					Partograph_CommonClass.showDialogToUpdateReferral(SlidingActivity.this, woman);
				} else {
					Intent ref = new Intent(SlidingActivity.this, ReferralInfo_Activity.class);
					ref.putExtra("woman", woman);
					startActivity(ref);
				}
				return true;

			case R.id.pdf:
				Partograph_CommonClass.exportPDFs(SlidingActivity.this, woman);
				return true;

			case R.id.disc:
				Intent discharge = new Intent(SlidingActivity.this, DiscargeDetails_Activity.class);
				discharge.putExtra("woman", woman);
				startActivity(discharge);
				return true;

			case R.id.apgar:
				if (woman.getDel_type() != 0) {
					Intent apgar = new Intent(SlidingActivity.this, Activity_Apgar.class);
					apgar.putExtra("woman", woman);
					startActivity(apgar);
				} else
					Toast.makeText(getApplicationContext(),
							getResources().getString(R.string.msg_applicableafterdelivery), Toast.LENGTH_LONG).show();
				return true;

			case R.id.stages:
				if (woman.getDel_type() != 0) {
					Intent stages = new Intent(SlidingActivity.this, StageofLabor.class);
					stages.putExtra("woman", woman);
					startActivity(stages);
				} else
					Toast.makeText(getApplicationContext(),
							getResources().getString(R.string.msg_applicableafterdelivery), Toast.LENGTH_LONG).show();
				return true;

			case R.id.post:
				if (woman.getDel_type() != 0) {
					Intent post = new Intent(SlidingActivity.this, PostPartumCare_Activity.class);
					post.putExtra("woman", woman);
					startActivity(post);
				} else
					Toast.makeText(getApplicationContext(),
							getResources().getString(R.string.msg_applicableafterdelivery), Toast.LENGTH_LONG).show();
				return true;
				
			case R.id.lat:
				Intent lat = new Intent(SlidingActivity.this, LatentPhase_Activity.class);
				lat.putExtra("woman", woman);
				startActivity(lat);
				return true;

			case R.id.parto:
				Intent parto = new Intent(SlidingActivity.this, SlidingActivity.class);
				parto.putExtra("woman", woman);
				startActivity(parto);
				return true;// 03Oct2017 Arpitha

			}
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
		return super.onOptionsItemSelected(item);

	}

	/*
	 * * Called when invalidateOptionsMenu() is triggered
	 */
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// if nav drawer is opened, hide the action items
		boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
		// menu.findItem(R.id.action_settings).setVisible(!drawerOpen);

		if (Partograph_CommonClass.isRecordExistsForSync)
			menu.findItem(R.id.sync).setIcon(getResources().getDrawable(R.drawable.ic_sync_red));
		else
			menu.findItem(R.id.sync).setIcon(getResources().getDrawable(R.drawable.ic_sync_green));

		if (isMsgFHR || isMsgAFM || isMsgDil || isMsgCon || isMsgOxy || isMsgDrugs || isMsgPBP || isMsgTemp || isMsgUT)
			menu.findItem(R.id.ic_msg).setVisible(true);
		else
			menu.findItem(R.id.ic_msg).setVisible(false);

		if (drawerOpen) {

			// navDrawerItems.remove(1);
			try {
				calNavDrawerItems();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
						+ this.getClass().getSimpleName(), e);
				e.printStackTrace();
			}

			adapter.notifyDataSetChanged();
			mDrawerList.setAdapter(adapter);
		}
		if (isview) {
			menu.findItem(R.id.viewpartograph).setVisible(false);
		}

		if (!(AppContext.checkSimState() == TelephonyManager.SIM_STATE_READY)) {// 12May2017
																				// Arpitha
			menu.findItem(R.id.sms).setVisible(false);
		}

		menu.findItem(R.id.delete).setVisible(false);// 12May2017 Arpitha
		menu.findItem(R.id.addcomments).setVisible(false);// 12May2017 Arpitha
		menu.findItem(R.id.registration).setVisible(false);// 12May2017 Arpitha

		menu.findItem(R.id.search).setVisible(false);// 14Jun2017 Arpitha

		menu.findItem(R.id.save).setVisible(false);// 08Aug2017 Arpitha

		// 03Oct2017 Arpitha
		menu.findItem(R.id.del).setVisible(true);
		menu.findItem(R.id.ref).setVisible(true);
		menu.findItem(R.id.pdf).setVisible(true);
		menu.findItem(R.id.stages).setVisible(true);
		menu.findItem(R.id.apgar).setVisible(true);
		menu.findItem(R.id.lat).setVisible(true);
		menu.findItem(R.id.disc).setVisible(true);
		menu.findItem(R.id.viewprofile).setVisible(true);
		// menu.findItem(R.id.ad).setVisible(true);
		menu.findItem(R.id.post).setVisible(true);
		// menu.findItem(R.id.parto).setVisible(true);// 03Oct2017 Arpitha

		return super.onPrepareOptionsMenu(menu);
	}

	/**
	 * Diplaying fragment view for selected nav drawer list item
	 */
	private void displayView(int position) throws Exception {
		// update the main content by replacing fragments
		Fragment fragment = null;
		isview = false;
		switch (position) {
		case 0: {
			// updated 31Aug2016 - bindu to check dil is entered first
			boolean isPartoDildataAvailable = dbh.getPartoDilData(womenid, userid);
			if (isPartoDildataAvailable || isReferred || woman.getDel_type() != 0 || woman.getregtype() == 2
					|| SlidingActivity.arrVal.size() > 0) {// 28Sep2016
				// Arpitha
				// -
				// include
				// isReferred
				// and
				// deltype
				// conditions
				fragment = new Fragment_FHR();
			} else {
				Toast.makeText(getApplicationContext(), getResources().getString(R.string.alertusertoenterdil),
						Toast.LENGTH_LONG).show();
				fragment = new Fragment_Dilatation();
				position = 2;
			}
		}
			break;
		case 1: {
			// updated 31Aug2016 - bindu to check dil is entered first
			boolean isPartoDildataAvailable = dbh.getPartoDilData(womenid, userid);
			if (isPartoDildataAvailable || isReferred || woman.getDel_type() != 0 || woman.getregtype() == 2
					|| SlidingActivity.arrVal.size() > 0) {// 28Sep2016
				// Arpitha
				// -
				// include
				// isReferred
				// and
				// deltype
				// conditions)
				// {
				fragment = new Fragment_AFM();
			} else {
				Toast.makeText(getApplicationContext(), getResources().getString(R.string.alertusertoenterdil),
						Toast.LENGTH_LONG).show();
				fragment = new Fragment_Dilatation();
				position = 2;
			}
		}
			break;
		case 2:
			fragment = new Fragment_Dilatation();
			break;
		case 3: {
			// updated 31Aug2016 - bindu to check dil is entered first
			boolean isPartoDildataAvailable = dbh.getPartoDilData(womenid, userid);
			if (isPartoDildataAvailable || isReferred || woman.getDel_type() != 0 || woman.getregtype() == 2
					|| SlidingActivity.arrVal.size() > 0) {// 28Sep2016
				// Arpitha
				// -
				// include
				// isReferred
				// and
				// deltype
				// conditions)
				// {
				fragment = new Fragment_Contraction();
			} else {
				Toast.makeText(getApplicationContext(), getResources().getString(R.string.alertusertoenterdil),
						Toast.LENGTH_LONG).show();
				fragment = new Fragment_Dilatation();
			}
		}
			break;
		case 4: {
			// updated 31Aug2016 - bindu to check dil is entered first
			boolean isPartoDildataAvailable = dbh.getPartoDilData(womenid, userid);
			if (isPartoDildataAvailable || isReferred || woman.getDel_type() != 0 || woman.getregtype() == 2
					|| SlidingActivity.arrVal.size() > 0) {// 28Sep2016
				// Arpitha
				// -
				// include
				// isReferred
				// and
				// deltype
				// conditions)
				// {
				fragment = new Fragment_Oxytocin();
			} else {
				Toast.makeText(getApplicationContext(), getResources().getString(R.string.alertusertoenterdil),
						Toast.LENGTH_LONG).show();
				fragment = new Fragment_Dilatation();
			}
		}

			break;// commented on 13Oct2016 Arpitha
		case 5: {
			// updated 31Aug2016 - bindu to check dil is entered first
			boolean isPartoDildataAvailable = dbh.getPartoDilData(womenid, userid);
			if (isPartoDildataAvailable || isReferred || woman.getDel_type() != 0 || woman.getregtype() == 2
					|| SlidingActivity.arrVal.size() > 0) {// 28Sep2016
				// Arpitha
				// -
				// include
				// isReferred
				// and
				// deltype
				// conditions)
				// {
				fragment = new Fragment_Drugs();
			} else {
				Toast.makeText(getApplicationContext(), getResources().getString(R.string.alertusertoenterdil),
						Toast.LENGTH_LONG).show();
				fragment = new Fragment_Dilatation();
			}
		}

			break;
		case 6: {
			// updated 31Aug2016 - bindu to check dil is entered first
			boolean isPartoDildataAvailable = dbh.getPartoDilData(womenid, userid);
			if (isPartoDildataAvailable || isReferred || woman.getDel_type() != 0 || woman.getregtype() == 2
					|| SlidingActivity.arrVal.size() > 0) {// 28Sep2016
				// Arpitha
				// -
				// include
				// isReferred
				// and
				// deltype
				// conditions)
				// {
				fragment = new Fragment_PulseBP();
			} else {
				Toast.makeText(getApplicationContext(), getResources().getString(R.string.alertusertoenterdil),
						Toast.LENGTH_LONG).show();
				fragment = new Fragment_Dilatation();
			}
		}

			break;
		case 7: {
			// updated 31Aug2016 - bindu to check dil is entered first
			boolean isPartoDildataAvailable = dbh.getPartoDilData(womenid, userid);
			if (isPartoDildataAvailable || isReferred || woman.getDel_type() != 0 || woman.getregtype() == 2
					|| SlidingActivity.arrVal.size() > 0) {// 28Sep2016
				// Arpitha
				// -
				// include
				// isReferred
				// and
				// deltype
				// conditions)
				// {
				fragment = new Fragment_Temperature();
			} else {
				Toast.makeText(getApplicationContext(), getResources().getString(R.string.alertusertoenterdil),
						Toast.LENGTH_LONG).show();
				fragment = new Fragment_Dilatation();
			}
		}

			break;
		case 8: {
			// updated 31Aug2016 - bindu to check dil is entered first
			boolean isPartoDildataAvailable = dbh.getPartoDilData(womenid, userid);
			if (isPartoDildataAvailable || isReferred || woman.getDel_type() != 0 || woman.getregtype() == 2
					|| SlidingActivity.arrVal.size() > 0) {// 28Sep2016
				// Arpitha
				// -
				// include
				// isReferred
				// and
				// deltype
				// conditions)
				// {
				fragment = new Fragment_Urinetest();
			} else {
				Toast.makeText(getApplicationContext(), getResources().getString(R.string.alertusertoenterdil),
						Toast.LENGTH_LONG).show();
				fragment = new Fragment_Dilatation();
			}
		}

			break;
		case 9:

			// updated 18Aug2016 - bindu
			// boolean isPartodataAvailable = dbh.getPartoData(womenid, userid);
			// if (isReferred || woman.getDel_type() != 0) {// 28Sep2016
			// Arpitha
			// -
			// include
			// isReferred
			// and
			// deltype
			// conditions)
			// {
			isview = true;
			fragment = new Fragment_viewpartograph();
			/*
			 * } else { Toast.makeText(getApplicationContext(),
			 * getResources().getString(R.string.nodata), Toast.LENGTH_LONG)
			 * .show(); mDrawerLayout.closeDrawer(mDrawerList); }commented on
			 * 10Nov2016 Arpitha
			 */

			break;

		// 07Sep2017 Arpitha

		case 10:
			Intent latent = new Intent(SlidingActivity.this, LatentPhase_Activity.class);
			latent.putExtra("woman", woman);
			startActivity(latent);
			break;

		// 29Dec2016 Arpitha
		case 11:

			if (woman.getDel_type() == 0)// 09Jan2017 Arpitha
			{
				Intent del = new Intent(SlidingActivity.this, DeliveryInfo_Activity.class);
				del.putExtra("woman", woman);
				startActivity(del);
			} else
				Partograph_CommonClass.showDialogToUpdateStatus(context, woman);// 20July2017
																				// Arpitha
			// Toast.makeText(getApplicationContext(),
			// getResources().getString(R.string.del_status_has_been_updated),
			// Toast.LENGTH_LONG).show();

			break;

		// 29Dec2016 Arpitha
		case 12:

			if (woman.getDel_type() == 0) {
				Toast.makeText(getApplicationContext(), getResources().getString(R.string.msg_applicableafterdelivery),
						Toast.LENGTH_LONG).show();
				boolean isPartoDildataAvailable = dbh.getPartoDilData(womenid, userid);
				if (isPartoDildataAvailable) {
					fragment = new Fragment_FHR();
				} else
					fragment = new Fragment_Dilatation();

			} else {
				Intent stages = new Intent(SlidingActivity.this, StageofLabor.class);
				stages.putExtra("woman", woman);
				startActivity(stages);
			}

			break;

		// 29Dec2016 Arpitha
		case 13:

			if (woman.getDel_type() == 0) {
				Toast.makeText(getApplicationContext(), getResources().getString(R.string.msg_applicableafterdelivery),
						Toast.LENGTH_LONG).show();
				boolean isPartoDildataAvailable = dbh.getPartoDilData(womenid, userid);
				if (isPartoDildataAvailable) {
					fragment = new Fragment_FHR();
				} else
					fragment = new Fragment_Dilatation();

			} else {

				Intent apgar = new Intent(SlidingActivity.this, Activity_Apgar.class);
				apgar.putExtra("woman", woman);
				startActivity(apgar);
			}

			break;
		// 11May2017 Arpitha - v2.6
		case 14:
			if (isReferred)// 20July2017 Arpitha
			{
				Partograph_CommonClass.showDialogToUpdateReferral(context, woman);
			} else {// 20July2017 Arpitha
				Intent ref = new Intent(SlidingActivity.this, ReferralInfo_Activity.class);
				ref.putExtra("woman", woman);
				startActivity(ref);// 11May2017 Arpitha - v2.6
			}
			break;

		default:
			break;
		}

		if (fragment != null) {
			FragmentManager fragmentManager = getFragmentManager();
			fragmentManager.beginTransaction().replace(R.id.frame_container, fragment).commit();

			// update selected item and title, then close the drawer
			// mDrawerList.setItemChecked(position, true);//commented on
			// 31oct2016 by Bindu for oxctocin
			// mDrawerList.setSelection(position);//commented on 31oct2016 by
			// Bindu for oxctocin
			// setTitle(navMenuTitles[position]); //31Aug2016 - Set woman name
			// instead of title
			setTitle(mTitle);
			mDrawerLayout.closeDrawer(mDrawerList);
		} else {
			// error in creating fragment
			Log.e("MainActivity", "Error in creating fragment");
		}
	}

	@Override
	public void setTitle(CharSequence title) {
		mTitle = title;
		getActionBar().setTitle(mTitle);
	}

	/**
	 * When using the ActionBarDrawerToggle, you must call it during
	 * onPostCreate() and onConfigurationChanged()...
	 */

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Pass any configuration change to the drawer toggls
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	// Retrieve comments from tbl_comment from server
	private void calRetrieveComments() throws Exception {

		int interval = 1;

		commentsTimer = new CountDownTimer(Long.MAX_VALUE, interval * START_AFTER_SECONDS) {

			@Override
			public void onTick(long millisUntilFinished) {
				AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
						+ this.getClass().getSimpleName());

				AsyncCallWS_Comments task = new AsyncCallWS_Comments();
				task.execute();
			}

			@Override
			public void onFinish() {
				// TODO Auto-generated method stub

			}
		}.start();

	}

	// Async cls to sync
	public class AsyncCallWS_Comments extends AsyncTask<String, Void, String> {

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());

			if (!inetOn)
				System.out.println("Internet :" + getResources().getString(R.string.chk_internet));

			else {
				if (Partograph_CommonClass.movedToInputFolder && Partograph_CommonClass.responseAckCount <= 5) {
					if (!SyncFunctions.syncUptoDate) {
						AsyncCallWS_Comments task = new AsyncCallWS_Comments();
						task.execute();
					} else {
						AsyncCallWS_Comments.this.cancel(true);

					}
				} else {
					AsyncCallWS_Comments.this.cancel(true);

				}

				try {
					getCommentCountNotRead();

				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
		}

		@Override
		protected String doInBackground(String... params) {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
			try {
				inetOn = Partograph_CommonClass.isConnectionAvailable(getApplicationContext());
				if (inetOn)
					/*
					 * serverResult = new SyncFunctions(getApplicationContext())
					 * .SyncFunctionComments( rowItems.get(pos).getUserId(),
					 * getResources().getString(R.string.dbName),
					 * Partograph_CommonClass.ipAdd);
					 */
					if (Partograph_CommonClass.sPojo != null) {
					serverResult = new SyncFunctions(getApplicationContext()).SyncFunctionComments(woman.getUserId(), Partograph_CommonClass.sPojo.getServerDb(), Partograph_CommonClass.sPojo.getIpAddress());
					}

			} catch (Exception e) {
				AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
						+ this.getClass().getSimpleName(), e);
				return e.getLocalizedMessage();
			}
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
			return serverResult;
		}

		@Override
		protected void onPreExecute() {
			System.out.println("BG PROCESS");
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
		}
	}

	// Get the count of comments not read
	public void getCommentCountNotRead() throws Exception {

		Cursor cursor = dbh.getCommentsUnread(woman.getWomenId(), woman.getUserId());

		if (cursor != null && cursor.getCount() > 0) {

			// isMsgRec = true;
			cursor.moveToFirst();

			do {

				switch (cursor.getInt(1)) {

				// fhr
				case 1: {
					Partograph_CommonClass.fhrcmt_cnt = cursor.getInt(0);
					isMsgFHR = true;
					break;
				}
				// afm
				case 2: {
					Partograph_CommonClass.afmcmt_cnt = cursor.getInt(0);
					isMsgAFM = true;
					break;
				}

				// dilatation
				case 3: {
					Partograph_CommonClass.dilcmt_cnt = cursor.getInt(0);
					isMsgDil = true;
					break;
				}
				// contraction
				case 4: {
					Partograph_CommonClass.contcmt_cnt = cursor.getInt(0);
					isMsgCon = true;
					break;
				}
				// oxytocin
				case 5: {
					Partograph_CommonClass.oxycmt_cnt = cursor.getInt(0);
					isMsgOxy = true;
					break;
				}
				// drugs
				case 6: {
					Partograph_CommonClass.drugscmt_cnt = cursor.getInt(0);
					isMsgDrugs = true;
					break;
				}
				// pbp
				case 7: {
					Partograph_CommonClass.pbpcmt_cnt = cursor.getInt(0);
					isMsgPBP = true;
					break;
				}
				// temp
				case 8: {
					Partograph_CommonClass.tempcmt_cnt = cursor.getInt(0);
					isMsgTemp = true;
					break;
				}
				// urine test
				case 9: {
					Partograph_CommonClass.utcmt_cnt = cursor.getInt(0);
					isMsgUT = true;
					break;
				}
				case 10:
					Partograph_CommonClass.apgar_cnt = cursor.getInt(0);
					isMsgApg = true;
					break;

				}

			} while (cursor.moveToNext());

			invalidateOptionsMenu();

		} else {

			// isMsgRec = false;

			Partograph_CommonClass.fhrcmt_cnt = 0;
			Partograph_CommonClass.afmcmt_cnt = 0;
			Partograph_CommonClass.dilcmt_cnt = 0;
			Partograph_CommonClass.contcmt_cnt = 0;
			Partograph_CommonClass.oxycmt_cnt = 0;
			Partograph_CommonClass.drugscmt_cnt = 0;
			Partograph_CommonClass.pbpcmt_cnt = 0;
			Partograph_CommonClass.tempcmt_cnt = 0;
			Partograph_CommonClass.utcmt_cnt = 0;
		}

	}

	/** This method Calls the idle timeout */
	@Override
	public void onUserInteraction() {
		super.onUserInteraction();
		KillAllActivitesAndGoToLogin
				.delayedIdle(Integer.parseInt(Partograph_CommonClass.properties.getProperty("idleTimeOut")));
	}

	// Changed on 26Mar2015

	// Display Confirmation to exit the screen
	private void displayConfirmationAlert(String exit_msg, final String classname) throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		final Dialog dialog = new Dialog(SlidingActivity.this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.alertdialog);

		dialog.show();

		final TextView txtdialog = (TextView) dialog.findViewById(R.id.txtdialog);
		Button imgbtnyes = (Button) dialog.findViewById(R.id.imgbtnyes);
		Button imgbtnno = (Button) dialog.findViewById(R.id.imgbtnno);
		final TextView txtdialog1 = (TextView) dialog.findViewById(R.id.txtval);
		final TextView txtdialog2 = (TextView) dialog.findViewById(R.id.txtval1);

		txtdialog.setText(exit_msg);
		txtdialog1.setVisibility(View.GONE);
		txtdialog2.setVisibility(View.GONE);

		imgbtnyes.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				dialog.cancel();

				if (classname.equalsIgnoreCase("login")) {
					Intent login = new Intent(getApplicationContext(), LoginActivity.class);
					startActivity(login);
				} else if (classname.equalsIgnoreCase("view_profile")) {
					Intent viewprofile = new Intent(getApplicationContext(), ViewProfile_Activity.class);
					viewprofile.putExtra("woman", woman);
					// viewprofile.putExtra("position", pos);
					startActivity(viewprofile);
				} else if (classname.equalsIgnoreCase("settings")) {
					Intent settings = new Intent(getApplicationContext(), Settings_parto.class);
					startActivity(settings);
				} else if (classname.equalsIgnoreCase("info")) {
					Intent info = new Intent(getApplicationContext(), GraphInformation.class);
					startActivity(info);
				} else if (classname.equalsIgnoreCase(getResources().getString(R.string.user_manual))) {// 12May2017
																										// Arpitha
																										// -
																										// v2.6
					Intent info = new Intent(getApplicationContext(), UseManual.class);
					startActivity(info);
				} else if (classname.equalsIgnoreCase(getResources().getString(R.string.summary))) {
					Intent info = new Intent(getApplicationContext(), Summary_Activity.class);
					startActivity(info);
				} else if (classname.equalsIgnoreCase(getResources().getString(R.string.about))) {
					Intent info = new Intent(getApplicationContext(), About.class);
					startActivity(info);
				} else if (classname.equalsIgnoreCase(getResources().getString(R.string.comments))) {
					Intent info = new Intent(getApplicationContext(), Comments_Activity.class);
					startActivity(info);
				} // 12May2017 Arpitha - v2.6
				else {
					Intent main = new Intent(getApplicationContext(), Activity_WomenView.class);
					startActivity(main);
				}

			}
		});

		imgbtnno.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.cancel();
			}
		});
	}

	// Sync
	private void calSyncMtd() throws Exception {
		SyncFunctions.syncUptoDate = false;
		Partograph_CommonClass.responseAckCount = 0;

		AsyncCallWS task = new AsyncCallWS();
		task.execute();
	}

	private void showExpertComments() throws Exception {

		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		// aq = new AQuery(getApplicationContext());

		dbh = Partograph_DB.getInstance(getApplicationContext());

		// pos = getIntent().getIntExtra("position", 0);
		wpojo = new Women_Profile_Pojo();
		// womenId_ex = woman.getWomenId();
		// String userId = woman.getUserId();

		ArrayList<Add_value_pojo> values1 = new ArrayList<Add_value_pojo>();
		try {
			Cursor cursor = dbh.getExpertComments(woman.getWomenId(), userid);

			values1 = new ArrayList<Add_value_pojo>();
			if (cursor.getCount() > 0) {
				cursor.moveToFirst();

				do {
					Add_value_pojo commentsData = new Add_value_pojo();
					commentsData.setObj_Id(cursor.getInt(0));
					commentsData.setComments(cursor.getString(1));
					commentsData.setStrdate(cursor.getString(2));
					commentsData.setStrTime(cursor.getString(3));

					values1.add(commentsData);
				} while (cursor.moveToNext());

			}

			if (values1.size() > 0) {
				final Dialog dialog = new Dialog(SlidingActivity.this);
				dialog.setContentView(R.layout.exprt_comments);
				dialog.setTitle(getResources().getString(R.string.expert_comments));
				dialog.show();
				Button btnok = (Button) dialog.findViewById(R.id.btnok);

				// if (values1.size() > 0) {
				listcommentsfhr = (ListView) dialog.findViewById(R.id.listCommentsfhr);
				// Parcelable state = listcommentsfhr.onSaveInstanceState();

				Adapter_ExpertComments adapter = new Adapter_ExpertComments(getApplicationContext(),
						R.layout.exprt_comments, values1);
				listcommentsfhr.setAdapter(adapter);
				btnok.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						dialog.cancel();
					}
				});

				// }
			} else {
				Toast.makeText(getApplicationContext(), getResources().getString(R.string.no_exprt_cmnt),
						Toast.LENGTH_LONG).show();
			}

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}

	}

	private void showpartograph() throws Exception {

		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		final Dialog dialog = new Dialog(SlidingActivity.this);
		dialog.requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		dialog.setContentView(R.layout.activity_printparto);

		dialog.getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.dialog_title);
		dialog.getWindow().setBackgroundDrawableResource(R.color.white);

		String doa;
		String toa;
		String risk;

		txtdialogtitle = (TextView) dialog.findViewById(R.id.graph_title);
		txtdialogtitle.setText(getResources().getString(R.string.partograph));
		ImageButton imgbtncanceldialog = (ImageButton) dialog.findViewById(R.id.imgbtncanceldialog);
		TextView etname = (TextView) dialog.findViewById(R.id.wname);
		TextView etage = (TextView) dialog.findViewById(R.id.wage);
		TextView etdoa = (TextView) dialog.findViewById(R.id.wdoa);
		// TextView ettoa = (TextView) dialog.findViewById(R.id.wtoa);
		TextView etgravida = (TextView) dialog.findViewById(R.id.wgravida);
		TextView etgest = (TextView) dialog.findViewById(R.id.wgest);
		TextView etrisk = (TextView) dialog.findViewById(R.id.wtrisk);
		TextView txtdelsttaus = (TextView) dialog.findViewById(R.id.wdelstatus);
		TextView txtdeldate = (TextView) dialog.findViewById(R.id.wdeldate);
		dialog.show();

		// 07Dec2016 Arpitha
		imgpdf = (ImageView) dialog.findViewById(R.id.imgpdf);

		imgpdf.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				try {
					path = AppContext.mainDir + "/PDF/PartographSheet";// 03April2017
																		// Arpitha

					dir = new File(path);
					if (!dir.exists())
						dir.mkdirs();

					Log.d("PDFCreator", "PDF Path: " + path);

					file = new File(dir, woman.getWomen_name() + "_PartographSheet.pdf");
					Partograph_CommonClass.pdfAlertDialog(context, woman, "partograph", null, null);
					;
					// View_Partograph parto = new View_Partograph();
					// //parto.putExtra("woman", woman);//24
					// parto.
				} catch (Exception e) {
					// TODO Auto-generated catch block
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					e.printStackTrace();
				}

			}
		});// 07Dec2016 Arpitha

		// String dod = "", tod = "";

		etname.setText(woman.getWomen_name());
		etage.setText(woman.getAge() + getResources().getString(R.string.yrs));
		if (woman.getregtype() != 2) {
			doa = Partograph_CommonClass.getConvertedDateFormat(woman.getDate_of_admission(),
					Partograph_CommonClass.defdateformat);
			toa = Partograph_CommonClass.gettimein12hrformat(woman.getTime_of_admission());
			etdoa.setText(getResources().getString(R.string.reg) + ": " + doa + "/" + toa);
		} else {
			etdoa.setText(getResources().getString(R.string.reg) + ": " + woman.getDate_of_reg_entry());
		}
		etgest.setText(getResources().getString(R.string.gest_age) + ":"
				+ (woman.getGestationage() == 0 ? getResources().getString(R.string.notknown)
						: woman.getGestationage() + getResources().getString(R.string.wks)));// 23Aug2016
		// -
		// bindu
		// -
		// gest
		// age
		// not
		// known
		// chk
		etgravida.setText(getResources().getString(R.string.gravida_short_label) + ":" + woman.getGravida() + ", "
				+ getResources().getString(R.string.para_short_label) + ":" + woman.getPara());
		woman.getRisk_category();
		if (woman.getRisk_category() == 1) {
			risk = getResources().getString(R.string.high);
		} else
			risk = getResources().getString(R.string.low);
		etrisk.setText(getResources().getString(R.string.risk_short_label) + ":" + risk);

		if (woman.getDel_type() > 0) {
			String[] deltype = getResources().getStringArray(R.array.del_type);
			txtdelsttaus.setText(getResources().getString(R.string.delstatus) + ":" + deltype[woman.getDel_type() - 1]);
			String dod = Partograph_CommonClass.getConvertedDateFormat(woman.getDel_Date(),
					Partograph_CommonClass.defdateformat);
			// aq.id(R.id.wdeldate).text(getResources().getString(R.string.dod)
			// + ": " + dod);
			String tod = Partograph_CommonClass.gettimein12hrformat(woman.getDel_Time());
			// 31August2016 Arpitha
			txtdeldate.setText(getResources().getString(R.string.del) + ": " + dod + "/" + tod);
			// aq.id(R.id.wdeltime).text(getResources().getString(R.string.tod)
			// + ": " + tod);

		}

		imgbtncanceldialog.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				try {
					dialog.cancel();
				} catch (Exception e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					e.printStackTrace();
				}
			}
		});

		try {
			dbh = Partograph_DB.getInstance(getApplicationContext());
			comptabpos = Partograph_CommonClass.curr_tabpos;
			isViewParto = true;

			womenid = woman.getWomenId();
			userid = woman.getUserId();

			tbllayoutfhr = (TableLayout) dialog.findViewById(R.id.tblfhr);
			tbllayoutdil = (TableLayout) dialog.findViewById(R.id.tbldil);
			tbllayoutcontract = (TableLayout) dialog.findViewById(R.id.tblcontractions);
			tbllayoutpbp = (TableLayout) dialog.findViewById(R.id.tblpbp);
			tbllayoutafm = (TableLayout) dialog.findViewById(R.id.tblafm);
			tbllayoutoxytocin = (TableLayout) dialog.findViewById(R.id.tbloxytocin);
			tbllayoutdrugs = (TableLayout) dialog.findViewById(R.id.tbldrugs);
			tbllayouttemp = (TableLayout) dialog.findViewById(R.id.tbltemp);
			tbllayouturinetest = (TableLayout) dialog.findViewById(R.id.tblut);

			// 08jan2016
			// txtdata = (TextView) dialog.findViewById(R.id.txtdata);
			llwinfo = (LinearLayout) dialog.findViewById(R.id.llpartodata);

			// 16May2017 Arpitha - v2.6
			DisplayFHRGraph();
			DisplayAFMGraph();
			DisplayDilatationGraph();
			DisplayContrcationGraph();
			DisplayOxytocinGraph();
			DisplayDrugsGraph();
			DisplayPulseBPGraph();
			DisplayTempGraph();
			DisplayUrineTestGraph();// 16May2017 Arpitha - v2.6

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
	}

	// updated on 7july2016 by Arpitha
	/*
	 * this method is to display the notification to the user when the values
	 * are not entered after minimum duration(i.e only after first entry) for
	 * applicable parameters
	 */
	public void notification() throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		// updated 18Aug2016 - bindu
		ispulsetimevalid = false;
		isfhrtimevalid = false;
		isdilatationtimevalid = false;
		iscontractiontimevalid = false;
		boolean isnotify = false;
		if (woman.getDel_type() == 0) {
			Cursor cur = dbh.getReferralDetails(userid, womenid);
			if (cur != null && cur.getCount() > 0) {
				cur.moveToFirst();
				isnotify = false;
				// 23Sep2016 Arpitha
				isReferred = true;
			} else {
				isnotify = true;
				// 23Sep2016 Arpitha
				isReferred = false;
			}

			if (isnotify) {
				isfhrtimevalid = dbh.getNotifyForObject(womenid, 1);
				isdilatationtimevalid = dbh.getNotifyForObject(womenid, 3);
				iscontractiontimevalid = dbh.getNotifyForObject(womenid, 4);
				ispulsetimevalid = dbh.getNotifyForObject(womenid, 7);
			}
		}

	}

	// 20oct2016 Arpitha
	@Override
	public void onBackPressed() {
		Intent i = new Intent(SlidingActivity.this, Activity_WomenView.class);
		startActivity(i);
	}

	// 31Oct2016 Arpitha
	private void calNavDrawerItems() throws Exception {

		try {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
			dangerList = dbh.getDangerList(womenid, userid);

			navDrawerItems = new ArrayList<NavDrawerItem>();

			// adding nav drawer items to array

			ArrayList<Integer> cmnt_cnt = new ArrayList<Integer>();

			cmnt_cnt.add(Partograph_CommonClass.fhrcmt_cnt);
			cmnt_cnt.add(Partograph_CommonClass.afmcmt_cnt);
			cmnt_cnt.add(Partograph_CommonClass.dilcmt_cnt);
			cmnt_cnt.add(Partograph_CommonClass.contcmt_cnt);
			cmnt_cnt.add(Partograph_CommonClass.oxycmt_cnt);
			cmnt_cnt.add(Partograph_CommonClass.drugscmt_cnt);
			cmnt_cnt.add(Partograph_CommonClass.pbpcmt_cnt);
			cmnt_cnt.add(Partograph_CommonClass.tempcmt_cnt);
			cmnt_cnt.add(Partograph_CommonClass.utcmt_cnt);

			for (int i = 1; i <= 14; i++) {// 12may2017 Arpitha - v2.6 - changed
											// i<=13 to i<=14

				if (!(LoginActivity.objectid.contains(i))) {
					if (i <= 9) {
						navDrawerItems.add(new NavDrawerItem(navMenuTitles[i - 1],
								navMenuIcons.getResourceId(i - 1, -1), true, cmnt_cnt.get(i - 1)));
					} else {
						navDrawerItems.add(
								new NavDrawerItem(navMenuTitles[i - 1], navMenuIcons.getResourceId(0, -1), true, 0));
					}
				}
			}

			// Recycle the typed array
			// navMenuIcons.recycle();commented on 02Nov2016

			mDrawerList.setOnItemClickListener(new SlideMenuClickListener());

			// setting the nav drawer list adapter
			adapter = new NavDrawerListAdapter(getApplicationContext(), navDrawerItems);
			mDrawerList.setAdapter(adapter);
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
	}

	// 16May2017 Arpitha - v2.6
	void DisplayFHRGraph() throws Exception {
		if (!(LoginActivity.objectid.contains(1))) {
			tbllayoutfhr.setVisibility(View.VISIBLE);

			mChartViewFHR = Partograph_CommonClass.createChart(woman.getWomenId(), woman.getUserId(), context, 1);
			if (mChartViewFHR != null) {
				RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
						RelativeLayout.LayoutParams.MATCH_PARENT, 400);
				mChartViewFHR.setLayoutParams(params);

				// tbllayoutfhr.setVisibility(View.VISIBLE);
				tbllayoutfhr.setBackground(getResources().getDrawable(R.drawable.edit_text_style_blue));
				tbllayoutfhr.addView(mChartViewFHR);

				if (mChartViewFHR != null) {
					mChartViewFHR.repaint();
				}
			}
		} else
			tbllayoutfhr.setVisibility(View.GONE);

	}

	// 16May2017 Arpitha - v2.6
	void DisplayDilatationGraph() throws Exception {
		if (!(LoginActivity.objectid.contains(3))) {

			mChartViewDilatation = Partograph_CommonClass.createDilatationGraph(woman.getWomenId(), woman.getUserId(),
					context, 3);

			tbllayoutdil.setVisibility(View.VISIBLE);
			RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
					RelativeLayout.LayoutParams.WRAP_CONTENT, 450);
			mChartViewDilatation.setLayoutParams(params);

			tbllayoutdil.addView(mChartViewDilatation);

			if (mChartViewDilatation != null) {
				mChartViewDilatation.repaint();
			}
		} else
			tbllayoutdil.setVisibility(View.GONE);

	}

	// 16May2017 Arpitha - v2.6
	void DisplayAFMGraph() throws Exception {
		if (!(LoginActivity.objectid.contains(2))) {
			tbllayoutafm.setVisibility(View.VISIBLE);

			tbllayoutafm = Partograph_CommonClass.dispalyAFMData(context, 2, woman.getWomenId(), woman.getUserId(),
					tbllayoutafm);

		} else
			tbllayoutafm.setVisibility(View.GONE);

	}

	// 16May2017 Arpitha - v2.6
	void DisplayContrcationGraph() throws Exception {
		if (!(LoginActivity.objectid.contains(4))) {

			mChartViewContraction = Partograph_CommonClass.DisplayContraction(4, woman.getWomenId(), woman.getUserId(),
					context);

			RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
					RelativeLayout.LayoutParams.WRAP_CONTENT, 450);
			mChartViewContraction.setLayoutParams(params);

			tbllayoutcontract.addView(mChartViewContraction);

			if (mChartViewContraction != null) {
				mChartViewContraction.repaint();
			}
		}
	}

	// 16May2017 Arpitha - v2.6
	void DisplayOxytocinGraph() throws Exception {
		if (!(LoginActivity.objectid.contains(5))) {
			tbllayoutoxytocin.setVisibility(View.VISIBLE);
			tbllayoutoxytocin = Partograph_CommonClass.DisplayOxytocinData(context, 5, woman.getWomenId(),
					woman.getUserId(), tbllayoutoxytocin);
		} else
			tbllayoutoxytocin.setVisibility(View.GONE);

	}

	// 16May2017 Arpitha - v2.6
	void DisplayDrugsGraph() throws Exception {
		if (!(LoginActivity.objectid.contains(6))) {
			tbllayoutdrugs.setVisibility(View.VISIBLE);
			tbllayoutdrugs = Partograph_CommonClass.DisplayDrugsData(tbllayoutdrugs, 6, woman.getWomenId(),
					woman.getUserId(), context);
		} else
			tbllayoutdrugs.setVisibility(View.GONE);

	}

	// 16May2017 Arpitha - v2.6
	void DisplayUrineTestGraph() throws Exception {
		if (!(LoginActivity.objectid.contains(6))) {
			tbllayouturinetest.setVisibility(View.VISIBLE);
			tbllayouturinetest = Partograph_CommonClass.DisplayUrineTestData(context, 9, woman.getWomenId(),
					woman.getUserId(), tbllayouturinetest);
		} else
			tbllayouturinetest.setVisibility(View.GONE);

	}

	// 16May2017 Arpitha - v2.6
	void DisplayTempGraph() throws Exception {
		if (!(LoginActivity.objectid.contains(8))) {
			tbllayouttemp.setVisibility(View.VISIBLE);
			tbllayouttemp = Partograph_CommonClass.DisplayTemperatureData(context, 8, woman.getWomenId(),
					woman.getUserId(), tbllayouttemp);
		} else
			tbllayouttemp.setVisibility(View.GONE);

	}

	// 16May2017 Arpitha - v2.6
	void DisplayPulseBPGraph() throws Exception {

		if (!(LoginActivity.objectid.contains(7))) {
			tbllayoutpbp.setVisibility(View.VISIBLE);
			mChartViewPBP = Partograph_CommonClass.DisplayPulseBPData(context, woman.getWomenId(), woman.getUserId(),
					7);

			RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
					RelativeLayout.LayoutParams.WRAP_CONTENT, 400);
			mChartViewPBP.setLayoutParams(params);

			tbllayoutpbp.addView(mChartViewPBP);

			if (mChartViewPBP != null) {
				mChartViewPBP.repaint();
			}
		} else
			tbllayoutpbp.setVisibility(View.GONE);

	}

}
