package com.bc.partograph.sync;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Stack;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;





public class XmlParser {

	public static XMLObject parseXml(InputStream is) throws Exception{

		if(is == null) throw new IOException("Inputstream is null from the server");

		String key = null;

		String value = null;

		/**

		 * Stack is use to know the hierarchy of the XML file

		 * This will push all the tags when tag starts and pop when tag will end.

		 */

		Stack<String> mStack = null;

		/**

		 * This hashmap is for add the XMLObject.

		 */

		HashMap<String, XMLObject> temp = new HashMap<String, XMLObject>();

		XMLObject mXmlObject = null;

		XmlPullParserFactory mXmlPullParserFactory = XmlPullParserFactory.newInstance();

		mXmlPullParserFactory.setNamespaceAware(true);

		XmlPullParser xpp = mXmlPullParserFactory.newPullParser();

		xpp.setInput(new InputStreamReader(is));

		int eventType = xpp.getEventType();

		while(eventType != XmlPullParser.END_DOCUMENT) {

			if(eventType == XmlPullParser.START_DOCUMENT) {

				mStack = new Stack<String>();

			}else if(eventType == XmlPullParser.START_TAG) {

				key = xpp.getName();

				mXmlObject = new XMLObject();

				mXmlObject.setName(key);

				mStack.push(key);

				int attrCount = xpp.getAttributeCount();

				if(attrCount > 0) {

					HashMap<String, String> attribs = new HashMap<String, String>(attrCount);

					for(int i=0;i<attrCount;i++) {

						attribs.put(xpp.getAttributeName(i), xpp.getAttributeValue(i));

					}      mXmlObject.setParams(attribs);

				}

				/**

				 * Here we are temporary adding a current object in the map.

				 * we will get this object back when this tag will end.

				 */

				temp.put(key, mXmlObject);

			}else if(eventType == XmlPullParser.END_TAG) {

				key = xpp.getName();

				/**

				 * We are removing this current tab because it is now closed

				 */

				String current = mStack.pop();

				/**

				 * Size of the stack will be 0 when we current tag will be last tag of xml.

				 */

				if(mStack.size() == 0) {

					/**

					 * Here we are fetching the XMLObject which we had set when current tag was started.

					 * Here we are fetching the current object and updating the value in it.

					 */

					XMLObject mXmlObject2 = temp.get(key);

					mXmlObject2.setValue(value);

					temp.put(current, mXmlObject2);

					mXmlObject = mXmlObject2;          }else {

						String parent = mStack.get(mStack.size()-1);

						/**

						 * Here we are fetching the parent tag and updating the parent object to add the current child.

						 */

						if(temp.containsKey(key)) {

							XMLObject mXmlObject2 = temp.get(key);

							temp.remove(mXmlObject2);

							mXmlObject2.setValue(value);

							XMLObject parentObj = temp.get(parent);

							List<XMLObject> list = parentObj.getChilds();

							if(list == null) list = new ArrayList<XMLObject>();

							list.add(mXmlObject2);

							parentObj.setChilds(list);

							temp.put(parent, parentObj);

						}     }

				value = null;

			}else if(eventType == XmlPullParser.TEXT) {

				value = xpp.getText();

			}

			eventType = xpp.next();

		}

		return mXmlObject;

	}

}