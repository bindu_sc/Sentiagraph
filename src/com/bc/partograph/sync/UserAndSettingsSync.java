package com.bc.partograph.sync;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bc.partograph.common.AppContext;
import com.bc.partograph.common.Partograph_CommonClass;
import com.bc.partograph.common.Partograph_DB;
import com.bluecrimson.partograph.UserPojo;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.net.wifi.WifiManager;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;

public class UserAndSettingsSync {

	public static boolean syncUptoDate = false;

	public static String RegisteredMain;
	public static String ValidatedMain;
	public static String EmailId;
	public static String Password;
	public static String GroupId;
	public static String UserId;

	public static String userUpdateStr = "";
	public static String settingsUpdateStr = "";

	String displayText;

	Context Cx = null;

	SQLiteDatabase dbh;

	public static boolean blnUserisValid = false;
	public static ArrayList<String> strUserupdate = null;
	public static ArrayList<String> strSettingsupdate = null;
	public static String strSqlupdateuser, strSqlupdateSettings;

	// 14March2017 Arpitha
	public static boolean serValidUser = false, serTabLost = false, invalidDb = false, readyToDownload = false;
	public static String userIdReturned = "", returnFilename = "", returnedInfo = "";

	public UserAndSettingsSync(Context context, Partograph_DB dbh) {
		Cx = context;
		dbh = dbh;
	}

	public String getMacAddress() {
		WifiManager wimanager = (WifiManager) Cx.getSystemService(Context.WIFI_SERVICE);
		String macAddress = wimanager.getConnectionInfo().getMacAddress();
		if (macAddress == null) {
			macAddress = "";
		} else {
			macAddress = macAddress.replace(":", "");
			macAddress = macAddress.substring(macAddress.length() - 4);
		}

		return macAddress;
	}

	public void parseResponseXML(String data) throws Exception {

		InputStream is = new ByteArrayInputStream(data.getBytes("UTF-8"));

		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(is);
		doc.getDocumentElement().normalize();

		String struservalid = doc.getElementsByTagName("ValidatedMain").item(0).getTextContent();// 05April2017
																									// Arpitha
																									// validate
																									// -->validated
		if (struservalid.equalsIgnoreCase("1"))// 04April2017 Arpitha
			blnUserisValid = true;// 04April2017 Arpitha
		// else

		// blnUserisValid = Boolean.parseBoolean(struservalid);

		if (blnUserisValid || serTabLost) {
			NodeList nList = doc.getElementsByTagName("Table");

			strUserupdate = new ArrayList<String>();
			strSettingsupdate = new ArrayList<String>();

			if (nList.getLength() > 0) {

				for (int temp = 0; temp < nList.getLength(); temp++) {

					Node nNode = nList.item(temp);

					if (nNode.getNodeType() == Node.ELEMENT_NODE) {

						Element eElement = (Element) nNode;

						if (eElement.getElementsByTagName("state").item(0) != null) {

							strSqlupdateuser = " Update tbl_user Set ";

							strSqlupdateuser = strSqlupdateuser + " state = '" + getTagValue(eElement, "state") + "', ";
							strSqlupdateuser = strSqlupdateuser + " district = '" + getTagValue(eElement, "district")
									+ "', ";
							strSqlupdateuser = strSqlupdateuser + " taluk = '" + getTagValue(eElement, "taluk") + "', ";
							strSqlupdateuser = strSqlupdateuser + " facility = '" + getTagValue(eElement, "facility")
									+ "', ";
							strSqlupdateuser = strSqlupdateuser + " facility_name = '"
									+ getTagValue(eElement, "facility_name") + "', ";

							if (eElement.getElementsByTagName("modifieddate").item(0) != null) {
								strSqlupdateuser = strSqlupdateuser + " lastmodifieddate = '"
										+ getTagValue(eElement, "modifieddate") + "'";
							}

							// 21March2017 Arpitha
							if (eElement.getElementsByTagName("LastWomenNumber").item(0) != null) {
								strSqlupdateuser = strSqlupdateuser + ", " + " LastWomenNumber = "
										+ getTagValue(eElement, "LastWomenNumber");
							} // 21March2017 Arpitha

							// 21March2017 Arpitha
							if (eElement.getElementsByTagName("LastTransNumber").item(0) != null) {
								strSqlupdateuser = strSqlupdateuser + ", " + " LastTransNumber = "
										+ getTagValue(eElement, "LastTransNumber");
							} // 21March2017 Arpitha

							// 21March2017 Arpitha
							if (eElement.getElementsByTagName("LastRequestNumber").item(0) != null) {
								strSqlupdateuser = strSqlupdateuser + ", " + " LastRequestNumber = "
										+ getTagValue(eElement, "LastRequestNumber") + " ";
							} // 21March2017 Arpitha

							// 21March2017 Arpitha
							if (eElement.getElementsByTagName("user_Id").item(0) != null) {
								strSqlupdateuser = strSqlupdateuser + ", " + " user_Id = '"
										+ getTagValue(eElement, "user_Id") + "'";
							} // 21March2017 Arpitha

							strSqlupdateuser = strSqlupdateuser + " Where emailid = '"
									+ getTagValue(eElement, "emailid") + "' ";
							strSqlupdateuser = strSqlupdateuser + "; ";

							strUserupdate.add(strSqlupdateuser);

						} else {

							strSqlupdateSettings = " Update tbl_settings Set ";
							if (eElement.getElementsByTagName("ipAddress").item(0) != null) {
								strSqlupdateSettings = strSqlupdateSettings + " ipAddress = '"
										+ getTagValue(eElement, "ipAddress") + "', ";
								strSqlupdateSettings = strSqlupdateSettings + " serverdb = '"
										+ getTagValue(eElement, "serverdb") + "', ";
							}

							if (eElement.getElementsByTagName("lastmodifieddate").item(0) != null) {
								strSqlupdateSettings = strSqlupdateSettings + " lastmodifieddate = '"
										+ getTagValue(eElement, "lastmodifieddate") + "' ";
							}

							// 25oct2016 Arpitha
							if (eElement.getElementsByTagName("excludePartoParams").item(0) != null) {
								strSqlupdateSettings = strSqlupdateSettings + ", " + " customfield1 = '"
										+ getTagValue(eElement, "excludePartoParams") + "' ";
							} // 25oct2016 Arpitha

							if (eElement.getElementsByTagName("customfield2").item(0) != null) {
								strSqlupdateSettings = strSqlupdateSettings + ", " + " customfield2 = '"
										+ getTagValue(eElement, "customfield2") + "' ";
							} // 25oct2016 Arpitha

							// 21March2017 Arpitha
							if (eElement.getElementsByTagName("user_Id").item(0) != null) {
								strSqlupdateSettings = strSqlupdateSettings + ", " + " user_Id = '"
										+ getTagValue(eElement, "user_Id") + "' ";
							} // 21March2017 Arpitha

							// 11Sep2017 Arpitha
							if (eElement.getElementsByTagName("evalEndDate").item(0) != null) {
								byte[] evalDate = getTagValue(eElement, "evalEndDate").getBytes("UTF-8");
								String SendMessage = Base64.encodeToString(evalDate, Base64.DEFAULT);
								strSqlupdateSettings = strSqlupdateSettings + ", " + " evalEndDate = '" + SendMessage
										+ "' ";
							} // 11Sep2017 Arpitha

							strSqlupdateSettings = strSqlupdateSettings + " Where emailid = '"
									+ getTagValue(eElement, "emailid") + "' ";
							strSqlupdateSettings = strSqlupdateSettings + "; ";

							strSettingsupdate.add(strSqlupdateSettings);
							/*
							 * for (int temp = 0; temp < nList.getLength();
							 * temp++) {
							 * 
							 * Node nNode = nList.item(temp);
							 * 
							 * if (nNode.getNodeType() == Node.ELEMENT_NODE) {
							 * 
							 * Element eElement = (Element) nNode;
							 * 
							 * if
							 * (eElement.getElementsByTagName("state").item(0)
							 * != null) {
							 * 
							 * strSqlupdateuser = " Update tbl_user Set ";
							 * 
							 * strSqlupdateuser = strSqlupdateuser +
							 * " state = '" + getTagValue(eElement, "state") +
							 * "', "; strSqlupdateuser = strSqlupdateuser +
							 * " district = '" + getTagValue(eElement,
							 * "district") + "', "; strSqlupdateuser =
							 * strSqlupdateuser + " taluk = '" +
							 * getTagValue(eElement, "taluk") + "', ";
							 * strSqlupdateuser = strSqlupdateuser +
							 * " facility = '" + getTagValue(eElement,
							 * "facility") + "', "; strSqlupdateuser =
							 * strSqlupdateuser + " facility_name = '" +
							 * getTagValue(eElement, "facility_name") + "', ";
							 * 
							 * 
							 * 
							 * strSqlupdateuser = strSqlupdateuser +
							 * " user_id = '" + getTagValue(eElement, "user_Id")
							 * + "', ";//05April2017 Arpitha
							 * 
							 * 
							 * if
							 * (eElement.getElementsByTagName("modifieddate").
							 * item(0) != null) { strSqlupdateuser =
							 * strSqlupdateuser + " lastmodifieddate = '" +
							 * getTagValue(eElement, "modifieddate") + "' "; }
							 * 
							 * strSqlupdateuser = strSqlupdateuser +
							 * " Where emailid = '" + getTagValue(eElement,
							 * "emailid") + "' "; strSqlupdateuser =
							 * strSqlupdateuser + "; ";
							 * 
							 * strUserupdate.add(strSqlupdateuser);
							 * 
							 * } else {
							 * 
							 * strSqlupdateSettings =
							 * " Update tbl_settings Set "; if
							 * (eElement.getElementsByTagName("ipAddress").item(
							 * 0) != null) { strSqlupdateSettings =
							 * strSqlupdateSettings + " ipAddress = '" +
							 * getTagValue(eElement, "ipAddress") + "', ";
							 * strSqlupdateSettings = strSqlupdateSettings +
							 * " serverdb = '" + getTagValue(eElement,
							 * "serverdb") + "', "; }
							 * 
							 * if
							 * (eElement.getElementsByTagName("lastmodifieddate"
							 * ).item(0) != null) { strSqlupdateSettings =
							 * strSqlupdateSettings + " lastmodifieddate = '" +
							 * getTagValue(eElement, "lastmodifieddate") + "' ";
							 * }
							 * 
							 * 
							 * if
							 * (eElement.getElementsByTagName("user_Id").item(0)
							 * != null) {//05April2017 arpitha
							 * strSqlupdateSettings = strSqlupdateSettings +", "
							 * + " user_id = '" + getTagValue(eElement,
							 * "user_Id") + "' "; }//05April2017 arpitha
							 * 
							 * 
							 * // 25oct2016 Arpitha if
							 * (eElement.getElementsByTagName("customfield1").
							 * item(0) != null) { strSqlupdateSettings =
							 * strSqlupdateSettings + ", " + " customfield1 = '"
							 * + getTagValue(eElement, "customfield1") + "' "; }
							 * // 25oct2016 Arpitha
							 * 
							 * if
							 * (eElement.getElementsByTagName("customfield2").
							 * item(0) != null) { strSqlupdateSettings =
							 * strSqlupdateSettings + ", " + " customfield2 = '"
							 * + getTagValue(eElement, "customfield2") + "' "; }
							 * // 25oct2016 Arpitha
							 * 
							 * strSqlupdateSettings = strSqlupdateSettings +
							 * " Where emailid = '" + getTagValue(eElement,
							 * "emailid") + "' "; strSqlupdateSettings =
							 * strSqlupdateSettings + "; ";
							 * 
							 * strSettingsupdate.add(strSqlupdateSettings);
							 */
						}

					}
				}
			}
		}

		/*
		 * 
		 * InputStream is = new ByteArrayInputStream(xmlStr.getBytes("UTF-8"));
		 * 
		 * DocumentBuilderFactory dbFactory =
		 * DocumentBuilderFactory.newInstance(); DocumentBuilder dBuilder =
		 * dbFactory.newDocumentBuilder(); Document doc = dBuilder.parse(is);
		 * doc.getDocumentElement().normalize();
		 * 
		 * NodeList nList = doc.getElementsByTagName("Table");
		 * 
		 * if(nList.getLength() > 0){
		 * 
		 * for (int temp = 0; temp < nList.getLength(); temp++) {
		 * 
		 * Node nNode = nList.item(temp);
		 * 
		 * if (nNode.getNodeType() == Node.ELEMENT_NODE) {
		 * 
		 * Element eElement = (Element) nNode;
		 * 
		 * if(eElement.getElementsByTagName("Validated").item(0) != null){
		 * 
		 * String stemp = " Update tblUsers Set ";
		 * 
		 * stemp = stemp + " GroupId = '" + getTagValue(eElement, "GroupId") +
		 * "', "; stemp = stemp + " UserId = '" + getTagValue(eElement,
		 * "UserId") + "', "; // stemp = stemp + " Password = '" +
		 * getTagValue(eElement, "Password") + "', "; // stemp = stemp +
		 * " FirstTimeLogin = '" + getTagValue(eElement, "FirstTimeLogin") +
		 * "', "; stemp = stemp + " DeviceID = '" + getTagValue(eElement,
		 * "DeviceID") + "', "; stemp = stemp + " PhoneNumber = '" +
		 * getTagValue(eElement, "PhoneNumber") + "', "; stemp = stemp +
		 * " username = '" + getTagValue(eElement, "username") + "', "; stemp =
		 * stemp + " userImage = '" + getTagValue(eElement, "userImage") + "', "
		 * ; // stemp = stemp + " LastWomenNumber = '" + getTagValue(eElement,
		 * "LastWomenNumber") + "', "; // stemp = stemp + " LastTransNumber = '"
		 * + getTagValue(eElement, "LastTransNumber") + "', "; // stemp = stemp
		 * + " LastRequestNumber = '" + getTagValue(eElement,
		 * "LastRequestNumber") + "', "; stemp = stemp + " RunningId = '" +
		 * getTagValue(eElement, "RunningId") + "', "; // stemp = stemp +
		 * " LastDangerNumber = '" + getTagValue(eElement, "LastDangerNumber") +
		 * "', "; stemp = stemp + " Validated = '" + getTagValue(eElement,
		 * "Validated") + "', "; stemp = stemp + " Verified = '" +
		 * getTagValue(eElement, "Verified") + "', "; stemp = stemp +
		 * " TrialVersion = '" + getTagValue(eElement, "TrialVersion") + "', ";
		 * stemp = stemp + " TrialPeriod = '" + getTagValue(eElement,
		 * "TrialPeriod") + "', "; stemp = stemp + " LicenseKey = '" +
		 * getTagValue(eElement, "LicenseKey") + "', "; stemp = stemp +
		 * " LastUserModifiedDate = '" + getTagValue(eElement,
		 * "LastUserModifiedDate") + "' ";
		 * 
		 * stemp = stemp + " Where EmailId = '" + getTagValue(eElement,
		 * "EmailId") + "' "; stemp = stemp + "; ";
		 * 
		 * userUpdateStr = stemp;
		 * 
		 * }
		 * 
		 * if(eElement.getElementsByTagName("ipAddress").item(0) != null){
		 * 
		 * String stemp = " Update tblSettings Set ";
		 * 
		 * stemp = stemp + " ipAddress = '" + getTagValue(eElement, "ipAddress")
		 * + "', "; stemp = stemp + " AudioOnOff = '" + getTagValue(eElement,
		 * "AudioOnOff") + "', "; stemp = stemp + " ApkVersion = '" +
		 * getTagValue(eElement, "ApkVersion") + "', "; stemp = stemp +
		 * " DbVersion = '" + getTagValue(eElement, "DbVersion") + "', "; stemp
		 * = stemp + " CNA = '" + getTagValue(eElement, "CNA") + "', "; stemp =
		 * stemp + " MOName = '" + getTagValue(eElement, "MOName") + "', ";
		 * stemp = stemp + " MOAddress = '" + getTagValue(eElement, "MOAddress")
		 * + "', "; stemp = stemp + " MOAddress2 = '" + getTagValue(eElement,
		 * "MOAddress2") + "', "; stemp = stemp + " MOPhone = '" +
		 * getTagValue(eElement, "MOPhone") + "', "; stemp = stemp +
		 * " MOHotLine = '" + getTagValue(eElement, "MOHotLine") + "', "; stemp
		 * = stemp + " Targetfrom = '" + getTagValue(eElement, "Targetfrom") +
		 * "', "; stemp = stemp + " PHCPhoneNo = '" + getTagValue(eElement,
		 * "PHCPhoneNo") + "', "; stemp = stemp + " FRUPhoneNo = '" +
		 * getTagValue(eElement, "FRUPhoneNo") + "', "; stemp = stemp +
		 * " EmergencyNo = '" + getTagValue(eElement, "EmergencyNo") + "', ";
		 * stemp = stemp + " DsSyncIntervalInMin = '" + getTagValue(eElement,
		 * "DsSyncIntervalInMin") + "', "; stemp = stemp + " ServerDb = '" +
		 * getTagValue(eElement, "ServerDb") + "', "; stemp = stemp +
		 * " IsSpanText = '" + getTagValue(eElement, "IsSpanText") + "', ";
		 * 
		 * 
		 * if(eElement.getElementsByTagName("LastModifiedDate").item(0) !=
		 * null){ stemp = stemp + " TargetTo = '" + getTagValue(eElement,
		 * "Targetto") + "', "; stemp = stemp + " LastSettingsModifiedDate = '"
		 * + getTagValue(eElement, "LastModifiedDate") + "' "; }else{ stemp =
		 * stemp + " TargetTo = '" + getTagValue(eElement, "Targetto") + "' "; }
		 * 
		 * stemp = stemp + " Where EmailId = '" + getTagValue(eElement,
		 * "EmailId") + "' "; stemp = stemp + "; ";
		 * 
		 * settingsUpdateStr = stemp;
		 * 
		 * }
		 * 
		 * } } }
		 */}

	public String getTagValue(Element eElement, String tagName) {
		if (eElement.getElementsByTagName(tagName).item(0) != null)
			return eElement.getElementsByTagName(tagName).item(0).getTextContent();
		else
			return "";
	}

	public void parseAndCheckResult(XMLObject mObject) throws Exception {

		if (mObject != null) {

			List<XMLObject> mXmlObjects = mObject.getChilds();

			if (mXmlObjects != null && mXmlObjects.size() > 0) {
				for (XMLObject xmlObject : mXmlObjects) {

					int varSize = 0;

					if (xmlObject.getChilds() != null)
						varSize = xmlObject.getChilds().size();

					if (xmlObject != null && varSize > 0) {
						parseAndCheckResult(xmlObject);
					}

					if ((xmlObject.getName() != null) && (xmlObject.getValue() != null)) {
						Log.d("KEY:" + xmlObject.getName() + " :: Value=", xmlObject.getValue());

						String xKey = xmlObject.getName();
						String xValue = xmlObject.getValue();

						if (xKey.equals("RegisteredMain")) {
							RegisteredMain = xValue;
						} else if (xKey.equals("ValidatedMain")) {
							ValidatedMain = xValue;
						} else if (xKey.equals("EmailId")) {
							EmailId = xValue;
						} else if (xKey.equals("Password")) {
							Password = xValue;
						} else if (xKey.equals("GroupId")) {
							GroupId = xValue;
						} else if (xKey.equals("UserId")) {
							UserId = xValue;
						}

						// 14March2017 Arpitha
						else if (xKey.equals("isvalidate")) {
							serValidUser = Boolean.parseBoolean(xValue);
						} /*
							 * else if(xKey.equals("UserIdreturned")) {
							 * userIdReturned = xValue.trim(); }
							 */else if (xKey.equals("Losttab")) {
							serTabLost = Boolean.parseBoolean(xValue);
						} else if (xKey.equals("InvalidDb")) {
							invalidDb = Boolean.parseBoolean(xValue);
						} else if (xKey.equals("ReadyToDownload")) {
							readyToDownload = Boolean.parseBoolean(xValue);
						} else if (xKey.equals("ReturnFileTag")) {
							returnFilename = xValue.trim();
						} else if (xKey.equals("InfoTag")) {
							returnedInfo = xValue.trim();
						}
					}
				}
			}
		}
	}

	public String userAndSettingsResponseXML(UserPojo user, boolean fromLogin) throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		String returnValue = null;

		try {
			PrepareUserAndSettingSyncXML xExportXML = new PrepareUserAndSettingSyncXML(dbh, Cx);
			returnValue = xExportXML.WriteResponseXML(user, fromLogin);
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			throw e;
			// e.printStackTrace();
		}
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		return returnValue;
	}

	/**
	 * Get All current data from Server in xml format Call
	 * updateResponseOfServer()
	 * 
	 * @param fromLogin
	 * @param user
	 */
	public String GetAllCurrentServerData(String urlAddress, UserPojo user, boolean fromLogin) throws Exception {

		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		try {
			String serverIp = Partograph_CommonClass.properties.getProperty("ipAddress");
			String sendXml = userAndSettingsResponseXML(user, fromLogin);
			writeTxtFile(sendXml, "ReqFile");
			displayText = WebService.invokeHelloWorldWS(sendXml, "GetAllCurrentData", serverIp);
			System.out.println("GetAllCurrentData  " + displayText);
			updateResponseOfServer(fromLogin);
		} catch (Exception e) {
			syncUptoDate = false;
			Partograph_CommonClass.movedToInputFolder = false;
			e.printStackTrace();
			throw e;
		}

		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		return displayText;
	}

	/**
	 * This method append the starting XML syntax to the Server data And Calls
	 * updateSettingsAfterAsyncCall()
	 * 
	 * @param fromLogin
	 */
	private void updateResponseOfServer(boolean fromLogin) throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		if (displayText == "Error") {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
			syncUptoDate = false;
			Partograph_CommonClass.movedToInputFolder = false;
			Partograph_CommonClass.responseAckCount++;
		} else {
			displayText = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" + displayText;
			userAndSettingsUpdateResponse(displayText, fromLogin);
			syncUptoDate = true;
			Partograph_CommonClass.responseAckCount = 0;
			Partograph_CommonClass.movedToInputFolder = true;
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
		}
	}

	/**
	 * This method calls displayDsObject() Prepares the Settings update sql from
	 * Server data and executes
	 * 
	 * @param fromLogin
	 */
	public void userAndSettingsUpdateResponse(String dText, boolean fromLogin) throws Exception {
		System.out.println("Server response XML : \n" + displayText);
		writeTxtFile(displayText, "RespFile");

		InputStream is = new ByteArrayInputStream(displayText.getBytes("UTF-8"));
		XMLObject mObject = XmlParser.parseXml(is);
		parseAndCheckResult(mObject);

		if ((ValidatedMain!=null && ValidatedMain.equalsIgnoreCase("1")) || serTabLost) {
			parseResponseXML(dText);
		}
	}

	public void writeTxtFile(String sg, String filename) throws Exception {
		InputStream inputStream = new ByteArrayInputStream(sg.getBytes());
		@SuppressWarnings("unused")
		String state = Environment.getExternalStorageState();

		// if (Environment.MEDIA_MOUNTED.equals(state))
		// {
		// SDcard is available
		File f = new File(AppContext.mainDir + "/" + filename + ".xml");
		try {
			f.createNewFile();
			OutputStream out = new FileOutputStream(f);
			byte buf[] = new byte[1024];
			int len;
			while ((len = inputStream.read(buf)) > 0)
				out.write(buf, 0, len);
			out.close();
			inputStream.close();
			System.out.println("\nFile is created...................................");

		} catch (IOException e) {
			e.printStackTrace();
		}
		// }
	}

	// public void writeData ( String data ) {
	// try {
	// FileOutputStream fOut = Cx.openFileOutput("abc.xml",
	// MODE_WORLD_READABLE);
	// OutputStreamWriter osw = new OutputStreamWriter ( fOut ) ;
	// osw.write ( data ) ;
	// osw.flush ( ) ;
	// osw.close ( ) ;
	// } catch ( Exception e ) {
	// e.printStackTrace ( ) ;
	// }
	// }

}