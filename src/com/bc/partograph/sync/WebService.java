package com.bc.partograph.sync;

import java.text.DateFormat;
import java.util.Date;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import com.bc.partograph.common.AppContext;
import com.bc.partograph.common.KillAllActivitesAndGoToLogin;
import com.bc.partograph.common.Partograph_CommonClass;
import com.bc.partograph.common.Partograph_DB;

import android.content.Context;

public class WebService {
	// Namespace of the Webservice - It is http://tempuri.org for .NET
	// webservice
	private static String NAMESPACE = "http://tempuri.org/";
	// Webservice URL - It is asmx file location hosted in the server in case of
	// .Net
	// Change the IP address to your machine IP address
	// private static String URL =
	// "http://192.168.1.13/iASHAWebSrvcWOValidation/iAshaWebService.asmx";

	// private static String URL =
	// "http://192.168.1.14/iAshaWebService/iAshaWebService.asmx";
	// private static String URL =
	// "http://192.168.1.9:85/WebService2/Service1.asmx";
	// SOAP Action URI again http://tempuri.org
	private static String SOAP_ACTION = "http://tempuri.org/MessageForwarder";
	private static String SOAP_ACTION2 = "http://tempuri.org/GetDataFromServer";
	private static String SOAP_ACTION3 = "http://tempuri.org/ValidUserForDownLoad";
	private static String SOAP_ACTION4 = "http://tempuri.org/GetCommentsData";
	private static String SOAP_ACTION5 = "http://tempuri.org/GetUserData";
	private static String SOAP_ACTION6 = "http://tempuri.org/GetSettingsData";
	private static String SOAP_ACTION7 = "http://tempuri.org/validUserandGetDataForUser";
	private static String SOAP_ACTION8 = "http://tempuri.org/GetUserandSettingdetails";

	private static String SOAP_ACTION9 = "http://tempuri.org/ChangePassword";

	private static String SOAP_ACTION10 = "http://tempuri.org/updateUserDetails";// 03April2017
	// Arpitha

	private static String SOAP_ACTION11 = "http://tempuri.org/GetAllCurrentData";// 03April2017
	// Arpitha

	private static String SOAP_ACTION12 = "http://tempuri.org/GetColorCodedValues";// 03May2017
																					// Arpitha
																					// -
																					// color
																					// coded
																					// values

	// updated on 13Nov2015
	static Partograph_DB dbh = null;

	// updated 26Aug2016
	static String strwebService = "/sentiagraphWebS/";

	public static String invokeHelloWorldWS(String xMessage, String webMethName, String URL) {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + WebService.class.getSimpleName());

		// updated bindu - 26Aug2016
		URL = "http://" + URL + strwebService + "partographWebService.asmx";

		String resTxt = null;
		// Create request
		SoapObject request = new SoapObject(NAMESPACE, webMethName);
		// Property which holds input parameters
		PropertyInfo sayHelloPI = new PropertyInfo();
		// Set Name
		sayHelloPI.setName("xml");
		// Set Value
		sayHelloPI.setValue(xMessage);
		// Set dataType
		sayHelloPI.setType(String.class);
		// Add the property to request object
		request.addProperty(sayHelloPI);

		resTxt = invokeServiceCall(request, webMethName, URL);

		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + WebService.class.getSimpleName());

		return resTxt;
	}

	// 03April2017 Arpitha - included three int arguments
	public static String invokeHelloWorldWS(String userid, String pswd, String webMethName, String URL, Context context,
			String lastadddate, int lastWomanNumber, int lastTransNumber, int lastRequestNumber) {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + WebService.class.getSimpleName());

		// URL = "http://" + URL + "/partographWebS/partographWebService.asmx";
		// updated bindu - 26Aug2016
		URL = "http://" + URL + strwebService + "partographWebService.asmx";

		String resTxt = null;
		// Create request
		SoapObject request = new SoapObject(NAMESPACE, webMethName);
		// Property which holds input parameters
		PropertyInfo useridProp = new PropertyInfo();
		// Set Name
		useridProp.setName("UserId");
		// Set Value
		useridProp.setValue(userid);
		// Set dataType
		useridProp.setType(String.class);
		// Add the property to request object
		request.addProperty(useridProp);

		PropertyInfo pswdProp = new PropertyInfo();
		// Set Name
		pswdProp.setName("Pswd");
		// Set Value
		pswdProp.setValue(pswd);
		// Set dataType
		pswdProp.setType(String.class);
		// Add the property to request object
		request.addProperty(pswdProp);

		PropertyInfo dbnameProp = new PropertyInfo();
		// Set Name
		dbnameProp.setName("dbname");
		// Set Value
		dbnameProp.setValue(Partograph_CommonClass.properties.getProperty("serverdbmaster"));
		// Set dataType
		dbnameProp.setType(String.class);
		// Add the property to request object
		request.addProperty(dbnameProp);

		if (webMethName.equalsIgnoreCase("updateUserDetails"))// 03April2017
																// Arpitha
		{

			PropertyInfo lastWomanNumberProp = new PropertyInfo();
			// Set Name
			lastWomanNumberProp.setName("LastWomenNumber");
			// Set Value
			lastWomanNumberProp.setValue(lastWomanNumber);
			// Set dataType
			lastWomanNumberProp.setType(String.class);
			// Add the property to request object
			request.addProperty(lastWomanNumberProp);

			PropertyInfo lastTransNumberProp = new PropertyInfo();
			// Set Name
			lastTransNumberProp.setName("LastTransNumber");
			// Set Value
			lastTransNumberProp.setValue(lastTransNumber);
			// Set dataType
			lastTransNumberProp.setType(String.class);
			// Add the property to request object
			request.addProperty(lastTransNumberProp);

			PropertyInfo lastRequestNumberProp = new PropertyInfo();
			// Set Name
			lastRequestNumberProp.setName("LastRequestNumber");
			// Set Value
			lastRequestNumberProp.setValue(lastRequestNumber);
			// Set dataType
			lastRequestNumberProp.setType(String.class);
			// Add the property to request object
			request.addProperty(lastRequestNumberProp);

		} else// 03April2017 Arpitha
		{
			PropertyInfo lastadddateProp = new PropertyInfo();
			// Set Name
			lastadddateProp.setName("lastadddate");
			// Set Value
			lastadddateProp.setValue("");
			// Set dataType
			lastadddateProp.setType(String.class);
			// Add the property to request object
			request.addProperty(lastadddateProp);
		}

		resTxt = invokeServiceCall(request, webMethName, URL);

		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + WebService.class.getSimpleName());

		return resTxt;
	}

	private static String invokeServiceCall(SoapObject request, String webMethName, String URL) {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + WebService.class.getSimpleName());

		String resTxt = null;
		// Create envelope
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		// Set envelope as dotNet
		envelope.dotNet = true;
		// Set output SOAP object
		envelope.setOutputSoapObject(request);
		// Create HTTP call object
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL, 30000);

		try {

			System.out.println("Sent Time : " + DateFormat.getDateTimeInstance().format(new Date()));

			// Invoke web service
			// androidHttpTransport.call(SOAP_ACTION, envelope);
			if (webMethName.equals("MessageForwarder")) {
				// changed on 20july2016 by Arpitha
				// androidHttpTransport.call(SOAP_ACTION, envelope);
				dbh = Partograph_DB.getInstance(KillAllActivitesAndGoToLogin.context);
				dbh.UpdateTransStatus(Partograph_CommonClass.requestId, "S");
				androidHttpTransport.call(SOAP_ACTION, envelope);
			}

			if (webMethName.equals("GetDataFromServer"))
				androidHttpTransport.call(SOAP_ACTION2, envelope);

			if (webMethName.equals("ValidUserForDownLoad")) {
				androidHttpTransport.call(SOAP_ACTION3, envelope);
			}

			if (webMethName.equals("GetCommentsData")) {
				androidHttpTransport.call(SOAP_ACTION4, envelope);
			}

			if (webMethName.equals("GetUserData")) {
				androidHttpTransport.call(SOAP_ACTION5, envelope);
			}

			if (webMethName.equals("GetSettingsData")) {
				androidHttpTransport.call(SOAP_ACTION6, envelope);
			}

			if (webMethName.equals("validUserandGetDataForUser")) {
				androidHttpTransport.call(SOAP_ACTION7, envelope);
			}

			if (webMethName.equals("GetUserandSettingdetails")) {
				androidHttpTransport.call(SOAP_ACTION8, envelope);
			}

			// 10Jan2017 Arpitha

			if (webMethName.equals("ChangePassword")) {
				androidHttpTransport.call(SOAP_ACTION9, envelope);
			}

			// 03April2017 Arpitha
			if (webMethName.equals("updateUserDetails")) {
				androidHttpTransport.call(SOAP_ACTION10, envelope);
			}

			// 03April2017 Arpitha
			if (webMethName.equals("GetAllCurrentData")) {
				androidHttpTransport.call(SOAP_ACTION11, envelope);
			}

			// 03May2017 Arpitha
			if (webMethName.equals("GetColorCodedValues")) {
				androidHttpTransport.call(SOAP_ACTION12, envelope);
			} // 03May2017 Arpitha

			System.out.println("OUT Time : " + DateFormat.getDateTimeInstance().format(new Date()));

			// Get the response
			SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
			// Assign it to resTxt variable static variable
			resTxt = response.toString();

			System.out.println("RESPONSE Time : " + DateFormat.getDateTimeInstance().format(new Date()));

		} catch (Exception e) {
			AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ WebService.class.getSimpleName(), e);
			e.printStackTrace();
			// Assign error message to resTxt
			resTxt = "Error";
		}
		System.out.println("Return Time : " + DateFormat.getDateTimeInstance().format(new Date()));

		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + WebService.class.getSimpleName());
		// Return resTxt to calling object

		return resTxt;
	}

	public static String invokeHelloWorldWS(String user_Id, String dbName, String webMethName, String lastModifieddate,
			String URL, Context context) {
		// URL = "http://" + URL + "/partographWebS/partographWebService.asmx";
		// updated bindu - 26Aug2016
		URL = "http://" + URL + strwebService + "partographWebService.asmx";

		String resTxt = null;
		// Create request
		SoapObject request = new SoapObject(NAMESPACE, webMethName);
		// 03May2017 Arpitha
		if (!(webMethName.equals("GetColorCodedValues"))) {

			// Property which holds input parameters
			PropertyInfo useridProp = new PropertyInfo();
			// Set Name
			useridProp.setName("UserId");
			// Set Value
			useridProp.setValue(user_Id);
			// Set dataType
			useridProp.setType(String.class);
			// Add the property to request object
			request.addProperty(useridProp);

		}

		PropertyInfo dbnameProp = new PropertyInfo();
		// Set Name
		dbnameProp.setName("dbname");
		// Set Value
		// dbnameProp.setValue(Partograph_CommonClass.properties.getProperty("serverdbmaster"));
		dbnameProp.setValue(dbName);
		// Set dataType
		dbnameProp.setType(String.class);
		// Add the property to request object
		request.addProperty(dbnameProp);

		// Set Name
		if (webMethName.equalsIgnoreCase("ChangePassword"))// 10Jan2017 Arpitha
		{

			PropertyInfo pswdProp = new PropertyInfo();
			// Set Name
			pswdProp.setName("password");
			// Set Value
			pswdProp.setValue(lastModifieddate);
			// Set dataType
			pswdProp.setType(String.class);
			// Add the property to request object
			request.addProperty(pswdProp);

		} else {// 10Jan2017 Arpitha

			PropertyInfo lastadddateProp = new PropertyInfo();

			lastadddateProp.setName("lastadddate");
			// Set Value
			lastadddateProp.setValue(lastModifieddate != null ? lastModifieddate : "");
			// Set dataType
			lastadddateProp.setType(String.class);
			// Add the property to request object
			request.addProperty(lastadddateProp);
		}

		resTxt = invokeServiceCall(request, webMethName, URL);

		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + WebService.class.getSimpleName());

		return resTxt;
	}

	public static String invokeHelloWorldWSuser(String user_Id, String dbName, String webMethName, String URL,
			Context context) {
		// URL = "http://" + URL + "/partographWebS/partographWebService.asmx";
		// updated bindu - 26Aug2016
		URL = "http://" + URL + strwebService + "partographWebService.asmx";

		String resTxt = null;
		// Create request
		SoapObject request = new SoapObject(NAMESPACE, webMethName);
		// Property which holds input parameters
		PropertyInfo useridProp = new PropertyInfo();
		// Set Name
		useridProp.setName("user_Id");
		// Set Value
		useridProp.setValue(user_Id);
		// Set dataType
		useridProp.setType(String.class);
		// Add the property to request object
		request.addProperty(useridProp);

		PropertyInfo dbnameProp = new PropertyInfo();
		// Set Name
		dbnameProp.setName("dbname");
		// Set Value
		dbnameProp.setValue(Partograph_CommonClass.properties.getProperty("serverdbmaster"));
		// Set dataType
		dbnameProp.setType(String.class);
		// Add the property to request object
		request.addProperty(dbnameProp);

		resTxt = invokeServiceCall(request, webMethName, URL);

		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + WebService.class.getSimpleName());

		return resTxt;
	}
}
