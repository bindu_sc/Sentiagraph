package com.bc.partograph.settings;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.androidquery.AQuery;
import com.bc.partograph.comments.Comments_Activity;
import com.bc.partograph.common.AppContext;
import com.bc.partograph.common.KillAllActivitesAndGoToLogin;
import com.bc.partograph.common.Partograph_CommonClass;
import com.bc.partograph.common.Partograph_DB;
import com.bc.partograph.login.LoginActivity;
import com.bc.partograph.sliding.GraphInformation;
import com.bc.partograph.womenview.Activity_WomenView;
import com.bluecrimson.additionalDetails.Summary_Activity;
import com.bluecrimson.dischargedetails.DischargedWomanList_Activity;
import com.bluecrimson.partograph.PhoneNumber_Pojo;
import com.bluecrimson.partograph.R;
import com.bluecrimson.partograph.Settings_Adapter;
import com.bluecrimson.partograph.UserPojo;
import com.bluecrimson.usermanual.UseManual;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources.NotFoundException;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.telephony.TelephonyManager;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AbsListView.MultiChoiceModeListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class Settings_parto extends Activity {

	AQuery aq;
	Partograph_DB dbh;
	Locale locale = null;
	String dateFormat;
	String language;
	PhoneNumber_Pojo ppojo;

	CheckBox chkreg, chkref1, chkref2, chkref3;
	int regsms = 0;
	int refno = 0;
	int option;
	boolean isadded;

	// TableLayout tblphno;
	ArrayList<PhoneNumber_Pojo> val;

	TextView txtcommentsval;
	ArrayList<String> phn_nos;
	ListView listcommentsfhr;
	String strphn_delete;
	ArrayList<String> strphn_del;
	int option_delete;
	ArrayList<Integer> option_del;
	UserPojo user;

	// 24Aug2016 - bindu
	Settings_Adapter adapter;

	SharedPreferences sharedpreferences;// 04Nov2016
	public static final String MyPREFERENCES = "Prefs";// 04Nov2016

	TextView txtlang, txtdateformat;// 17Nov2016 Arpitha

	String notificationOnOFF;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings);

		try {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
			aq = new AQuery(this);
			dbh = Partograph_DB.getInstance(getApplicationContext());
			initializeScreen(aq.id(R.id.rlsettings).getView());

			// 05Nov2016 Arpitha
			Locale locale = null;

			if (AppContext.prefs.getBoolean("isEnglish", false)) {
				locale = new Locale("en");
				Locale.setDefault(locale);
			} else if (AppContext.prefs.getBoolean("isHindi", false)) {
				locale = new Locale("hi");
				Locale.setDefault(locale);
			}

			// String notificationOn =
			// Partograph_CommonClass.prefs.getString("notification", null);

			Configuration config = new Configuration();
			config.locale = locale;
			getResources().updateConfiguration(config, getResources().getDisplayMetrics());

			AppContext.setNamesAccordingToRes(getResources());// 05Nov2016
																// Arpitha

			ArrayList<String> list = new ArrayList(Arrays.asList(getResources().getStringArray(R.array.langarray))); // array
																														// id
			listcommentsfhr = (ListView) findViewById(R.id.listCommentsfhr); // of

			txtlang = (TextView) findViewById(R.id.txtlang);// 17Nov2016 Arpitha
			txtdateformat = (TextView) findViewById(R.id.txtdateformat);// 17Nov2016
																		// Arpitha
			// string
			// resource
			int position = list.indexOf(Partograph_CommonClass.defLanguage);
			aq.id(R.id.spnsellang).setSelection(position);

			ArrayList<String> datelist = new ArrayList(
					Arrays.asList(getResources().getStringArray(R.array.dateformat))); // array
																						// id
																						// of
			user = dbh.getUserProfile();
			Partograph_CommonClass.user = user; // string
			// resource
			int pos = datelist.indexOf(Partograph_CommonClass.defdateformat);
			aq.id(R.id.spndateformat).setSelection(pos);

			sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_WORLD_READABLE);// 04nov2016
																									// Arpitha
			// String s = sharedpreferences.getString(Name, null);

			intialview();
			phn_nos = new ArrayList<String>();
			if (val != null && val.size() > 0) {
				for (int i = 0; i < val.size(); i++) {
					String phn_no = val.get(i).getPhn_no();
					phn_nos.add(phn_no);
				}
			}

			notificationOnOFF = Partograph_CommonClass.prefs.getString("notification", null);
			if ((notificationOnOFF != null && notificationOnOFF.trim().length() > 0)
					&& (notificationOnOFF.equalsIgnoreCase("No"))) {

				aq.id(R.id.chknotification).checked(true);

			}

			// aq.id(R.id.etmailid).getEditText().addTextChangedListener(watcher);//
			// 10Jan2017
			// Arpitha

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}

		// KillAllActivitesAndGoToLogin.activity_stack.add(this);
		 KillAllActivitesAndGoToLogin.addToStack(this);
	}

	/**
	 * This is a Recursive method that traverse through the group and subgroup
	 * of view Sets text, assigns clickListners
	 */
	private ArrayList<View> initializeScreen(View v) {
		if (!(v instanceof ViewGroup)) {
			ArrayList<View> viewArrayList = new ArrayList<View>();
			viewArrayList.add(v);

			// 17Nov2016 Arpitha
			// TextView
			if (v.getClass() == TextView.class) {
				aq.id(v.getId()).getText();
			}

			// Edittext
			if (v.getClass() == EditText.class) {
				// aq.id(v.getId()).getEditText().setOnClickListener(this);
			} // 17Nov2016 Arpitha

			// EditText - Assign Listners
			if ((v.getClass() == Button.class)) {
				aq.id(v.getId()).clicked(this, "commonClick");
			}

			return viewArrayList;
		} else if (v instanceof Spinner) {
			aq.id(v.getId()).itemSelected(this, "commonSpinnerClick");
		}

		ArrayList<View> result = new ArrayList<View>();

		ViewGroup viewGroup = (ViewGroup) v;
		for (int i = 0; i < viewGroup.getChildCount(); i++) {

			View child = viewGroup.getChildAt(i);

			ArrayList<View> viewArrayList = new ArrayList<View>();
			viewArrayList.add(v);
			viewArrayList.addAll(initializeScreen(child));

			result.addAll(viewArrayList);
		}
		return result;
	}

	/** This method invokes on Spinner Clicks */
	public void commonSpinnerClick(AdapterView<?> adapter, View v, int position, long id) {
		try {
			switch (adapter.getId()) {
			case R.id.spnsellang: {

				AppContext.editor.putBoolean("isEnglish", false);
				AppContext.editor.putBoolean("isHindi", false);

				if (position == 0) {
					AppContext.editor.putBoolean("isEnglish", true);
					locale = new Locale("en");
					Locale.setDefault(locale);
				} else if (position == 1) {
					AppContext.editor.putBoolean("isHindi", true);
					locale = new Locale("hi");
					Locale.setDefault(locale);
				}

				language = (String) adapter.getSelectedItem();
				break;
			}

			case R.id.spndateformat: {
				dateFormat = (String) adapter.getSelectedItem();
				break;
			}

			case R.id.spnoptions: {
				option = adapter.getSelectedItemPosition();
				break;

			}

			default:
				break;
			}
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
		}
	}

	/** Common method invokes when any button clicked */
	public void commonClick(View v) {
		try {
			switch (v.getId()) {
			case R.id.btnsavesettings: {

				AppContext.editor.commit();
				Configuration config = getResources().getConfiguration();
				config.locale = locale;
				getResources().updateConfiguration(config, getResources().getDisplayMetrics());

				Partograph_CommonClass.defdateformat = dateFormat;
				Partograph_CommonClass.defLanguage = language;

				// SharedPreferences.Editor editor = sharedpreferences.edit();

				Partograph_CommonClass.editor.putString("language", language);
				Partograph_CommonClass.editor.commit();

				if (aq.id(R.id.chknotification).isChecked()) {
					Partograph_CommonClass.editor.putString("notification", "no");
					Partograph_CommonClass.editor.commit();
				} else {
					Partograph_CommonClass.editor.putString("notification", "yes");
					Partograph_CommonClass.editor.commit();

				}

				savephnos(aq.id(R.id.etregno).getText().toString(), option);

				// 17Nov2016 Arpitha
				txtlang.setText(getResources().getString(R.string.choose_lang));
				txtdateformat.setText(getResources().getString(R.string.date_format));
				ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,
						getResources().getStringArray(R.array.phn_no));
				aq.id(R.id.spnoptions).getSpinner().setAdapter(adapter);
				aq.id(R.id.etregno).getEditText().setHint(getResources().getString(R.string.phn_no));
				aq.id(R.id.btnsavesettings).text(getResources().getString(R.string.save));// 17Nov2016
																							// Arpitha

				aq.id(R.id.txt1).text(getResources().getString(R.string.def_settings));// 18Feb2017
																						// Arpitha

				aq.id(R.id.chknotification).text(getResources().getString(R.string.turn_off_notification));// 17April2017
																											// Arpitha
				loadphnnumbers();// 17April2017 Arpitha
				aq.id(R.id.txtheading).text(getResources().getString(R.string.def_settings));// 17April2017
																								// Arpitha
				
				if(LoginActivity.decyptedEvalEndDate!=null && LoginActivity.decyptedEvalEndDate.trim().length()>0)
				{
				aq.id(R.id.treval).visible();
				aq.id(R.id.txtevalenddate).text(Partograph_CommonClass.getConvertedDateFormat(LoginActivity.decyptedEvalEndDate, Partograph_CommonClass.defdateformat));
				}
				break;

			}

			}
		} catch (Exception e) {
			// TODO: handle exception
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
	}

	private boolean savephnos(String phnno, int option_number) throws NotFoundException, Exception {

		if (validatefields()) {

			// 27Sep2016 Arpitha
			if (Partograph_CommonClass.autodatetime(Settings_parto.this)) {

				String strlastinserteddate = dbh.getlastmaxdate(user.getUserId(),
						getResources().getString(R.string.settings));
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				Date lastregdate = null;
				if (strlastinserteddate != null) {
					lastregdate = format.parse(strlastinserteddate);
				}

				Date currentdatetime = new Date();
				if (lastregdate != null && currentdatetime.before(lastregdate)) {
					Partograph_CommonClass.showSettingsAlert(Settings_parto.this,
							getResources().getString(R.string.plz_enable_automatic_date_and_set_current_date));
				} else {// 27Sep2016 Arpitha

					if (phnno.trim().length() > 0) {
						ppojo = new PhoneNumber_Pojo();

						ppojo.setPhn_no(phnno);
						ppojo.setSelectedoption(option_number);
						ppojo.setUserid(Partograph_CommonClass.user.getUserId());

						dbh.db.beginTransaction();
						isadded = dbh.addphno(ppojo);
						if (isadded) {
							loadphnnumbers();
							dbh.db.setTransactionSuccessful();
							dbh.db.endTransaction();

							// updated on1Aug2016 by Arpitha
							if (option_number != 4) {
								phn_nos.add(aq.id(R.id.etregno).getText().toString());
								aq.id(R.id.etregno).text("");
								aq.id(R.id.spnoptions).setSelection(0);
							} else
								// aq.id(R.id.etmailid).text("");
								Toast.makeText(getApplicationContext(), getResources().getString(R.string.save_phno),
										Toast.LENGTH_LONG).show();
						}
					} else
						Toast.makeText(getApplicationContext(),
								getResources().getString(R.string.settingsupdatedsuccessfully), Toast.LENGTH_LONG)
								.show();
				}
			}
		}

		return isadded;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_womenlist, menu);
		// menu.findItem(R.id.help).setVisible(false);

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		try {
			switch (item.getItemId()) {
			// Respond to the action bar's Up/Home button
			case android.R.id.home:
				NavUtils.navigateUpFromSameTask(this);
				return true;

			case R.id.logout:
				displayConfirmationAlert(getResources().getString(R.string.exit_msg),
						getResources().getString(R.string.login));

				return true;

			case R.id.home:
				displayConfirmationAlert(getResources().getString(R.string.exit_msg),
						getResources().getString(R.string.home));

				return true;

			case R.id.about:
				displayConfirmationAlert(getResources().getString(R.string.exit_msg),
						getResources().getString(R.string.about));

				return true;

			case R.id.settings:

				displayConfirmationAlert(getResources().getString(R.string.exit_msg),
						getResources().getString(R.string.settings));

				return true;

			// 10May2017 Arpitha - v2.6
			case R.id.sms:
				Partograph_CommonClass.display_messagedialog(Settings_parto.this, user.getUserId());
				return true;

			case R.id.comments:
				Intent comments = new Intent(getApplicationContext(), Comments_Activity.class);
				startActivity(comments);
				return true;

			case R.id.summary:

				Intent summary = new Intent(getApplicationContext(), Summary_Activity.class);
				startActivity(summary);

				return true;

			case R.id.info:
				displayConfirmationAlert(getResources().getString(R.string.exit_msg),
						getResources().getString(R.string.info));

				return true;

			case R.id.changepass:
				Partograph_CommonClass.dispalyChangePasswordDialog(Settings_parto.this);
				return true;

			case R.id.usermanual:

				Intent usermanual = new Intent(getApplicationContext(), UseManual.class);
				startActivity(usermanual);
				return true;
			// 10May2017 Arpitha - v2.6

			case R.id.disch:
				Intent disch = new Intent(Settings_parto.this, DischargedWomanList_Activity.class);
				startActivity(disch);
				return true;
			}
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		menu.findItem(R.id.sync).setVisible(false);
		menu.findItem(R.id.delete).setVisible(false);
		// menu.findItem(R.id.delete).setVisible(false);
		menu.findItem(R.id.expertcomments).setVisible(false);
		menu.findItem(R.id.viewpartograph).setVisible(false);
		menu.findItem(R.id.ic_msg).setVisible(false);
		menu.findItem(R.id.addcomments).setVisible(false);
		menu.findItem(R.id.viewprofile).setVisible(false);
		menu.findItem(R.id.settings).setVisible(false);
		menu.findItem(R.id.registration).setVisible(false);
		if (!(AppContext.checkSimState() == TelephonyManager.SIM_STATE_READY)) {// 22oct2016
			// Arpitha
			menu.findItem(R.id.sms).setVisible(false);
		}

		menu.findItem(R.id.search).setVisible(false);// 14Jun2017 Arpitha

		menu.findItem(R.id.save).setVisible(false);// 08Aug2017 Arpitha

		return super.onPrepareOptionsMenu(menu);
	}

	// updated on 19july2016 by Arpitha
	boolean validatefields() throws NotFoundException, Exception {

		if (option == 0 && aq.id(R.id.etregno).getText().length() > 0) {
			displayAlertDialog(getResources().getString(R.string.sele_opt));
			return false;
		}

		if (option != 0) {
			if (aq.id(R.id.etregno).getText().length() <= 0) {
				displayAlertDialog(getResources().getString(R.string.entr_no));
				aq.id(R.id.etregno).getEditText().requestFocus();
				return false;
			}

			if (aq.id(R.id.etregno).getText().toString().length() > 0
					&& aq.id(R.id.etregno).getText().toString().trim().length() < 10) {
				displayAlertDialog(getResources().getString(R.string.phno_validate));
				aq.id(R.id.etregno).getEditText().requestFocus();
				return false;
			}

			if (phn_nos != null && phn_nos.size() > 0) {
				for (int i = 0; i < phn_nos.size(); i++)

				{
					String phno = phn_nos.get(i);
					if (aq.id(R.id.etregno).getText().toString().equalsIgnoreCase(phno)) {
						ArrayList<Integer> opt = dbh.getoption2(Partograph_CommonClass.user.getUserId(), phno);
						if (opt.contains(option)) {
							displayAlertDialog(getResources().getString(R.string.phno_exist));
							return false;
						}
					}
				}
			}
		}

		return true;
	}

	// Alert dialog
	private void displayAlertDialog(String message) throws Exception {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

		// set dialog message
		alertDialogBuilder.setMessage(message)

				.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// if this button is clicked, just close
						// the dialog box and do nothing
						dialog.cancel();
					}
				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();
		// show it
		alertDialog.show();
	}

	void intialview() throws Exception {

		strphn_del = new ArrayList<String>();
		option_del = new ArrayList<Integer>();
		loadphnnumbers();

		
		
		if(LoginActivity.decyptedEvalEndDate!=null && LoginActivity.decyptedEvalEndDate.trim().length()>0)
		{
		aq.id(R.id.treval).visible();
		aq.id(R.id.txtevalenddate).text(Partograph_CommonClass.getConvertedDateFormat(LoginActivity.decyptedEvalEndDate, Partograph_CommonClass.defdateformat));
		}
		
		if (Partograph_CommonClass.prefs.getString("notification", null).equalsIgnoreCase("no")) {
			aq.id(R.id.chknotification).checked(true);
		} else
			aq.id(R.id.chknotification).checked(false);

	}

	void loadphnnumbers() throws Exception {
		val = dbh.getphn_numbers(Partograph_CommonClass.user.getUserId());

		if (val != null && val.size() > 0) {
			listcommentsfhr.setVisibility(View.VISIBLE);
			adapter = new Settings_Adapter(getApplicationContext(), R.layout.exprt_comments, val);
			listcommentsfhr.setAdapter(adapter);
			listcommentsfhr.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);

			listcommentsfhr.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
			listcommentsfhr.setSelector(android.R.color.holo_blue_light);
			listcommentsfhr.setMultiChoiceModeListener(new MultiChoiceModeListener() {

				@Override
				public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
					// TODO Auto-generated method stub
					return false;
				}

				@Override
				public void onDestroyActionMode(ActionMode mode) {
					// TODO Auto-generated method stub

				}

				@Override
				public boolean onCreateActionMode(ActionMode mode, Menu menu) {
					mode.getMenuInflater().inflate(R.menu.activity_womenlist, menu);
					menu.findItem(R.id.sync).setVisible(false);

					menu.findItem(R.id.home).setVisible(false);
					menu.findItem(R.id.info).setVisible(false);
					menu.findItem(R.id.about).setVisible(false);
					menu.findItem(R.id.logout).setVisible(false);
					// 10May2017 Arpitha
					menu.findItem(R.id.settings).setVisible(false);
					menu.findItem(R.id.sms).setVisible(false);
					menu.findItem(R.id.comments).setVisible(false);
					menu.findItem(R.id.summary).setVisible(false);
					menu.findItem(R.id.usermanual).setVisible(false);// 10May2017
																		// Arpitha
					// 26Jun2017 Arpitha
					menu.findItem(R.id.expertcomments).setVisible(false);
					menu.findItem(R.id.search).setVisible(false);
					menu.findItem(R.id.viewpartograph).setVisible(false);
					menu.findItem(R.id.registration).setVisible(false);
					menu.findItem(R.id.sms).setVisible(false);
					menu.findItem(R.id.addcomments).setVisible(false);
					menu.findItem(R.id.viewprofile).setVisible(false);
					menu.findItem(R.id.ic_msg).setVisible(false);
					menu.findItem(R.id.changepass).setVisible(false);// 26Jun2017
																		// Arpitha

					return true;
				}

				@Override
				public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
					// TODO Auto-generated method stub

					try {
						SparseBooleanArray selected = adapter.getSelectedIds();
						// Captures all selected ids with a loop
						for (int i = (selected.size() - 1); i >= 0; i--) {
							if (selected.valueAt(i)) {
								PhoneNumber_Pojo selecteditem = adapter.getItem(selected.keyAt(i));
								// Remove selected items following the db
								dbh.delete_phnno(selecteditem.getPhn_no(), selecteditem.getSelectedoption(),
										LoginActivity.user_id);
							}
						}

						loadphnnumbers();

						Toast.makeText(getApplicationContext(), getResources().getString(R.string.del_success),
								Toast.LENGTH_LONG).show();
					} catch (Exception e) {
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}
					return true;
				}

				@Override
				public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {
					try {
						final int checkedCount = listcommentsfhr.getCheckedItemCount();
						// Set the CAB title according to total checked items
						mode.setTitle(checkedCount + " Selected");
						adapter.toggleSelection(position);

						strphn_delete = val.get(position).getPhn_no();
						strphn_del.add(strphn_delete);
						option_delete = val.get(position).getSelectedoption();
						option_del.add(option_delete);
					} catch (Exception e) {
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}
				}
			});
		} else {
			listcommentsfhr.setVisibility(View.GONE);
		}

	}

	// Display Confirmation to exit the screen
	private void displayConfirmationAlert(String exit_msg, final String classname) throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		final Dialog dialog = new Dialog(Settings_parto.this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.alertdialog);

		dialog.show();

		final TextView txtdialog = (TextView) dialog.findViewById(R.id.txtdialog);
		Button imgbtnyes = (Button) dialog.findViewById(R.id.imgbtnyes);
		Button imgbtnno = (Button) dialog.findViewById(R.id.imgbtnno);
		final TextView txtdialog1 = (TextView) dialog.findViewById(R.id.txtval);
		final TextView txtdialog2 = (TextView) dialog.findViewById(R.id.txtval1);

		txtdialog.setText(exit_msg);
		txtdialog1.setVisibility(View.GONE);
		txtdialog2.setVisibility(View.GONE);

		imgbtnyes.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				dialog.cancel();

				if (classname.equalsIgnoreCase(getResources().getString(R.string.home))) {
					Intent home = new Intent(Settings_parto.this, Activity_WomenView.class);
					startActivity(home);
				}
				if (classname.equalsIgnoreCase(getResources().getString(R.string.login))) {
					Intent login = new Intent(Settings_parto.this, LoginActivity.class);
					startActivity(login);

				}
				if (classname.equalsIgnoreCase(getResources().getString(R.string.about))) {
					Intent about = new Intent(Settings_parto.this, About.class);
					startActivity(about);
				}
				if (classname.equalsIgnoreCase(getResources().getString(R.string.info))) {
					Intent about = new Intent(Settings_parto.this, GraphInformation.class);
					startActivity(about);
				}

			}
		});

		imgbtnno.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.cancel();
			}
		});
	}

	/** This method Calls the idle timeout */
	@Override
	public void onUserInteraction() {
		super.onUserInteraction();
		// KillAllActivitesAndGoToLogin
		// .delayedIdle(Integer.parseInt(Partograph_CommonClass.properties.getProperty("idleTimeOut")));
	}

	// 20oct2016 Arpitha
	@Override
	public void onBackPressed() {
		Intent i = new Intent(Settings_parto.this, Activity_WomenView.class);
		startActivity(i);
	}

	// 10Jan2017 Arpitha
	public static boolean isEmailValid(String email) {
		boolean isValid = false;

		String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
		CharSequence inputStr = email;

		Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(inputStr);
		if (matcher.matches()) {
			isValid = true;
		}
		return isValid;
	}

	/*
	 * TextWatcher watcher = new TextWatcher() {
	 * 
	 * @Override public void onTextChanged(CharSequence s, int start, int
	 * before, int count) { // TODO Auto-generated method stub if (s ==
	 * aq.id(R.id.etmailid).getEditable()) { boolean isvalid; isvalid =
	 * isEmailValid(aq.id(R.id.etmailid).getText().toString()); // isvalid = //
	 * isEmailValid(aq.id(R.id.etmailid).getText().toString()); if (!(isvalid))
	 * { aq.id(R.id.etmailid).getEditText()
	 * .setError(getResources().getString(R.string.invalid_email_address)); }
	 * 
	 * }
	 * 
	 * }
	 * 
	 * @Override public void beforeTextChanged(CharSequence s, int start, int
	 * count, int after) { // TODO Auto-generated method stub
	 * 
	 * }
	 * 
	 * @Override public void afterTextChanged(Editable s) { // TODO
	 * Auto-generated method stub
	 * 
	 * } };
	 * 
	 * boolean isEmailValid(CharSequence email) { return
	 * android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches(); }
	 */
}
