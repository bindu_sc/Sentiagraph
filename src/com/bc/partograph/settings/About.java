package com.bc.partograph.settings;

import java.util.Locale;

import com.bc.partograph.comments.Comments_Activity;
import com.bc.partograph.common.AppContext;
import com.bc.partograph.common.KillAllActivitesAndGoToLogin;
import com.bc.partograph.common.Partograph_CommonClass;
import com.bc.partograph.login.LoginActivity;
import com.bc.partograph.sliding.GraphInformation;
import com.bc.partograph.womenview.Activity_WomenView;
import com.bluecrimson.additionalDetails.Summary_Activity;
import com.bluecrimson.dischargedetails.DischargedWomanList_Activity;
import com.bluecrimson.partograph.R;
import com.bluecrimson.partograph.Women_Profile_Pojo;
import com.bluecrimson.usermanual.UseManual;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources.NotFoundException;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.telephony.TelephonyManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class About extends Activity {
	TextView txtlink;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
			setContentView(R.layout.activity_about);

			txtlink = (TextView) findViewById(R.id.txtlink);

			// 05Nov2016 Arpitha
			Locale locale = null;

			if (AppContext.prefs.getBoolean("isEnglish", false)) {
				locale = new Locale("en");
				Locale.setDefault(locale);
			} else if (AppContext.prefs.getBoolean("isHindi", false)) {
				locale = new Locale("hi");
				Locale.setDefault(locale);
			}

			Configuration config = new Configuration();
			config.locale = locale;
			getResources().updateConfiguration(config, getResources().getDisplayMetrics());

			AppContext.setNamesAccordingToRes(getResources());// 05Nov2016
																// Arpitha

			txtlink.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					Uri uriUrl = Uri.parse("http://www.bluecrimson.net/");
					Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
					startActivity(launchBrowser);

				}
			});

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}

		// 13Sep2016
		KillAllActivitesAndGoToLogin.activity_stack.add(this);
		KillAllActivitesAndGoToLogin.addToStack(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_womenlist, menu);

		menu.findItem(R.id.delete).setVisible(false);
		menu.findItem(R.id.search).setVisible(false);//14Jun2017 Arpitha
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		try {
			switch (item.getItemId()) {
			// Respond to the action bar's Up/Home button
			case android.R.id.home:
				NavUtils.navigateUpFromSameTask(this);
				return true;

			case R.id.logout:
				Intent login = new Intent(this, LoginActivity.class);
				startActivity(login);
				return true;

			case R.id.home:
				Intent home = new Intent(this, Activity_WomenView.class);
				startActivity(home);
				return true;

			case R.id.about:
				Intent about = new Intent(this, About.class);
				startActivity(about);
				return true;

			case R.id.info:
				Intent info = new Intent(this, GraphInformation.class);
				startActivity(info);
				return true;
			// 10May2017 Arpitha - v2.6
			case R.id.settings:

				Intent settings = new Intent(getApplicationContext(), Settings_parto.class);
				startActivity(settings);
				return true;

			case R.id.sms:
				Partograph_CommonClass.display_messagedialog(About.this, Partograph_CommonClass.user.getUserId());
				return true;

			case R.id.comments:
				Intent comments = new Intent(getApplicationContext(), Comments_Activity.class);
				startActivity(comments);
				return true;

			case R.id.summary:

				Intent summary = new Intent(getApplicationContext(), Summary_Activity.class);
				startActivity(summary);

				return true;
				
			case R.id.changepass:
				Partograph_CommonClass.dispalyChangePasswordDialog(About.this);
				return true;

			case R.id.usermanual:

				Intent usermanual = new Intent(getApplicationContext(), UseManual.class);
				startActivity(usermanual);
				return true;
				// 10May2017 Arpitha - v2.6
				
			case R.id.disch:
				Intent disch = new Intent(About.this, DischargedWomanList_Activity.class);
				startActivity(disch);

			}
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		menu.findItem(R.id.about).setVisible(false);
		menu.findItem(R.id.sync).setVisible(false);
		menu.findItem(R.id.delete).setVisible(false);
		menu.findItem(R.id.expertcomments).setVisible(false);
		menu.findItem(R.id.viewpartograph).setVisible(false);
		menu.findItem(R.id.ic_msg).setVisible(false);
		menu.findItem(R.id.addcomments).setVisible(false);
		menu.findItem(R.id.viewprofile).setVisible(false);
		// menu.findItem(R.id.sync).setVisible(false);
		menu.findItem(R.id.registration).setVisible(false);

		if (!(AppContext.checkSimState() == TelephonyManager.SIM_STATE_READY)) {// 22oct2016
			// Arpitha
			menu.findItem(R.id.sms).setVisible(false);
		}
		return super.onPrepareOptionsMenu(menu);
	}

	/** This method Calls the idle timeout */
	/*
	 * @Override public void onUserInteraction() { super.onUserInteraction();
	 * KillAllActivitesAndGoToLogin
	 * .delayedIdle(Integer.parseInt(Partograph_CommonClass.properties.
	 * getProperty("idleTimeOut"))); }
	 */

	// 17Nov2016 Arpitha
	@Override
	public void onBackPressed() {
		try {
			Intent i = new Intent(About.this, Activity_WomenView.class);
			startActivity(i);
		} catch (NotFoundException e) {
			// TODO Auto-generated catch block
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
	}

}
