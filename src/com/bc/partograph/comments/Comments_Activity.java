//13Oct2016 Arpitha
package com.bc.partograph.comments;

import java.util.ArrayList;
import java.util.Locale;

import com.bc.partograph.common.AppContext;
import com.bc.partograph.common.KillAllActivitesAndGoToLogin;
import com.bc.partograph.common.Partograph_CommonClass;
import com.bc.partograph.common.Partograph_DB;
import com.bc.partograph.login.LoginActivity;
import com.bc.partograph.settings.About;
import com.bc.partograph.settings.Settings_parto;
import com.bc.partograph.sliding.GraphInformation;
import com.bc.partograph.sync.SyncFunctions;
import com.bc.partograph.womenview.Activity_WomenView;
import com.bluecrimson.additionalDetails.Summary_Activity;
import com.bluecrimson.dischargedetails.DischargedWomanList_Activity;
import com.bluecrimson.partograph.Comments_Pojo;
import com.bluecrimson.partograph.R;
import com.bluecrimson.partograph.UserPojo;
import com.bluecrimson.usermanual.UseManual;

import android.app.ActionBar;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources.NotFoundException;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.text.InputFilter;
import android.text.Spanned;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class Comments_Activity extends FragmentActivity {

	String selecteddate, todaysDate;
	int year, mon, day, hour, minute;
	Partograph_DB dbh;
	EditText etdate;
	static final int TIME_DIALOG_ID = 999;
	String time;
	EditText ettime;
	UserPojo user;
	ArrayList<Comments_Pojo> arrval;
	Adapter_GeneralComments adapter;
	ListView listgeneralcomments;
	String date_of_insertion;
	TextView txtnodata;
	Thread myThread;
	public static MenuItem menuItem;// 31Oct2016 Arpitha
	EditText etcomment;
	public static boolean isComments;// 03Sep2017 Arpitha
	public static int commentscount;// 03Sep2017 Arpitha

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_comments);

		try {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());

			ActionBar actionBar = getActionBar();// get the action bar

			actionBar.setDisplayHomeAsUpEnabled(true);// Enabling Back
														// navigation on
														// Action Bar icon

			// 05Nov2016 Arpitha
			Locale locale = null;

			if (AppContext.prefs.getBoolean("isEnglish", false)) {
				locale = new Locale("en");
				Locale.setDefault(locale);
			} else if (AppContext.prefs.getBoolean("isHindi", false)) {
				locale = new Locale("hi");
				Locale.setDefault(locale);
			}

			Configuration config = new Configuration();
			config.locale = locale;
			getResources().updateConfiguration(config, getResources().getDisplayMetrics());

			AppContext.setNamesAccordingToRes(getResources());// 05Nov2016
																// Arpitha

			commentscount = Partograph_CommonClass.curr_tabpos;// 03Sep2017
																// Arpitha
			isComments = true;// 03Sep2017 Arpitha

			dbh = Partograph_DB.getInstance(getApplicationContext());
			todaysDate = Partograph_CommonClass.getTodaysDate();
			user = dbh.getUserProfile();
			Partograph_CommonClass.user = user;
			listgeneralcomments = (ListView) findViewById(R.id.listgeneralcomments);
			txtnodata = (TextView) findViewById(R.id.nodata);
			txtnodata.setVisibility(View.GONE);
			displaydata();

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
		KillAllActivitesAndGoToLogin.addToStack(this);// 17Nov2016 Arpitha
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_womenlist, menu);

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		try {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
			switch (item.getItemId()) {
			// Respond to the action bar's Up/Home button
			case android.R.id.home:
				NavUtils.navigateUpFromSameTask(this);
				return true;

			case R.id.logout:
				displayConfirmationAlert(getResources().getString(R.string.exit_msg),
						getResources().getString(R.string.logout));
				return true;

			case R.id.home:

				displayConfirmationAlert(getResources().getString(R.string.exit_msg),
						getResources().getString(R.string.main));

				return true;

			case R.id.info:
				displayConfirmationAlert(getResources().getString(R.string.exit_msg),
						getResources().getString(R.string.info));

				return true;

			case R.id.about:

				displayConfirmationAlert(getResources().getString(R.string.exit_msg),
						getResources().getString(R.string.about));

				return true;

			case R.id.settings:

				displayConfirmationAlert(getResources().getString(R.string.exit_msg),
						getResources().getString(R.string.settings));

				return true;

			case R.id.addcomments:
				display_messagedialog();
				return true;
			// 31Oct2016 Arpitha
			case R.id.sync:
				menuItem = item;
				menuItem.setActionView(R.layout.progressbar);
				menuItem.expandActionView();
				calSyncMtd();
				return true;
			case R.id.sms:
				Partograph_CommonClass.display_messagedialog(Comments_Activity.this, user.getUserId());
				return true;

			// 10May2017 Arpitha - v2.6
			case R.id.comments:
				Intent comments = new Intent(getApplicationContext(), Comments_Activity.class);
				startActivity(comments);
				return true;

			case R.id.summary:

				Intent summary = new Intent(getApplicationContext(), Summary_Activity.class);
				startActivity(summary);

				return true;
			case R.id.changepass:
				Partograph_CommonClass.dispalyChangePasswordDialog(Comments_Activity.this);
				return true;

			case R.id.usermanual:

				Intent usermanual = new Intent(getApplicationContext(), UseManual.class);
				startActivity(usermanual);// 10May2017 Arpitha - v2.6
				return true;

			case R.id.disch:
				Intent disch = new Intent(Comments_Activity.this, DischargedWomanList_Activity.class);
				startActivity(disch);

			}
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// 25Sep2016 Arpitha - addToTrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		if (!(AppContext.checkSimState() == TelephonyManager.SIM_STATE_READY)) {// 22oct2016
																				// Arpitha
			menu.findItem(R.id.sms).setVisible(false);
		} // 22oct2016 Arpitha

		// 31oct2016 Arpitha
		if (Partograph_CommonClass.isRecordExistsForSync)
			menu.findItem(R.id.sync).setIcon(getResources().getDrawable(R.drawable.ic_sync_red));
		else
			menu.findItem(R.id.sync).setIcon(getResources().getDrawable(R.drawable.ic_sync_green));

		menu.findItem(R.id.registration).setVisible(false);// 10May2017 Arpitha
															// - v2.6

		menu.findItem(R.id.delete).setVisible(false);
		menu.findItem(R.id.expertcomments).setVisible(false);
		menu.findItem(R.id.viewpartograph).setVisible(false);
		menu.findItem(R.id.ic_msg).setVisible(false);
		menu.findItem(R.id.viewprofile).setVisible(false);
		menu.findItem(R.id.comments).setVisible(false);// 12May2017 Arpitha
														// -v2.6

		menu.findItem(R.id.search).setVisible(false);// 14Jun2017 Arpitha

		menu.findItem(R.id.save).setVisible(false);// 08Aug2017 Arpitha
		return super.onPrepareOptionsMenu(menu);
	}

	public void display_messagedialog() throws Exception {
		try {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
			final Dialog sms_dialog = new Dialog(Comments_Activity.this);

			sms_dialog.requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
			sms_dialog.setContentView(R.layout.activity_add_comments);
			sms_dialog.getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.dialog_title);
			TextView txtdialogtitle = (TextView) sms_dialog.findViewById(R.id.graph_title);
			txtdialogtitle.setTextColor(getResources().getColor(R.color.Maroon));
			txtdialogtitle.setText(getResources().getString(R.string.add_my_notes_comment));
			ImageButton imgbtncanceldialog = (ImageButton) sms_dialog.findViewById(R.id.imgbtncanceldialog);

			etdate = (EditText) sms_dialog.findViewById(R.id.etdate);
			ettime = (EditText) sms_dialog.findViewById(R.id.ettime);
			etcomment = (EditText) sms_dialog.findViewById(R.id.etcomment);
			final EditText etname = (EditText) sms_dialog.findViewById(R.id.etname);
			ImageButton imgadd = (ImageButton) sms_dialog.findViewById(R.id.imgadd);

			date_of_insertion = todaysDate;
			etdate.setTextColor(getResources().getColor(R.color.fontcolor_disable));// 31Oct2016
																					// Arpitha
			ettime.setTextColor(getResources().getColor(R.color.fontcolor_disable));// 31Oct2016
																					// Arpitha
			etdate.setText(Partograph_CommonClass.getConvertedDateFormat(date_of_insertion,
					Partograph_CommonClass.defdateformat));
			ettime.setText(Partograph_CommonClass.getCurrentTime());

			imgbtncanceldialog.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					try {
						sms_dialog.cancel();
					} catch (Exception e) {
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}
				}
			});

			imgadd.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					try {

						if (etdate.getText().toString().trim().length() <= 0
								|| ettime.getText().toString().trim().length() <= 0
								|| etcomment.getText().toString().trim().length() <= 0
								|| etname.getText().toString().trim().length() <= 0) {
							Toast.makeText(getApplicationContext(), getResources().getString(R.string.enter_all_fields),
									Toast.LENGTH_SHORT).show();
						}

						else {
							sms_dialog.cancel();
							Comments_Pojo cpojo = new Comments_Pojo();
							cpojo.setDate_of_insertion(date_of_insertion);

							cpojo.setTime_of_insertion(ettime.getText().toString());
							cpojo.setGivenby(etname.getText().toString());
							cpojo.setComment(etcomment.getText().toString());
							cpojo.setUserid(user.getUserId());
							dbh.db.beginTransaction();
							int transId = dbh.iCreateNewTrans(user.getUserId());

							boolean isadded = dbh.addcomment(cpojo, transId);
							if (isadded) {
								dbh.iNewRecordTrans(cpojo.getUserid(), transId, Partograph_DB.TBL_GENERALCOMMENTS);
								commitTrans();
							} else
								rollbackTrans();
						}

					} catch (Exception e) {
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}

				}

			});

			int dividerId = sms_dialog.getContext().getResources().getIdentifier("android:id/titleDivider", null, null);
			View divider = sms_dialog.findViewById(dividerId);
			divider.setBackgroundColor(getResources().getColor(R.color.Maroon));// 08Feb2017

			setInputFiltersForEdittext();// 10April2017 Arpitha
			sms_dialog.show();

			sms_dialog.setCancelable(false);

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}

	}

	// Sync
	private void calSyncMtd() throws Exception {
		// 25Sep2016 Arpitha - addToTrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		SyncFunctions.syncUptoDate = false;
		Partograph_CommonClass.responseAckCount = 0;
		Partograph_CommonClass.AsyncCallWS task = new Partograph_CommonClass.AsyncCallWS();
		task.execute();
	}

	/** Method to rollback the trnsaction */
	private void rollbackTrans() throws Exception {
		dbh.db.endTransaction();
		Toast.makeText(getApplicationContext(), getResources().getString(R.string.failed), Toast.LENGTH_SHORT).show();
	}

	private void commitTrans() throws Exception {
		// TODO Auto-generated method stub
		dbh.db.setTransactionSuccessful();
		dbh.db.endTransaction();
		calSyncMtd();
		Toast.makeText(getApplicationContext(), getResources().getString(R.string.cmnt_add_succes), Toast.LENGTH_SHORT)
				.show();
		displaydata();
	}

	// 20oct2016 Arpitha
	@Override
	public void onBackPressed() {
		try {
			displayConfirmationAlert(getResources().getString(R.string.exit_msg),
					getResources().getString(R.string.main));
		} catch (NotFoundException e) {
			// TODO Auto-generated catch block
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
	}

	void displaydata() throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		arrval = new ArrayList<Comments_Pojo>();
		arrval = dbh.getCommentsToDisplay(user.getUserId());
		if (arrval.size() > 0) {

			adapter = new Adapter_GeneralComments(getApplicationContext(), R.layout.activity_womenlist, arrval);
			listgeneralcomments.setAdapter(adapter);
			listgeneralcomments.setVisibility(View.VISIBLE);
			txtnodata.setVisibility(View.GONE);
		} else {
			listgeneralcomments.setVisibility(View.GONE);
			txtnodata.setVisibility(View.VISIBLE);
		}
	}

	private void displayConfirmationAlert(String exit_msg, final String classname) throws Exception {

		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		final Dialog dialog = new Dialog(Comments_Activity.this);
		dialog.setTitle(Html.fromHtml("<font color='" + getResources().getColor(R.color.appcolor) + "'>"
				+ getResources().getString(R.string.alert) + "</font>"));// 08Feb2017
																			// Arpitha
		dialog.setContentView(R.layout.temp_alertdialog);

		int dividerId = dialog.getContext().getResources().getIdentifier("android:id/titleDivider", null, null);
		View divider = dialog.findViewById(dividerId);
		divider.setBackgroundColor(getResources().getColor(R.color.appcolor));// 08Feb2017

		dialog.show();

		final TextView txtdialog = (TextView) dialog.findViewById(R.id.txtpulse);
		Button imgbtnyes = (Button) dialog.findViewById(R.id.imgbtnyes);
		Button imgbtnno = (Button) dialog.findViewById(R.id.imgbtnno);
		// Arpitha 27may16
		final TextView txtdialog1 = (TextView) dialog.findViewById(R.id.txtpulseval);

		final TextView txtdialog6 = (TextView) dialog.findViewById(R.id.txt);

		txtdialog1.setVisibility(View.GONE);
		txtdialog.setVisibility(View.GONE);
		txtdialog6.setText(exit_msg);
		imgbtnyes.setText(getResources().getString(R.string.yes));
		imgbtnno.setText(getResources().getString(R.string.no));

		imgbtnyes.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				dialog.cancel();

				if (classname.equalsIgnoreCase(getResources().getString(R.string.main))) {
					Intent i = new Intent(Comments_Activity.this, Activity_WomenView.class);
					startActivity(i);
				}
				if (classname.equalsIgnoreCase(getResources().getString(R.string.logout))) {
					Intent i = new Intent(Comments_Activity.this, LoginActivity.class);
					startActivity(i);
				}
				if (classname.equalsIgnoreCase(getResources().getString(R.string.settings))) {
					Intent settings = new Intent(Comments_Activity.this, Settings_parto.class);
					startActivity(settings);
				}
				if (classname.equalsIgnoreCase(getResources().getString(R.string.info))) {
					Intent i = new Intent(Comments_Activity.this, GraphInformation.class);
					startActivity(i);
				}

				if (classname.equalsIgnoreCase(getResources().getString(R.string.about))) {
					Intent about = new Intent(Comments_Activity.this, About.class);
					startActivity(about);
				}

			}
		});

		imgbtnno.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.cancel();

			}
		});

		dialog.show();

		dialog.setCancelable(false);

	}

	// 07Nov2016 Arpitha
	/** This method Calls the idle timeout */
	@Override
	public void onUserInteraction() {
		super.onUserInteraction();
		KillAllActivitesAndGoToLogin
				.delayedIdle(Integer.parseInt(Partograph_CommonClass.properties.getProperty("idleTimeOut")));
	}

	// change made on 01Apr2015
	// setInput Filters
	private void setInputFiltersForEdittext() throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		etcomment.setFilters(new InputFilter[] { filter });

	}

	// To avoid special characters in Input type
	public static InputFilter filter = new InputFilter() {
		@Override
		public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
			String blockCharacterSet = "~#^|$%*!@/()-'\":;?{}=!$^';?Ã—Ã·<>{}â‚¬Â£Â¥â‚©%&+*[]~#^|$%&*!`@_=?;]}{[()\"\\Â¥Â®Â¿ÃŒâ„¢â‚¹Â°^âˆšÏ€Ã·Ã—â–³Â¶Â£â€¢Â¢â‚¬â™¥â™¡â˜…â˜†â–²â–¼â†‘â†�â†“â†’Â¤â–³â™‚â™€â„ƒ||â–³Â©c/o||Â¿Â¡â„…â„¢Â®â‚¹Â°Â¢`â€¢âˆšÏ€Â¶âˆ†Â¢Â°âˆ†Â¶Ï€âˆšâ€¢`";
			if (source != null && blockCharacterSet.contains(("" + source))) {
				return "";
			}
			return null;
		}
	};

}
