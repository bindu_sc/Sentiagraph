package com.bc.partograph.womenview;

import java.util.ArrayList;

import com.androidquery.AQuery;
import com.bc.partograph.common.AppContext;
import com.bc.partograph.common.FragmentState;
import com.bc.partograph.common.Partograph_CommonClass;
import com.bc.partograph.common.Partograph_DB;
import com.bc.partograph.comprehensiveview.View_Partograph;
import com.bc.partograph.deliveryinfo.DeliveryInfo_Activity;
import com.bc.partograph.referralinfo.ReferralInfo_Activity;
import com.bc.partograph.regwomenlist.RecentCustomListAdapterDIP24;
import com.bc.partograph.sliding.SlidingActivity;
import com.bc.partograph.stagesoflabor.StageofLabor;
import com.bluecrimson.additionalDetails.AdditionalDetails_Activity;
import com.bluecrimson.apgar.Activity_Apgar;
import com.bluecrimson.partograph.ActionItem;
import com.bluecrimson.partograph.QuickAction;
import com.bluecrimson.partograph.R;
import com.bluecrimson.partograph.UserPojo;
import com.bluecrimson.partograph.Women_Profile_Pojo;
import com.bluecrimson.viewprofile.ViewProfile_Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources.NotFoundException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.SearchView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Spinner;
import android.widget.Toast;

public class Fragment_DIPgt24hrs extends Fragment
		implements FragmentState, OnClickListener, SearchView.OnQueryTextListener {

	View rootView;
	AQuery aq;
	Partograph_DB dbh;
	UserPojo user;
	String todaysDate, womenId, wUserId;
	static String strRegdate;
	QuickAction mQuickAction;
	Thread myThread;
	public static String delstatus_today;
	int option;
	public static final int ID_PROFILE = 1;
	public static final int ID_GRAPH = 2;
	public static final int ID_DELSTATUS = 3;
	public static final int ID_APGAR = 4;
	public static final int ID_COMPREHENSIVEVIEW = 5;
	public static final int ID_REFERRAL = 6;
	public static final int ID_STAGEOFLABOR = 7;
	public static final int ID_PRINTPARTO = 8;
	RecentCustomListAdapterDIP24 adapter;

	ArrayList<String> values;
	String reg_phno;
	String p_no = "";

	// updated on 29july2016 by Arpitha
	static String strlastentrytime = null; // 22Aug2016-bindu change to null

	// 27oct2016 - bindu
	private int VIEW_COUNT = 10;
	private int index = 0, totalCount = 0, listviewFocusItemPosition = 0;
	private boolean isUpdating = false, isfirstLoad = true;
	ArrayList<Women_Profile_Pojo> arrDispWList = new ArrayList<Women_Profile_Pojo>();
	String mainSql;
	Women_Profile_Pojo woman;// 03Nov2016 Arpitha
	ProgressDialog mProgressDialog;
	public static final int ID_ADDITIONALDETAILS = 9;// 06Dec2016 Arpitha

	SearchView searchviewdip24;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		rootView = inflater.inflate(R.layout.fragment_dipgt24, container, false);
		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		try {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());

			aq = new AQuery(getActivity());

			dbh = Partograph_DB.getInstance(getActivity());
			user = Partograph_CommonClass.user;

			initializeScreen(aq.id(R.id.rlweekly).getView());
			
			initialView();

			

			

//			searchviewdip24 = (SearchView) rootView.findViewById(R.id.search_view_dip24);
//			searchviewdip24.setOnQueryTextListener(this);

			Fragment_DIP.istears = false; // 27Sep2016 Arpitha

			aq.id(R.id.listwomenweekly).getListView().setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> adapter, View v, int pos, long id) {
					try {

						woman = (Women_Profile_Pojo) adapter.getItemAtPosition(pos);
						womenId = woman.getWomenId();

						wUserId = woman.getUserId();
						strRegdate = woman.getDate_of_admission() + " " + woman.getTime_of_admission();
						mQuickAction.show(v);
						// updated on 18Aug2016 by Arpitha
						strlastentrytime = dbh.getlastentrytime(womenId, 1);
					} catch (Exception e) {
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}

				}
			});
			
			
			mQuickAction = new QuickAction(getActivity());
			displayOptions();

			// 26dec2015

			myThread = null;
			Runnable myRunnableThread = new CountDownRunner();
			myThread = new Thread(myRunnableThread);
			myThread.start();

			values = new ArrayList<String>();
			values = Partograph_CommonClass.getphonenumber(option);

		} catch (Exception e) {
			e.printStackTrace();
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
		}

	}

	/**
	 * This is a Recursive method that traverse through the group and subgroup
	 * of view Sets text, assigns clickListners
	 * 
	 * @throws Exception
	 */
	private ArrayList<View> initializeScreen(View v) throws Exception {
		if (!(v instanceof ViewGroup)) {
			ArrayList<View> viewArrayList = new ArrayList<View>();
			viewArrayList.add(v);

			return viewArrayList;
		}

		ArrayList<View> result = new ArrayList<View>();

		ViewGroup viewGroup = (ViewGroup) v;
		for (int i = 0; i < viewGroup.getChildCount(); i++) {

			View child = viewGroup.getChildAt(i);

			ArrayList<View> viewArrayList = new ArrayList<View>();
			viewArrayList.add(v);
			viewArrayList.addAll(initializeScreen(child));

			result.addAll(viewArrayList);
		}
		if (v instanceof Spinner) {
			aq.id(v.getId()).itemSelected(this, "onSpinnerClicked");
		}
		return result;
	}

	private void initialView() throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
//		aq.id(R.id.listwomenweekly).gone();
		todaysDate = Partograph_CommonClass.getTodaysDate();
		option = 2;

		// updated on 23july2016 by Arpitha
//		aq.id(R.id.txtnowoman).gone();

		setOrResetAndCall();
	}

	class CountDownRunner implements Runnable {
		// @Override
		public void run() {
			while (!Thread.currentThread().isInterrupted()) {
				try {
					doWork();
					Thread.sleep(1000); // Pause of 1 Second
				} catch (InterruptedException e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					Thread.currentThread().interrupt();
				} catch (Exception e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
				}
			}
		}
	}

	public void doWork() throws Exception {
		if (getActivity() != null) {
			getActivity().runOnUiThread(new Runnable() {
				public void run() {
					try {
						// loadWomendata();
						if (adapter != null) {
							adapter.notifyDataSetChanged();
						}
					} catch (Exception e) {
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}
				}
			});
		}
	}

	// Load women Data
	/*
	 * private void loadWomendata() throws Exception { AppContext.addToTrace(
	 * new RuntimeException().getStackTrace()[0].getMethodName() + " - " +
	 * this.getClass().getSimpleName()); rowItems = new
	 * ArrayList<Women_Profile_Pojo>(); Women_Profile_Pojo wdata;
	 * 
	 * 
	 *//**
		 * adapter = new RecentCustomListAdapter( getActivity(),
		 * R.layout.activity_womenlist, rowItems, dbh);
		 * aq.id(R.id.listwomenweekly).adapter(adapter);
		 * adapter.notifyDataSetChanged();
		 * 
		 * 
		 * 
		 * if(rddeltypetoday != null) delstatus_today = rddeltypetoday;
		 * 
		 * Cursor cursor = dbh.getWomenDataByDate(option, todaysDate,
		 * user.getUserId(), delstatus_today, 0, 0, "", "");
		 *//*
		 * 
		 * Cursor cursor = dbh.getdipgt24hrs(user.getUserId()); if
		 * (cursor.getCount() > 0) {
		 * 
		 * cursor.moveToFirst();
		 * 
		 * do { wdata = new Women_Profile_Pojo();
		 * wdata.setWomenId(cursor.getString(1));
		 * 
		 * if (cursor.getType(2) > 0) { String b = (cursor.getString(2).length()
		 * > 1) ? cursor.getString(2) : null; byte[] decoded = (b != null) ?
		 * Base64.decode(b, Base64.DEFAULT) : null;
		 * wdata.setWomen_Image(decoded); }
		 * 
		 * wdata.setWomen_name(cursor.getString(3));
		 * wdata.setDate_of_admission(cursor.getString(4));
		 * wdata.setTime_of_admission(cursor.getString(5));
		 * wdata.setAge(cursor.getInt(6));
		 * wdata.setAddress(cursor.getString(7));
		 * wdata.setPhone_No(cursor.getString(8));
		 * wdata.setDel_type(cursor.getInt(9));
		 * wdata.setDoc_name(cursor.getString(10));
		 * wdata.setNurse_name(cursor.getString(11));
		 * wdata.setW_attendant(cursor.getString(12));
		 * wdata.setGravida(cursor.getInt(13));
		 * wdata.setPara(cursor.getInt(14));
		 * wdata.setHosp_no(cursor.getString(15));
		 * wdata.setFacility(cursor.getString(16));
		 * wdata.setWomenId(cursor.getString(1));
		 * wdata.setSpecial_inst(cursor.getString(17));
		 * wdata.setUserId(cursor.getString(0));
		 * wdata.setComments(cursor.getString(19)); //
		 * wdata.setCond_while_admn(cursor.getString(20));
		 * wdata.setRisk_category(cursor.getInt(20));
		 * wdata.setDel_Comments(cursor.getString(21));
		 * wdata.setDel_Time(cursor.getString(22));
		 * wdata.setDel_Date(cursor.getString(23));
		 * wdata.setDel_result1(cursor.getInt(24));
		 * wdata.setNo_of_child(cursor.getInt(25));
		 * wdata.setBabywt1(cursor.getInt(26));
		 * wdata.setBabywt2(cursor.getInt(27));
		 * wdata.setBabysex1(cursor.getInt(28));
		 * wdata.setBabysex2(cursor.getInt(29));
		 * wdata.setGestationage(cursor.getInt(30));
		 * wdata.setMothersdeath((cursor.getInt(32)) == 1 ? 1 : 0);
		 * wdata.setDel_result2(cursor.getInt(31));
		 * wdata.setAdmitted_with(cursor.getString(34));
		 * wdata.setMemb_pres_abs(cursor.getInt(35));
		 * wdata.setMothers_death_reason(cursor.getString(36));
		 * wdata.setState(cursor.getString(38));
		 * wdata.setDistrict(cursor.getString(39));
		 * wdata.setTaluk(cursor.getString(40));
		 * wdata.setFacility_name(cursor.getString(41));
		 * wdata.setGest_age_days(cursor.getInt(45));
		 * 
		 * // updated on 23Nov2015 wdata.setDel_time2(cursor.getString(33));
		 * 
		 * // updated on 30Nov2015
		 * wdata.setThayicardnumber(cursor.getString(46));
		 * wdata.setDelTypeReason(cursor.getString(47));
		 * wdata.setDeltype_otherreasons(cursor.getString(48));
		 * 
		 * // 27dec2015 wdata.setLmp(cursor.getString(49));
		 * wdata.setRiskoptions(cursor.getString(50));
		 * 
		 * // 04jan2016 wdata.setTears(cursor.getString(51));
		 * wdata.setEpisiotomy(cursor.getInt(52));
		 * wdata.setSuturematerial(cursor.getInt(53));
		 * 
		 * // 19jan2016 wdata.setBloodgroup(cursor.getInt(54));
		 * 
		 * wdata.setEdd(cursor.getString(55));
		 * wdata.setHeight(cursor.getString(56)); //
		 * wdata.setWeight(cursor.getInt(57));
		 * wdata.setW_weight(cursor.getString(57));
		 * wdata.setOther(cursor.getString(58));
		 * 
		 * // updated on 29May by Arpitha
		 * wdata.setHeight_unit(cursor.getInt(59));
		 * 
		 * wdata.setExtracomments(cursor.getString(60));// 13Oct2016 // Arpitha
		 * 
		 * wdata.setregtype(cursor.getInt(61));// 16Oct2016 Arpitha
		 * 
		 * wdata.setRegtypereason(cursor.getString(62));// 18Oct2016 // Arpitha
		 * 
		 * boolean isDanger = dbh.chkIsDanger(cursor.getString(1),
		 * user.getUserId());
		 * 
		 * wdata.setDanger(isDanger);
		 * 
		 * rowItems.add(wdata); } while (cursor.moveToNext());
		 * 
		 * adapter = new RecentCustomListAdapterDIP24(getActivity(),
		 * R.layout.activity_womenlist, rowItems, dbh);
		 * aq.id(R.id.listwomenweekly).adapter(adapter);
		 * 
		 * // cursor.close(); }
		 * 
		 * if (rowItems.size() <= 0) { aq.id(R.id.listwomenweekly).gone();
		 * aq.id(R.id.txtnowoman).visible(); } else {
		 * aq.id(R.id.listwomenweekly).visible(); aq.id(R.id.txtnowoman).gone();
		 * }
		 * 
		 * 
		 * if(rddeltypetoday != null) {
		 * if(rddeltypetoday.equalsIgnoreCase(getResources().getString(R.string.
		 * del_prog))){ rd_delprogtoday.setChecked(true);
		 * rd_deliveredtoday.setChecked(false); } else {
		 * rd_deliveredtoday.setChecked(true);
		 * rd_delprogtoday.setChecked(false); }
		 * 
		 * // }
		 * 
		 * }
		 */

	// Display options on click of list item
	private void displayOptions() {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		try {

			ActionItem viewprofile_Item = new ActionItem(ID_PROFILE, getResources().getString(R.string.view_profile),
					getResources().getDrawable(R.drawable.ic_viewprofile_menu));
			ActionItem graph_Item = new ActionItem(ID_GRAPH, getResources().getString(R.string.partograph),
					getResources().getDrawable(R.drawable.ic_add_parto_menu));

			ActionItem deliverystatus_Item = new ActionItem(ID_DELSTATUS,
					getResources().getString(R.string.delivery_status),
					getResources().getDrawable(R.drawable.ic_del_sttaus_menu));
			ActionItem apgarscore_Item = new ActionItem(ID_APGAR, getResources().getString(R.string.apgar_score),
					getResources().getDrawable(R.drawable.ic_apgar_menu));

			// updated on 16Nov2015
			ActionItem referredto_Item = new ActionItem(ID_REFERRAL, getResources().getString(R.string.referrral_info),
					getResources().getDrawable(R.drawable.ic_referral_menu));

			// updated on 26Nov2015
			ActionItem stagesoflabor_Item = new ActionItem(ID_STAGEOFLABOR,
					getResources().getString(R.string.stagesoflabor),
					getResources().getDrawable(R.drawable.thid_fourth_menu));

			// updated on 23dec2015
			ActionItem print_partoitem = new ActionItem(ID_PRINTPARTO, getResources().getString(R.string.printparto),
					getResources().getDrawable(R.drawable.ic_view_graph_menu));
			// updated on 17Oct2016 Arpitha

			ActionItem additonal_details = new ActionItem(ID_ADDITIONALDETAILS,
					getResources().getString(R.string.additional_details),
					getResources().getDrawable(R.drawable.ic_additional));// 06Dec2016

			/*
			 * ActionItem viewprofile_Item = new ActionItem(ID_PROFILE,
			 * getResources().getString(R.string.view_profile),getResources().
			 * getDrawable(R.drawable.ic_viewprofile)); ActionItem graph_Item =
			 * new ActionItem(ID_GRAPH,
			 * getResources().getString(R.string.partograph),getResources().
			 * getDrawable(R.drawable.add_graph));
			 * 
			 * ActionItem deliverystatus_Item = new ActionItem(ID_DELSTATUS,
			 * getResources().getString(R.string.delivery_status),getResources()
			 * .getDrawable(R.drawable.ic_del_status)); ActionItem
			 * apgarscore_Item = new ActionItem(ID_APGAR,
			 * getResources().getString(R.string.apgar_score),
			 * getResources().getDrawable(R.drawable.ic_apgar));
			 * 
			 * // updated on 16Nov2015 ActionItem referredto_Item = new
			 * ActionItem(ID_REFERRAL,
			 * getResources().getString(R.string.referredto),getResources().
			 * getDrawable(R.drawable.ic_referral_old));
			 * 
			 * // updated on 26Nov2015 ActionItem stagesoflabor_Item = new
			 * ActionItem(ID_STAGEOFLABOR,
			 * getResources().getString(R.string.stagesoflabor),getResources().
			 * getDrawable(R.drawable.third_fourth));
			 * 
			 * // updated on 23dec2015 ActionItem print_partoitem = new
			 * ActionItem(ID_PRINTPARTO,
			 * getResources().getString(R.string.printparto),getResources().
			 * getDrawable(R.drawable.chart)); // updated on 17Oct2016 Arpitha
			 * 
			 * ActionItem additonal_details = new
			 * ActionItem(ID_ADDITIONALDETAILS,
			 * getResources().getString(R.string.additional_details));//
			 * 06Dec2016 // Arpitha
			 */
			/*
			 * ActionItem viewprofile_Item = new ActionItem(ID_PROFILE,
			 * getResources().getString(R.string.view_profile)); ActionItem
			 * graph_Item = new ActionItem(ID_GRAPH,
			 * getResources().getString(R.string.partograph));
			 * 
			 * ActionItem deliverystatus_Item = new ActionItem(ID_DELSTATUS,
			 * getResources().getString(R.string.delivery_status)); ActionItem
			 * apgarscore_Item = new ActionItem(ID_APGAR,
			 * getResources().getString(R.string.apgar_score));
			 * 
			 * // updated on 16Nov2015 ActionItem referredto_Item = new
			 * ActionItem(ID_REFERRAL,
			 * getResources().getString(R.string.referredto));
			 * 
			 * // updated on 26Nov2015 ActionItem stagesoflabor_Item = new
			 * ActionItem(ID_STAGEOFLABOR,
			 * getResources().getString(R.string.stagesoflabor));
			 * 
			 * // updated on 23dec2015 ActionItem print_partoitem = new
			 * ActionItem(ID_PRINTPARTO,
			 * getResources().getString(R.string.printparto)); // updated on
			 * 17Oct2016 Arpitha
			 * 
			 * ActionItem additonal_details = new
			 * ActionItem(ID_ADDITIONALDETAILS,
			 * getResources().getString(R.string.additional_details));//
			 * 06Dec2016 // Arpitha // updated on 23dec2015
			 */ mQuickAction.addActionItem(print_partoitem);
			mQuickAction.addActionItem(graph_Item);
			mQuickAction.addActionItem(deliverystatus_Item);
			mQuickAction.addActionItem(apgarscore_Item);

			// updated on 26Nov2015
			mQuickAction.addActionItem(stagesoflabor_Item);

			// changed by Arpitha 26Feb2016
			mQuickAction.addActionItem(referredto_Item);

			mQuickAction.addActionItem(additonal_details);// 06Dec2016 Arpitha

			mQuickAction.addActionItem(viewprofile_Item);
			// Arpitha

			// setup the action item click listener
			mQuickAction.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
				@Override
				public void onItemClick(QuickAction quickAction, int pos, int actionId) {

					try {
						if (actionId == ID_PROFILE) { // Message item
														// selected

							if (Partograph_CommonClass.autodatetime(getActivity())) {// 27Sep2016
																						// Arpitha
								Intent view = new Intent(getActivity(), ViewProfile_Activity.class);
								view.putExtra("woman", woman);
								startActivity(view);
							}

						}
						if (actionId == ID_GRAPH) {
							if (Partograph_CommonClass.autodatetime(getActivity())) {// 27Sep2016
																						// Arpitha

								Intent graph = new Intent(getActivity(), SlidingActivity.class);
								graph.putExtra("woman", woman);
								startActivity(graph);
							}

						}
						if (actionId == ID_DELSTATUS) {

							if (Partograph_CommonClass.autodatetime(getActivity())) {// 27Sep2016
																						// Arpitha

								if (woman != null) {

									Intent graph = new Intent(getActivity(), DeliveryInfo_Activity.class);
									graph.putExtra("woman", woman);
									startActivity(graph);
								}
							}

						}

						if (actionId == ID_APGAR) {

							if (Partograph_CommonClass.autodatetime(getActivity())) {// 27Sep2016
																						// Arpitha

								if (woman.getDel_type() != 0 &&

								(woman.getDel_result1() != 0) || (woman.getDel_result2() != 0)) {
									Intent viewapgar = new Intent(getActivity(), Activity_Apgar.class);
									viewapgar.putExtra("woman", woman);
									startActivity(viewapgar);
								} else {
									Toast.makeText(getActivity(),
											getResources().getString(R.string.msg_applicableafterdelivery),
											Toast.LENGTH_SHORT).show();
								}
							}

						}

						// updated on 16Nov2015
						if (actionId == ID_REFERRAL) {
							if (Partograph_CommonClass.autodatetime(getActivity())) {// 27Sep2016
																						// Arpitha
								Intent ref = new Intent(getActivity(), ReferralInfo_Activity.class);
								ref.putExtra("woman", woman);
								startActivity(ref);

							}

						}

						// updated on 26Nov2015
						if (actionId == ID_STAGEOFLABOR) {
							if (Partograph_CommonClass.autodatetime(getActivity())) {// 27Sep2016
																						// Arpitha
								if (woman.getDel_type() != 0 &&

								(woman.getDel_result1() != 0) || (woman.getDel_result2() != 0)) {
									Intent stagesoflabor = new Intent(getActivity(), StageofLabor.class);
									stagesoflabor.putExtra("woman", woman);
									startActivity(stagesoflabor);
								} else {
									Toast.makeText(getActivity(),
											getResources().getString(R.string.msg_applicableafterdelivery),
											Toast.LENGTH_SHORT).show();
								}
							}

						}

						// updated bindu - 26july2016
						if (actionId == ID_PRINTPARTO) {
							try {

								Intent view_partograph = new Intent(getActivity(), View_Partograph.class);
								view_partograph.putExtra("woman", woman);
								startActivity(view_partograph);

							} catch (Exception e) {
								e.printStackTrace();
							}
						}

						// 06Dec2016 Arpitha
						if (actionId == ID_ADDITIONALDETAILS) {

							Intent addtionalDetails = new Intent(getActivity(), AdditionalDetails_Activity.class);
							addtionalDetails.putExtra("woman", woman);

							startActivity(addtionalDetails);
						} // 06Dec2016 Arpitha

					} catch (NotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
			});

			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

	}

	@Override
	public void fragmentVisible() {
		// TODO Auto-generated method stub
		try {
			if (adapter != null) {
				adapter.notifyDataSetChanged();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	// 27Oct2016-bindu
	private void setOrResetAndCall() {
		index = 0;
		totalCount = 0;
		listviewFocusItemPosition = 0;
		isUpdating = false;
		isfirstLoad = true;
		arrDispWList = new ArrayList<Women_Profile_Pojo>();
		new LoadList().execute();
	}

	class LoadList extends AsyncTask<String, String, ArrayList<Women_Profile_Pojo>> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// mProgressDialog = ProgressDialog.show(getActivity(), "",
			// (getResources().getString(R.string.data_is_loading)), true,
			// false);
		}

		@Override
		protected ArrayList<Women_Profile_Pojo> doInBackground(String... aurl) {
			try {

				if (isfirstLoad) {
					isUpdating = true;
					return populateWomenList();
				} else {
					isUpdating = true;
					return prepareRowItemsForDisplay();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(ArrayList<Women_Profile_Pojo> list) {
			try {
				// mProgressDialog.dismiss();
				if (isfirstLoad) {
					arrDispWList.addAll(list);
					createOrRefereshList(arrDispWList);
					isfirstLoad = false;
					isUpdating = false;
					if (totalCount > index)
						index = index + VIEW_COUNT;
					else
						index = totalCount - index;
				} else {
					arrDispWList.addAll(list);
					createOrRefereshList(arrDispWList);
					aq.id(R.id.listwomenweekly).getListView().setSelection(listviewFocusItemPosition);
					isUpdating = false;
					if (totalCount > index)
						index = index + VIEW_COUNT;
					else
						index = totalCount - index;
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	// 27Oct2016-bindu
	public void createOrRefereshList(ArrayList<Women_Profile_Pojo> rowItems) {
		
		if (rowItems != null && rowItems.size() > 0) {
			aq.id(R.id.listwomenweekly).visible();
			aq.id(R.id.txtnowomandip24).gone();
//			aq.id(R.id.txtdataloadingdip24).gone();// 05Jan2017 Arpitha
			adapter = new RecentCustomListAdapterDIP24(getActivity(), R.layout.activity_womenlist, rowItems, dbh);
			aq.id(R.id.listwomenweekly).adapter(adapter);
		} else {
			aq.id(R.id.listwomenweekly).gone();
			aq.id(R.id.txtnowomandip24).visible();
			aq.id(R.id.txtdataloadingdip24).gone();// 05Jan2017 Arpitha
		}

		aq.id(R.id.listwomenweekly).getListView().setOnScrollListener(new AbsListView.OnScrollListener() {

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
				if (firstVisibleItem + visibleItemCount >= totalItemCount && !isUpdating) {
					if (totalItemCount < totalCount) {
						listviewFocusItemPosition = firstVisibleItem + 2;
						new LoadList().execute();
					}
				}
			}
		});
	}

	public ArrayList<Women_Profile_Pojo> populateWomenList() throws Exception {
		ArrayList<String> womanIdList = null;

		womanIdList = dbh.getWomenIdList(user.getUserId(), 2, "", "");

		return prepareRowItemsForIds();
	}

	private ArrayList<Women_Profile_Pojo> prepareRowItemsForIds(ArrayList<String> womanIdList) throws Exception {
		ArrayList<Women_Profile_Pojo> rowItems = new ArrayList<Women_Profile_Pojo>();

		if (womanIdList != null && womanIdList.size() > 0) {
			String xStr = womanIdList.toString().replace("[", "(");
			xStr = xStr.replace("]", ")");

			mainSql = "Select *  from  tbl_registeredwomen where  user_Id = '" + user.getUserId() + "' and women_id IN "
					+ xStr + " order by dateinserted desc";
			totalCount = dbh.getDisplayWomanListCount(mainSql);

			rowItems = dbh.getDisplayWomanList(mainSql + " limit " + index + ", " + VIEW_COUNT + " ");
		}

		return rowItems;
	}

	// 08Nov2016
	private ArrayList<Women_Profile_Pojo> prepareRowItemsForIds() throws Exception {
		ArrayList<Women_Profile_Pojo> rowItems = new ArrayList<Women_Profile_Pojo>();

		mainSql = "Select * from " + Partograph_DB.TBL_REGISTEREDWOMEN + " where " + Partograph_DB.DEL_STATUS
				+ " is null and regtype!=2 and user_Id = '" + user.getUserId()
				+ "' and ((strftime('%s', datetime('now', 'localtime')) - strftime('%s', date_of_adm  || ' '  || time_of_adm)) / (60) )>1440 and women_id NOT IN(Select women_id from tbl_referral) order by dateinserted desc";

		totalCount = dbh.getDisplayWomanListCount(mainSql);

		rowItems = dbh.getDisplayWomanList(mainSql + " limit " + index + ", " + VIEW_COUNT + " ");

		return rowItems;
	}

	/**
	 * get the Display pojo list from the given womanIds
	 * 
	 * @throws Exception
	 */
	private ArrayList<Women_Profile_Pojo> prepareRowItemsForDisplay() throws Exception {

		ArrayList<Women_Profile_Pojo> rowItems = dbh
				.getDisplayWomanList(mainSql + " limit " + index + ", " + VIEW_COUNT + " ");

		return rowItems;

	}

	@Override
	public boolean onQueryTextChange(String newText) {
		// TODO Auto-generated method stub
		if (adapter != null) {
			adapter.getFilter().filter(newText);
		}
		return true;
	}

	@Override
	public boolean onQueryTextSubmit(String query) {
		// TODO Auto-generated method stub
		return true;
	}

}
