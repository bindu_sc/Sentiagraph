package com.bc.partograph.regwomenview;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.bc.partograph.common.AppContext;
import com.bc.partograph.common.Partograph_CommonClass;
import com.bc.partograph.common.Partograph_DB;
import com.bc.partograph.sliding.SlidingActivity;
import com.bluecrimson.partograph.R;
import com.bluecrimson.partograph.Women_Profile_Pojo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ExpandableListAdapter extends BaseExpandableListAdapter {

	private Activity context;
	private Map<String, ArrayList<Women_Profile_Pojo>> CategoryList;
	private Map<String, Integer> MnthName;
	Partograph_DB dbh;
	String todaysDate;
	String currTime;
	// Animation
	Animation animBlink;
	ArrayList<Women_Profile_Pojo> rowItems;

	public ExpandableListAdapter(Activity context, Map<String, Integer> monthList1,
			HashMap<String, ArrayList<Women_Profile_Pojo>> categoryDataCollection1, Partograph_DB dbh) {
		// TODO Auto-generated constructor stub
		this.context = context;
		this.MnthName = monthList1;
		this.CategoryList = categoryDataCollection1;
		this.dbh = dbh;
	}

	private class WomenListItem {
		TextView txtWomenname;
		TextView txtstatus;
		TextView txtdate_of_adm;
		TextView txttime_of_adm;
		ImageView imgwphoto;
		ImageView imgdangerSign;
		ImageView imghighrisk;
		ImageView imgmsg;
		TextView txtcommentcount;
		TextView txtdelstatusnotupdated;
		// 10Dec2015
		TextView txtreferred;
		TextView txtdateofdelivery;
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {

		Object returnvalue = null;
		ArrayList<String> lKeys = new ArrayList<String>(MnthName.keySet());
		String xKey = lKeys.get(groupPosition);
		returnvalue = CategoryList.get(xKey).get(childPosition);
		return returnvalue;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		int returnvalue = 0;
		try {
			ArrayList<String> lKeys = new ArrayList<String>(MnthName.keySet());
			String xKey = lKeys.get(groupPosition);
			returnvalue = CategoryList.get(xKey).size();
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
		return returnvalue;
	}

	@Override
	public Object getGroup(int groupPosition) {
		Object returnvalue = null;

		try {
			ArrayList<String> lKeys = new ArrayList<String>(MnthName.keySet());
			String xKey = lKeys.get(groupPosition);
			returnvalue = xKey;
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
		return returnvalue;
	}

	public int getGroupCount() {
		return MnthName.size();
	}

	@Override
	public View getChildView(int groupPosition, int childPosition, boolean isLAstchild, View convertView,
			ViewGroup parent) {

		// TODO Auto-generated method stub
		try {
			WomenListItem holder = null;
			final Women_Profile_Pojo rowItem = (Women_Profile_Pojo) getChild(groupPosition, childPosition);
			LayoutInflater inflater = context.getLayoutInflater();

			if (convertView == null) {
				convertView = inflater.inflate(R.layout.new_womenlist_adapter, null);

				holder = new WomenListItem();
				holder.txtWomenname = (TextView) convertView.findViewById(R.id.txtwname);
				holder.txtstatus = (TextView) convertView.findViewById(R.id.txtstatus);
				holder.txtdate_of_adm = (TextView) convertView.findViewById(R.id.txtDateOfAdm);
				
				 /** holder.txttime_of_adm = (TextView) convertView
				 * .findViewById(R.id.txttimeofadm);*/
				 
				holder.imgwphoto = (ImageView) convertView.findViewById(R.id.imgwphoto);
				holder.imgdangerSign = (ImageView) convertView.findViewById(R.id.imgdangersign);
				holder.imghighrisk = (ImageView) convertView.findViewById(R.id.imgrisk);

				holder.imgmsg = (ImageView) convertView.findViewById(R.id.imgmsg);
				holder.txtcommentcount = (TextView) convertView.findViewById(R.id.txtcommentcount);
				holder.txtdelstatusnotupdated = (TextView) convertView.findViewById(R.id.txtdelstatusnotupdated);
				holder.txtdateofdelivery = (TextView) convertView.findViewById(R.id.txtadddate);
				holder.txtreferred = (TextView) convertView.findViewById(R.id.txtreferred);

				convertView.setTag(holder);
			} else
				holder = (WomenListItem) convertView.getTag();

			holder.imgmsg.setVisibility(View.GONE);

			String[] deltype = context.getResources().getStringArray(R.array.del_type);

			holder.txtWomenname.setText(rowItem.getWomen_name() == null ? " " : rowItem.getWomen_name());
			holder.txtstatus.setText(rowItem.getDel_type() == 0
					? rowItem.getGestationage() + " " + context.getResources().getString(R.string.weeks)
					: deltype[rowItem.getDel_type() - 1]);
			holder.txtdate_of_adm
					.setText(rowItem.getDate_of_admission() == null ? " "
							: context.getResources().getString(R.string.regdate) + " : "
									+ Partograph_CommonClass.getConvertedDateFormat(rowItem.getDate_of_admission(),
											Partograph_CommonClass.defdateformat)
									+ " / " + rowItem.getTime_of_admission());

			boolean isReferred = dbh.getisWomenReferred(rowItem.getWomenId(), Partograph_CommonClass.user.getUserId());
			if (isReferred) {
				holder.txtreferred.setText(context.getResources().getString(R.string.referred));
			} else {
				holder.txtreferred.setText("");
			}

			// load the animation
			animBlink = AnimationUtils.loadAnimation(context, R.anim.blink);

			byte[] image1 = rowItem.getWomen_Image();
			if (image1 != null) {
				Bitmap btmp = Bitmap.createScaledBitmap(BitmapFactory.decodeByteArray(image1, 0, image1.length), 64, 64,
						false);
				holder.imgwphoto.setImageBitmap(btmp);
			}

			int risk_cat = rowItem.getRisk_category();

			if (risk_cat == 1)
				holder.imghighrisk.setImageResource(R.drawable.highrisk);
			else
				holder.imghighrisk.setImageResource(0);// changed on 13May16

			if (rowItem.isDanger())
				holder.imgdangerSign.setImageResource(R.drawable.dangersign_icon);
			else
				holder.imgdangerSign.setImageResource(0);// changed on 13May16

			int comment_count = dbh.getComentsCount(rowItem.getWomenId(), Partograph_CommonClass.user.getUserId());
			if (comment_count > 0) {
				holder.imgmsg.setVisibility(View.VISIBLE);
				holder.txtcommentcount.setText("" + comment_count);
			} else {
				holder.imgmsg.setVisibility(View.GONE);
				holder.txtcommentcount.setText("");
			}

			if (rowItem.getDel_type() == 0) {
				String prevdate = rowItem.getDate_of_admission() + "_" + rowItem.getTime_of_admission();
				todaysDate = Partograph_CommonClass.getTodaysDate();
				currTime = Partograph_CommonClass.getCurrentTime();
				String currdate = todaysDate + "_" + currTime;

				boolean isValid = Partograph_CommonClass.getisValidTime(prevdate, currdate, 1440);

				if (isValid) {
					holder.txtdelstatusnotupdated
							.setText(context.getResources().getString(R.string.delstatusnotupdated));
					holder.txtdelstatusnotupdated.setVisibility(View.VISIBLE);
					holder.txtdelstatusnotupdated.startAnimation(animBlink);
				}

			} else {
				holder.txtdelstatusnotupdated.setVisibility(View.GONE);
				// 10Dec2015
				holder.txtdateofdelivery.setText(rowItem.getDel_Date() == null ? " "
						: context.getResources().getString(R.string.actualdd) + " : " + Partograph_CommonClass
								.getConvertedDateFormat(rowItem.getDel_Date(), Partograph_CommonClass.defdateformat)
								+ " / " + rowItem.getDel_Time());

			}

			holder.imgdangerSign.setOnTouchListener(new View.OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					// TODO Auto-generated method stub

					try {
						loadwomendata(rowItem.getWomenId(), rowItem.getUserId());

						Intent graph = new Intent(context, SlidingActivity.class);
						graph.putExtra("rowitems", rowItems);
						graph.putExtra("position", 0);
						context.startActivity(graph);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}
					return false;
				}
			});

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}

		return convertView;
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		String MnthName = (String) getGroup(groupPosition);
		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.group_item, null);
		}
		TextView item = (TextView) convertView.findViewById(R.id.txtMnthName);
		item.setTypeface(null, Typeface.BOLD);
		int strId = context.getResources().getIdentifier("Jan", "string", context.getPackageName());
		item.setText(context.getResources().getString(strId));

		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return true;
	}

	public void unregisterDataSetObserver(DataSetObserver observer) {
		if (observer != null) {
			super.unregisterDataSetObserver(observer);
		}
	}

	protected void loadwomendata(String womenid, String userid) throws Exception {
		rowItems = new ArrayList<Women_Profile_Pojo>();
		Women_Profile_Pojo wdata;
		Cursor cursor = null;
		//= dbh.getYearlyWomenData(womenid, userid, "");
		if (cursor.getCount() > 0) {
			cursor.moveToFirst();

			do {
				wdata = new Women_Profile_Pojo();
				wdata.setWomenId(cursor.getString(1));

				if (cursor.getType(2) > 0) {
					String b = (cursor.getString(2).length() > 1) ? cursor.getString(2) : null;
					byte[] decoded = (b != null) ? Base64.decode(b, Base64.DEFAULT) : null;
					wdata.setWomen_Image(decoded);
				}

				wdata.setWomen_name(cursor.getString(3));
				wdata.setDate_of_admission(cursor.getString(4));
				wdata.setTime_of_admission(cursor.getString(5));
				wdata.setAge(cursor.getInt(6));
				wdata.setAddress(cursor.getString(7));
				wdata.setPhone_No(cursor.getString(8));
				wdata.setDel_type(cursor.getInt(9));
				wdata.setDoc_name(cursor.getString(10));
				wdata.setNurse_name(cursor.getString(11));
				wdata.setW_attendant(cursor.getString(12));
				wdata.setGravida(cursor.getInt(13));
				wdata.setPara(cursor.getInt(14));
				wdata.setHosp_no(cursor.getString(15));
				wdata.setFacility(cursor.getString(16));
				wdata.setWomenId(cursor.getString(1));
				wdata.setSpecial_inst(cursor.getString(17));
				wdata.setUserId(cursor.getString(0));
				wdata.setComments(cursor.getString(19));
				// wdata.setCond_while_admn(cursor.getString(20));
				wdata.setRisk_category(cursor.getInt(20));
				wdata.setDel_Comments(cursor.getString(21));
				wdata.setDel_Time(cursor.getString(22));
				wdata.setDel_Date(cursor.getString(23));
				wdata.setDel_result1(cursor.getInt(24));
				wdata.setNo_of_child(cursor.getInt(25));
				wdata.setBabywt1(cursor.getInt(26));
				wdata.setBabywt2(cursor.getInt(27));
				wdata.setBabysex1(cursor.getInt(28));
				wdata.setBabysex2(cursor.getInt(29));
				wdata.setGestationage(cursor.getInt(30));
				wdata.setMothersdeath((cursor.getInt(32)) == 1 ? 1 : 0);
				wdata.setDel_result2(cursor.getInt(31));
				wdata.setAdmitted_with(cursor.getString(34));
				wdata.setMemb_pres_abs(cursor.getInt(35));
				wdata.setMothers_death_reason(cursor.getString(36));
				wdata.setState(cursor.getString(38));
				wdata.setDistrict(cursor.getString(39));
				wdata.setTaluk(cursor.getString(40));
				wdata.setFacility_name(cursor.getString(41));
				wdata.setGest_age_days(cursor.getInt(45));

				// updated by ARpitha
				wdata.setEdd(cursor.getString(55));
				wdata.setHeight(cursor.getString(56));
				//wdata.setWeight(cursor.getInt(57));
				wdata.setOther(cursor.getString(58));

				rowItems.add(wdata);
			} while (cursor.moveToNext());

		}

	}
}
