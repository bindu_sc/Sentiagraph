package com.bc.partograph.deliveryinfo;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;

import com.bc.partograph.comments.Comments_Activity;
import com.bc.partograph.common.AppContext;
import com.bc.partograph.common.KillAllActivitesAndGoToLogin;
import com.bc.partograph.common.MultiSelectionSpinner;
import com.bc.partograph.common.Partograph_CommonClass;
import com.bc.partograph.common.Partograph_DB;
import com.bc.partograph.latentphase.LatentPhase_Activity;
import com.bc.partograph.login.LoginActivity;
import com.bc.partograph.referralinfo.ReferralInfo_Activity;
import com.bc.partograph.settings.About;
import com.bc.partograph.settings.Settings_parto;
import com.bc.partograph.sliding.GraphInformation;
import com.bc.partograph.sliding.SlidingActivity;
import com.bc.partograph.stagesoflabor.StageofLabor;
import com.bc.partograph.womenview.Activity_WomenView;
import com.bc.partograph.womenview.Fragment_DIP;
import com.bluecrimson.additionalDetails.Summary_Activity;
import com.bluecrimson.apgar.Activity_Apgar;
import com.bluecrimson.dischargedetails.DiscargeDetails_Activity;
import com.bluecrimson.dischargedetails.DiscgargePojo;
import com.bluecrimson.dischargedetails.DischargedWomanList_Activity;
import com.bluecrimson.partograph.R;
import com.bluecrimson.partograph.Women_Profile_Pojo;
import com.bluecrimson.postpartumcare.PostPartumCare_Activity;
import com.bluecrimson.usermanual.UseManual;
import com.bluecrimson.viewprofile.ViewProfile_Activity;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources.NotFoundException;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.Html;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

public class DeliveryInfo_Activity extends FragmentActivity implements OnClickListener {

	EditText etbabywt1, etbabywt2, etdelcomments, etdeldate, etmothersdeathreason, tvmothersdeathreason;
	static String strDeldate;
	static EditText etdeltime2;
	static boolean del_time1;
	static EditText etdeltime;
	TableRow trsuturematerial;
	// episiotomy default value is made as 0 before it was 1 updated on
	// 12july2016 by Arpitha
	int episiotomy = 0, suturematerial = 0;
	Spinner spndel_type, spn_delres1, spn_delres2, spn_noofchild;
	TableRow trchilddet1, tr_childdetsex1, tr_childdet2, tr_childsex2, tr_secondchilddetails, tr_result2, tr_deltime2;
	TextView txtfirstchilddetails;
	int delivery_type, delivery_result1, delivery_result2, babysex1, babysex2;
	TableRow tr_deltypereason, tr_deltypeotherreasons;
	RadioButton rdmale1, rdfemale1, rdmale2, rdfemale2;
	RadioButton rdmotheralive, rdmotherdead;
	TextView txtdeltype, txtnoofchild, txtdelres1, txtchildwt1, txtchildsex1, txtdelres2, txtchildwt2, txtchildsex2,
			txtdelcomments, txtdeldate, txtdeltime, txtdeltime2, txtmothersdeathreason;
	Button imgbtnok;
	boolean isDelUpdate = false;
	int numofchildren;
	MultiSelectionSpinner mspndeltypereason;
	LinkedHashMap<String, Integer> deltypereasonMap;
	EditText etdeltypeotherreasons;
	MultiSelectionSpinner mspntears;
	LinkedHashMap<String, Integer> tearsMap;
	RadioGroup rdchildSexGrpbaby1, rdchildSexGrpbaby2;
	EditText txtdeltypereason, txtdeltypeotherreasons;
	EditText ettears;
	RadioButton rd_babyalive, rd_babydead, rdepiyes, rdepino, rdcatgut, rdvicryl;
	ImageView imgwt1_warning, imgwt2_warning;
	TextView txtwname, txtwage, txtwdoa, txtwtoa, txtwgest, txtwgravida, txtwrisk;
	ImageView imgdelresult, imgsex2, imgdeltime2, wardeltype, wardaltypereason;
	Partograph_DB dbh;

	String todaysDate, womenId, wUserId;
	String no_of_child, selecteddate;
	private static int hour;
	private static int minute;
	int year, mon, day;
	static String strlastentrytime = null;
	int weight1, weight2;
	static String strRegdate;
	static EditText ettimeofreferral;
	static final int TIME_DIALOG_ID = 999;
	static String strrefdatetime = null;
	Date refdatetime;

	Women_Profile_Pojo woman;

	RadioButton rdvertext, rdbreech;// 20NOv2016 Arpitha
	int normaltype = 1;// 20NOv2016 Arpitha
	TextView txtnormaltype;// 22Nov2016 Arpitha
	// ImageView imgnormaltype;// 22Nov2016 Arpitha

	RadioGroup rdnormaltype;

	String presentation;// 11May2017 Arpitha - v2.6
	boolean vertex;// 11May2017 Arpitha - v2.6

	TextView txtdisabled;// 10Aug2017 Arpitha
	RelativeLayout reldel;// 10Aug2017 Arpitha

	public static boolean isDel;
	public static int delscreen;
	RadioGroup rdsuture;// 11Sep2017 Arpitha

	String strlatdatetime;// 20Sep2017 Arpitha
	Date latDatetime;// 20Sep2017 Arpitha

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.edit_status_dialog);
		try {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());

			woman = (Women_Profile_Pojo) getIntent().getSerializableExtra("woman");

			dbh = new Partograph_DB(getApplicationContext());

			delscreen = Partograph_CommonClass.curr_tabpos;// 03Sep2017 Arpitha
			isDel = true;// 03Sep2017 Arpitha

			// 05Nov2016 Arpitha
			Locale locale = null;

			if (AppContext.prefs.getBoolean("isEnglish", false)) {
				locale = new Locale("en");
				Locale.setDefault(locale);
			} else if (AppContext.prefs.getBoolean("isHindi", false)) {
				locale = new Locale("hi");
				Locale.setDefault(locale);
			}

			Configuration config = new Configuration();
			config.locale = locale;
			getResources().updateConfiguration(config, getResources().getDisplayMetrics());

			AppContext.setNamesAccordingToRes(getResources());// changed on
																// 30March2017
																// Arpitha

			// 02nov2016 Arpitha
			// get the action bar
			ActionBar actionBar = getActionBar();

			this.setTitle(getResources().getString(R.string.delivery_status));

			// Enabling Back navigation on Action Bar icon
			actionBar.setDisplayHomeAsUpEnabled(true);// 02nov2016 Arpitha

			womenId = woman.getWomenId();
			wUserId = woman.getUserId();
			if (woman.getregtype() != 2)// 12Jan2017 Arpitha
				strRegdate = woman.getDate_of_admission() + " " + woman.getTime_of_admission();
			else
				strRegdate = woman.getDate_of_reg_entry().split("/")[0] + " "
						+ woman.getDate_of_reg_entry().split("/")[1];

			AssignIdsToWidgets();

			initialViewDialog();

			etbabywt1.addTextChangedListener(watcher);
			etbabywt2.addTextChangedListener(watcher);

			strlastentrytime = dbh.getlastentrytime(womenId, 0);

			rdepiyes.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					trsuturematerial.setVisibility(View.VISIBLE);
					rdcatgut.setVisibility(View.VISIBLE);
					rdvicryl.setVisibility(View.VISIBLE);
					episiotomy = 1;
				}
			});

			rdepino.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					trsuturematerial.setVisibility(View.GONE);
					rdcatgut.setVisibility(View.GONE);
					rdvicryl.setVisibility(View.GONE);
					// cmented on 11Jan2017 Arpitha
					// rdcatgut.setEnabled(false);// 19Nov2016 Arpitha
					// cmented on 11Jan2017 Arpitha
					// rdvicryl.setEnabled(false);// 19Nov2016 Arpitha
					suturematerial = 0;// 19Nov2016 Arpitha
					episiotomy = 0;

					rdsuture.clearCheck();// 11Sep2017 Arpitha
					/*
					 * rdcatgut.setSelected(false);//08Sep2017 Arpitha
					 * rdvicryl.setSelected(false);//08Sep2017 Arpitha
					 */
				}
			});

			rdcatgut.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					suturematerial = 1;
				}
			});

			rdvicryl.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					suturematerial = 2;
				}
			});

			// Num of children
			spn_noofchild.setOnItemSelectedListener(new OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> adapter, View view, int pos, long id) {
					no_of_child = (String) adapter.getSelectedItem();

					if (pos == 0) {
						trchilddet1.setVisibility(View.VISIBLE);
						tr_childdetsex1.setVisibility(View.VISIBLE);
						tr_secondchilddetails.setVisibility(View.GONE);
						tr_result2.setVisibility(View.GONE);
						tr_childdet2.setVisibility(View.GONE);
						tr_childsex2.setVisibility(View.GONE);
						tr_deltime2.setVisibility(View.GONE);

						txtfirstchilddetails.setText(getResources().getString(R.string.child_details));

						etdeltime2.setVisibility(View.GONE);
						rdchildSexGrpbaby2.setVisibility(View.GONE);
						etbabywt2.setVisibility(View.GONE);
						imgwt2_warning.setVisibility(View.GONE);
						spn_delres2.setVisibility(View.GONE);
						imgdelresult.setVisibility(View.GONE);
						imgsex2.setVisibility(View.GONE);
						imgdeltime2.setVisibility(View.GONE);

					} else {
						tr_childdet2.setVisibility(View.VISIBLE);
						tr_childsex2.setVisibility(View.VISIBLE);
						tr_secondchilddetails.setVisibility(View.VISIBLE);
						tr_result2.setVisibility(View.VISIBLE);
						trchilddet1.setVisibility(View.VISIBLE);
						tr_childdetsex1.setVisibility(View.VISIBLE);
						tr_deltime2.setVisibility(View.VISIBLE);

						txtfirstchilddetails.setText(getResources().getString(R.string.first_child_details));

						etdeltime2.setVisibility(View.VISIBLE);
						rdchildSexGrpbaby2.setVisibility(View.VISIBLE);
						etbabywt2.setVisibility(View.VISIBLE);
						imgwt2_warning.setVisibility(View.INVISIBLE);
						spn_delres2.setVisibility(View.VISIBLE);
						imgdelresult.setVisibility(View.INVISIBLE);
						imgsex2.setVisibility(View.INVISIBLE);
						imgdeltime2.setVisibility(View.INVISIBLE);
					}

				}

				@Override
				public void onNothingSelected(AdapterView<?> arg0) {

				}
			});

			mspndeltypereason.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					// TODO Auto-generated method stub
					Fragment_DIP.istears = false;// 29Sep2016 ARpitha

					return false;
				}
			});
			// 29Sep2016 ARpitha
			mspntears.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					// TODO Auto-generated method stub
					Fragment_DIP.istears = true;// 29Sep2016 ARpitha

					return false;
				}
			});

			Fragment_DIP.istears = false;// 06Nov2016 Arpitha

			// Delivery type
			spndel_type.setOnItemSelectedListener(new OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> adapter, View arg1, int arg2, long arg3) {
					// delivery_type = (String) adapter.getSelectedItem();
					delivery_type = adapter.getSelectedItemPosition() + 1;
					// 25Nov2015
					try {
						if (delivery_type > 1) {
							tr_deltypereason.setVisibility(View.VISIBLE);
							tr_deltypeotherreasons.setVisibility(View.VISIBLE);
							setDelTypeReason(delivery_type);

							mspndeltypereason.setVisibility(View.VISIBLE);
							wardeltype.setVisibility(View.INVISIBLE);
							etdeltypeotherreasons.setVisibility(View.VISIBLE);
							wardaltypereason.setVisibility(View.INVISIBLE);// 17Nov2016
																			// Arpitha
							// 20Nov2016 Arpitha
							rdbreech.setVisibility(View.GONE);
							rdvertext.setVisibility(View.GONE);
							rdnormaltype.clearCheck();// 11Jan2017 Arpitha
							if (woman.getNormaltype() != 2)// 11Aug2017 Arpitha
								normaltype = 0;// 20Nov2016 Arpitha
							txtnormaltype.setVisibility(View.GONE);// 22Nov2016
																	// Arpitha

							// imgnormaltype.setVisibility(View.GONE);//
							// 22Nov2016
							// Arpitha

							if (woman.getNormaltype() == 2)// 11May2017 Arpitha
															// -v2.6 mantis
															// id-0000231
							{
								if (delivery_type == 2) {
									mspndeltypereason.setSelection(5);
								}
							} // 11May2017 Arpitha -v2.6 mantis id-0000231

						} else {
							tr_deltypereason.setVisibility(View.GONE);
							tr_deltypeotherreasons.setVisibility(View.GONE);

							mspndeltypereason.setVisibility(View.GONE);
							etdeltypeotherreasons.setVisibility(View.GONE);
							wardeltype.setVisibility(View.GONE);
							wardaltypereason.setVisibility(View.GONE);// 17Nov2016
																		// Arpitha
							// wardeltype.setVisibility(View.GONE);

							rdbreech.setVisibility(View.VISIBLE);// 20Nov2016
																	// Arpitha
							rdvertext.setVisibility(View.VISIBLE);// 20Nov2016
																	// Arpitha

							if (woman.getNormaltype() != 2) {// 05May2017
																// Arpitha -v2.6
																// mantis
																// id-0000231
								rdvertext.setChecked(true);// 20Nov2016 Arpitha
								rdbreech.setChecked(false);// 27Nov2016 Arpitha
								normaltype = 1;
							} else {// 05May2017 Arpitha -v2.6 mantis id-0000231
								rdvertext.setChecked(false);// 20Nov2016 Arpitha
								rdbreech.setChecked(true);// 27Nov2016 Arpitha
								normaltype = 2;

							} // 05May2017 Arpitha -v2.6 mantis id-0000231

							txtnormaltype.setVisibility(View.VISIBLE);// 22Nov2016
																		// Arpitha

							// imgnormaltype.setVisibility(View.INVISIBLE);//
							// 11Dec2016
							// Arpitha

						}

						vertex = rdvertext.isChecked();// 11May2017 Arpitha-
														// v2.6
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

				@Override
				public void onNothingSelected(AdapterView<?> arg0) {

				}
			});

			// Delivery result 1
			spn_delres1.setOnItemSelectedListener(new OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> adapter, View view, int pos, long id) {
					delivery_result1 = adapter.getSelectedItemPosition() + 1;

				}

				@Override
				public void onNothingSelected(AdapterView<?> arg0) {

				}
			});

			// Delivery result 2

			spn_delres2.setOnItemSelectedListener(new OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> adapter, View arg1, int arg2, long arg3) {
					delivery_result2 = adapter.getSelectedItemPosition() + 1;

				}

				@Override
				public void onNothingSelected(AdapterView<?> arg0) {

				}
			});

			rdmale1.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					babysex1 = 0;

				}
			});

			rdfemale1.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					babysex1 = 1;
				}
			});

			rdmale2.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					babysex2 = 0;
				}
			});

			rdfemale2.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					babysex2 = 1;
				}
			});

			etdeldate.setOnTouchListener(new View.OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if (MotionEvent.ACTION_UP == event.getAction()) {
						try {

							caldatepicker();
						} catch (Exception e) {
							AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
									+ this.getClass().getSimpleName(), e);
							e.printStackTrace();
						}
					}
					return true;
				}
			});

			etdeltime.setOnTouchListener(new View.OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if (MotionEvent.ACTION_UP == event.getAction()) {
						del_time1 = true;
						showDialog(TIME_DIALOG_ID);
					}
					return true;
				}
			});

			etdeltime2.setOnTouchListener(new View.OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if (MotionEvent.ACTION_UP == event.getAction()) {
						del_time1 = false;
						showDialog(TIME_DIALOG_ID);
					}
					return true;
				}
			});

			rdmotheralive.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					txtmothersdeathreason.setVisibility(View.GONE);
					etmothersdeathreason.setVisibility(View.GONE);
					etmothersdeathreason.setText("");
				}
			});

			rdmotherdead.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					txtmothersdeathreason.setVisibility(View.VISIBLE);
					etmothersdeathreason.setVisibility(View.VISIBLE);

					etmothersdeathreason.requestFocus();
				}
			});

			final Calendar c = Calendar.getInstance();
			hour = c.get(Calendar.HOUR_OF_DAY);
			minute = c.get(Calendar.MINUTE);

			Button imgbtncancel = (Button) findViewById(R.id.btnclear);

			imgbtncancel.setText(getResources().getString(R.string.clear));

			// 06OCt2016 Arpitha

			imgbtncancel.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

				}
			});

			imgbtnok.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {

					// String updateres = "";

					try {

						weight1 = 0;
						weight2 = 0;

						if (etbabywt1.getText().toString().trim().length() > 0) {
							weight1 = Integer.parseInt(etbabywt1.getText().toString());
						}
						if (etbabywt2.getText().toString().trim().length() > 0) {
							weight2 = Integer.parseInt(etbabywt2.getText().toString());
						}

						if (validateDelFields()) {
							if ((weight1 > 0 && (weight1 < 2500 || weight1 > 4000))
									|| (weight2 > 0 && (weight2 < 2500 || weight2 > 4000))) {

								displayConfirmationAlert("", getResources().getString(R.string.deldata));// 05Oct2016
																											// Arpitha
																											// -
																											// string
																											// value
																											// from
																											// strings.xml
							}

							else {

								if (Partograph_CommonClass.autodatetime(getApplicationContext())) {

									Date lastregdate = null;
									String strlastinserteddate = dbh.getlastmaxdate(wUserId,
											getResources().getString(R.string.viewprofile));// 02Oct2016
																							// Arpitha
																							// -
																							// string
																							// value
																							// from
																							// strings.xml
									SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd_HHmmss");
									if (strlastinserteddate != null) {
										lastregdate = format.parse(strlastinserteddate);
									}
									Date currentdatetime = new Date();
									if (lastregdate != null && currentdatetime.before(lastregdate)) {
										Partograph_CommonClass.showSettingsAlert(getApplicationContext(), getResources()
												.getString(R.string.plz_enable_automatic_date_and_set_current_date));
									} else {

										String msg = getResources().getString(R.string.deldatetimerecheck)
												+ etdeldate.getText().toString() + " / "
												+ etdeltime.getText().toString()
												+ getResources().getString(R.string.confirmrecheck);
										String del_datetime = strDeldate + "_" + etdeltime.getText().toString();
										int duration = 0;
										if (strlastentrytime != null) {
											strlastentrytime = strlastentrytime.replace(" ", "_");
											duration = Partograph_CommonClass.getHoursBetDates(del_datetime,
													strlastentrytime);
										}
										if (duration > 12) {
											msg = getResources()
													.getString(R.string.deldatetime_twelvehour_after_lastparto_entry);
										}

										ConfirmAlert(msg, getResources().getString(R.string.deldata));// 02Oct2016
																										// Arpitha
																										// -
																										// string
																										// value
																										// from
																										// strings.xml

									}
								}
							}
						}
					} catch (Exception e) {
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}

				}
			});

			imgbtncancel.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					try {
						displayConfirmationAlert("", getResources().getString(R.string.clear));
					} catch (NotFoundException e) {
						// TODO Auto-generated catch block
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}
				}
			});

			vertex = rdvertext.isChecked();

			// 20Nov2016 Arpitha
			rdbreech.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					try {

						// 11May2017 Arpitha -v2.6
						presentation = getResources().getString(R.string.vertex);
						if (woman.getDel_type() == 0 && vertex && woman.getNormaltype() == 1)
							displayAlertDialog(getResources().getString(R.string.you_have_selected) + " "
									+ getResources().getString(R.string.vertex) + " "
									+ getResources().getString(R.string.during_reg) + " "
									+ getResources().getString(R.string.are_you_sure_want_to_change), "breech");
						/*
						 * Toast.makeText(getApplicationContext(),
						 * getResources().getString(R.string.you_have_selected)
						 * + " " + presentation + " " +
						 * getResources().getString(R.string.during_reg),
						 * Toast.LENGTH_LONG).show();// 11May2017 // Arpitha
						 * -v2.6
						 */
						else
							normaltype = 2;

					} catch (Exception e) {
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}

				}
			});

			rdvertext.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					try {
						// 11May2017 Arpitha -v2.6
						presentation = getResources().getString(R.string.breech);
						if (woman.getDel_type() == 0 && (!vertex) && woman.getNormaltype() == 2)
							displayAlertDialog(getResources().getString(R.string.you_have_selected) + " "
									+ getResources().getString(R.string.breech) + " "
									+ getResources().getString(R.string.during_reg) + " "
									+ getResources().getString(R.string.are_you_sure_want_to_change), "vertex");
						/*
						 * Toast.makeText( getApplicationContext(),
						 * getResources().getString(R.string.you_have_selected)
						 * + " " + presentation + " " +
						 * getResources().getString(R.string.during_reg),
						 * Toast.LENGTH_LONG).show();// 11May2017 // Arpitha
						 * -v2.6
						 */ else
							normaltype = 1;
					} catch (Exception e) {
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}

				}
			});// 20Nov2016 Arpitha

			setInputFiltersForEdittext();// 10April2017 Arpitha

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}

		KillAllActivitesAndGoToLogin.addToStack(this);
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub

	}

	// Assign Ids To Widgets
	private void AssignIdsToWidgets() throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		etdelcomments = (EditText) findViewById(R.id.etdelcomments);
		etdeldate = (EditText) findViewById(R.id.etdeldate);
		etdeltime = (EditText) findViewById(R.id.etdeltime);
		etdeltime2 = (EditText) findViewById(R.id.etdeltime2);
		etbabywt1 = (EditText) findViewById(R.id.etchildwt1);
		etbabywt2 = (EditText) findViewById(R.id.etchildwt2);

		ettimeofreferral = (EditText) findViewById(R.id.ettimeofreferrence);

		rdmale1 = (RadioButton) findViewById(R.id.rd_male1);
		rdfemale1 = (RadioButton) findViewById(R.id.rd_female1);
		rdmale2 = (RadioButton) findViewById(R.id.rd_male2);
		rdfemale2 = (RadioButton) findViewById(R.id.rd_female2);

		tr_childdet2 = (TableRow) findViewById(R.id.tr_childdet2);
		tr_childsex2 = (TableRow) findViewById(R.id.tr_childsex2);
		trchilddet1 = (TableRow) findViewById(R.id.tr_childdet1);
		tr_childdetsex1 = (TableRow) findViewById(R.id.tr_childdetsex1);
		tr_secondchilddetails = (TableRow) findViewById(R.id.tr_secondchilddetails);
		tr_result2 = (TableRow) findViewById(R.id.tr_result2);
		tr_deltime2 = (TableRow) findViewById(R.id.tr_deltime2);

		spn_delres1 = (Spinner) findViewById(R.id.spndelresult);
		spn_delres2 = (Spinner) findViewById(R.id.spndelresult2);
		spndel_type = (Spinner) findViewById(R.id.spndeltype);
		spn_noofchild = (Spinner) findViewById(R.id.spnnoofchild);
		imgbtnok = (Button) findViewById(R.id.btnsave);

		rdchildSexGrpbaby1 = (RadioGroup) findViewById(R.id.rdsex1);
		rdchildSexGrpbaby2 = (RadioGroup) findViewById(R.id.rdsex2);

		etmothersdeathreason = (EditText) findViewById(R.id.etmothersdeathreason);
		txtmothersdeathreason = (TextView) findViewById(R.id.txtmothersdeathreason);

		rdmotheralive = (RadioButton) findViewById(R.id.rdmotheralive);
		rdmotherdead = (RadioButton) findViewById(R.id.rdmotherdead);

		mspndeltypereason = (MultiSelectionSpinner) findViewById(R.id.mspndeltypereason);
		tr_deltypereason = (TableRow) findViewById(R.id.tr_deltypereason);
		tr_deltypeotherreasons = (TableRow) findViewById(R.id.tr_deltypeotherreasons);
		etdeltypeotherreasons = (EditText) findViewById(R.id.etdeltypeotherreasons);

		mspntears = (MultiSelectionSpinner) findViewById(R.id.mspntears);
		rdepiyes = (RadioButton) findViewById(R.id.rd_epiyes);
		rdepino = (RadioButton) findViewById(R.id.rd_epino);
		rdcatgut = (RadioButton) findViewById(R.id.rd_catgut);
		rdvicryl = (RadioButton) findViewById(R.id.rd_vicryl);
		trsuturematerial = (TableRow) findViewById(R.id.tr_suturematerial);

		txtfirstchilddetails = (TextView) findViewById(R.id.txtfirstchilddetails);
		txtdeltypereason = (EditText) findViewById(R.id.txtdeltypereasonval);
		ettears = (EditText) findViewById(R.id.ettears);

		trsuturematerial.setVisibility(View.GONE);

		imgwt1_warning = (ImageView) findViewById(R.id.warbabywt1);
		imgwt2_warning = (ImageView) findViewById(R.id.warbabywt2);

		txtwage = (TextView) findViewById(R.id.wage);
		txtwdoa = (TextView) findViewById(R.id.wdoa);
		txtwgravida = (TextView) findViewById(R.id.wgravida);
		txtwname = (TextView) findViewById(R.id.wname);

		txtwrisk = (TextView) findViewById(R.id.wtrisk);
		txtwtoa = (TextView) findViewById(R.id.wtoa);
		txtwgest = (TextView) findViewById(R.id.wgest);

		imgdelresult = (ImageView) findViewById(R.id.imgdelresult);
		imgsex2 = (ImageView) findViewById(R.id.imgsex2);
		imgdeltime2 = (ImageView) findViewById(R.id.imgdeltime2);
		wardeltype = (ImageView) findViewById(R.id.wardeltype);
		wardaltypereason = (ImageView) findViewById(R.id.wardaltypereason);

		imgdelresult.setVisibility(View.GONE);
		imgwt2_warning.setVisibility(View.GONE);
		imgsex2.setVisibility(View.GONE);
		imgdeltime2.setVisibility(View.GONE);
		wardeltype.setVisibility(View.GONE);
		wardaltypereason.setVisibility(View.GONE);

		rdcatgut.setVisibility(View.GONE);
		rdvicryl.setVisibility(View.GONE);

		rdbreech = (RadioButton) findViewById(R.id.rd_breech);// 20Nov2016
																// Arpitha
		rdvertext = (RadioButton) findViewById(R.id.rd_vertex);// 20Nov2016
																// Arpitha

		txtnormaltype = (TextView) findViewById(R.id.txtnormaltype);// 22Nov2016
																	// Arpitha

		// imgnormaltype = (ImageView) findViewById(R.id.imgnormaltype);//
		// 22Nov2016
		// Arpitha

		rdnormaltype = (RadioGroup) findViewById(R.id.rdnormaltype);// 11Jan2017
																	// Arpitha

		txtdisabled = (TextView) findViewById(R.id.txtdisable);// 10Aug2017
																// Arpitha

		reldel = (RelativeLayout) findViewById(R.id.reldel);// 10Aug2017 Arpitha

		rdsuture = (RadioGroup) findViewById(R.id.rdsuturematerial);// 11Sep2017
																	// Arpitha

	}

	private void initialViewDialog() throws Exception {

		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		Women_Profile_Pojo wdataold = new Women_Profile_Pojo();

		wdataold = dbh.getWomanDetails(woman.getUserId(), woman.getWomenId());

		if (wdataold != null)
			Partograph_CommonClass.oldWomanObj = wdataold;

		getwomanbasicdata();

		imgwt1_warning.setVisibility(View.INVISIBLE);

		imgwt2_warning.setVisibility(View.GONE);

		if (woman.getDel_type() != 0) {
			isDelUpdate = true;
		} else {
			isDelUpdate = false;
			txtdeltypereason.setVisibility(View.GONE);
			txtmothersdeathreason.setVisibility(View.GONE);
			etmothersdeathreason.setVisibility(View.GONE);
			mspndeltypereason.setVisibility(View.VISIBLE);
			mspntears.setVisibility(View.VISIBLE);
			ettears.setVisibility(View.GONE);
			spndel_type.setEnabled(true);
			etdeltypeotherreasons.setEnabled(true);
			spn_noofchild.setEnabled(true);
			spn_delres1.setEnabled(true);
			spn_delres2.setEnabled(true);
			etdeltime.setEnabled(true);
			etdeltime2.setEnabled(true);
			etdeldate.setEnabled(true);
			rdepiyes.setEnabled(true);
			rdepino.setEnabled(true);
			rdcatgut.setEnabled(true);
			rdvicryl.setEnabled(true);

			etdeldate.setText(woman.getDel_Date() == null
					? Partograph_CommonClass.getConvertedDateFormat(Partograph_CommonClass.getTodaysDate(),
							Partograph_CommonClass.defdateformat)
					: Partograph_CommonClass.getConvertedDateFormat(woman.getDel_Date(),
							Partograph_CommonClass.defdateformat));

			etdeltime.setText(
					woman.getDel_Time() == null ? Partograph_CommonClass.getCurrentTime() : woman.getDel_Time());

			etdeltime2.setText(
					woman.getDel_time2() == null ? Partograph_CommonClass.getCurrentTime() : woman.getDel_time2());

			todaysDate = Partograph_CommonClass.getTodaysDate();
			selecteddate = etdeldate.getText().toString();

			strDeldate = todaysDate;
			selecteddate = todaysDate;

			String delTime = etdeltime.getText().toString();

			Calendar calendar = Calendar.getInstance();

			Date d = null;
			d = dbh.getTime(delTime);
			calendar.setTime(d);
			hour = calendar.get(Calendar.HOUR_OF_DAY);
			minute = calendar.get(Calendar.MINUTE);

			tr_deltypereason.setVisibility(View.GONE);
			tr_deltypeotherreasons.setVisibility(View.GONE);

			setTears();

			txtfirstchilddetails.setText(getResources().getString(R.string.child_details));

			imgwt1_warning.setVisibility(View.INVISIBLE);
			imgwt2_warning.setVisibility(View.GONE);

		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");

		strrefdatetime = dbh.getrefdate(woman.getWomenId(), woman.getUserId());

		if (strrefdatetime != null) {
			refdatetime = sdf.parse(strrefdatetime);
		}
		// 10Aug2017 Atpitha
		ArrayList<DiscgargePojo> arrVal = new ArrayList<DiscgargePojo>();
		arrVal = dbh.getDischargeData(woman.getUserId(), womenId);
		if (arrVal.size() > 0 && !(DiscargeDetails_Activity.updateDel)) {
			txtdisabled.setVisibility(View.VISIBLE);
			reldel.setVisibility(View.GONE);

		} // 10Aug2017 Atpitha

		strlatdatetime = dbh.getLatenPhaseDateTime(womenId, woman.getUserId());// 20Sep2017
																				// Arpitha

	}

	// Set options for tears - 04jan2016
	protected void setTears() throws Exception {
		int i = 0;
		tearsMap = new LinkedHashMap<String, Integer>();
		List<String> tearsStrArr = null;

		List<String> tearsStrArrvalues = null;

		tearsStrArrvalues = Arrays.asList(getResources().getStringArray(R.array.tearsvalues));

		if (tearsStrArrvalues != null) {
			for (String str : tearsStrArrvalues) {
				tearsMap.put(str, i);
				i++;
			}
		}

		tearsStrArr = Arrays.asList(getResources().getStringArray(R.array.tears));

		if (tearsStrArr != null) {
			mspntears.setItems(tearsStrArr);
		}
	}

	void getwomanbasicdata() throws Exception {

		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		if (woman != null) {

			String doa = "", toa = "", risk = ""/* , dod = "", tod = "" */;

			txtwname.setText(woman.getWomen_name());
			txtwage.setText(woman.getAge() + getResources().getString(R.string.yrs));// 02Oct2016
																						// Arpitha
																						// -
																						// string
																						// value
																						// from

			if (woman.getregtype() != 2) {// strings.xml
				doa = Partograph_CommonClass.getConvertedDateFormat(woman.getDate_of_admission(),
						Partograph_CommonClass.defdateformat);
				toa = woman.getTime_of_admission();
				txtwdoa.setText(getResources().getString(R.string.reg) + ": " + doa + "/" + toa);
			} else {

				txtwdoa.setText(getResources().getString(R.string.doe) + ": " + woman.getDate_of_reg_entry());
			}
			txtwgest.setText(getResources().getString(R.string.gest_age) + ":"
					+ (woman.getGestationage() == 0 ? getResources().getString(R.string.notknown)
							: woman.getGestationage() + getResources().getString(R.string.wks)));// 23Aug2016

			txtwgravida.setText(getResources().getString(R.string.gravida_short_label) + ":" + woman.getGravida() + ", "
					+ getResources().getString(R.string.para_short_label) + ":" + woman.getPara());
			// woman.getRisk_category();
			if (woman.getRisk_category() == 1) {
				risk = getResources().getString(R.string.high);
			} else
				risk = getResources().getString(R.string.low);
			txtwrisk.setText(getResources().getString(R.string.risk_short_label) + ":" + risk);

		}
	}

	// Set Junk Items
	private void setDelTypeReason(int dtype) throws Exception {
		int i = 0;
		deltypereasonMap = new LinkedHashMap<String, Integer>();
		List<String> reasonStrArr = null;
		if (dtype == 2) {
			reasonStrArr = Arrays.asList(getResources().getStringArray(R.array.spnreasoncesareanValues));// 29Sep2016
																											// Arpitha
		} else if (dtype == 3) {
			reasonStrArr = Arrays.asList(getResources().getStringArray(R.array.spnreasoninstrumental));
		}

		if (reasonStrArr != null) {
			for (String str : reasonStrArr) {
				deltypereasonMap.put(str, i);
				i++;
			}
		}

		if (dtype == 2)

		{
			reasonStrArr = Arrays.asList(getResources().getStringArray(R.array.spnreasoncesarean));
		} else {
			reasonStrArr = Arrays.asList(getResources().getStringArray(R.array.spnreasoninstrumental));
		}
		if (reasonStrArr != null) {
			mspndeltypereason.setItems(reasonStrArr);
		}
	}

	private void ConfirmAlert(String msg, final String goToScreen) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(DeliveryInfo_Activity.this);

		// set dialog message
		alertDialogBuilder.setMessage(msg).setCancelable(false)
				.setNegativeButton(getResources().getString(R.string.recheck), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {

						// Arpitha
						// -
						// string
						// value
						// from
						// strings.xml
						etdeldate.requestFocus();

					}
				}).setPositiveButton(getResources().getString(R.string.save), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						try {
							// Arpitha
							// -
							// string
							// value
							// from
							// strings.xml
							deldata();

						} catch (Exception e) {
							AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
									+ this.getClass().getSimpleName(), e);

							e.printStackTrace();
						}

					}
				});
		AlertDialog alertDialog1 = alertDialogBuilder.create();
		alertDialog1.show();
		alertDialog1.setCancelable(false);// 31oct2016 Arpitha
	}

	void deldata() throws Exception {

		Women_Profile_Pojo wpojo = new Women_Profile_Pojo();
		if (isDelUpdate) {
			int childwt1 = Integer.parseInt(etbabywt1.getText().toString());

			// 06Jan2016
			int childwt2 = 0;
			if (etbabywt2.getText().toString().length() > 0) {
				childwt2 = Integer.parseInt(etbabywt2.getText().toString());
				wpojo.setDel_Comments(etdelcomments.getText().toString());
				wpojo.setBabywt1(childwt1);
				wpojo.setBabywt2(childwt2);
				wpojo.setBabysex1(babysex1);
				wpojo.setBabysex2(babysex2);
				int motherdead;

				/**
				 * if (chkmotherdead.isChecked()) motherdead = 1; else
				 * motherdead = 0;
				 */

				if (rdmotherdead.isChecked())
					motherdead = 1;
				else
					motherdead = 0;

				wpojo.setMothersdeath(motherdead);
				wpojo.setMothers_death_reason(etmothersdeathreason.getText().toString());
				wpojo.setUserId(wUserId);
				wpojo.setWomenId(womenId);

			}
		} else {

			if (!(no_of_child.equals("") || (no_of_child == null)))
				numofchildren = Integer.parseInt(no_of_child);

			int childwt1 = Integer.parseInt(etbabywt1.getText().toString());

			// 06Jan2016
			int childwt2 = 0;
			if (etbabywt2.getText().toString().length() > 0) {
				childwt2 = Integer.parseInt(etbabywt2.getText().toString());
			}

			wpojo.setDel_type(delivery_type);
			wpojo.setDel_Comments(etdelcomments.getText().toString());
			wpojo.setDel_result1(delivery_result1);
			wpojo.setDel_result2(delivery_result2);
			wpojo.setNo_of_child(numofchildren);
			wpojo.setBabywt1(childwt1);
			wpojo.setBabywt2(childwt2);
			wpojo.setBabysex1(babysex1);
			wpojo.setBabysex2(babysex2);

			int motherdead;

			if (rdmotherdead.isChecked())
				motherdead = 1;
			else
				motherdead = 0;

			wpojo.setMothersdeath(motherdead);
			wpojo.setDel_Time(etdeltime.getText().toString());
			wpojo.setDel_Date(strDeldate);
			if (numofchildren == 2)
				wpojo.setDel_time2(etdeltime2.getText().toString());
			else
				wpojo.setDel_time2("");

			wpojo.setMothers_death_reason(etmothersdeathreason.getText().toString());
			wpojo.setUserId(wUserId);
			wpojo.setWomenId(womenId);

			/**
			 * updateres = dbh.updateStatus(wpojo, wUserId, womenId);
			 */

			// 02Dec2015
			String selIds = "";
			if (delivery_type > 1) {
				List<String> selecteddeltypereason = mspndeltypereason.getSelectedStrings();
				if (selecteddeltypereason != null && selecteddeltypereason.size() > 0) {
					for (String str : selecteddeltypereason) {
						if (selecteddeltypereason.indexOf(str) != selecteddeltypereason.size() - 1)
							selIds = selIds + deltypereasonMap.get(str) + ",";
						else
							selIds = selIds + deltypereasonMap.get(str);
					}
				}
			}
			wpojo.setDelTypeReason(selIds);

			wpojo.setDeltype_otherreasons("" + etdeltypeotherreasons.getText().toString());

			String seltearsIds = "";
			List<String> selectedtears = mspntears.getSelectedStrings();
			if (selectedtears != null && selectedtears.size() > 0) {
				for (String str : selectedtears) {
					if (selectedtears.indexOf(str) != selectedtears.size() - 1)
						seltearsIds = seltearsIds + tearsMap.get(str) + ",";
					else
						seltearsIds = seltearsIds + tearsMap.get(str);
				}
			}

			wpojo.setTears(seltearsIds);
			wpojo.setEpisiotomy(episiotomy);
			wpojo.setSuturematerial(suturematerial);
		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
		String currentDateandTime = sdf.format(new Date());

		wpojo.setDatelastupdated(currentDateandTime);

		wpojo.setNormaltype(normaltype);

		dbh.db.beginTransaction();
		String regSql = "";

		int transId = dbh.iCreateNewTrans(wUserId);

		// 06Jan2061
		if (isDelUpdate)
			regSql = dbh.updateDelDetailsData(wpojo, transId, Partograph_CommonClass.oldWomanObj);
		else
			regSql = dbh.updateDelData(wpojo, transId, Partograph_CommonClass.oldWomanObj);

		if (regSql.length() > 0) {
			commitTrans();
			dbh.deletetblnotificationrow(womenId, wUserId);

		} else
			rollbackTrans();

	}

	/**
	 * Method to commit the trnsaction
	 * 
	 * @throws Exception
	 */
	private void commitTrans() throws Exception {

		dbh.db.setTransactionSuccessful();
		dbh.db.endTransaction();
		Toast.makeText(getApplicationContext(), getResources().getString(R.string.del_sucess), Toast.LENGTH_SHORT)
				.show();

		if (DiscargeDetails_Activity.isdisc) {
			Intent i = new Intent(DeliveryInfo_Activity.this, DischargedWomanList_Activity.class);
			startActivity(i);
		} else {
			Fragment_DIP.isdelstatus = true;
			Intent i = new Intent(DeliveryInfo_Activity.this, Activity_WomenView.class);
			startActivity(i);
		}

	}

	/** Method to rollback the trnsaction */
	private void rollbackTrans() throws Exception {
		dbh.db.endTransaction();
		Toast.makeText(getApplicationContext(), getResources().getString(R.string.failed), Toast.LENGTH_SHORT).show();
	}

	private void caldatepicker() throws Exception {
		DialogFragment newFragment = new SelectDateFragment();
		newFragment.show(this.getSupportFragmentManager(), "DatePicker");
	}

	@SuppressLint("ValidFragment")
	public class SelectDateFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
		@TargetApi(Build.VERSION_CODES.HONEYCOMB)
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			DatePickerDialog dialog = null;
			try {
				Calendar calendar = Calendar.getInstance();
				Date d = null;
				d = dbh.getDateS(selecteddate == null ? todaysDate : selecteddate);
				calendar.setTime(d);
				year = calendar.get(Calendar.YEAR);
				mon = calendar.get(Calendar.MONTH);
				day = calendar.get(Calendar.DAY_OF_MONTH);

				dialog = new DatePickerDialog(getActivity(), this, year, mon, day);

				// remove calendar view
				dialog.getDatePicker().setCalendarViewShown(false);

				// Spinner View
				dialog.getDatePicker().setSpinnersShown(true);

				dialog.setTitle(getResources().getString(R.string.pickadate));
			} catch (Exception e) {
				AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
						+ this.getClass().getSimpleName(), e);
				e.printStackTrace();
			}
			return dialog;
		}

		public void onDateSet(DatePicker view, int yy, int mm, int dd) {
			if (view.isShown()) {
				try {
					populateSetDate(yy, mm + 1, dd);
				} catch (Exception e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					e.printStackTrace();
				}
			}
		}
	}

	public void populateSetDate(int year, int month, int day) throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		selecteddate = String.valueOf(year) + "-" + String.format("%02d", month) + "-" + String.format("%02d", day);

		try {

			Date taken = new SimpleDateFormat("yyyy-MM-dd").parse(selecteddate);
			Date regdate = null;
			if (woman.getDate_of_admission() != null)
				regdate = new SimpleDateFormat("yyyy-MM-dd").parse(woman.getDate_of_admission());
			else
				regdate = new SimpleDateFormat("yyyy-MM-dd").parse(woman.getDate_of_reg_entry());
			Date lastentrydate = null;
			Date refdate = null;

			// 23oct2016 Arpitha
			if (strrefdatetime != null) {
				refdate = new SimpleDateFormat("yyyy-MM-dd").parse(strrefdatetime);
			}
			if (strlastentrytime != null) {
				lastentrydate = new SimpleDateFormat("yyyy-MM-dd").parse(strlastentrytime);
			}

			// 20Sep2017 Arpitha
			if (strlatdatetime != null && strlatdatetime.trim().length() > 0) {
				latDatetime = new SimpleDateFormat("yyyy-MM-dd").parse(strlatdatetime);
			} // 20Sep2017 Arpitha

			if (taken.after(new Date())) {
				Toast.makeText(getApplicationContext(), getResources().getString(R.string.curr_date_validation),
						Toast.LENGTH_SHORT).show();

				etdeldate.setText(Partograph_CommonClass.getConvertedDateFormat(todaysDate,
						Partograph_CommonClass.defdateformat));
				strDeldate = todaysDate;

			} else if (taken.before(regdate) && woman.getregtype() != 2) {

				String regDate = Partograph_CommonClass.getConvertedDateFormat(strRegdate,
						Partograph_CommonClass.defdateformat);// 17April2017
																// Arpitha
				Toast.makeText(getApplicationContext(),
						getResources().getString(R.string.date_cant_be_before_reg_date) + " - " + regDate,
						Toast.LENGTH_SHORT).show();

				etdeldate.setText(Partograph_CommonClass.getConvertedDateFormat(todaysDate,
						Partograph_CommonClass.defdateformat));
				strDeldate = todaysDate;

			}
			// 20Sep2017 Arpitha
			else if (latDatetime != null && taken.before(latDatetime)) {

				String latdate = Partograph_CommonClass.getConvertedDateFormat(strlatdatetime.split("/")[0],
						Partograph_CommonClass.defdateformat);
				Toast.makeText(getApplicationContext(),
						getResources().getString(R.string.del_datetime_cannot_be_before_lattentphase_datetime) + " - "
								+ latdate,
						Toast.LENGTH_SHORT).show();

				etdeldate.setText(Partograph_CommonClass.getConvertedDateFormat(todaysDate,
						Partograph_CommonClass.defdateformat));
				strDeldate = todaysDate;

			} // 20Sep2017 Arpitha

			else if (lastentrydate != null && taken.before(lastentrydate)) {

				String lastEntryDtae = Partograph_CommonClass.getConvertedDateFormat(strlastentrytime,
						Partograph_CommonClass.defdateformat);// 17April2017
																// Arpitha

				Toast.makeText(getApplicationContext(),
						getResources().getString(R.string.deldate_before_lastpartodate) + " - " + lastEntryDtae,
						Toast.LENGTH_SHORT).show();

				etdeldate.setText(Partograph_CommonClass.getConvertedDateFormat(todaysDate,
						Partograph_CommonClass.defdateformat));
				strDeldate = todaysDate;
			}

			else if (refdate != null && taken.before(refdate)) {

				String referraldate = Partograph_CommonClass.getConvertedDateFormat(strrefdatetime,
						Partograph_CommonClass.defdateformat);// 17April2017
																// Arpitha

				Toast.makeText(getApplicationContext(),
						getResources().getString(R.string.deldate_before_refdate) + " - " + referraldate,
						Toast.LENGTH_SHORT).show();

				etdeldate.setText(Partograph_CommonClass.getConvertedDateFormat(todaysDate,
						Partograph_CommonClass.defdateformat));
				strDeldate = todaysDate;

			}

			else {

				// etdeldate.setText(selecteddate);
				etdeldate.setText(Partograph_CommonClass.getConvertedDateFormat(selecteddate,
						Partograph_CommonClass.defdateformat));
				strDeldate = selecteddate;
			}
			// }11Nov2016 Arpitha

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
	}

	@Override
	protected Dialog onCreateDialog(int id) {

		try {
			switch (id) {
			case TIME_DIALOG_ID:
				// set time picker as current time
				return new TimePickerDialog(this, timePickerListener, hour, minute, false);

			}
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
		return null;
	}

	private TimePickerDialog.OnTimeSetListener timePickerListener = new TimePickerDialog.OnTimeSetListener() {
		public void onTimeSet(TimePicker view, int selectedHour, int selectedMinute) {
			hour = selectedHour;
			minute = selectedMinute;

			try {

				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
				// Date rtime = null;
				// String reftime = null;
				Date lastentrytime = null;

				// 20Sep2017 Arpitha
				if (strlatdatetime != null && strlatdatetime.trim().length() > 0) {
					strlatdatetime = strlatdatetime.replace("/", " ");
					latDatetime = sdf.parse(strlatdatetime);
				} // 20Sep2017 Arpitha

				Date regdate = null;
				if (strRegdate != null)
					regdate = sdf.parse(strRegdate);
				if (strlastentrytime != null) {
					lastentrytime = sdf.parse(strlastentrytime);
				}
				String strcurrenttime = sdf.format(new Date());
				Date currenttime = sdf.parse(strcurrenttime);

				if (del_time1) {
					Date selecteddate;
					String deldatetime = strDeldate + " " + ("" + hour) + ":" + ("" + minute);
					selecteddate = sdf.parse(deldatetime);
					// if (woman.getregtype() != 2) {11Nov2016 Arpitha
					if (regdate != null && selecteddate.before(regdate) && woman.getregtype() != 2) {

						String regtime = Partograph_CommonClass.getConvertedDateFormat(strRegdate,
								Partograph_CommonClass.defdateformat);// 17April2017
																		// Arpitha

						regtime = regtime + " " + strRegdate.substring(11);// 17Arpitha2017
																			// Arpitha

						Toast.makeText(getApplicationContext(),
								getResources().getString(R.string.deltime_before_reg) + " - " + regtime,
								Toast.LENGTH_LONG).show();
						etdeltime.setText(Partograph_CommonClass.getCurrentTime());
					}

					// 20Sep2017 Arpitha
					else if (latDatetime != null && selecteddate.before(latDatetime)) {

						String latdate = Partograph_CommonClass.getConvertedDateFormat(strlatdatetime.split("/")[0],
								Partograph_CommonClass.defdateformat);
						Toast.makeText(getApplicationContext(),
								getResources().getString(R.string.del_datetime_cannot_be_before_lattentphase_datetime)
										+ " - " + latdate + " " + strlatdatetime.split(" ")[1],
								Toast.LENGTH_SHORT).show();

						etdeltime.setText(Partograph_CommonClass.getCurrentTime());

					} // 20Sep2017 Arpitha

					else if (lastentrytime != null && selecteddate.before(lastentrytime)) {

						String lastpartoentrytime = Partograph_CommonClass.getConvertedDateFormat(strlastentrytime,
								Partograph_CommonClass.defdateformat);// 17April2017
																		// Arpitha

						lastpartoentrytime = lastpartoentrytime + " " + strlastentrytime.substring(11);// 17Arpitha2017
																										// Arpitha

						Toast.makeText(getApplicationContext(),
								getResources().getString(R.string.deltime_before_lastpartotime) + " - "
										+ lastpartoentrytime,

								Toast.LENGTH_LONG).show();
						etdeltime.setText(Partograph_CommonClass.getCurrentTime());
					}

					else if (refdatetime != null && selecteddate.before(refdatetime)) {

						String reftime = Partograph_CommonClass.getConvertedDateFormat(strrefdatetime,
								Partograph_CommonClass.defdateformat);// 17April2017
																		// Arpitha

						reftime = reftime + "/" + strrefdatetime.substring(11);// 17Arpitha2017
																				// Arpitha

						Toast.makeText(getApplicationContext(),
								getResources().getString(R.string.deldate_before_refdate) + " - " + reftime,
								Toast.LENGTH_LONG).show();
						etdeltime.setText(Partograph_CommonClass.getCurrentTime());

					} else if (currenttime != null && selecteddate.after(currenttime)) {

						String currentTime = Partograph_CommonClass.getConvertedDateFormat(strcurrenttime,
								Partograph_CommonClass.defdateformat);// 17April2017
																		// Arpitha

						currentTime = currentTime + " " + strcurrenttime.substring(11);// 17Arpitha2017
																						// Arpitha

						Toast.makeText(getApplicationContext(),
								getResources().getString(R.string.deltime_after_currenttime) + " - " + currentTime,
								Toast.LENGTH_LONG).show();
						etdeltime.setText(Partograph_CommonClass.getCurrentTime());

					}

					else
						etdeltime.setText(
								new StringBuilder().append(padding_str(hour)).append(":").append(padding_str(minute)));

				} else {

					Date selecteddate;
					String deldatetime = strDeldate + " " + ("" + hour) + ":" + ("" + minute);
					selecteddate = sdf.parse(deldatetime);
					// if (woman.getregtype() != 2) {11Nov2016 Arpitha
					if (regdate != null && selecteddate.before(regdate) && woman.getregtype() != 2) {

						String regDate = Partograph_CommonClass.getConvertedDateFormat(strRegdate,
								Partograph_CommonClass.defdateformat);// 17April2017
																		// Arpitha

						regDate = regDate + strrefdatetime.substring(11);// 17Arpitha2017
																			// Arpitha
																			// =
																			// RefTime
																			// +
																			// strrefdatetime.substring(11);//17Arpitha2017
																			// Arpitha

						Toast.makeText(getApplicationContext(),
								getResources().getString(R.string.deltime_before_reg) + " - " + regDate,
								Toast.LENGTH_LONG).show();
						etdeltime2.setText(Partograph_CommonClass.getCurrentTime());
					}

					// 20Sep2017 Arpitha
					else if (latDatetime != null && selecteddate.before(latDatetime)) {

						String latdate = Partograph_CommonClass.getConvertedDateFormat(strlatdatetime.split("/")[0],
								Partograph_CommonClass.defdateformat);
						Toast.makeText(getApplicationContext(),
								getResources().getString(R.string.del_datetime_cannot_be_before_lattentphase_datetime)
										+ " - " + latdate + " " + strlatdatetime.split(" ")[0],
								Toast.LENGTH_SHORT).show();

						etdeltime2.setText(Partograph_CommonClass.getCurrentTime());

					} // 20Sep2017 Arpitha

					else if (lastentrytime != null && selecteddate.before(lastentrytime)) {

						String lastEntryTime = Partograph_CommonClass.getConvertedDateFormat(strlastentrytime,
								Partograph_CommonClass.defdateformat);// 17April2017
																		// Arpitha

						lastEntryTime = lastEntryTime + " " + strlastentrytime.substring(11);// 17Arpitha2017
																								// Arpitha

						Toast.makeText(getApplicationContext(),
								getResources().getString(R.string.deltime_before_lastpartotime) + " - " + lastEntryTime,
								Toast.LENGTH_LONG).show();
						etdeltime2.setText(Partograph_CommonClass.getCurrentTime());
					} else if (refdatetime != null && selecteddate.before(refdatetime)) {

						String RefTime = Partograph_CommonClass.getConvertedDateFormat(strrefdatetime,
								Partograph_CommonClass.defdateformat);// 17April2017
																		// Arpitha

						RefTime = RefTime + " " + strrefdatetime.substring(11);// 17Arpitha2017
																				// Arpitha

						Toast.makeText(getApplicationContext(),
								getResources().getString(R.string.deldate_before_refdate) + " - " + RefTime,
								Toast.LENGTH_LONG).show();
						etdeltime2.setText(Partograph_CommonClass.getCurrentTime());

					}

					else if (currenttime != null && selecteddate.after(currenttime)) {

						String curtime = Partograph_CommonClass.getConvertedDateFormat(strcurrenttime,
								Partograph_CommonClass.defdateformat);// 17April2017
																		// Arpitha

						curtime = curtime + " " + strcurrenttime.substring(11);// 17Arpitha2017
																				// Arpitha

						Toast.makeText(getApplicationContext(),
								getResources().getString(R.string.deltime_after_currenttime) + " - " + curtime,
								Toast.LENGTH_LONG).show();
						etdeltime2.setText(Partograph_CommonClass.getCurrentTime());

					} else
						etdeltime2.setText(
								new StringBuilder().append(padding_str(hour)).append(":").append(padding_str(minute)));

				}

			} catch (Exception e) {
				AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
						+ this.getClass().getSimpleName(), e);
				e.printStackTrace();
			}
		}
	};

	private static String padding_str(int c) throws Exception {
		if (c >= 10)
			return String.valueOf(c);
		else
			return "0" + String.valueOf(c);
	}

	protected boolean validateDelFields() throws Exception {

		if (isDelUpdate) {

			if (etbabywt1.getText().toString().trim().length() < 4) {
				displayAlertDialog(getResources().getString(R.string.child_wt), "");
				etbabywt1.requestFocus();
				return false;
			}

			if (Integer.parseInt(etbabywt1.getText().toString()) == 0) {
				displayAlertDialog(getResources().getString(R.string.child_wt_val), "");
				etbabywt1.requestFocus();
				return false;
			}

			if (no_of_child != null) {
				if (Integer.parseInt(no_of_child) == 2) {
					if (etbabywt2.getText().length() < 4) {
						displayAlertDialog(getResources().getString(R.string.child_wt), "");
						etbabywt2.requestFocus();
						return false;
					}
					if (Integer.parseInt(etbabywt2.getText().toString()) == 0) {
						displayAlertDialog(getResources().getString(R.string.child_wt_val), "");
						etbabywt2.requestFocus();
						return false;
					}
				}
			}

			if (rdmotherdead.isChecked()) {
				if (etmothersdeathreason.getText().toString().trim().length() <= 0) {
					displayAlertDialog(getResources().getString(R.string.enter_mothersdeathreason), "");
					etmothersdeathreason.requestFocus();
					return false;
				}
			}

		} else {

			if (etbabywt1.getText().length() > 0) {
				if (Integer.parseInt(etbabywt1.getText().toString()) == 0) {
					displayAlertDialog(getResources().getString(R.string.child_wt_val), "");
					etbabywt1.requestFocus();
					return false;
				}
			}

			if (etdeldate.getText().length() <= 0) {
				displayAlertDialog(getResources().getString(R.string.ent_deldate), "");
				etdeldate.requestFocus();
				return false;
			}

			if (etdeltime.getText().length() <= 0) {
				displayAlertDialog(getResources().getString(R.string.ent_deltime), "");
				etdeltime.requestFocus();
				return false;
			}

			if (etbabywt1.getText().toString().trim().length() < 4) {
				displayAlertDialog(getResources().getString(R.string.child_wt), "");
				etbabywt1.requestFocus();
				return false;
			}

			if (etdeltime.getText().length() >= 1) {
				String date1, date2;
				date1 = strRegdate;
				String dtime = etdeltime.getText().toString();
				date2 = strDeldate + " " + dtime;
				etdeltime.requestFocus();
				if (woman.getregtype() != 2) {// 11May2017 Arpitha - v2.6
					if ((!isDeliveryTimevalid(date1, date2, 1)) && woman.getregtype() != 2)
						return false;
				} //// 11May2017 Arpitha - v2.6
				if (strlastentrytime != null) {
					if (strlastentrytime.contains("_")) {
						strlastentrytime = strlastentrytime.replace("_", " ");
					}
					if (!isDeliveryTimevalid(strlastentrytime, date2, 2))
						return false;
				}
				// }11Nov2016 Arpitha

				// 02Nov2016 Arpitha
				if (strrefdatetime != null) {
					if (!isDeliveryTimevalid(strrefdatetime, date2, 4))
						return false;
				} // 02Nov2016 Arpitha
				String currenttime = Partograph_CommonClass.getTodaysDate() + " "
						+ Partograph_CommonClass.getCurrentTime();
				if (!isDeliveryTimevalid(date2, currenttime, 3))
					return false;
				// 20Sep2017 Arpitha
				if (strlatdatetime != null && strlatdatetime.trim().length() > 0) {
					strlatdatetime = strlatdatetime.replace("/", " ");
					if (!isDeliveryTimevalid(strlatdatetime, date2, 5))
						return false;// 20Sep2017 Arpitha
				}

				if (woman.getregtype() == 2) {
					// else {
					// 21Nov2016 Arpitha
					int days;
					if (woman.getLmp() != null && woman.getLmp().trim().length() > 0) {
						days = Partograph_CommonClass.getDaysBetweenDates(strDeldate, woman.getLmp());
						if (days < 196) {
							displayAlertDialog(
									getResources().getString(R.string.deldate_cannot_be_before_sevenmonths_from_lmp),
									"");
							return false;
						} else if (days > 301) {
							displayAlertDialog(
									getResources().getString(R.string.deldate_cannot_be_after_ninemonths_from_lmp), "");
							return false;
						}
					} // 21Nov2016 Arpitha
				}
				// }
			}

			if (no_of_child != null) {
				if (Integer.parseInt(no_of_child) == 2) {
					if (etbabywt2.getText().toString().trim().length() < 4) {
						displayAlertDialog(getResources().getString(R.string.child_wt), "");
						etbabywt2.requestFocus();
						return false;
					}
					if (etbabywt2.getText().length() > 0) {
						if (Integer.parseInt(etbabywt2.getText().toString()) == 0) {
							displayAlertDialog(getResources().getString(R.string.child_wt_val), "");
							etbabywt2.requestFocus();
							return false;
						}
					}

					if (etdeltime2.getText().length() >= 1) {
						String date1, date2;
						date1 = strRegdate;
						String dtime = etdeltime2.getText().toString();
						date2 = strDeldate + " " + dtime;
						etdeltime2.requestFocus();
						// if (woman.getregtype() != 2) {11Nov2016 Arpitha
						if ((!isDeliveryTimevalid(date1, date2, 1)) && woman.getregtype() != 2)
							return false;
						if (strlastentrytime != null) {
							if (!isDeliveryTimevalid(strlastentrytime, date2, 2))
								return false;
						}
						// }11Nov2016 Arpitha

						// 02Nov2016 Arpitha
						if (strrefdatetime != null) {
							if (!isDeliveryTimevalid(strrefdatetime, date2, 4))
								return false;
						} // 02Nov2016 Arpitha

						String currenttime = Partograph_CommonClass.getTodaysDate() + " "
								+ Partograph_CommonClass.getCurrentTime();
						if (!isDeliveryTimevalid(date2, currenttime, 3))
							return false;

						// 20Sep2017 Arpitha
						if (strlatdatetime != null && strlatdatetime.trim().length() > 0) {
							strlatdatetime = strlatdatetime.replace("/", "");
							if (!isDeliveryTimevalid(strlatdatetime, date2, 5))
								return false;// 20Sep2017 Arpitha
						}
					}

				}
			}

			if (rdmotherdead.isChecked()) {
				if (etmothersdeathreason.getText().toString().trim().length() <= 0) {
					displayAlertDialog(getResources().getString(R.string.enter_mothersdeathreason), "");
					etmothersdeathreason.requestFocus();
					return false;
				}
			}
		}

		return true;
	}

	// Alert dialog
	private void displayAlertDialog(String message, final String classname) throws Exception {// 11May2017
																								// Arpitha
																								// -
																								// v2.6
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(DeliveryInfo_Activity.this);

		// set dialog message
		alertDialogBuilder.setMessage(message)

				.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// if this button is clicked, just close
						// the dialog box and do nothing
						dialog.cancel();
						if (classname.equalsIgnoreCase("breech"))// 18May2017
						// v2.6
						{
							normaltype = 2;

						} else if (classname.equalsIgnoreCase("vertex")) {
							normaltype = 1;

						}
						/*
						 * if
						 * (classname.equalsIgnoreCase(getResources().getString(
						 * R.string.breech)))// 11May2017 // Arpitha {
						 * rdnormaltype.clearCheck();// - // v2.6 normaltype =
						 * 2;// May2017 Arpitha - v2.6
						 * rdbreech.setChecked(true); } if
						 * (classname.equalsIgnoreCase(getResources().getString(
						 * R.string.vertex)))// 11May2017 // Arpitha // - //
						 * v2.6 normaltype = 1;// May2017 Arpitha - v2.6 }
						 */
					}

				});

		if (classname.equalsIgnoreCase("breech") || classname.equalsIgnoreCase("vertex")) {
			alertDialogBuilder.setNegativeButton(getResources().getString(R.string.no),
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							dialog.cancel();
							if (classname.equalsIgnoreCase("breech")) {
								normaltype = 1;
								rdvertext.setChecked(true);
								rdbreech.setChecked(false);
							} else if (classname.equalsIgnoreCase("vertex")) {
								normaltype = 2;
								rdvertext.setChecked(false);
								rdbreech.setChecked(true);
							}

						}
					});
		}

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();
		// show it
		alertDialog.show();
		alertDialog.setCancelable(false);
	}

	@SuppressLint("SimpleDateFormat")
	private boolean isDeliveryTimevalid(String date1, String date2, int i) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Date dateofreg, dateofdelivery;
		try {
			dateofreg = sdf.parse(date1);
			dateofdelivery = sdf.parse(date2);

			String strDate1 = Partograph_CommonClass.getConvertedDateFormat(date1,
					Partograph_CommonClass.defdateformat);// 17April2017 Arpitha
			strDate1 = strDate1 + " " + date1.substring(11);

			String strDate2 = Partograph_CommonClass.getConvertedDateFormat(date2,
					Partograph_CommonClass.defdateformat);// 17April2017 Arpitha
			strDate2 = strDate2 + " " + date2.substring(11);

			if (dateofdelivery.before(dateofreg)) {
				if (i == 1)
					displayAlertDialog(getResources().getString(R.string.deltime_before_reg) + " - " + strDate1, "");
				else if (i == 2)
					displayAlertDialog(
							getResources().getString(R.string.deltime_before_lastpartotime) + " - " + strDate1, "");
				else if (i == 3)
					displayAlertDialog(getResources().getString(R.string.invaliddeltime), "");
				// 02Nov2016 Arpitha
				else if (i == 4)
					displayAlertDialog(getResources().getString(R.string.deldate_before_refdate), "");
				else if (i == 5)
					displayAlertDialog(
							getResources().getString(R.string.del_datetime_cannot_be_before_lattentphase_datetime), "");
				return false;
			} else
				return true;

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);

			e.printStackTrace();
			return false;
		}
	}

	private void displayConfirmationAlert(String exit_msg, final String classname) throws Exception {

		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		final Dialog dialog = new Dialog(DeliveryInfo_Activity.this);
		dialog.setTitle(Html.fromHtml("<font color='" + getResources().getColor(R.color.appcolor) + "'>"
				+ getResources().getString(R.string.warning) + "</font>"));
		dialog.setContentView(R.layout.temp_alertdialog);

		int dividerId = dialog.getContext().getResources().getIdentifier("android:id/titleDivider", null, null);
		View divider = dialog.findViewById(dividerId);
		divider.setBackgroundColor(getResources().getColor(R.color.appcolor));// 08Feb2017

		dialog.show();

		final TextView txtdialog = (TextView) dialog.findViewById(R.id.txtpulse);
		Button imgbtnyes = (Button) dialog.findViewById(R.id.imgbtnyes);
		Button imgbtnno = (Button) dialog.findViewById(R.id.imgbtnno);
		final TextView txtdialog1 = (TextView) dialog.findViewById(R.id.txtpulseval);

		final TextView txtdialog6 = (TextView) dialog.findViewById(R.id.txt);

		String strwarningmess = "";
		String strval = "";

		if (classname.equalsIgnoreCase(getResources().getString(R.string.deldata))) {
			if (weight1 > 0 && (weight1 < 2500 || weight1 > 4000)) {
				if (Integer.parseInt(no_of_child) == 2)
					strwarningmess = strwarningmess + getResources().getString(R.string.first_baby_weight) + "\n";
				else
					strwarningmess = strwarningmess + getResources().getString(R.string.baby_weight) + "\n";
				strval = strval + "" + weight1 + getResources().getString(R.string.grams) + "\n";
			}

			if (weight2 > 0 && (weight2 < 2500 || weight2 > 4000)) {
				strwarningmess = strwarningmess + getResources().getString(R.string.second_baby_weight) + "\n";
				strval = strval + "" + weight2 + getResources().getString(R.string.grams) + "\n";
			}
			txtdialog.setText(strwarningmess);
			txtdialog1.setText(strval);
			txtdialog6.setText(getResources().getString(R.string.seems_to_be_abnormal_recheck_or_save));
		}

		if (classname.equalsIgnoreCase(getResources().getString(R.string.clear))) {
			txtdialog6.setText(getResources().getString(R.string.clear_msg));
			txtdialog1.setVisibility(View.GONE);
			txtdialog1.setVisibility(View.GONE);
			imgbtnyes.setText(getResources().getString(R.string.yes));
			imgbtnno.setText(getResources().getString(R.string.no));

		}

		if (classname.equalsIgnoreCase(getResources().getString(R.string.exit_msg))) {
			txtdialog6.setText(getResources().getString(R.string.exit_msg));
			txtdialog1.setVisibility(View.GONE);
			txtdialog1.setVisibility(View.GONE);
			imgbtnyes.setText(getResources().getString(R.string.yes));
			imgbtnno.setText(getResources().getString(R.string.no));

		}

		imgbtnyes.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				try {
					dialog.cancel();

					if (classname.equalsIgnoreCase(getResources().getString(R.string.deldata))) {
						String msg = getResources().getString(R.string.deldatetimerecheck)
								+ etdeldate.getText().toString() + " / " + etdeltime.getText().toString()
								+ getResources().getString(R.string.confirmrecheck);
						ConfirmAlert(msg, getResources().getString(R.string.deldata));
					}
					if (classname.equalsIgnoreCase(getResources().getString(R.string.clear))) {
						clear();
					}
					if (classname.equalsIgnoreCase(getResources().getString(R.string.exit_msg))) {
						Intent i = new Intent(DeliveryInfo_Activity.this, Activity_WomenView.class);
						startActivity(i);

					}

				} catch (Exception e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					e.printStackTrace();
				}

			}
		});

		imgbtnno.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.cancel();

			}
		});

		dialog.setCancelable(false);

	}

	public void clear() throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		spndel_type.setSelection(0);
		mspndeltypereason.setSelected(false);
		etdeltypeotherreasons.setText("");
		spn_noofchild.setSelection(0);
		spn_delres1.setSelection(0);
		spn_delres2.setSelection(0);
		// etbabywt1.setText("");
		// etbabywt2.setText("");
		etdeltime.setText(Partograph_CommonClass.getCurrentTime());
		etdeltime2.setText(Partograph_CommonClass.getCurrentTime());
		etdeldate.setText(Partograph_CommonClass.getConvertedDateFormat(Partograph_CommonClass.getTodaysDate(),
				Partograph_CommonClass.defdateformat));
		rdmale1.setChecked(true);
		rdmale2.setChecked(true);
		rdfemale1.setChecked(false);
		rdfemale2.setChecked(false);
		mspntears.setSelected(false);
		mspntears.setSelection(9);
		rdepino.setChecked(true);
		rdepiyes.setChecked(false);
		rdvicryl.setChecked(false);
		rdcatgut.setChecked(false);
		rdvicryl.setVisibility(View.GONE);
		rdcatgut.setVisibility(View.GONE);
		rdmotheralive.setChecked(true);
		rdmotherdead.setChecked(false);
		etmothersdeathreason.setText("");
		etmothersdeathreason.setVisibility(View.GONE);

		rdmale2.setVisibility(View.GONE);
		rdfemale2.setVisibility(View.GONE);

		episiotomy = 0;// 11Aug2017 Arpitha
		suturematerial = 0;// 11Aug2017 Arpitha

		normaltype = 0;// 11Aug2017 Arpitha

		tr_secondchilddetails.setVisibility(View.GONE);
		tr_result2.setVisibility(View.GONE);
		tr_childdet2.setVisibility(View.GONE);
		tr_childsex2.setVisibility(View.GONE);
		tr_deltime2.setVisibility(View.GONE);

		txtfirstchilddetails.setText(getResources().getString(R.string.child_details));

		etdeltime2.setVisibility(View.GONE);
		rdchildSexGrpbaby2.setVisibility(View.GONE);
		etbabywt2.setVisibility(View.GONE);
		imgwt2_warning.setVisibility(View.GONE);
		spn_delres2.setVisibility(View.GONE);
		imgdelresult.setVisibility(View.GONE);
		imgsex2.setVisibility(View.GONE);
		imgdeltime2.setVisibility(View.GONE);

		etdelcomments.setText("");
		trsuturematerial.setVisibility(View.GONE);
		txtmothersdeathreason.setVisibility(View.GONE);

		rdbreech.setChecked(false);
		rdvertext.setChecked(true);

	}

	TextWatcher watcher = new TextWatcher() {

		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
			// TODO Auto-generated method stub
			try {
				AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
						+ this.getClass().getSimpleName());
				if (s == etbabywt1.getEditableText()) {
					if (count > 0) {
						int babywt1 = Integer.parseInt(etbabywt1.getText().toString());

						if ((babywt1 < 2500 || babywt1 > 4000)) {
							imgwt1_warning.setVisibility(View.VISIBLE);
						} else {
							imgwt1_warning.setVisibility(View.INVISIBLE);
						}
					} else
						imgwt1_warning.setVisibility(View.INVISIBLE);

				} else if (s == etbabywt2.getEditableText()) {
					if (count > 0) {
						int babywt2 = Integer.parseInt(etbabywt2.getText().toString());

						if ((babywt2 < 2500 || babywt2 > 4000)) {
							imgwt2_warning.setVisibility(View.VISIBLE);
						} else {
							imgwt2_warning.setVisibility(View.INVISIBLE);
						}
					} else
						imgwt2_warning.setVisibility(View.GONE);
				}

			} catch (Exception e) {
				AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
						+ this.getClass().getSimpleName(), e);
				e.printStackTrace();
			}

		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			// TODO Auto-generated method stub

		}

		@Override
		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub

		}
	};

	@Override
	public void onBackPressed() {
		try {
			displayConfirmationAlert("", getResources().getString(R.string.exit_msg));
		} catch (NotFoundException e) {
			// TODO Auto-generated catch block
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// 25Sep2016 Arpitha - addToTrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_womenlist, menu);

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		try {
			// 25Sep2016 Arpitha - addToTrace
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
			switch (item.getItemId()) {
			// Respond to the action bar's Up/Home button
			case android.R.id.home:
				NavUtils.navigateUpFromSameTask(this);
				return true;

			case R.id.logout:
				Intent login = new Intent(this, LoginActivity.class);
				startActivity(login);
				return true;

			case R.id.settings:
				if (Partograph_CommonClass.autodatetime(DeliveryInfo_Activity.this)) {
					Intent settings = new Intent(this, Settings_parto.class);
					startActivity(settings);
				}
				return true;

			case R.id.about:

				Intent about = new Intent(this, About.class);
				startActivity(about);
				return true;

			case R.id.info:
				Intent i = new Intent(getApplicationContext(), GraphInformation.class);
				startActivity(i);
				return true;

			// 13Oct2016 Arpitha
			case R.id.sms:
				// if (AppContext.checkSimState() ==
				// TelephonyManager.SIM_STATE_READY) {
				Partograph_CommonClass.display_messagedialog(DeliveryInfo_Activity.this, wUserId);
				/*
				 * } else Toast.makeText(getApplicationContext(),
				 * getResources().getString(R.string.no_sim),
				 * Toast.LENGTH_LONG).show();
				 */
				return true;

			// 13Oct2016 Arpitha
			case R.id.comments:
				Intent comments = new Intent(getApplicationContext(), Comments_Activity.class);
				startActivity(comments);
				return true;

			case R.id.changepass:
				Partograph_CommonClass.dispalyChangePasswordDialog(DeliveryInfo_Activity.this);
				return true;

			case R.id.summary:
				Intent sum = new Intent(getApplicationContext(), Summary_Activity.class);
				startActivity(sum);
				return true;

			case R.id.usermanual:
				Intent usermanual = new Intent(getApplicationContext(), UseManual.class);
				startActivity(usermanual);
				return true;

			case R.id.home:
				Intent settings = new Intent(DeliveryInfo_Activity.this, Activity_WomenView.class);
				startActivity(settings);
				return true;

			case R.id.disch:
				Intent disch = new Intent(DeliveryInfo_Activity.this, DischargedWomanList_Activity.class);
				startActivity(disch);
				return true;

			// 03Oct2017 Arpitha
			case R.id.del:
				// Intent disc = new Intent(DeliveryInfo_Activity.this,
				// DeliveryInfo_Activity.class);
				// disc.putExtra("woman", woman);
				// startActivity(disc);

				return true;
			case R.id.viewprofile:
				Intent view = new Intent(DeliveryInfo_Activity.this, ViewProfile_Activity.class);
				view.putExtra("woman", woman);
				startActivity(view);
				return true;

			case R.id.ref:
				/*
				 * Intent ref = new Intent(DeliveryInfo_Activity.this,
				 * ReferralInfo_Activity.class); ref.putExtra("woman", woman);
				 * startActivity(ref);
				 */
				// 27Sep2016
				// Arpitha
				Cursor cur = dbh.getReferralDetails(woman.getUserId(), woman.getWomenId());
				if (cur != null && cur.getCount() > 0) {
					Partograph_CommonClass.showDialogToUpdateReferral(DeliveryInfo_Activity.this, woman);
				} else {
					Intent ref = new Intent(DeliveryInfo_Activity.this, ReferralInfo_Activity.class);
					ref.putExtra("woman", woman);
					startActivity(ref);
				}

				return true;

			case R.id.pdf:
				Partograph_CommonClass.exportPDFs(DeliveryInfo_Activity.this, woman);
				return true;

			case R.id.disc:
				Intent discharge = new Intent(DeliveryInfo_Activity.this, DiscargeDetails_Activity.class);
				discharge.putExtra("woman", woman);
				startActivity(discharge);
				return true;

			case R.id.apgar:
				if (woman.getDel_type() != 0) {
					Intent apgar = new Intent(DeliveryInfo_Activity.this, Activity_Apgar.class);
					apgar.putExtra("woman", woman);
					startActivity(apgar);
				} else
					Toast.makeText(getApplicationContext(),
							getResources().getString(R.string.msg_applicableafterdelivery), Toast.LENGTH_LONG).show();
				return true;

			case R.id.stages:
				if (woman.getDel_type() != 0) {
					Intent stages = new Intent(DeliveryInfo_Activity.this, StageofLabor.class);
					stages.putExtra("woman", woman);
					startActivity(stages);
				} else
					Toast.makeText(getApplicationContext(),
							getResources().getString(R.string.msg_applicableafterdelivery), Toast.LENGTH_LONG).show();
				return true;

			case R.id.post:
				if (woman.getDel_type() != 0) {
					Intent post = new Intent(DeliveryInfo_Activity.this, PostPartumCare_Activity.class);
					post.putExtra("woman", woman);
					startActivity(post);
				} else
					Toast.makeText(getApplicationContext(),
							getResources().getString(R.string.msg_applicableafterdelivery), Toast.LENGTH_LONG).show();
				return true;
				
			case R.id.lat:
				Intent lat = new Intent(DeliveryInfo_Activity.this, LatentPhase_Activity.class);
				lat.putExtra("woman", woman);
				startActivity(lat);
				return true;

			case R.id.parto:
				Intent parto = new Intent(DeliveryInfo_Activity.this, SlidingActivity.class);
				parto.putExtra("woman", woman);
				startActivity(parto);
				return true;// 03Oct2017 Arpitha

			}
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
		return super.onOptionsItemSelected(item);
	}

	/*
	 * * Called when invalidateOptionsMenu() is triggered
	 */
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// if nav drawer is opened, hide the action items
		menu.findItem(R.id.sync).setVisible(false);
		menu.findItem(R.id.registration).setVisible(false);
		menu.findItem(R.id.search).setVisible(false);// 14Jun2017 Arpitha
		menu.findItem(R.id.save).setVisible(false);// 08Aug2017 Arpitha

		if (!(AppContext.checkSimState() == TelephonyManager.SIM_STATE_READY)) {
			menu.findItem(R.id.sms).setVisible(false);
		}

		// menu.findItem(R.id.changepass).setVisible(v);// 03April2017 Arpitha

		menu.findItem(R.id.delete).setVisible(false);
		menu.findItem(R.id.expertcomments).setVisible(false);
		menu.findItem(R.id.viewpartograph).setVisible(false);
		menu.findItem(R.id.ic_msg).setVisible(false);
		menu.findItem(R.id.addcomments).setVisible(false);
		menu.findItem(R.id.viewprofile).setVisible(false);
		menu.findItem(R.id.sync).setVisible(false);
		menu.findItem(R.id.registration).setVisible(false);

		// 03Oct2017 Arpitha
		menu.findItem(R.id.lat).setVisible(true);
		menu.findItem(R.id.ref).setVisible(true);
		menu.findItem(R.id.pdf).setVisible(true);
		menu.findItem(R.id.stages).setVisible(true);
		menu.findItem(R.id.apgar).setVisible(true);
		menu.findItem(R.id.post).setVisible(true);
		menu.findItem(R.id.parto).setVisible(true);// 03Oct2017 Arpitha

		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	public void onUserInteraction() {
		// TODO Auto-generated method stub
		super.onUserInteraction();
		KillAllActivitesAndGoToLogin
				.delayedIdle(Integer.parseInt(Partograph_CommonClass.properties.getProperty("idleTimeOut")));
	}

	// 10April2017 Arpitha
	// setInput Filters
	private void setInputFiltersForEdittext() throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		etdelcomments.setFilters(new InputFilter[] { filter, new InputFilter.LengthFilter(400) });
		etmothersdeathreason.setFilters(new InputFilter[] { filter, new InputFilter.LengthFilter(150) });
		etdeltypeotherreasons.setFilters(new InputFilter[] { filter, new InputFilter.LengthFilter(450) });
	}

	// To avoid special characters in Input type
	public static InputFilter filter = new InputFilter() {
		@Override
		public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
			// String blockCharacterSet =
			// "~#^|$%*!@/()-'\":;?{}=!$^';?Ã—Ã·<>{}â‚¬Â£Â¥â‚©%&+*.[]1234567890Â¶";
			String blockCharacterSet = "~#^|$%*!@/()-'\":;?{}=!$^';?Ã—Ã·<>{}â‚¬Â£Â¥â‚©%&+*[]~#^|$%&*!`@_=?;]}{[()\"\\Â¥Â®Â¿ÃŒâ„¢â‚¹Â°^âˆšÏ€Ã·Ã—â–³Â¶Â£â€¢Â¢â‚¬â™¥â™¡â˜…â˜†â–²â–¼â†‘â†�â†“â†’Â¤â–³â™‚â™€â„ƒ||â–³Â©c/o||Â¿Â¡â„…â„¢Â®â‚¹Â°Â¢`â€¢âˆšÏ€Â¶âˆ†Â¢Â°âˆ†Â¶Ï€âˆšâ€¢`";
			if (source != null && blockCharacterSet.contains(("" + source))) {
				return "";
			}
			return null;
		}
	};

}
