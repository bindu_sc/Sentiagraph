package com.bc.partograph.common;

public interface FragmentState{
    void fragmentVisible();
}
