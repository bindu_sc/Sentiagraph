package com.bc.partograph.common;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import com.bluecrimson.partograph.R;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;

@SuppressLint("SimpleDateFormat")
public class AppContext extends Application {

	public static OutputStream logFile = null;
	public static PrintStream printlog = null;
	public static Date curDt = new Date();
	public static GregorianCalendar curCal = new GregorianCalendar();
	static String calStr = "";
//	public static String mainDir = Environment.getExternalStorageDirectory() + "/Partograph";
	public static String mainDir = Environment.getExternalStorageDirectory() + "/Partograph";//03April2017 Arpitha
	public static String dbDirRef;
	public static String logDirRef;
	public static String imgDirRef;

	public static Context context;
	private static SQLiteDatabase dbObj;

	// 23Mar2015
	public static SharedPreferences prefs = null;
	public static SharedPreferences.Editor editor = null;

	// 28Sep2016 -Arpitha
	public static String serverURL1 = "/BCWebS26Aug/pmessages/";
	public static boolean cSharp = false;

	//
	@Override
	public void onCreate() {
		super.onCreate();
		context = getApplicationContext();

		// 23Mar2015
		prefs = PreferenceManager.getDefaultSharedPreferences(context);
		editor = prefs.edit();
		
		
		
		//30March2017 Arpitha
		Locale locale = null;

		if (AppContext.prefs.getBoolean("isEnglish", false)) {
			locale = new Locale("en");
			Locale.setDefault(locale);
		} else if (AppContext.prefs.getBoolean("isHindi", false)) {
			locale = new Locale("hi");
			Locale.setDefault(locale);
		}

		Configuration config = new Configuration();
		config.locale = locale;
		context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());

		AppContext.setNamesAccordingToRes(context.getResources());// changed on
															// 30March2017
															// Arpitha
	}

	static {
		dbDirRef = "/" + mainDir + "/Database";
		logDirRef = "/" + mainDir + "/Logs";
		imgDirRef = "/" + mainDir + "/Images_Dir";

		new File(dbDirRef).mkdirs();
		new File(logDirRef).mkdirs();
		new File(imgDirRef).mkdirs();
		
		//System.out.println("Hello");

	}

	// Trace the app - which methods were called
	public static void addToTrace(String logStr) {
		try {
			String fName = context.getResources().getString(R.string.traceFileName);
			// 08jan2016- (month+1)
			calStr = (curCal.get(Calendar.DAY_OF_MONTH)) + "-" + (curCal.get(Calendar.MONTH) + 1) + "-"
					+ curCal.get(Calendar.YEAR);
			File f = new File(logDirRef + "/" + fName + "-" + calStr + ".txt");
			if (!f.exists())
				f.createNewFile();

			logFile = new FileOutputStream(f, true);
			printlog = new PrintStream(logFile);
			String sdf = new SimpleDateFormat("yyyy-MM-dd'/'HH:mm:ss").format(curDt);
			printlog.append(sdf + " - " + logStr + "\n\n");
			printlog.close();
			logFile.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

	// Write Log files
	public static void addLog(String logStr, Exception e) {
		try {
			String fName = context.getResources().getString(R.string.logFileName);
			// 08jan2016- (month+1)
			calStr = (curCal.get(Calendar.DAY_OF_MONTH)) + "-" + (curCal.get(Calendar.MONTH) + 1) + "-"
					+ curCal.get(Calendar.YEAR);
			File f = new File(logDirRef + "/" + fName + "-" + calStr + ".txt");
			if (!f.exists())
				f.createNewFile();

			logFile = new FileOutputStream(f, true);
			printlog = new PrintStream(logFile);
			String sdf = new SimpleDateFormat("yyyy-MM-dd'/'HH:mm:ss").format(curDt);
			printlog.append(sdf + "\n");
			printlog.append(logStr + "\n\n");
			e.printStackTrace(printlog);
			printlog.close();
			logFile.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

	// Create db object
	public static Partograph_DB createNewDbObj(Partograph_DB dbh) {
		dbObj = Partograph_DB.getInstance(context).db;
		return dbh;
	}

	public static SQLiteDatabase getDb() {
		return dbObj;
	}

	// Set the names based on language set
	public static void setNamesAccordingToRes(Resources res) {
		// TODO Auto-generated method stub
		if (prefs.getBoolean("isHindi", false)) {
			Locale locale = new Locale("hi");
			Locale.setDefault(locale);
			Configuration config = new Configuration();
			config.locale = locale;
			res.updateConfiguration(config, res.getDisplayMetrics());
		}
	}

	// 27dec2015 - check the status of the sim
	public static int checkSimState() {
		int simState = -1;
		// This is to know the status of the Sim
		TelephonyManager telMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		simState = telMgr.getSimState();

		return simState;
	}

	// This code checks Network is up or not
	public static boolean isNetworkUp() {
		ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		// 8jan2016
		NetworkInfo mWifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		if (cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isAvailable()
				&& cm.getActiveNetworkInfo().isConnected())
			return true;
		else if (mWifi.isConnected())
			return true;
		else
			return false;
	}

}
