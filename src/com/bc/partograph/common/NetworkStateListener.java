package com.bc.partograph.common;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;

public class NetworkStateListener extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {

		try {

			if (!Partograph_CommonClass.isNetworkTestAlreadyCalled) {

				switch (AppContext.checkSimState()) {
				case TelephonyManager.SIM_STATE_ABSENT:
					Partograph_CommonClass.isNetworkTestAlreadyCalled = false;
					break;
				case TelephonyManager.SIM_STATE_NETWORK_LOCKED:
					Partograph_CommonClass.isNetworkTestAlreadyCalled = false;
					break;
				case TelephonyManager.SIM_STATE_PIN_REQUIRED:
					Partograph_CommonClass.isNetworkTestAlreadyCalled = false;
					break;
				case TelephonyManager.SIM_STATE_PUK_REQUIRED:
					Partograph_CommonClass.isNetworkTestAlreadyCalled = false;
					break;
				case TelephonyManager.SIM_STATE_READY: {
					checkSimCurrentStatus(context);
					Partograph_CommonClass.isNetworkTestAlreadyCalled = true;
					break;
				}
				case TelephonyManager.SIM_STATE_UNKNOWN:
					Partograph_CommonClass.isNetworkTestAlreadyCalled = false;
					break;
				default:
					// Toast.makeText(context, "Network down",
					// Toast.LENGTH_SHORT).show();
					Partograph_CommonClass.isNetworkTestAlreadyCalled = false;
					break;
				}

			}

		} catch (Exception e) {
			Partograph_CommonClass.isNetworkTestAlreadyCalled = false;
			e.printStackTrace();
		}
	}

	// This code checks for the current state of the phone
	private void checkSimCurrentStatus(Context context) throws Exception {

		TelephonyManager telMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

		switch (telMgr.getCallState()) {
		case TelephonyManager.CALL_STATE_IDLE: {

			break;
		}
		case TelephonyManager.CALL_STATE_RINGING:
			break;
		case TelephonyManager.CALL_STATE_OFFHOOK:
			break;
		default:
			break;
		}
	}

}
