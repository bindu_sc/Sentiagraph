/*package com.bc.partograph.common;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnMultiChoiceClickListener;
import android.util.AttributeSet;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;


public class Multi extends Spinner implements MultiSelectionSpinner.OnMultipleItemsSelectedListener,
OnMultiChoiceClickListener {
	
//   updated on 7/6/16 by Arpitha
	public interface OnMultipleItemsSelectedListener{
        void selectedIndices(List<Integer> indices);
        void selectedStrings(List<String> strings);
    }
	
	private OnMultipleItemsSelectedListener listener;//Arpitha on7/6/16

    boolean[] mSelectionAtStart = null;//Arpitha on 7/6/16
    String _itemsAtStart = null;//Arpitha on 7/6/16

	String[] _items = null;
	boolean[] mSelection = null;

	ArrayAdapter<String> simple_adapter;

	public Multi(Context context) {
		super(context);
		try {
		simple_adapter = new ArrayAdapter<String>(context,
				android.R.layout.simple_spinner_item);
		super.setAdapter(simple_adapter);
		} catch(Exception e){
			e.printStackTrace();
			AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - " + new RuntimeException().getClass().getSimpleName(), e);
		}
	}

	public Multi(Context context, AttributeSet attrs) {
		super(context, attrs);
		try{
		simple_adapter = new ArrayAdapter<String>(context,
				android.R.layout.simple_spinner_item);
		super.setAdapter(simple_adapter);
		} catch(Exception e) {
			e.printStackTrace();
			AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - " + new RuntimeException().getClass().getSimpleName(), e);
		}
	}
//  updated on 7/6/16 by Arpitha	
	 public void setListener(OnMultipleItemsSelectedListener listener){
	        this.listener = listener;
	    }

	public void onClick(DialogInterface dialog, int which, boolean isChecked) {
		try {
			if (mSelection != null && which < mSelection.length) {
				mSelection[which] = isChecked;
	
				simple_adapter.clear();
				simple_adapter.add(buildSelectedItemString());
			} else {
				throw new IllegalArgumentException(
						"Argument 'which' is out of bounds.");
			}
		} catch(Exception e) {
			e.printStackTrace();
			AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - " + new RuntimeException().getClass().getSimpleName(), e);
		}
	}

	@Override
	public boolean performClick() {
		try {
			super.performClick();
			AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
			builder.setTitle("Please select!!!");
			builder.setMultiChoiceItems(_items, mSelection, this);
			  _itemsAtStart = getSelectedItemsAsString();
			  builder.setPositiveButton("Submit", new DialogInterface.OnClickListener() {
		            @Override
		            public void onClick(DialogInterface dialog, int which) {
		                System.arraycopy(mSelection, 0, mSelectionAtStart, 0, mSelection.length);
		                try {
							listener.selectedIndices(getSelectedIndicies());
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
		                try {
							listener.selectedStrings(getSelectedStrings());
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
		            }
		        });
		        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
		            @Override
		            public void onClick(DialogInterface dialog, int which) {
		                simple_adapter.clear();
		                simple_adapter.add(_itemsAtStart);
		                System.arraycopy(mSelectionAtStart, 0, mSelection, 0, mSelectionAtStart.length);
		            }
		        });
			builder.show();
			return true;
		} catch(Exception e){
			e.printStackTrace();
			AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - " + new RuntimeException().getClass().getSimpleName(), e);
			return false;
		}
	}

	@Override
	public void setAdapter(SpinnerAdapter adapter) {
		throw new RuntimeException(
				"setAdapter is not supported by MultiSelectSpinner.");
	}

	public void setItems(String[] items) {
		try{
			_items = items;
			mSelection = new boolean[_items.length];
			 mSelectionAtStart = new boolean[_items.length];
			simple_adapter.clear();
			simple_adapter.add(_items[0]);
			Arrays.fill(mSelection, false);
		} catch(Exception e) {
			e.printStackTrace();
			AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - " + new RuntimeException().getClass().getSimpleName(), e);
		}
	}

	public void setItems(List<String> items) {
		try {
			_items = items.toArray(new String[items.size()]);
			mSelection = new boolean[_items.length];
			 mSelectionAtStart = new boolean[_items.length];
			simple_adapter.clear();
			simple_adapter.add(_items[0]);
			Arrays.fill(mSelection, false);
		} catch(Exception e){
			e.printStackTrace();
			AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - " + new RuntimeException().getClass().getSimpleName(), e);
		}
	}

	public void setSelection(String[] selection) {
		try {
			for (String cell : selection) {
				for (int j = 0; j < _items.length; ++j) {
					if (_items[j].equals(cell)) {
						mSelection[j] = true;
					}
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
			AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - " + new RuntimeException().getClass().getSimpleName(), e);
		}
	}

	public void setSelection(List<String> selection) {
		try {
			for (int i = 0; i < mSelection.length; i++) {
				mSelection[i] = false;
				mSelectionAtStart[i] = false;
			}
			for (String sel : selection) {
				for (int j = 0; j < _items.length; ++j) {
					if (_items[j].equals(sel)) {
						mSelection[j] = true;
						mSelectionAtStart[j] = true;
					}
				}
			}
			simple_adapter.clear();
			simple_adapter.add(buildSelectedItemString());
		}catch(Exception e){
			e.printStackTrace();
			AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - " + new RuntimeException().getClass().getSimpleName(), e);
		}
	}

	public void setSelection(int index) {
		try {
			for (int i = 0; i < mSelection.length; i++) {
				mSelection[i] = false;
				mSelectionAtStart[i] = false;
			}
			if (index >= 0 && index < mSelection.length) {
				mSelection[index] = true;
				mSelectionAtStart[index] = true;
			} else {
				throw new IllegalArgumentException("Index " + index
						+ " is out of bounds.");
			}
			simple_adapter.clear();
			simple_adapter.add(buildSelectedItemString());
		} catch(Exception e){
			e.printStackTrace();
			AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - " + new RuntimeException().getClass().getSimpleName(), e);
		}
	}

	public void setSelection(int[] selectedIndicies) {
		try {
			for (int i = 0; i < mSelection.length; i++) {
				mSelection[i] = false;
			}
			for (int index : selectedIndicies) {
				if (index >= 0 && index < mSelection.length) {
					mSelection[index] = true;
				} else {
					throw new IllegalArgumentException("Index " + index
							+ " is out of bounds.");
				}   
			}
			simple_adapter.clear();
			simple_adapter.add(buildSelectedItemString());
		} catch(Exception e){
			e.printStackTrace();
			AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - " + new RuntimeException().getClass().getSimpleName(), e);
		}
	}

	public List<String> getSelectedStrings() throws Exception {
		List<String> selection = new LinkedList<String>();
		try{
	
		for (int i = 0; i < _items.length; ++i) {
			if (mSelection[i]) {
				selection.add(_items[i]);
			}
		
	}
		}catch(Exception e)
		{
			e.printStackTrace();
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			return null;
		}
		return selection;
	}

	public List<Integer> getSelectedIndicies() throws Exception{
		List<Integer> selection = new LinkedList<Integer>();
		
		for (int i = 0; i < _items.length; ++i) {
			if (mSelection[i]) {
				selection.add(i);
			}
		
		}
		return selection;
	}

	private String buildSelectedItemString() throws Exception{
		StringBuilder sb = new StringBuilder();
		boolean foundOne = false;

		for (int i = 0; i < _items.length; ++i) {
			if (mSelection[i]) {
				if (foundOne) {
					sb.append(", ");
				}
				foundOne = true;

				sb.append(_items[i]);
			}
		}
		return sb.toString();
	}

	public String getSelectedItemsAsString() throws Exception{
		StringBuilder sb = new StringBuilder();
		boolean foundOne = false;

		for (int i = 0; i < _items.length; ++i) {
			if (mSelection[i]) {
				if (foundOne) {
					sb.append(", ");
				}
				foundOne = true;
				sb.append(_items[i]);
			}
		}
		return sb.toString();
	}

	@Override
	public void selectedIndices(List<Integer> indices) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void selectedStrings(List<String> strings) {
		// TODO Auto-generated method stub
		
	}
}
*/