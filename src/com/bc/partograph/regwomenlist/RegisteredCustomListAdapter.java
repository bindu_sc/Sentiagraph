//02Jun2017 Arpitha - Adapter for Registeredwomen list
package com.bc.partograph.regwomenlist;

import java.util.ArrayList;

import com.bc.partograph.common.AppContext;
import com.bc.partograph.common.Partograph_CommonClass;
import com.bc.partograph.common.Partograph_DB;
import com.bluecrimson.partograph.R;
import com.bluecrimson.partograph.Women_Profile_Pojo;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

public class RegisteredCustomListAdapter extends ArrayAdapter<Women_Profile_Pojo> {
	Context context;
	ArrayList<Women_Profile_Pojo> Regdata;
	Partograph_DB dbh;
	String risk_observed;
	int pos;
	private static byte[] image1 = null;
	ArrayList<Women_Profile_Pojo> mStringFilterListRegistered;// 05Jun2017
	ValueFilter valueFilterReg;// 05Jun2017 Arpitha

	// constructor
	public RegisteredCustomListAdapter(Context context, int textViewResourceId, ArrayList<Women_Profile_Pojo> data,
			Partograph_DB dbh) {
		super(context, textViewResourceId, data);
		this.context = context;
		this.dbh = dbh;
		this.Regdata = data;
		this.mStringFilterListRegistered = data;

	}

	private class WomenListItem {
		TextView txtWomenname;
		TextView txtstatus;
		TextView txtdate_of_adm;
		ImageView imgwphoto;
		ImageView imghighrisk;
		TextView txtbreech;
	}

	@Override
	public int getViewTypeCount() {
		return getCount();
	}

	@Override
	public int getItemViewType(int position) {
		return position;
	}

	@Override
	public int getCount() {
		return (Regdata != null && Regdata.size() > 0) ? Regdata.size() : 0;
	}

	@Override
	public Women_Profile_Pojo getItem(int position) {
		return Regdata.get(position);
	}

	// Get main view
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		try {
			WomenListItem holder = null;
			final Women_Profile_Pojo rowItem = getItem(position);

			LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

			if (convertView == null) {
				convertView = mInflater.inflate(R.layout.registeredcustomlistadapter, null);

				holder = new WomenListItem();
				holder.txtWomenname = (TextView) convertView.findViewById(R.id.txtwnamereg);
				holder.txtstatus = (TextView) convertView.findViewById(R.id.txtstatusreg);
				holder.txtdate_of_adm = (TextView) convertView.findViewById(R.id.txtDateOfAdmreg);
				holder.imgwphoto = (ImageView) convertView.findViewById(R.id.imgwphotoreg);
				holder.imghighrisk = (ImageView) convertView.findViewById(R.id.imgriskreg);
				holder.txtbreech = (TextView) convertView.findViewById(R.id.txtbreechreg);

				convertView.setTag(holder);
			} else
				holder = (WomenListItem) convertView.getTag();

			holder.txtWomenname.setText(rowItem.getWomen_name());

			if (rowItem.getGestationage() == 0) {
				holder.txtstatus.setText(context.getResources().getString(R.string.gest_nt_known));

			} else {

				holder.txtstatus
						.setText(rowItem.getGestationage() + " " + context.getResources().getString(R.string.weeks));

			}

			holder.txtdate_of_adm
					.setText(rowItem.getDate_of_admission() == null ? " "
							: context.getResources().getString(R.string.regdate) + " : "
									+ Partograph_CommonClass.getConvertedDateFormat(rowItem.getDate_of_admission(),
											Partograph_CommonClass.defdateformat)
									+ " / " + rowItem.getTime_of_admission());

			image1 = rowItem.getWomen_Image();
			if (image1 != null)
				new DownloadImageTask().execute(holder);

			int risk_cat = rowItem.getRisk_category();

			if (risk_cat == 1) {
				holder.imghighrisk.setImageResource(R.drawable.ic_hr);

				holder.imghighrisk.setOnTouchListener(new OnTouchListener() {

					@Override
					public boolean onTouch(View v, MotionEvent event) {
						try {

							if (MotionEvent.ACTION_UP == event.getAction()) {

								risk_observed = rowItem.getComments();

								if (rowItem.getRiskoptions().length() > 0
										|| (rowItem.getComments() != null && rowItem.getComments().length() > 0)) {
									Partograph_CommonClass.displayHighRiskReasons(rowItem.getRiskoptions(),
											risk_observed, context);
								}
							}
						} catch (Exception e) {
							AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
									+ this.getClass().getSimpleName(), e);
							e.printStackTrace();
						}
						return true;
					}
				});

			}

			holder.imgwphoto.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					try {
						pos = position;
						Partograph_CommonClass.displaySummaryDialog(Regdata, pos, context);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}
				}
			});

			if (rowItem.getNormaltype() == 2)
				holder.txtbreech.setVisibility(View.VISIBLE);
			else
				holder.txtbreech.setVisibility(View.GONE);

		}

		catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}

		return convertView;
	}

	public class DownloadImageTask extends AsyncTask<WomenListItem, Void, Bitmap> {

		ImageView imageView = null;

		protected Bitmap doInBackground(WomenListItem... item) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			this.imageView = (ImageView) item[0].imgwphoto;

			return getBitmapDownloaded();
		}

		protected void onPostExecute(Bitmap result) {
			if (result != null)
				imageView.setImageBitmap(result);
		}

		/** This function downloads the image and returns the Bitmap **/
		private Bitmap getBitmapDownloaded() {
			Bitmap btmp = null;
			if (image1 != null) {
				btmp = Bitmap.createScaledBitmap(BitmapFactory.decodeByteArray(image1, 0, image1.length), 64, 64,
						false);
			}
			return btmp;
		}
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	// 05Jun2017 Arpitha
	private class ValueFilter extends Filter {
		@Override
		protected FilterResults performFiltering(CharSequence constraint) {
			FilterResults resultsReg = new FilterResults();

			if (constraint != null && constraint.length() > 0) {
				ArrayList<Women_Profile_Pojo> filterList = new ArrayList<Women_Profile_Pojo>();
				for (int i = 0; i < mStringFilterListRegistered.size(); i++) {
					if (mStringFilterListRegistered.get(i).getWomen_name().toUpperCase()
							.contains(constraint.toString().toUpperCase())
							|| mStringFilterListRegistered.get(i).getPhone_No()
									.contains(constraint.toString().toUpperCase())) {

						filterList.add(mStringFilterListRegistered.get(i));
					}

				}
				resultsReg.count = filterList.size();
				resultsReg.values = filterList;
			} else {
				resultsReg.count = mStringFilterListRegistered.size();
				resultsReg.values = mStringFilterListRegistered;
			}
			return resultsReg;

		}

		@Override
		protected void publishResults(CharSequence constraint, FilterResults results) {
			Regdata = (ArrayList<Women_Profile_Pojo>) results.values;
			notifyDataSetChanged();
		}

	}

	@Override
	public Filter getFilter() {
		if (valueFilterReg == null) {
			valueFilterReg = new ValueFilter();
		}
		return valueFilterReg;
	}

}
