package com.bc.partograph.regwomenlist;

import java.util.ArrayList;

import com.bc.partograph.common.AppContext;
import com.bc.partograph.common.Partograph_CommonClass;
import com.bc.partograph.common.Partograph_DB;
import com.bc.partograph.womenview.Activity_WomenView;
import com.bluecrimson.partograph.R;
import com.bluecrimson.partograph.Women_Profile_Pojo;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

public class RecentCustomListAdapterRetrospective extends ArrayAdapter<Women_Profile_Pojo> {
	Context context;
	ArrayList<Women_Profile_Pojo> data;
	Partograph_DB dbh;
	ArrayList<Women_Profile_Pojo> mStringFilterList_dip;
	ValueFilter valueFilter;
	int pos;
	private static byte[] image1 = null;// 08Nov2016

	// constructor
	public RecentCustomListAdapterRetrospective(Context context, int textViewResourceId,
			ArrayList<Women_Profile_Pojo> data, Partograph_DB dbh) {
		super(context, textViewResourceId, data);
		this.context = context;
		this.dbh = dbh;
		this.data = data;
		this.mStringFilterList_dip = data;

	}

	private class WomenListItem {
		TextView txtWomenname;
		TextView txtstatus;
		TextView txtdate_of_adm;
		ImageView imgwphoto;
		TextView txtbreech;// 07May2017 Arpitha - v2.6
		TextView txtref;// 11May2017 Arpitha - v2.6
		TextView txtdel;// 11May2017 Arpitha - v2.6
	}

	@Override
	public int getViewTypeCount() {
		return getCount();
	}

	@Override
	public int getItemViewType(int position) {
		return position;
	}

	// 21oct2016 Arpitha
	@Override
	public int getCount() {
		return (data != null && data.size() > 0) ? data.size() : 0;
	}

	@Override
	public Women_Profile_Pojo getItem(int position) {
		return data.get(position);
	}

	// Get main view
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		try {
			WomenListItem holder = null;
			final Women_Profile_Pojo rowItem = getItem(position);

			LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

			if (convertView == null) {
				convertView = mInflater.inflate(R.layout.retrospective_adapter, null);

				holder = new WomenListItem();
				holder.txtWomenname = (TextView) convertView.findViewById(R.id.txtwname);
				holder.txtstatus = (TextView) convertView.findViewById(R.id.txtstatus);
				holder.txtdate_of_adm = (TextView) convertView.findViewById(R.id.txtDateOfAdm);
				holder.imgwphoto = (ImageView) convertView.findViewById(R.id.imgwphoto);
				// 07May2017 Arpitha- v2.6
				holder.txtbreech = (TextView) convertView.findViewById(R.id.txtbreechret);
				// 11May2017 Arpitha- v2.6
				holder.txtref = (TextView) convertView.findViewById(R.id.txtref);
				holder.txtdel = (TextView) convertView.findViewById(R.id.txtdel);

				convertView.setTag(holder);
			} else
				holder = (WomenListItem) convertView.getTag();

			String[] deltype = context.getResources().getStringArray(R.array.del_type);

			holder.txtWomenname.setText(rowItem.getWomen_name() == null ? " " : rowItem.getWomen_name());

			if (rowItem.getGestationage() == 0) {
				holder.txtstatus
						.setText(rowItem.getDel_type() == 0 ? context.getResources().getString(R.string.gest_nt_known)
								: deltype[rowItem.getDel_type() - 1]);

			} else {

				holder.txtstatus.setText(rowItem.getDel_type() == 0
						? rowItem.getGestationage() + " " + context.getResources().getString(R.string.weeks)
						: deltype[rowItem.getDel_type() - 1]);

			}

			holder.txtdate_of_adm.setText(rowItem.getDate_of_reg_entry() == null ? " "
					: context.getResources().getString(R.string.doe) + " : "
							+ Partograph_CommonClass.getConvertedDateFormat(rowItem.getDate_of_reg_entry(),
									Partograph_CommonClass.defdateformat)
							+ "/" + rowItem.getDate_of_reg_entry().split("/")[1]);

			// 08Nov2016
			image1 = rowItem.getWomen_Image();
			if (image1 != null)
				new DownloadImageTask().execute(holder);

			if (rowItem.getDel_type() != 0) {
				holder.txtdel.setVisibility(View.VISIBLE);// 11May2017 Arpitha
															// -v2.6

			} else
				holder.txtdel.setVisibility(View.GONE);// 11May2017 Arpitha -
														// v2.6

			// 21oct2016 Arpitha
			holder.imgwphoto.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					try {
						pos = position;// 01Nov2016 Arpitha
						Partograph_CommonClass.displaySummaryDialog(data, position, context);
						// displayConfirmationAlert_summary("", "");
					} catch (Exception e) {
						// TODO Auto-generated catch block
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}
				}
			});

			// 07May2017 Arpitha - v2.6
			if (rowItem.getNormaltype() == 2)
				holder.txtbreech.setVisibility(View.VISIBLE);
			else
				holder.txtbreech.setVisibility(View.GONE);// 07May2017 Arpitha -
															// v2.6

			if (Activity_WomenView.referredWomen.contains(rowItem.getWomenId()))
				holder.txtref.setVisibility(View.VISIBLE);
			else
				holder.txtref.setVisibility(View.GONE);// 11May2017 Arpitha -
														// v2.6

		}

		catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}

		return convertView;
	}

	private class ValueFilter extends Filter {

		@Override
		protected FilterResults performFiltering(CharSequence constraint) {
			FilterResults results = new FilterResults();

			if (constraint != null && constraint.length() > 0) {
				ArrayList<Women_Profile_Pojo> filterList = new ArrayList<Women_Profile_Pojo>();
				for (int i = 0; i < mStringFilterList_dip.size(); i++) {
					if (mStringFilterList_dip.get(i).getWomen_name().toUpperCase()
							.contains(constraint.toString().toUpperCase())
							|| mStringFilterList_dip.get(i).getPhone_No()
									.contains(constraint.toString().toUpperCase())) {

						filterList.add(mStringFilterList_dip.get(i));
					}

				}
				results.count = filterList.size();
				results.values = filterList;
			} else {
				results.count = mStringFilterList_dip.size();
				results.values = mStringFilterList_dip;
			}
			return results;

		}

		@Override
		protected void publishResults(CharSequence constraint, FilterResults results) {
			data = (ArrayList<Women_Profile_Pojo>) results.values;
			notifyDataSetChanged();
		}

	}

	@Override
	public Filter getFilter() {
		if (valueFilter == null) {
			valueFilter = new ValueFilter();
		}
		return valueFilter;
	}

	// 08Nov2016
	public class DownloadImageTask extends AsyncTask<WomenListItem, Void, Bitmap> {

		ImageView imageView = null;

		protected Bitmap doInBackground(WomenListItem... item) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			this.imageView = (ImageView) item[0].imgwphoto;
			return getBitmapDownloaded();
		}

		protected void onPostExecute(Bitmap result) {
			if (result != null)
				imageView.setImageBitmap(result);
		}

		/** This function downloads the image and returns the Bitmap **/
		private Bitmap getBitmapDownloaded() {
			Bitmap btmp = null;
			if (image1 != null) {
				btmp = Bitmap.createScaledBitmap(BitmapFactory.decodeByteArray(image1, 0, image1.length), 64, 64,
						false);
			}
			return btmp;
		}
	}

}
