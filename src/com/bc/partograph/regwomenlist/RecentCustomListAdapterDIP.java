package com.bc.partograph.regwomenlist;

import java.util.ArrayList;
import java.util.HashMap;

import com.bc.partograph.common.AppContext;
import com.bc.partograph.common.Partograph_CommonClass;
import com.bc.partograph.common.Partograph_DB;
import com.bluecrimson.partograph.R;
import com.bluecrimson.partograph.Women_Profile_Pojo;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

public class RecentCustomListAdapterDIP extends ArrayAdapter<Women_Profile_Pojo> {
	Context context;
	ArrayList<Women_Profile_Pojo> data;
	Partograph_DB dbh;
	String risk_observed;
	// 01Nov2016 Arpitha
	int pos;
	private static byte[] image1 = null;// 08Nov2016
	ArrayList<String> val;// 17Jan2017 Arpitha
	HashMap<String, ArrayList<String>> partoData = new HashMap<String, ArrayList<String>>();// 01Feb2017
																							// Arpitha
	ArrayList<Women_Profile_Pojo> mStringFilterListDIP;// 05Jun2017 Arpitha
	ValueFilterDIP valueFilterDIP;// 05Jun2017 Arpitha
	String message = "";

	// constructor
	public RecentCustomListAdapterDIP(Context context, int textViewResourceId, ArrayList<Women_Profile_Pojo> data,
			Partograph_DB dbh) {
		super(context, textViewResourceId, data);
		this.context = context;
		this.dbh = dbh;
		this.data = data;
		this.mStringFilterListDIP = data;

	}

	private class WomenListItem {
		TextView txtWomenname;
		TextView txtstatus;
		TextView txtdate_of_adm;
		ImageView imgwphoto;
		ImageView imgdangerSign;
		ImageView imghighrisk;
		ImageView imgmsg;
		TextView txtcommentcount;
		ImageView imgnotification;
		TextView txtbreech;// 07May2017 Arpitha - v2.6
	}

	@Override
	public int getViewTypeCount() {
		return getCount();
	}

	@Override
	public int getItemViewType(int position) {
		return position;
	}

	// 21oct2016 Arpitha
	@Override
	public int getCount() {
		return (data != null && data.size() > 0) ? data.size() : 0;
	}

	@Override
	public Women_Profile_Pojo getItem(int position) {
		return data.get(position);
	}

	// Get main view
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		try {
			WomenListItem holder = null;
			final Women_Profile_Pojo rowItem = getItem(position);

			LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

			if (convertView == null) {
				convertView = mInflater.inflate(R.layout.dip_adapter, null);

				holder = new WomenListItem();
				holder.txtWomenname = (TextView) convertView.findViewById(R.id.txtwname);
				holder.txtstatus = (TextView) convertView.findViewById(R.id.txtstatus);
				holder.txtdate_of_adm = (TextView) convertView.findViewById(R.id.txtDateOfAdm);
				holder.imgwphoto = (ImageView) convertView.findViewById(R.id.imgwphoto);
				holder.imgdangerSign = (ImageView) convertView.findViewById(R.id.imgdangersign);
				holder.imghighrisk = (ImageView) convertView.findViewById(R.id.imgrisk);
				holder.imgmsg = (ImageView) convertView.findViewById(R.id.imgmsg);
				holder.txtcommentcount = (TextView) convertView.findViewById(R.id.txtcommentcount);
				holder.imgnotification = (ImageView) convertView.findViewById(R.id.imgnotification);
				holder.txtbreech = (TextView) convertView.findViewById(R.id.txtbreechdip);// 07May2017
																							// Arpitha
																							// -
																							// v2.6

				convertView.setTag(holder);
			} else
				holder = (WomenListItem) convertView.getTag();

			final String womenid = rowItem.getWomenId();

			holder.txtWomenname.setText(rowItem.getWomen_name());

			if (rowItem.getGestationage() == 0) {
				holder.txtstatus.setText(context.getResources().getString(R.string.gest_nt_known));

			} else {

				holder.txtstatus
						.setText(rowItem.getGestationage() + " " + context.getResources().getString(R.string.weeks));

			}

			holder.txtdate_of_adm
					.setText(rowItem.getDate_of_admission() == null ? " "
							: context.getResources().getString(R.string.regdate) + " : "
									+ Partograph_CommonClass.getConvertedDateFormat(rowItem.getDate_of_admission(),
											Partograph_CommonClass.defdateformat)
									+ " / " + rowItem.getTime_of_admission());

			// 08Nov2016
			image1 = rowItem.getWomen_Image();
			if (image1 != null)
				new DownloadImageTask().execute(holder);

			int risk_cat = rowItem.getRisk_category();

			int comment_count = dbh.getComentsCount(rowItem.getWomenId(), Partograph_CommonClass.user.getUserId());

			if (comment_count > 0) {
				holder.imgmsg.setVisibility(View.VISIBLE);
				holder.txtcommentcount.setVisibility(View.VISIBLE);
				holder.txtcommentcount.setText("" + comment_count);
			}

			if (risk_cat == 1) {
				holder.imghighrisk.setImageResource(R.drawable.ic_hr);

				holder.imghighrisk.setOnTouchListener(new OnTouchListener() {

					@Override
					public boolean onTouch(View v, MotionEvent event) {
						try {

							if (MotionEvent.ACTION_UP == event.getAction()) {

								risk_observed = rowItem.getComments();

								if (rowItem.getRiskoptions().length() > 0
										|| (rowItem.getComments() != null && rowItem.getComments().length() > 0)) {
									Partograph_CommonClass.displayHighRiskReasons(rowItem.getRiskoptions(),
											risk_observed, context);
								}
							}
						} catch (Exception e) {
							AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
									+ this.getClass().getSimpleName(), e);
							e.printStackTrace();
						}
						return true;
					}
				});

			}

			if (rowItem.isDanger()) {
				holder.imgdangerSign.setImageResource(R.drawable.ic_compl);

				holder.imgdangerSign.setOnTouchListener(new View.OnTouchListener() {

					@Override
					public boolean onTouch(View v, MotionEvent event) {

						try {

							ArrayList<String> arr;

							arr = dbh.getobjectid_danger(rowItem.getWomenId());

							if (MotionEvent.ACTION_UP == event.getAction()) {

								Partograph_CommonClass.displayDangervalues(arr, context);

							}
						} catch (Exception e) {
							AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
									+ this.getClass().getSimpleName(), e);
							e.printStackTrace();
						}

						return true;
					}
				});
			}

			val = new ArrayList<String>();
			val = dbh.getNotificationData(womenid, rowItem.getUserId());

			partoData.put(womenid, val);// 01Feb2017 Arpitha

			if (val.size() > 0 && (val.contains("1") || val.contains("2"))) {
				holder.imgnotification.setVisibility(View.VISIBLE);

				holder.imgnotification.setOnTouchListener(new OnTouchListener() {

					@Override
					public boolean onTouch(View v, MotionEvent event) {
						if (MotionEvent.ACTION_UP == event.getAction()) {

							try {
								if (partoData != null && partoData.size() > 0) {

									for (int i = 0; i < partoData.get(womenid).size(); i++) {

										if (partoData.get(womenid).get(i).equalsIgnoreCase("2")
												|| partoData.get(womenid).get(i).equalsIgnoreCase("1")) {

											if (i == 0) {
												message = message + "\n"
														+ context.getResources().getString(R.string.fhr);
											} else if (i == 1) {
												message = message + "\n"
														+ context.getResources().getString(R.string.dilatation);
											} else if (i == 2) {
												message = message + "\n"
														+ context.getResources().getString(R.string.contraction);
											} else {
												message = message + "\n"
														+ context.getResources().getString(R.string.pulse_bp);
											}

										}

									}
								}
								Partograph_CommonClass.displayAlertDialog(
										context.getResources().getString(R.string.notify_alert_mess) + "\n" + message,
										context);
							} catch (Exception e) {
								e.printStackTrace();
							}

						}
						return true;
					}
				});

			}

			// 21oct2016 Arpitha
			holder.imgwphoto.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					try {
						pos = position;// 01Nov2016 Arpitha
						Partograph_CommonClass.displaySummaryDialog(data, pos, context);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}
				}
			});

			// 07May2017 Arpitha - v2.6
			if (rowItem.getNormaltype() == 2)
				holder.txtbreech.setVisibility(View.VISIBLE);
			else
				holder.txtbreech.setVisibility(View.GONE);// 07May2017 Arpitha -
															// v2.6

		}

		catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}

		return convertView;
	}

	// 08Nov2016
	public class DownloadImageTask extends AsyncTask<WomenListItem, Void, Bitmap> {

		ImageView imageView = null;

		protected Bitmap doInBackground(WomenListItem... item) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			this.imageView = (ImageView) item[0].imgwphoto;

			return getBitmapDownloaded();
		}

		protected void onPostExecute(Bitmap result) {
			if (result != null)
				imageView.setImageBitmap(result);
		}

		/** This function downloads the image and returns the Bitmap **/
		private Bitmap getBitmapDownloaded() {
			Bitmap btmp = null;
			if (image1 != null) {
				btmp = Bitmap.createScaledBitmap(BitmapFactory.decodeByteArray(image1, 0, image1.length), 64, 64,
						false);
			}
			return btmp;
		}
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	// 05Jun2017 Arpitha
	private class ValueFilterDIP extends Filter {
		@Override
		protected FilterResults performFiltering(CharSequence constraint) {

			FilterResults resultsDIP = new FilterResults();

			if (constraint != null && constraint.length() > 0) {
				ArrayList<Women_Profile_Pojo> filterListDIP = new ArrayList<Women_Profile_Pojo>();
				for (int i = 0; i < mStringFilterListDIP.size(); i++) {
					if (mStringFilterListDIP.get(i).getWomen_name().toUpperCase()
							.contains(constraint.toString().toUpperCase())
							|| mStringFilterListDIP.get(i).getPhone_No()
									.contains(constraint.toString().toUpperCase())) {
						filterListDIP.add(mStringFilterListDIP.get(i));
					}

				}

				resultsDIP.count = filterListDIP.size();
				resultsDIP.values = filterListDIP;
			} else {
				resultsDIP.count = mStringFilterListDIP.size();
				resultsDIP.values = mStringFilterListDIP;
			}
			return resultsDIP;

		}

		@Override
		protected void publishResults(CharSequence constraint, FilterResults results) {
			// if (Looper.myLooper() == null)
			// Looper.prepare();
			data = (ArrayList<Women_Profile_Pojo>) results.values;
			notifyDataSetChanged();
		}

	}

	@Override
	public Filter getFilter() {

		if (valueFilterDIP == null) {
			valueFilterDIP = new ValueFilterDIP();
		}
		return valueFilterDIP;
	}

}
