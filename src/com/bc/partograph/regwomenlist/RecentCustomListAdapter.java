/*package com.bc.partograph.regwomenlist;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources.NotFoundException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bc.partograph.common.AppContext;
import com.bc.partograph.common.MultiSelectionSpinner;
import com.bc.partograph.common.Partograph_CommonClass;
import com.bc.partograph.common.Partograph_DB;
import com.bluecrimson.partograph.R;
import com.bluecrimson.partograph.WomenReferral_pojo;
import com.bluecrimson.partograph.Women_Profile_Pojo;

public class RecentCustomListAdapter extends ArrayAdapter<Women_Profile_Pojo> implements AnimationListener {
	Context context;
	ArrayList<Women_Profile_Pojo> data;
	Partograph_DB dbh;
	String todaysDate;
	String currTime;
	// Animation
	Animation animBlink;
	ArrayList<String> referral_women;
	public static LinkedHashMap<String, Integer> riskoptionsMap;
	EditText etmesss;
	ArrayList<WomenReferral_pojo> rpojo;
	int mSelectedRow = 0;
	MultiSelectionSpinner mspnreasonforreferral;
	String admResDisplay = "";
	String strCurrentTime;
	String user_id;
	String lastdate;
	String lasttime;
	boolean ispulsetimevalid = false;
	String currenttime;
	String islasttime;
	boolean isfhrtimevalid = false;
	boolean isdilatationtimevalid = false;
	boolean iscontractiontimevalid = false;
	String risk_observed;
	String ref_phno;
	int option = 2;
	ArrayList<String> values;
	String reg_phno;
	String p_no = "";
	// changed on 3August2016 by Arpitha
	boolean isReferred;
	ArrayList<WomenReferral_pojo> refdata;

	// constructor
	public RecentCustomListAdapter(Context context, int textViewResourceId, ArrayList<Women_Profile_Pojo> data,
			Partograph_DB dbh) {
		super(context, textViewResourceId, data);
		this.context = context;
		this.dbh = dbh;
		this.data = data;

	}

	private class WomenListItem {
		TextView txtWomenname;
		TextView txtstatus;
		TextView txtdate_of_adm;
		TextView txttime_of_adm;
		ImageView imgwphoto;
		ImageView imgdangerSign;
		ImageView imghighrisk;
		ImageView imgmsg;
		TextView txtcommentcount;
		TextView txtdelstatusnotupdated;

		// 10Dec2015
		TextView txtreferred;
		TextView txtdateofdelivery;
		ImageView imgnotification;
		TextView txtreferredplace;
		// TextView txtplace;
		MultiSelectionSpinner mspnriskoptions;
		TextView txtdateofreferal;
		ImageView imgsms;
	}

	@Override
	public int getViewTypeCount() {
		return getCount();
	}

	@Override
	public int getItemViewType(int position) {
		return position;
	}

	// Get main view
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		try {
			WomenListItem holder = null;
			final Women_Profile_Pojo rowItem = getItem(position);

			LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

			if (convertView == null) {
				convertView = mInflater.inflate(R.layout.new_womenlist_adapter, null);

				holder = new WomenListItem();
				holder.txtWomenname = (TextView) convertView.findViewById(R.id.txtwname);
				holder.txtstatus = (TextView) convertView.findViewById(R.id.txtstatus);
				holder.txtdate_of_adm = (TextView) convertView.findViewById(R.id.txtDateOfAdm);
				holder.txttime_of_adm = (TextView) convertView.findViewById(R.id.txttimeofadm);
				holder.imgwphoto = (ImageView) convertView.findViewById(R.id.imgwphoto);
				holder.imgdangerSign = (ImageView) convertView.findViewById(R.id.imgdangersign);
				holder.imghighrisk = (ImageView) convertView.findViewById(R.id.imgrisk);
				holder.imgmsg = (ImageView) convertView.findViewById(R.id.imgmsg);
				holder.txtcommentcount = (TextView) convertView.findViewById(R.id.txtcommentcount);
				holder.txtdelstatusnotupdated = (TextView) convertView.findViewById(R.id.txtdelstatusnotupdated);
				holder.txtdateofdelivery = (TextView) convertView.findViewById(R.id.txtadddate);
				holder.txtreferred = (TextView) convertView.findViewById(R.id.txtreferred);
				holder.imgnotification = (ImageView) convertView.findViewById(R.id.imgnotification);
				holder.txtreferredplace = (TextView) convertView.findViewById(R.id.referredplace);
				holder.txtdateofreferal = (TextView) convertView.findViewById(R.id.txtdateofreferral);
				holder.imgsms = (ImageView) convertView.findViewById(R.id.imgsms);
				mspnreasonforreferral = (MultiSelectionSpinner) convertView.findViewById(R.id.mspnreasonforreferral);

				convertView.setTag(holder);
			} else
				holder = (WomenListItem) convertView.getTag();

			setRiskOptions();

			// load the animation
			animBlink = AnimationUtils.loadAnimation(context, R.anim.blink);

			holder.imgmsg.setVisibility(View.GONE);
			holder.imgnotification.setVisibility(View.GONE);
			holder.txtreferredplace.setVisibility(View.GONE);
			holder.txtdateofreferal.setVisibility(View.GONE);
			holder.imgsms.setVisibility(View.GONE);

			final String womenid = rowItem.getWomenId();
			user_id = rowItem.getUserId();

			String[] deltype = context.getResources().getStringArray(R.array.del_type);
			holder.txtWomenname.setText(rowItem.getWomen_name() == null ? " " : rowItem.getWomen_name());

			// updated on 17july2016 by Arpitha

			if (rowItem.getGestationage() == 0) {
				holder.txtstatus
						.setText(rowItem.getDel_type() == 0 ? context.getResources().getString(R.string.gest_nt_known)
								: deltype[rowItem.getDel_type() - 1]);

			} else {

				holder.txtstatus.setText(rowItem.getDel_type() == 0
						? rowItem.getGestationage() + " " + context.getResources().getString(R.string.weeks)
						: deltype[rowItem.getDel_type() - 1]);

			}

			holder.txtdate_of_adm
					.setText(rowItem.getDate_of_admission() == null ? " "
							: context.getResources().getString(R.string.regdate) + " : "
									+ Partograph_CommonClass.getConvertedDateFormat(rowItem.getDate_of_admission(),
											Partograph_CommonClass.defdateformat)
									+ " / " + rowItem.getTime_of_admission());

			try {

				isReferred = dbh.getisWomenReferred(rowItem.getWomenId(), Partograph_CommonClass.user.getUserId());
				if (isReferred) {
					holder.txtreferred.setText(context.getResources().getString(R.string.referred));
				} else {
					holder.txtreferred.setText("");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			byte[] image1 = rowItem.getWomen_Image();
			if (image1 != null) {
				Bitmap btmp = Bitmap.createScaledBitmap(BitmapFactory.decodeByteArray(image1, 0, image1.length), 64, 64,
						false);
				holder.imgwphoto.setImageBitmap(btmp);
			}

			int risk_cat = rowItem.getRisk_category();

			if (risk_cat == 1) {
				holder.imghighrisk.setImageResource(R.drawable.highrisk);

			} else
				holder.imghighrisk.setImageResource(0);// changed on 13May16

			if (rowItem.isDanger())
				holder.imgdangerSign.setImageResource(R.drawable.dangersign_icon);
			else
				holder.imgdangerSign.setImageResource(0);// changed on 13May16

			int comment_count = dbh.getComentsCount(rowItem.getWomenId(), Partograph_CommonClass.user.getUserId());
			if (comment_count > 0) {
				holder.imgmsg.setVisibility(View.VISIBLE);
				holder.txtcommentcount.setVisibility(View.VISIBLE);
				holder.txtcommentcount.setText("" + comment_count);
			} else {
				holder.imgmsg.setVisibility(View.GONE);
				holder.txtcommentcount.setVisibility(View.GONE);
				holder.txtcommentcount.setText("");
			}

			if (rowItem.getDel_type() == 0) {
				String prevdate = rowItem.getDate_of_admission() + "_" + rowItem.getTime_of_admission();
				todaysDate = Partograph_CommonClass.getTodaysDate();
				currTime = Partograph_CommonClass.getCurrentTime();
				String currdate = todaysDate + "_" + currTime;

				boolean isValid = Partograph_CommonClass.getisValidTime(prevdate, currdate, 1440);

				if (isValid) {
					holder.txtdelstatusnotupdated
							.setText(context.getResources().getString(R.string.delstatusnotupdated));
					holder.txtdelstatusnotupdated.setVisibility(View.VISIBLE);
					holder.txtdelstatusnotupdated.startAnimation(animBlink);
				}

			} else {
				holder.txtdelstatusnotupdated.setVisibility(View.GONE);
				// 10Dec2015
				holder.txtdateofdelivery.setText(rowItem.getDel_Date() == null ? " "
						: context.getResources().getString(R.string.actualdd) + " : " + Partograph_CommonClass
								.getConvertedDateFormat(rowItem.getDel_Date(), Partograph_CommonClass.defdateformat)
								+ " / " + rowItem.getDel_Time());

			}

			// updated on 14july2016 by Arpitha
			risk_observed = rowItem.getComments();

			// changed on 3August2016 by Arpitha
			if (isReferred) {
				if (rowItem.getDel_type() == 0) {
					holder.imgsms.setVisibility(View.VISIBLE);
				} else
					holder.imgsms.setVisibility(View.GONE);
				// changed on 3August2016 by Arpitha
				refdata = dbh.getreferred_data(womenid);

				holder.txtreferredplace.setVisibility(View.VISIBLE);

				String place = refdata.get(mSelectedRow).getPlaceofreferral();
				holder.txtdateofreferal.setText("Ref To: " + place);

				holder.txtdateofreferal.setVisibility(View.VISIBLE);

				// changed on 3August2016 by Arpitha
				holder.txtreferredplace.setText("Ref Dt: " + Partograph_CommonClass.getConvertedDateFormat(
						refdata.get(mSelectedRow).getDateofreferral(), Partograph_CommonClass.defdateformat));
			}

			// changed on 3August2016 by Arpitha
			values = new ArrayList<String>();
			values = Partograph_CommonClass.getphonenumber(option);

			holder.imgsms.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					// TODO Auto-generated method stub

					try {
						if (MotionEvent.ACTION_UP == event.getAction()) {
							if (AppContext.checkSimState() == TelephonyManager.SIM_STATE_READY) {
								displayConfirmationAlert(context.getResources().getString(R.string.send_mess), womenid,
										position);
							} else {
								Toast.makeText(getContext(), context.getResources().getString(R.string.no_sim),
										Toast.LENGTH_LONG).show();

							}
						}
					} catch (NotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					return true;
				}
			});

			if (risk_cat == 1) {
				holder.imghighrisk.setImageResource(R.drawable.highrisk);

				holder.imghighrisk.setOnTouchListener(new OnTouchListener() {

					@Override
					public boolean onTouch(View v, MotionEvent event) {
						// TODO Auto-generated method stub
						try {

							if (MotionEvent.ACTION_UP == event.getAction()) {
								risk_observed = rowItem.getComments();

								if (rowItem.getRiskoptions().length() > 0
										|| (rowItem.getComments() != null && rowItem.getComments().length() > 0)) {
									displayConfirmationAlert_sms(rowItem.getRiskoptions());
								}
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						return true;
					}
				});

			} else
				holder.imghighrisk.setImageResource(0);

			if (rowItem.isDanger()) {
				holder.imgdangerSign.setImageResource(R.drawable.dangersign_icon);

				holder.imgdangerSign.setOnTouchListener(new View.OnTouchListener() {

					@Override
					public boolean onTouch(View v, MotionEvent event) {

						try {

							ArrayList<String> arr;

							arr = dbh.getobjectid_danger(rowItem.getWomenId());

							if (MotionEvent.ACTION_UP == event.getAction()) {

								displayConfirmationAlert_dangersign(arr, "");

							}
						} catch (Exception e) {
							AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
									+ this.getClass().getSimpleName(), e);
							e.printStackTrace();
						}

						return true;
					}
				});
			} else
				holder.imgdangerSign.setImageResource(0);

			String today_date = Partograph_CommonClass.getTodaysDate();
			String current_time = Partograph_CommonClass.getCurrentTime();
			strCurrentTime = today_date + "_" + current_time;

			// changed on3August2106 by Arpitha
			if (!(isReferred && rowItem.getDel_type() == 0))

			{
				ispulsetimevalid = false;
				isfhrtimevalid = false;
				isdilatationtimevalid = false;
				iscontractiontimevalid = false;

				int[] objectid = { 1, 3, 4, 7 };

				for (int i = 1; i <= objectid.length; i++) {
					if (i == 1) {
						lastdate = dbh.lastvaldate(womenid, i);
						lasttime = dbh.lastvaltime(womenid, i, lastdate);
						if (lastdate != null && lasttime != null) {
							islasttime = lastdate + "_" + lasttime;
							isfhrtimevalid = Partograph_CommonClass.getisValidTime(islasttime, strCurrentTime, 30);
						} else
							isfhrtimevalid = false;
					}

					if (i == 3) {
						lastdate = dbh.lastvaldate(womenid, i);
						lasttime = dbh.lastvaltime(womenid, i, lastdate);
						if (lastdate != null && lasttime != null) {
							islasttime = lastdate + "_" + lasttime;
							isdilatationtimevalid = Partograph_CommonClass.getisValidTime(islasttime, strCurrentTime,
									60);
						} else
							isdilatationtimevalid = false;
					}

					if (i == 4) {
						lastdate = dbh.lastvaldate(womenid, i);
						lasttime = dbh.lastvaltime(womenid, i, lastdate);
						if (lastdate != null && lasttime != null) {
							islasttime = lastdate + "_" + lasttime;
							iscontractiontimevalid = Partograph_CommonClass.getisValidTime(islasttime, strCurrentTime,
									30);
						} else
							iscontractiontimevalid = false;
					}

					if (i == 7) {
						lastdate = dbh.lastvaldate(womenid, i);
						lasttime = dbh.lastvaltime(womenid, i, lastdate);
						if (lastdate != null && lasttime != null) {
							islasttime = lastdate + "_" + lasttime;
							ispulsetimevalid = Partograph_CommonClass.getisValidTime(islasttime, strCurrentTime, 30);
						} else
							ispulsetimevalid = false;
					}
				}

				if (isfhrtimevalid || isdilatationtimevalid || iscontractiontimevalid || ispulsetimevalid) {
					holder.imgnotification.setVisibility(View.VISIBLE);
				} else {
					holder.imgnotification.setVisibility(View.INVISIBLE);
				}

			}

			holder.imgnotification.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					// TODO Auto-generated method stub
					if (MotionEvent.ACTION_UP == event.getAction()) {

						if (isfhrtimevalid || iscontractiontimevalid || isdilatationtimevalid || ispulsetimevalid) {
							try {
								displayAlertDialog(context.getResources().getString(R.string.notify_alert_mess));
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}

					}
					return true;
				}
			});
		}

		catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}

		return convertView;
	}

	@Override
	public void onAnimationEnd(Animation animation) {

	}

	@Override
	public void onAnimationRepeat(Animation animation) {

	}

	@Override
	public void onAnimationStart(Animation animation) {

	}

	// Set options for risk options if high risk - 25dec2015
	protected void setRiskOptions() throws Exception {
		int i = 0;
		riskoptionsMap = new LinkedHashMap<String, Integer>();
		List<String> reasonStrArr = null;

		reasonStrArr = Arrays.asList(context.getResources().getStringArray(R.array.riskoptions));

		if (reasonStrArr != null) {
			for (String str : reasonStrArr) {
				riskoptionsMap.put(str, i);
				i++;
			}
		}
	}

	// Display Confirmation to exit the screen
	private boolean displayConfirmationAlert_sms(String exit_msg) throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		try {
			final Dialog dialog = new Dialog(context);
			dialog.setContentView(R.layout.alertdialog_danger);
			dialog.setTitle("High Risk Reasons");
			dialog.setCancelable(true);

			ListView list = (ListView) dialog.findViewById(R.id.listoptions);

			Button imgbtnyes = (Button) dialog.findViewById(R.id.imgbtnyes);

			// 8july2016 by Arpitha
			ArrayList<String> op = new ArrayList<String>();
			String[] riskoptions = exit_msg.split(",");
			String[] riskoptArr = null;

			// riskoptArr = getResources().getStringArray(R.array.riskoptions);
			// 06jul2016 - use riskoptionsvalues instead of riskoptions array
			riskoptArr = context.getResources().getStringArray(R.array.riskoptionsvalues);

			String riskoptionDisplay = "";
			if (riskoptArr != null) {
				if (riskoptions != null && riskoptions.length > 0) {
					for (String str : riskoptions) {
						if (str != null && str.length() > 0)
							op.add(riskoptArr[Integer.parseInt(str)]);
					}
				}
			}
			if (risk_observed != null && risk_observed.length() > 0) {
				op.add("Other Risk Observed: " + risk_observed);
			}

			ArrayAdapter adapter = new ArrayAdapter<String>(context, R.layout.spinner_item, op);
			list.setAdapter(adapter);

			imgbtnyes.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {

					dialog.cancel();
				}
			});

			dialog.show();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return true;
	}

	// Display Confirmation to exit the screen
	private boolean displayConfirmationAlert_dangersign(ArrayList<String> exit_msg, final String classname)
			throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		final Dialog dialog = new Dialog(context);
		dialog.setContentView(R.layout.alertdialog_danger);
		dialog.setTitle("Partograph Parameters in Danger");

		final TextView txtdialog = (TextView) dialog.findViewById(R.id.txtdialog);
		TextView txt1 = (TextView) dialog.findViewById(R.id.txtval);
		TextView txt2 = (TextView) dialog.findViewById(R.id.txtval1);
		ListView list = (ListView) dialog.findViewById(R.id.listoptions);

		Button imgbtnyes = (Button) dialog.findViewById(R.id.imgbtnyes);

		ArrayList<String> op = new ArrayList<String>();
		ArrayList<String> riskoptions = exit_msg;
		String[] riskoptArr = null;

		// riskoptArr = getResources().getStringArray(R.array.riskoptions);
		// 06jul2016 - use riskoptionsvalues instead of riskoptions array
		riskoptArr = context.getResources().getStringArray(R.array.parameter);

		String riskoptionDisplay = "";
		if (riskoptArr != null) {
			if (riskoptions != null && riskoptions.size() > 0) {
				for (String str : riskoptions) {
					if (str != null && str.length() > 0)
						op.add(riskoptArr[Integer.parseInt(str)]);
				}
			}
		}

		ArrayAdapter adapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, op);
		list.setAdapter(adapter);
		dialog.show();

		imgbtnyes.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				dialog.cancel();
			}
		});

		return true;
	}

	public void display_messagedialog(String womenid, int position) throws Exception {
		try {
			final Dialog sms_dialog = new Dialog(context);
			sms_dialog.setTitle("Send SMS");

			sms_dialog.setContentView(R.layout.alertdialog_sms);

			sms_dialog.show();

			final TextView txtdialog = (TextView) sms_dialog.findViewById(R.id.txtdialog);
			TextView txt1 = (TextView) sms_dialog.findViewById(R.id.txtval);
			TextView txt2 = (TextView) sms_dialog.findViewById(R.id.txtval1);
			Button imgbtnyes = (Button) sms_dialog.findViewById(R.id.imgbtnyes);
			final EditText etphno = (EditText) sms_dialog.findViewById(R.id.etphnno);

			etmesss = (EditText) sms_dialog.findViewById(R.id.etmess);
			EditText etopt = (EditText) sms_dialog.findViewById(R.id.etreasonoptions);
			TextView txtreason = (TextView) sms_dialog.findViewById(R.id.txtval6);
			ImageButton imgsend = (ImageButton) sms_dialog.findViewById(R.id.imgsend);
			ImageButton imgcancel = (ImageButton) sms_dialog.findViewById(R.id.imgcancel);

			// rpojo = new ArrayList<WomenReferral_pojo>();

			p_no = "";
			for (int i = 0; i < values.size(); i++) {

				String s = values.get(i);
				p_no = p_no + values.get(i) + ",";
			}

			etphno.setText(p_no);
			rpojo = dbh.getreferred_data(womenid);
			if (refdata.size() > 0) {
				// changed on 3August2106 by Arpitha
				String s = rpojo.get(mSelectedRow).getWomenname();

				txt1.setText(s);

				// changed on 3August2106 by Arpitha
				txt2.setText(rpojo.get(mSelectedRow).getDescriptionofreferral());
				// changed on 3August2106 by Arpitha
				String selectedriskoption = rpojo.get(mSelectedRow).getReasonforreferral();

				ArrayList<String> op = new ArrayList<String>();
				String admResDisplay = "";

				String[] admres = selectedriskoption.split(",");
				String[] admResArr = null;

				admResArr = context.getResources().getStringArray(R.array.reasonforreferralvalues);

				if (admResArr != null) {
					if (admres != null && admres.length > 0) {
						for (String str : admres) {
							if (str != null && str.length() > 0)
								admResDisplay = admResDisplay + admResArr[Integer.parseInt(str)] + "\n";

						}
					}
				}

				if (admResDisplay.length() > 0) {
					etopt.setText(admResDisplay);
				} else {
					etopt.setVisibility(View.GONE);
					txtreason.setVisibility(View.GONE);
				}
			}

			imgsend.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {

					try {

						if (AppContext.checkSimState() == TelephonyManager.SIM_STATE_READY) {

							if (values != null && values.size() > 0) {
								sendmessage(p_no);
							} else {
								if (etphno.length() > 0) {
									sendmessage(etphno.getText().toString());
								} else {
									Toast.makeText(context, "Please Enter Phone Number", Toast.LENGTH_LONG).show();
								}
							}

							sms_dialog.cancel();

						} else
							Toast.makeText(context, "No Sim Card... Insert Sim and try Again", Toast.LENGTH_LONG)
									.show();

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
			});

			imgcancel.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					sms_dialog.cancel();
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void sendmessage(String phn_no) throws Exception {
		try {
			SmsManager smsManager = SmsManager.getDefault();
			ArrayList<String> str = new ArrayList<String>();

			String admResDisplay = "";

			String selectedriskoption = refdata.get(mSelectedRow).getReasonforreferral();

			String[] admres = selectedriskoption.split(",");
			String[] admResArr = null;

			admResArr = context.getResources().getStringArray(R.array.reasonforreferral);

			if (admResArr != null) {
				if (admres != null && admres.length > 0) {
					for (String str1 : admres) {
						if (str1 != null && str1.length() > 0)
							admResDisplay = admResDisplay + admResArr[Integer.parseInt(str1)] + ",";

					}
				}
			}
			str.add(refdata.get(mSelectedRow).getWomenname() + "\n");

			str.add("Desc: " + refdata.get(mSelectedRow).getDescriptionofreferral() + "\n");
			if (admResDisplay.length() > 0) {
				str.add("Reason: " + admResDisplay + "\n");
			}
			if (etmesss.getText().toString().length() > 0) {
				str.add("Comments: " + etmesss.getText().toString());
			}

			String[] d = phn_no.split("\\,");
			for (int i = 0; i < d.length; i++) {
				smsManager.sendMultipartTextMessage(d[i], " ", str, null, null);
			}
			Toast.makeText(context, "Sending SMS...", Toast.LENGTH_LONG).show();
			Toast.makeText(context, "SMS Sent!", Toast.LENGTH_LONG).show();
		} catch (Exception e) {
			Toast.makeText(context, "SMS faild, please try again later!", Toast.LENGTH_LONG).show();
			e.printStackTrace();
		}

	}

	// Display Confirmation to exit the screen
	private boolean displayConfirmationAlert(String exit_msg, final String classname, final int position)
			throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		final Dialog dialog = new Dialog(context);
		dialog.setTitle("SMS Alert");
		dialog.setContentView(R.layout.alertdialog);

		dialog.show();

		final TextView txtdialog = (TextView) dialog.findViewById(R.id.txtdialog);
		TextView txt1 = (TextView) dialog.findViewById(R.id.txtval);
		TextView txt2 = (TextView) dialog.findViewById(R.id.txtval1);
		Button imgbtnyes = (Button) dialog.findViewById(R.id.imgbtnyes);
		Button imgbtnno = (Button) dialog.findViewById(R.id.imgbtnno);

		txtdialog.setText(exit_msg);
		txt1.setVisibility(View.GONE);
		txt2.setVisibility(View.GONE);

		imgbtnyes.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				dialog.cancel();

				try {
					display_messagedialog(classname, position);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});

		imgbtnno.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				try {

					dialog.cancel();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		return true;
	}

	// Alert dialog
	private void displayAlertDialog(String message) throws Exception {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

		// set dialog message
		alertDialogBuilder.setMessage(message)

				.setPositiveButton(context.getResources().getString(R.string.ok),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// if this button is clicked, just close
								// the dialog box and do nothing
								dialog.cancel();
							}
						});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();
		// show it
		alertDialog.show();
	}

}
*/